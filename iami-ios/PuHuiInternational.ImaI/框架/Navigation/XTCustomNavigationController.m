//
//  CustomNavigationController.m
//  Exercises
//
//  Created by wjkang on 11-12-29.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "XTCustomNavigationController.h"
@implementation UINavigationBar (UINavigationBarCategory)

-(void)setBackgroundImage:(UIImage*)image  
{  
    NSArray * views = [self subviews];
    if ([views count]) {
        UIView * groundView = [views objectAtIndex:0];
        if(image == nil)
        {  
            UIView * subView = [groundView viewWithTag:1000];
            if (subView) {
                [subView removeFromSuperview];
            }
            return;
        }
        UIImageView *backgroundView = [[UIImageView alloc] initWithImage:image];  
        backgroundView.tag = 1000;  
        backgroundView.frame = CGRectMake(0.f, 0.f, self.frame.size.width, self.frame.size.height + 50);
        backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [groundView addSubview:backgroundView];
    }
}

@end 

@implementation XTCustomNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavigationBarBg];
}

-(void)setNavigationBarBg
{
//    if ([UserModel shareInfo].userType == 0) {
//        //[self.navigationBar setBackgroundImage:[UIImage imageWithColor:XT_MAINCOLOR_1]];
// [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[NSString colorWithHexString:@"fe8d18"]] forBarMetrics:UIBarMetricsDefault];    }
//    else
//    {
//        [self.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]];
//    }
    
   // self.navigationController.navigationBar setBackIndicatorImage:[UIImage imageNamed:@"navigation_back"]];
//    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:LYColor(231, 194, 99)] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_back"] forBarMetrics:UIBarMetricsDefault];
    
//    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigation_back"]
//
//                       forBarPosition:UIBarPositionAny
//
//                           barMetrics:UIBarMetricsDefault];
//
//    [self.navigationBar setShadowImage:[UIImage new]];
    
    
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([self respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.interactivePopGestureRecognizer.enabled = NO;
    }
    // 隐藏tabBar
    if (self.viewControllers.count == 1) {
        //viewController.hidesBottomBarWhenPushed = YES;
    }
     [super pushViewController:viewController animated:animated];
}
@end
