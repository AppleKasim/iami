//
//  CALToolBarView.m
//
//  Created by Cain on 2/6/2016.
//  Copyright © 2016 Cain. All rights reserved.
//

#import "CALToolBarView.h"

@interface CALToolBarView()

@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) UIView *selectedLine;

@property (nonatomic,assign) CGRect ViewFram;

@property (nonatomic, assign) NSInteger titleCount;


@end

@implementation CALToolBarView

#pragma mark - Set Tool Bar View Background Color
- (void)setBarBakcgroundColor:(UIColor *)barBakcgroundColor {
    self.barBakcgroundColor = barBakcgroundColor;
}

#pragma mark - Set Buttons
- (instancetype)initWithFrame:(CGRect)frame
                   titleArray:(NSArray<NSString *> *)titleArray
                selectedColor:(UIColor *)selectedColor
                deselectColor:(UIColor *)deselectColor
              bottomlineColor:(UIColor *)bottomlineColor
            selectedLineColor:(UIColor *)selectedLineColor
           selectIndex:(NSInteger)select{

    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor whiteColor];

        _ViewFram = frame;
        
        self.titleCount = titleArray.count;
        
        for (NSInteger i = 0; i < titleArray.count; i++) {
            
            CGFloat buttonWidth = self.frame.size.width / titleArray.count;
        
            UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeSystem];

            titleButton.frame = CGRectMake(i * buttonWidth, 0, buttonWidth, self.frame.size.height);
            titleButton.tag   = i;
            titleButton.backgroundColor = [UIColor clearColor];
            
            
            titleButton.titleLabel.font = [UIFont systemFontOfSize:12];
              titleButton.titleLabel.adjustsFontSizeToFitWidth = YES;
            if (self.titleCount == 2) {
                 titleButton.titleLabel.font = [UIFont systemFontOfSize:13];
                titleButton.titleLabel.adjustsFontSizeToFitWidth = NO;
            }
          
            if (SCREEN_WIDTH < 370) {
                 titleButton.titleLabel.font = [UIFont systemFontOfSize:10];
            }
            
            [titleButton setTintColor:[UIColor clearColor]];
            
            [titleButton setTitle:titleArray[i] forState:UIControlStateNormal];
            [titleButton setTitle:titleArray[i] forState:UIControlStateSelected];
           
            [titleButton setTitleColor:deselectColor forState:UIControlStateNormal];
            [titleButton setTitleColor:selectedColor forState:UIControlStateSelected];
            
            titleButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
            titleButton.titleEdgeInsets = UIEdgeInsetsMake(-3, 0, 0, 8);
            
            [titleButton addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
            
           
           

            if (i == select) {
                self.currentButton.selected = NO;
                titleButton.selected = YES;
                self.currentButton = titleButton;
                self.selectedLine.frame = CGRectMake(titleButton.frame.origin.x, self.frame.size.height - 2, self.frame.size.width / self.titleCount, 2);
            }
            
            if (titleArray.count == 4) {
                //因为图片不能识别繁体字符串，所以单独加载
                NSArray *Array = @[@"个人简介", @"所有帖文", @"好友清单",@"媒体内容"];
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                
                button.frame =CGRectMake(i * buttonWidth, 8, 30, 30);
                button.clipsToBounds = YES;
                button.tag = 10 + i;
                if (i == select) {
                    self.nowButton = button;
                    button.selected = YES;
                }
                [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",Array[i]]] forState:UIControlStateNormal];
                [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@1",Array[i]]] forState:UIControlStateSelected];
                [self addSubview:button];
                
                //添加间隔线
                if (i != 0) {
                    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(titleButton.frame.origin.x-1, 12, 1, self.frame.size.height - 24)];
                    view.backgroundColor = LYColor(223, 223, 223);
                    [self addSubview:view];
                }
                
            }
            
            [self addSubview:titleButton];
        }
        
        [self addSubview:self.bottomLine];
        [self addSubview:self.selectedLine];
        
        if (bottomlineColor) {
            
            self.bottomLine.backgroundColor   = bottomlineColor;
        }
        
//        self.SelectIndex = 0;
//        UIButton *selButton = (UIButton *)[self viewWithTag:self.SelectIndex];
//        self.currentButton = selButton;
//        selButton.selected = YES;
        
        
        if (selectedLineColor) {
            
            self.selectedLine.backgroundColor = selectedLineColor;
            
        } else {
            self.selectedLine.backgroundColor = selectedColor;
        }
    }
    
    return self;
}

#pragma mark - Set Buttons Action
- (void)buttonAction:(UIButton *)sender {
    
    self.currentButton.selected = NO;
    
    self.currentButton = sender;
    
    self.currentButton.selected = YES;
    
    UIButton *button = [self viewWithTag:sender.tag + 10];
    self.nowButton.selected = NO;
    self.nowButton = button;
    self.nowButton.selected = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.selectedLine.frame = CGRectMake(sender.frame.origin.x, self.frame.size.height - 2, _ViewFram.size.width / self.titleCount, 2);
        
    }];
    
    if (self.calToolBarSelectedBlock) {
        
        self.calToolBarSelectedBlock(sender.tag);
    }
}

#pragma mark - Set Line View
- (UIView *)bottomLine {
    
    if (!_bottomLine) {
        
        CGFloat height = 0.5;
        
        _bottomLine = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height - height, self.frame.size.width, height)];
    }
    
    return _bottomLine;
}

- (void)setIsNeedLine:(BOOL)isNeedLine {
    self.bottomLine.hidden = !isNeedLine;
}

#pragma mark - Set Selected Line View
- (UIView *)selectedLine {
    
    if (!_selectedLine) {
        _selectedLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 2, self.frame.size.width / self.titleCount, 2)];
       
    }
    
    return _selectedLine;
}

- (void)setIsNeedSelectedLine:(BOOL)isNeedSelectedLine {
    self.selectedLine.hidden = !isNeedSelectedLine;
}

@end
