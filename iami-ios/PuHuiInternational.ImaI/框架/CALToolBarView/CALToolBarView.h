//
//  CALToolBarView.h
//
//  Created by Cain on 2/6/2016.
//  Copyright © 2016 Cain. All rights reserved.
//

/*
 * 由于这个库并不是使用UICollectionView写的, 所以它本身是不带复用机制
 * 如果要使用该库, 首先得已知是否就只有5个或者5个以下的按钮
 * 如果不确定是多少个Button, 或者需要多个的, 请使用HTHorizontalSelectionList
 */
#import <UIKit/UIKit.h>

@interface CALToolBarView : UIView

/**
 *  点击事件
 */
@property (nonatomic, copy) void(^calToolBarSelectedBlock)(NSInteger index);

/**
 *  背景颜色, 默认是白色
 */
@property (nonatomic, strong) UIColor *barBakcgroundColor;

/**
 *  设置Button的标题, 必须是字符串数组
 *
 *  @param titleArray    标题数组
 *  @param selectedColor 选中的颜色
 *  @param deselectColor 非选中的颜色
 *  @param bottomlineColor   底部线的颜色
 *  @param selectedLineColor 选中条的颜色
 */
- (instancetype)initWithFrame:(CGRect)frame
                   titleArray:(NSArray<NSString *> *)titleArray
                selectedColor:(UIColor *)selectedColor
                deselectColor:(UIColor *)deselectColor
              bottomlineColor:(UIColor *)bottomlineColor
            selectedLineColor:(UIColor *)selectedLineColor
                  selectIndex:(NSInteger)select;

@property (nonatomic, assign) BOOL isNeedLine;
@property (nonatomic, assign) BOOL isNeedSelectedLine;
@property (nonatomic, strong) UIButton *currentButton;
@property (nonatomic,strong) UIButton *nowButton;
@property (nonatomic,assign) NSInteger SelectIndex;
@end
