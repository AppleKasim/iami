//
//  IAMIUserModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/7/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "IAMIUserModel.h"

@implementation IAMIUserModel
+(IAMIUserModel *)parsenDictionary:(NSDictionary *)dic{
    IAMIUserModel *model = [[IAMIUserModel alloc]init];
    model.avatar = [NetDataCommon stringFromDic:dic forKey:@"avatar"];
    model.banner = [NetDataCommon stringFromDic:dic forKey:@"banner"];
    model.level = [NetDataCommon stringFromDic:dic forKey:@"level"];
    model.member_id = [NetDataCommon stringFromDic:dic forKey:@"member_id"];
    model.nickname = [NetDataCommon stringFromDic:dic forKey:@"nickname"];
    return model;
}
@end
