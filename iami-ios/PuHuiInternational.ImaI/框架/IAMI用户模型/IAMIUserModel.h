//
//  IAMIUserModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/7/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IAMIUserModel : NSObject
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *banner;
@property (nonatomic,copy) NSString *level;
@property (nonatomic,copy) NSString *member_id;
@property (nonatomic,copy) NSString *nickname;

+(IAMIUserModel *)parsenDictionary:(NSDictionary *)dic;
@end
