//
//  XLTabBarController.m
//  MiLi
//
//  Created by xunlongorangepi on 16/7/25.
//  Copyright © 2016年 NJNightDayTechnology. All rights reserved.
//

#import "XLTabBarController.h"
#import "XTCustomNavigationController.h"

@interface XLTabBarController ()<UITabBarDelegate>

@end

@implementation XLTabBarController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.tabBar.backgroundColor = [UIColor whiteColor];
       // self.delegate = self;
    }
    return self;
//控制器没有这个方法
//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        DDLogError(@"%s %d",__func__,__LINE__);
//
//    }
//    return self;
//}
//控制器器的init方法默认会掉到initWithNibName，我们在initWithNibName
//方法中实现逻辑
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        UIViewController *main = [self addSubControllerWithImage:@"首页" selectedImageName:@"首页" titleName:@"首页" withClass:[MainViewController class]];
        
        
        UIViewController *calssfy = [self addSubControllerWithImage:@"邀请" selectedImageName:@"邀请" titleName:@"邀请" withClass:[InviteViewController class]];

        UIViewController *cart = [self addSubControllerWithImage:@"通知" selectedImageName:@"通知" titleName:@"通知" withClass:[InformViewController class]];
        UIViewController *myMili = [self addSubControllerWithImage:@"讯息" selectedImageName:@"讯息" titleName:@"讯息" withClass:[MessageViewController class]];
        UIViewController *Seet = [self addSubControllerWithImage:@"设定" selectedImageName:@"设定" titleName:@"设定" withClass:[SettingViewController class]];
        
        self.viewControllers = @[main, calssfy, cart, myMili,Seet];
    }
    return self;
}

-(UIViewController *)addSubControllerWithImage:(NSString *)imageName selectedImageName:(NSString *)selectedImageName titleName:(NSString *)name withClass:(Class)className
{
    UIViewController *vc = [[className alloc] init];
    XTCustomNavigationController *nav = [[XTCustomNavigationController alloc]initWithRootViewController:vc];
    
    vc.tabBarItem.image=[UIImage imageNamed:imageName];
    vc.tabBarItem.selectedImage=[self renderImage:selectedImageName];
   // [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateSelected];
//    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : LYColor(244, 169, 69)} forState:UIControlStateSelected];
//    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : LYColor(244, 169, 69)} forState:UIControlStateNormal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MainColor} forState:UIControlStateSelected];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName : MainColor} forState:UIControlStateNormal];
    vc.tabBarItem.title=NSLocalizedString(name, nil);
    
    //
    return nav;
    
}
//-(UIViewController *)addSubControllerWithImage:(NSString *)imageName selectedImageName:(NSString *)selectedImageName withClass:(Class)className
//{
//    UIViewController *vc = [[className alloc] init];
//    XTCustomNavigationController *nav = [[XTCustomNavigationController alloc]initWithRootViewController:vc];
//    vc.tabBarItem.image=[self renderImage:selectedImageName];
//    vc.tabBarItem.selectedImage=[self renderImage:imageName];
//    vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
//   // vc.tabBarItem.title=name;
//    return nav;
//
//}

-(UIImage *)renderImage:(NSString *)imageName
{
    UIImage *newImage =  [UIImage imageNamed:imageName];
    // 2.告诉系统原样显示
    newImage = [newImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if ([item.title isEqualToString:NSLocalizedString(@"首页", nil)] && self.selectedIndex == 0) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"tabbarReloadMain" object:nil];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
