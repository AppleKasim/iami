//
//  SearchModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/16.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchModel : NSObject
@property (nonatomic,strong) NSString *avatar;
@property (nonatomic,strong) NSString *member_id;
@property (nonatomic,strong) NSString *nickname;

+(SearchModel *)parsenWith:(NSDictionary *)dic;
@end
