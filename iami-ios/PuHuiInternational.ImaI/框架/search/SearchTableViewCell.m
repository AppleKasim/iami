//
//  SearchTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/16.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "SearchTableViewCell.h"

@implementation SearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(SearchModel *)mode{
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(15,  5, 40, 40)];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",mode.avatar]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 20;
    _UserImage.clipsToBounds = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    _UserImage.userInteractionEnabled = YES;
    [self.contentView addSubview:_UserImage];
    
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(70,  10, 200, 20)];
    _UserName.text = mode.nickname;
    _UserName.textColor = MainColor;
    _UserName.font = FONT(15);
    [self.contentView addSubview:_UserName];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
