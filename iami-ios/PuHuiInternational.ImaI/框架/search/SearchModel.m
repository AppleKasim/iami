//
//  SearchModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/16.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchModel
+(SearchModel *)parsenWith:(NSDictionary *)dic{
    SearchModel *model = [[SearchModel alloc]init];
    model.avatar = [NetDataCommon stringFromDic:dic forKey:@"avatar"];
    model.member_id = [NetDataCommon stringFromDic:dic forKey:@"member_id"];
    model.nickname =  [NetDataCommon stringFromDic:dic forKey:@"nickname"];
    return model;
}
@end
