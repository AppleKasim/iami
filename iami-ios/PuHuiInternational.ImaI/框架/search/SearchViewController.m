//
//  SearchViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/16.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchTableViewCell.h"
#import "MainUserViewController.h"//个人主页

@interface SearchViewController ()<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate>
{
    UITableView *_tableView;
   
    UISearchBar *_searchBar;
    UISearchDisplayController *searchDisplayController;
    NSMutableArray *searchResultsArr;
    NSInteger pageNum;
    NSString *historyString;
    
    UIButton *lastItem;
    
    UIView *menuView;
    UIButton *btn1;
    UIButton *btn2;
    UIButton *btn3;
    
    NSArray *searchSortBtn;
    NSArray *orderbykeyArray;
    NSArray *orderbyvaleArray;
    NSInteger sortInt;
}

@property (nonatomic,copy) NSMutableArray *dataArr;

#define searchurl @"/page/memberQuery"
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArr = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, SCREEN_HEIGHT-20)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.sectionHeaderHeight =40;
   
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 40)];
    view.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = view;
    [self.view addSubview:_tableView];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 20)];
    [self.view addSubview:_searchBar];
    _searchBar.delegate = self;
    [_searchBar setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [_searchBar sizeToFit];
    [_searchBar setPlaceholder:@"请输入好友名称"];
    
    searchDisplayController = [[UISearchDisplayController alloc]initWithSearchBar:_searchBar contentsController:self];
    searchDisplayController.active = NO;
    searchDisplayController.delegate = self;
    searchDisplayController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    searchDisplayController.searchResultsDataSource = self;
    searchDisplayController.searchResultsDelegate = self;
    [searchDisplayController.searchResultsTableView registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"cell"];
    searchDisplayController.searchResultsTableView.backgroundColor = [UIColor clearColor];
    searchDisplayController.searchResultsTableView.separatorStyle = NO;
    pageNum = 1;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(regist)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
}
-(void)regist{
    [_searchBar resignFirstResponder];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == searchDisplayController.searchResultsTableView) {
         return _dataArr.count;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    SearchModel *model = _dataArr[indexPath.row];

    static NSString *CellIdentifier = @"DemoTableViewCell";
    SearchTableViewCell  *cell = (SearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    if (cell == nil)
    {
        cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (tableView == searchDisplayController.searchResultsTableView) {
         [cell setModel:model];
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == searchDisplayController.searchResultsTableView) {
        return 50;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1f;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1f)];
    return view;
}
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString

{
    //一旦SearchBar輸入內容有變化，則執行這個方法，詢問要不要重裝searchResultTableView的數據
    // Return YES to cause the search result table view to be reloaded.
    
    [self loadData:searchString];
    return YES;
}
-(void)loadData:(NSString *)string{
    [_dataArr removeAllObjects];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,searchurl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"keyword":string};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parm in dataArr) {
                [_dataArr addObject:[SearchModel parsenWith:parm]];
            }
        }
        [searchDisplayController.searchResultsTableView reloadData];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

     [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == searchDisplayController.searchResultsTableView) {
        
       // [self serachResultWithKeyWord:_searchBar.text];
        SearchModel *model =_dataArr[indexPath.row];
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.member_id;
        controller.Username = model.nickname;
        controller.ImageUrl = model.avatar;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
   
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [_searchBar becomeFirstResponder];
    
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
