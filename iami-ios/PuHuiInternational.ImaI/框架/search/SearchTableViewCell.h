//
//  SearchTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/16.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchModel.h"

@interface SearchTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
-(void)setModel:(SearchModel *)mode;
@end

