//
//  UIImage+Color.h
//  MiLi
//
//  Created by bianjianfeng on 15/3/28.
//  Copyright (c) 2015年 NJNightDayTechnology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage(Color)

+ (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;
+ (UIImage*)imageFromImage:(UIImage*)image inRect:(CGRect)rect transform:(CGAffineTransform)transform;

@end
