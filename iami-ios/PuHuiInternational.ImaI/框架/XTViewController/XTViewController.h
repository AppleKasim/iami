//
//  XTViewController.h
//  XiaoliuFruits
//
//  Created by wjkang on 12-3-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BackRefreshBlock)(id param);


@interface UIView(XT)

@end

@interface NSString(Custom)

+ (NSString *)stringFromGLZ:(NSString *)timeString;
+ (NSInteger)compareTimeWithNowTime:(NSString *)time;
+ (NSString *)compareNowDataAndEnddate:(NSString *)dateStr;
@end

@interface UIButton(XT)

- (id)initWithFrame:(CGRect)frame
              title:(NSString *)title
     nor_titleColor:(UIColor *)norColor
     sel_titleColor:(UIColor *)selColor
nor_backgroundImage:(UIImage *)norImage
sel_backgroundImage:(UIImage *)selImage
         haveBorder:(BOOL)have;

@end

@interface XTViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>
{
    BOOL m_showBackBt;
    BOOL m_showRefurbishing;
    BOOL m_showRefurbishBt;
    UILabel *titleLabel;
    UIImageView *topView;
}
@property (nonatomic,strong)UITextField *currentTF;
@property (nonatomic,strong) UITextView *currentSZTF;

@property (nonatomic,strong)NSString *mRightBtnTitle;
@property (nonatomic,strong)NSString *mLeftBtnTitle;

@property (nonatomic,assign) BOOL m_showRightBt;
@property (nonatomic,assign) BOOL m_showBackBt;
@property (nonatomic,assign) BOOL m_showRefurbishing;
@property (nonatomic,assign) BOOL m_showRefurbishBt;
//@property (nonatomic,assign)BOOL mIsMainVC;
/**
 *  add by song
 *
 *  @param text 返回上一个vc需要刷新时候用
 */
@property (nonatomic,strong)BackRefreshBlock BackAction;



- (void)updateTitleLabel:(NSString *)text;

- (void)setupSkin;

- (void)createRightBtn;
- (void)createBtnBack;
- (void)createBtnRefubish;
- (void)createBtnRefubishing;

//- (void)createMainLeftBtn;
//- (void)showLeftView;

- (BOOL)keyboard;
- (void)keyboardWasShown:(NSNotification *) notif;
- (void)keyboardWasHidden:(NSNotification *) notif;
- (void)keyboardHide:(UITapGestureRecognizer *)tap;

- (void)backBtPressed;
- (void)rightBtnPressed;
- (void)refurbishBtPressed;

- (BOOL)validatePsw:(NSString *)password;

- (BOOL)isLoginFromController;

- (void)getUserInfo;

-(void)rightlogo;

-(void)requestIsHaveReview;
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font;

-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font;
//-(void)getCsrf_token;
-(UITextField *)addsearchBar:(NSString *)placeHolder;
//获取当前语系
- (NSString*)getPreferredLanguage;

@end
