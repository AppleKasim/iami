//
//  XTViewController.m
//  XiaoliuFruits
//
//  Created by wjkang on 12-3-20.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "XTViewController.h"
//#import "LoginAndRegisterViewController.h"
#import "MainViewController.h"
#import "ShortMovieViewController.h"


@implementation UIView(XT)

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [self.superview endEditing:YES];
    for (id obj in self.subviews) {
        if ([obj isKindOfClass:[UITextField class]]) {
            UITextField * obj1 = (UITextField *)obj;
            [obj1 resignFirstResponder];
        }
        if ([obj isKindOfClass:[UITextView class]]) {
            UITextView * obj1 = (UITextView *)obj;
            [obj1 resignFirstResponder];
        }
    }
}

@end


@implementation NSString(Custom)

+ (NSString *)stringFromGLZ:(NSString *)timeString
{
    if (!timeString && [timeString isEqualToString:@""]) {
        return @"";
    }
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[timeString doubleValue]];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *time =[dateFormatter stringFromDate:date];
    return time;
}
/**
 *    1即将过期 2 已到期
 */
+ (NSInteger)compareTimeWithNowTime:(NSString *)time
{
    NSLog(@"%@",time);
    if (!time && [time isEqualToString:@""]) {
        return 0;
    }
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date1=[dateFormatter dateFromString:time];
    NSDate *nowDate = [NSDate date];

   // int d = [dd day];
    NSTimeInterval timeIn=[date1 timeIntervalSinceDate:nowDate];
    //int hours=((int)timeIn)/3600;
    if (timeIn < 0) {
        return 2;
    }
    int hours=((int)timeIn)/(3600*24) * 24+ ((int)timeIn)%(3600*24)/3600;
    if(24>hours && hours>0)
    {
        return 1;
    }
    return 0;

}

+(NSString *)compareNowDataAndEnddate:(NSString *)dateStr
{
    if (!dateStr || [dateStr isEqualToString:@""]) {
        return @"";
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //比较准确度为“日”，如果提高比较准确度，可以在此修改时间格式
    NSString *stringDate1 = [dateFormatter stringFromDate:[NSDate date]];
    NSDate *dateA = [dateFormatter dateFromString:stringDate1];
    NSDate *dateB = [dateFormatter dateFromString:dateStr];
    NSComparisonResult result = [dateA compare:dateB];
    if (result == NSOrderedDescending) {
        return @"1"; //date1 比 date2 晚
    }
    else if (result == NSOrderedAscending){
        return @""; //date1 比 date2 早
    }
    return @"1"; //在当前准确度下，两个时间一致
}
@end

@implementation UIButton(XT)

- (id)initWithFrame:(CGRect)frame
              title:(NSString *)title
     nor_titleColor:(UIColor *)norColor
     sel_titleColor:(UIColor *)selColor
nor_backgroundImage:(UIImage *)norImage
sel_backgroundImage:(UIImage *)selImage
         haveBorder:(BOOL)have
{
    self = [UIButton buttonWithType:UIButtonTypeCustom];
    self.frame = frame;
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitleColor:norColor forState:UIControlStateNormal];
    [self setTitleColor:selColor forState:UIControlStateSelected];
    [self setBackgroundImage:norImage forState:UIControlStateNormal];
    [self setBackgroundImage:selImage forState:UIControlStateSelected];
    if (have) {
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 5.f;
//        self.layer.borderWidth = 1;
//        self.layer.borderColor = norColor.CGColor;
    }
    
    return self;
}

@end

@implementation XTViewController
@synthesize m_showBackBt,m_showRefurbishBt,m_showRefurbishing;
@synthesize m_showRightBt;
@synthesize mRightBtnTitle;
@synthesize mLeftBtnTitle;
//@synthesize mIsMainVC;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (IOS_VERSION >= 7) {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
        self.hidesBottomBarWhenPushed = NO;
    }

    return self;
    
}

#pragma mark--iOS7&iOS6适配--

- (UIStatusBarStyle)preferredStatusBarStyle

{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,150,30)];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setFont:Bold_FONT(19)];
    [titleLabel setText:self.title];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    titleLabel.tag = 1000;
    
    [self.navigationItem setTitleView:titleLabel];
    self.view.backgroundColor = [UIColor whiteColor];
    

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide:)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.view addGestureRecognizer:tapGestureRecognizer];
    self.navigationController.navigationBar.hidden = YES;
    
//    UIControl *control = [[UIControl alloc] initWithFrame:self.view.frame];
//    [control addTarget:self action:@selector(keyboardHide:) forControlEvents:UIControlEventTouchUpInside];
//    self.view = control;
    
    UIImageView *MainImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, Height_StatusBar, SCREEN_WIDTH, SCREEN_HEIGHT)];
    MainImage.image = [UIImage imageNamed:@"iami_splashbg.jpg"];
    MainImage.userInteractionEnabled = YES;
    
    
    
    topView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 30, 10, 60, 20)];
    topView.image = [UIImage imageNamed:@"logo_shadow"];
    topView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapMain = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightlogo)];
    [topView addGestureRecognizer:tapMain];
    [self.navigationController.navigationBar addSubview:topView];
    
    [self startLoop];
    
    
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;

    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];

    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
   
}


-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    
    return YES;
}
-(void)creatRight{
    UIImage *leftImage = [[UIImage imageNamed:@"右边logo.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(rightlogo)];
}
-(void)rightlogo{
    [UIView animateWithDuration:0.25f animations:^{
        self.tabBarController.selectedIndex = 0;
    }];
    
}
-(void)setting{
        ShortMovieViewController *controller = [[ShortMovieViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
}
-(void)keyboardHide:(UITapGestureRecognizer*)tap{
//    if ([tap.view isKindOfClass:[UITextField class]]) {
//        return;
//    }
    [self.view endEditing:YES];
    
//    [_currentTF resignFirstResponder];
//    [_currentTF endEditing:YES];
//    _currentTF = nil;
//    
//    [_currentSZTF resignFirstResponder];
//    [_currentSZTF endEditing:YES];
//    _currentSZTF = nil;
}
- (void)updateTitleLabel:(NSString *)text
{
    titleLabel.text = text;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //注册键盘显示与消失的通知观察者
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardWillHideNotification object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(transformView:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    self.tabBarController.tabBar.hidden = NO;
    self.navigationController.navigationBar.shadowImage= [UIImage new];

     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newMessageTask:) name:@"newMessage" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    //移除键盘显示与消失的通知观察者
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"newMessage" object:nil];
}

-(void)newMessageTask:(NSNotification *)noti{

    NSDictionary *dic = noti.object;
    if ([[dic allKeys]containsObject:@"task"]) {
        NSArray *dataArr = dic[@"task"];

        NSString *messageString = @"";

        for (NSDictionary *messageDictionary in dataArr) {
            NSString *nameString = messageDictionary[@"name"];
            NSString *rewardString = messageDictionary[@"reward"];

            messageString = [NSString stringWithFormat:@"%@%@ %@ \n", messageString, nameString, rewardString];

            //messageString +=   nameString + " " + rewardString + "\n";
        }

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"完成任務"
                                     message:messageString
                                     preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"確定"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {

                                    }];


        [alert addAction:yesButton];


        [[self currentViewController] presentViewController:alert animated:YES completion:nil];
    }
}

- (UIViewController*)currentViewController{
    //获得当前活动窗口的根视图
    UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (1)
    {
        //根据不同的页面切换方式，逐步取得最上层的viewController
        if ([vc isKindOfClass:[UITabBarController class]]) {
            vc = ((UITabBarController*)vc).selectedViewController;
        }
        if ([vc isKindOfClass:[UINavigationController class]]) {
            vc = ((UINavigationController*)vc).visibleViewController;
        }
        if (vc.presentedViewController) {
            vc = vc.presentedViewController;
        }else{
            break;
        }
    }
    return vc;
}

-(void)setupSkin
{
    
}

/**
 *  初始化返回按钮
 */
-(void)createBtnBack
{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35,18)];
    if (mLeftBtnTitle && ![mLeftBtnTitle isEqualToString:@""] && mLeftBtnTitle != nil) {
        leftButton.frame = CGRectMake(0, 0, 20, 36);
        [leftButton setTitle:mLeftBtnTitle forState:UIControlStateNormal];
        leftButton.titleLabel.font = Bold_FONT(17);
        leftButton.titleLabel.frame = leftButton.frame;
    }
    else
    {
       // [leftButton setBackgroundImage:[UIImage imageNamed:@"灰色返回"] forState:UIControlStateNormal];
        //[leftButton setBackgroundImage:[UIImage imageNamed:@"白色返回"] forState:UIControlStateSelected];
        [leftButton setImage:[UIImage imageNamed:@"白色返回"] forState:UIControlStateNormal];
        [leftButton setImage:[UIImage imageNamed:@"灰色返回"] forState:UIControlStateSelected];
    }
    
    [leftButton addTarget:self action:@selector(backBtPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
}
/**
 *  初始化右侧按钮
 */
- (void)createRightBtn
{
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 31)];
    [rightButton setTitle:mRightBtnTitle forState:UIControlStateNormal];
    rightButton.titleLabel.font = Bold_FONT(17);
    rightButton.titleLabel.frame = rightButton.frame;
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(rightBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)createBtnRefubish
{

}
-(void)createBtnRefubishing
{

}

//- (void)createMainLeftBtn
//{
//    UIButton *menu = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 15, 15)];
//    [menu setImage:[UIImage imageNamed:@"menu_homePage.png"] forState:UIControlStateNormal];
//    if ([UserModel shareInfo].userType == 0) {
//        [menu setImage:[UIImage imageNamed:@"menu_homePage_1"] forState:UIControlStateNormal];
//    }
//    [menu addTarget:self action:@selector(showLeftView) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:menu];
//    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
//    
//}
//
//- (void)showLeftView
//{
//    [[UserModel shareInfo].rootController showLeftView];
//}
//
//- (void)setMIsMainVC:(BOOL)isMainVC
//{
//    mIsMainVC = isMainVC;
//    if (mIsMainVC) {
//        [self createMainLeftBtn];
//    }
//    else
//    {
//        [self.navigationItem setRightBarButtonItem:nil];
//    }
//}

- (void)setM_showRightBt:(BOOL)showRightBt
{
    m_showRightBt = showRightBt;
    
    if( m_showRightBt )
    {
        [self createRightBtn];
    }
    else
    {
        [self.navigationItem setRightBarButtonItem:nil];
    }
}


-(void)setM_showBackBt:(BOOL)isShow
{
    m_showBackBt = isShow;
    
    if( m_showBackBt )
    {
        [self createBtnBack];
    }
    else
    {
        [self.navigationItem setHidesBackButton:YES];
    }

}

-(void)setM_showRefurbishBt:(BOOL)isShowRefurbishBt
{
    
}

-(void)setM_showRefurbishing:(BOOL)isShowRefurbishing
{
    
}

/**
 *  返回事件
 */
-(void)backBtPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  右侧按钮触发事件
 */
- (void)rightBtnPressed
{
    
}

-(void)refurbishBtPressed
{
    
}


//网络
-(void) netFinished:(id)aResult reqType:(NSInteger)aReqType
{
    
}
-(void) netFailed:(NSInteger)aReqType
{
    
}
/**
 *  键盘的frame变化的通知
 *
 */
-(void)transformView:(NSNotification *)noti{
//    NSValue *keyBoardBeginBounds=[[noti userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
//    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
//
//    //获取键盘弹出后的Rect
//    NSValue *keyBoardEndBounds=[[noti userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect  endRect=[keyBoardEndBounds CGRectValue];
//
//    //获取键盘位置变化前后纵坐标Y的变化值
//    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
//    NSLog(@"看看这个变化的Y值:%f",deltaY);
//
//    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
//    CGRect rect1 = [_currentTF convertRect: _currentTF.bounds toView:window];
//    //|| rect1.origin.y < SCREEN_HEIGHT - 258
//    if (rect1.origin.y < SCREEN_HEIGHT - deltaY  ) {
//        return;
//    }
//
//    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画
//    [UIView animateWithDuration:0.25f animations:^{
//        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+deltaY, self.view.frame.size.width, self.view.frame.size.height)];
//    }];

//    NSValue *keyBoardBeginBounds=[[noti userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
//    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
//
//    //获取键盘弹出后的Rect
//    NSValue *keyBoardEndBounds=[[noti userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
//    CGRect  endRect=[keyBoardEndBounds CGRectValue];
//
//
//    //获取键盘位置变化前后纵坐标Y的变化值
//    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
//    NSLog(@"看看这个变化的Y值:%f",deltaY);
//
//    //获取textfield当前的位置，判断是否需要偏移view
//    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
//    CGRect rect=[_currentSZTF convertRect: _currentSZTF.bounds toView:window];
//    CGRect rect1 = [_currentTF convertRect: _currentTF.bounds toView:window];
//    if (rect.origin.y < SCREEN_HEIGHT - 258 || rect1.origin.y < SCREEN_HEIGHT - 258) {
//        return;
//    }
//
//    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画
//    [UIView animateWithDuration:0.25f animations:^{
//        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+deltaY, self.view.frame.size.width, self.view.frame.size.height)];
//    }];
}
//键盘弹出、落下
- (void)keyboardWasShown:(NSNotification *) notif
{
    NSValue *keyBoardBeginBounds=[[notif userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
    
    //获取键盘弹出后的Rect
    NSValue *keyBoardEndBounds=[[notif userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect  endRect=[keyBoardEndBounds CGRectValue];
    
   
    //获取键盘位置变化前后纵坐标Y的变化值
    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
    //NSLog(@"xt看看这个变化的Y值:%f",deltaY);
    
    //获取textfield当前的位置，判断是否需要偏移view
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    
    //CGRect rect=[_currentSZTF convertRect: _currentSZTF.bounds toView:window];
    CGRect rect1 = [_currentTF convertRect: _currentTF.bounds toView:window];



    //|| rect1.origin.y < SCREEN_HEIGHT - 258
    if (rect1.origin.y < SCREEN_HEIGHT + deltaY -Height_TabBar  ) {
        //NSLog(@"scree+delay:%f",SCREEN_HEIGHT + deltaY);
        return;
    }
    
    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画
    [UIView animateWithDuration:0.25f animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+deltaY+ 42, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}

- (void) keyboardWasHidden:(NSNotification *) notif
{
    
//    CGRect frame = self.view.frame;
//    frame.origin.y = 0;
//    self.view.frame = frame;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.view.frame;
        
        frame.origin.y = Height_NavBar;
        
        
        self.view.frame = frame;
        
    }];
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.3 animations:^{

        CGRect frame = self.view.bounds;

        
        frame.origin.y = Height_NavBar;


        self.view.frame = frame;
        
    }];
    return YES;
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _currentSZTF = textView;
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    _currentTF = textField;
    return YES;
}
- (void)dismissKeyboard {
    [_currentTF resignFirstResponder];
    [_currentSZTF resignFirstResponder];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
        //在这里做你响应return键的代码
        [_currentSZTF resignFirstResponder];
        [_currentSZTF endEditing:YES];
        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}
//- (BOOL)isLoginFromController
//{
////    NSUserDefaults *accountDefaults = [NSUserDefaults standardUserDefaults];
////    BOOL login = [accountDefaults boolForKey:@"isLogin"];
////    if (!login || [[UserModel shareInfo].mUserInfo.mId isEqualToString:@""] || ![UserModel shareInfo].mUserInfo.mId) {
////        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
////        //[app initLoginAndRegister];
////        return NO;
////    }
////    return login;
//    if (![HLUserModel sharedHLUserModel].isLogin) {
//
//        LoginViewController *log = [[LoginViewController alloc]init];
//        [self presentViewController:log animated:YES completion:^{
//
//        }];
//        return NO;
//    }
//    return [HLUserModel sharedHLUserModel].isLogin;
//}

-(UITextField *)addsearchBar:(NSString *)placeHolder
{
    UIView *seaView = [[UIView alloc]initWithFrame:CGRectMake(10, 70, SCREEN_WIDTH-20, 45)];
    seaView.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:214.0/255.0 blue:204/255.0 alpha:1.0];
    seaView.layer.cornerRadius = 45/2.f;
    seaView.layer.masksToBounds = YES;
    [self.view addSubview:seaView];
    UIImageView *seaImage = [[UIImageView alloc]initWithFrame:CGRectMake(80, 9, 25, 25)];
    seaImage.image = [UIImage imageNamed:@"search-.png"];
    [seaView addSubview:seaImage];
    UITextField *textF = [[UITextField alloc]initWithFrame:CGRectMake(122, 77, seaView.frame.size.width, 30)];
    textF.font = [UIFont boldSystemFontOfSize:19.0f];
    textF.placeholder = placeHolder;
    textF.delegate = self;
    textF.returnKeyType = UIReturnKeySearch;
    
    textF.font = [UIFont systemFontOfSize:17.0f];
    
    [textF setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.textColor"];
    

    
    return textF;
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.height;
}
-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}

- (void)startLoop

{
    [NSThread detachNewThreadSelector:@selector(loopMethod) toTarget:self withObject:nil];
}

- (void)loopMethod

{
    
    [NSTimer scheduledTimerWithTimeInterval:600.0f target:self selector:@selector(requestIsHaveReview) userInfo:nil repeats:YES];
    
    NSRunLoop *loop = [NSRunLoop currentRunLoop];
    
    [loop run];
    
}
-(void)requestIsHaveReview{
    if ([PHUserModel sharedPHUserModel].isLogin == NO) {
        return;
    }
    
    NSDictionary *paramDic = @{@"email":[PHUserModel sharedPHUserModel].email,@"password":[PHUserModel sharedPHUserModel].Password,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
     NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/doLogin"];
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        if ([dic[@"message"] isEqualToString:@"success"]) {
            [PHUserModel sharedPHUserModel].isLogin = YES;
            [PHUserModel sharedPHUserModel].AccessToken =dic[@"data"][@"token"];

            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
        }else{
            [CYToast showErrorWithString:@"账号或密码错误"];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        
    }];
}
#pragma mark - 获取当前的语系
- (NSString*)getPreferredLanguage

{
    
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
    NSLog(@"当前语言:%@", preferredLang);
    
    return preferredLang;
    
}
@end
