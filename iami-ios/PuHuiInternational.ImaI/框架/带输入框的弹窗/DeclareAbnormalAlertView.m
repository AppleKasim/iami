//
//  DeclareAbnormalAlertView.m
//  iDeliver
//
//  Created by 蔡强 on 2017/4/3.
//  Copyright © 2017年 kuaijiankang. All rights reserved.
//

//========== 申报异常弹窗 ==========//

#import "DeclareAbnormalAlertView.h"
#import "UIView+frameAdjust.h"
#import "TagFriedTableViewCell.h"


@interface DeclareAbnormalAlertView ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    
    UITableView *MainTableView;
    
}
/** 弹窗主内容view */
@property (nonatomic,strong) UIView   *contentView;
/** 弹窗标题 */
@property (nonatomic,copy)   NSString *title;
/** 弹窗message */
@property (nonatomic,copy)   NSString *message;
/** message label */
@property (nonatomic,strong) UILabel  *messageLabel;
/** 左边按钮title */
@property (nonatomic,copy)   NSString *leftButtonTitle;
/** 右边按钮title */
@property (nonatomic,copy)   NSString *rightButtonTitle;

@property (nonatomic,copy)  NSMutableArray *DataArray;

#define url @"/post/getShares"
@end

//#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
//#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

@implementation DeclareAbnormalAlertView{
    UILabel *label;
}

#pragma mark - 构造方法
/**
 申报异常弹窗的构造方法
 
 @param title 弹窗标题
 @param message 弹窗message
 @param delegate 确定代理方
 @param leftButtonTitle 左边按钮的title
 @param rightButtonTitle 右边按钮的title
 @return 一个申报异常的弹窗
 */
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate leftButtonTitle:(NSString *)leftButtonTitle rightButtonTitle:(NSString *)rightButtonTitle{
    if (self = [super init]) {
        self.title = title;
        self.message = message;
        self.delegate = delegate;
        self.leftButtonTitle = leftButtonTitle;
        self.rightButtonTitle = rightButtonTitle;
        
        
        _DataArray = [[NSMutableArray alloc]init];
        // 接收键盘显示隐藏的通知
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
        
        // UI搭建
        [self setUpUI];
    }
    return self;
}
- (instancetype)initWithTitle:(NSString *)title{
    if (self = [super init]) {
        self.title = title;
        
        _DataArray = [[NSMutableArray alloc]init];
        // 接收键盘显示隐藏的通知
        //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
        //        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
        
        // UI搭建
        [self setupSimpleUI];
    }
    return self;
}
#pragma mark - UI搭建
/** UI搭建 简单弹窗 */
-(void)setupSimpleUI{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1;
    }];
    
    //------- 弹窗主内容 -------//
    self.contentView = [[UIView alloc]init];
    self.contentView.frame = CGRectMake(10, 80, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 250);
    //self.contentView.center = self.center;
    [self addSubview:self.contentView];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.layer.cornerRadius = 6;
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.contentView.width, 22)];
    [self.contentView addSubview:titleLabel];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = LYColor(237, 204, 105);
    titleLabel.text = self.title;
    
    
    //关闭按钮
    UIImageView *closeIma = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 45, 5, 20, 20)];
    closeIma.image = [UIImage imageNamed:@"分享关闭"];
    closeIma.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dis)];
    //self.userInteractionEnabled = YES;
    //[self addGestureRecognizer:tap];
    closeIma.contentMode = UIViewContentModeScaleAspectFit;
    [closeIma addGestureRecognizer:tap];
    [self.contentView addSubview:closeIma];
    
    // 填写异常情况描述的textView
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, titleLabel.maxY + 10, self.contentView.width - 20, 103)];
    [self.contentView addSubview:self.textView];
    self.textView.layer.cornerRadius = 6;
    //self.textView.backgroundColor = LYColor(237, 204, 105);
    self.textView.layer.borderColor =LYColor(237, 204, 105).CGColor;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.font = [UIFont systemFontOfSize:12];
    //[self.textView becomeFirstResponder];
    self.textView.delegate = self;
    
    // textView里面的占位label
    self.messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, self.textView.width - 16, self.textView.height - 16)];
    self.messageLabel.text = NSLocalizedString(@"想说什么", nil);
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.font = [UIFont systemFontOfSize:12];
    self.messageLabel.textColor = LYColor(218, 218, 218);
    [self.messageLabel sizeToFit];
    [self.textView addSubview:self.messageLabel];
    
    // 红色提示label
    UILabel *redLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.textView.x, self.textView.maxY + 5, self.textView.width, 20)];
    [self.contentView addSubview:redLabel];
    redLabel.textColor = LYColor(218, 218, 218);
    redLabel.font = [UIFont systemFontOfSize:12];
    redLabel.numberOfLines = 0;
    if ([self.message isEqualToString:@""]) {
        redLabel.text = @"";
    }else{
        redLabel.text = [NSString stringWithFormat:@"%@",self.message];
    }
    [redLabel sizeToFit];
    
    // 申报异常按钮
    UIButton *abnormalButton = [[UIButton alloc]initWithFrame:CGRectMake(self.textView.minX, redLabel.maxY + 5, 100, 40)];
    //[self.contentView addSubview:abnormalButton];
    abnormalButton.backgroundColor = LYColor(237, 204, 105);
    [abnormalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [abnormalButton setTitle:@"分享" forState:UIControlStateNormal];
    [abnormalButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    abnormalButton.layer.cornerRadius = 6;
    [abnormalButton addTarget:self action:@selector(abnormalButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    // 取消按钮
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(self.textView.maxX - 100, abnormalButton.minY, 100, 40)];
    [self.contentView addSubview:cancelButton];
    [cancelButton setTitle:@"分享" forState:UIControlStateNormal];
    cancelButton.backgroundColor = LYColor(237, 204, 105);
    // cancelButton.backgroundColor = [UIColor colorWithHexString:@"c8c8c8"];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    cancelButton.layer.cornerRadius = 6;
    [cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
}

/** UI搭建 */
- (void)setUpUI{
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1;
    }];
    
   
    
    //------- 弹窗主内容 -------//
    self.contentView = [[UIView alloc]init];
    //self.contentView.frame = CGRectMake(10, 80, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 250);
    CGFloat height = 35;
    if (IsiPhoneX) {
        height = 80;
    }
    self.contentView.frame = CGRectMake(10, height, SCREEN_WIDTH - 20, 370);
    //self.contentView.center = self.center;
    [self addSubview:self.contentView];
    self.contentView.backgroundColor = [UIColor whiteColor];
    self.contentView.layer.cornerRadius = 6;
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.contentView.width, 22)];
    [self.contentView addSubview:titleLabel];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = MainColor;
    titleLabel.text = self.title;
    
    
    //关闭按钮
    UIImageView *closeIma = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 45, 5, 20, 20)];
    closeIma.image = [UIImage imageNamed:@"分享关闭"];
    closeIma.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dis)];
    //self.userInteractionEnabled = YES;
    //[self addGestureRecognizer:tap];
    closeIma.contentMode = UIViewContentModeScaleAspectFit;
    [closeIma addGestureRecognizer:tap];
    [self.contentView addSubview:closeIma];
    
    // 填写异常情况描述的textView
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(10, titleLabel.maxY + 10, self.contentView.width - 20, 103)];
    [self.contentView addSubview:self.textView];
    self.textView.layer.cornerRadius = 6;
    //self.textView.backgroundColor = LYColor(237, 204, 105);
    self.textView.layer.borderColor =MainColor.CGColor;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.font = [UIFont systemFontOfSize:12];
    //[self.textView becomeFirstResponder];
    self.textView.delegate = self;
    
    // textView里面的占位label
    self.messageLabel = [[UILabel alloc]initWithFrame:CGRectMake(8, 8, self.textView.width - 16, self.textView.height - 16)];
    self.messageLabel.text = NSLocalizedString(@"想说什么", nil);
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.font = [UIFont systemFontOfSize:12];
    self.messageLabel.textColor = LYColor(218, 218, 218);
    [self.messageLabel sizeToFit];
    [self.textView addSubview:self.messageLabel];
    
    // 红色提示label
    UILabel *redLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.textView.x, self.textView.maxY + 30, self.textView.width, 20)];
    [self.contentView addSubview:redLabel];
    redLabel.textColor = LYColor(218, 218, 218);
    redLabel.font = [UIFont systemFontOfSize:12];
    redLabel.numberOfLines = 0;
    if ([self.message isEqualToString:@""]) {
        redLabel.text = @"";
    }else{
        redLabel.text = [NSString stringWithFormat:@"%@",self.message];
    }
    [redLabel sizeToFit];
    
    // 申报异常按钮
    UIButton *abnormalButton = [[UIButton alloc]initWithFrame:CGRectMake(self.textView.minX, self.textView.maxY + 20, 100, 40)];
    //[self.contentView addSubview:abnormalButton];
    abnormalButton.backgroundColor = LYColor(237, 204, 105);
    [abnormalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [abnormalButton setTitle:NSLocalizedString(@"分享", nil) forState:UIControlStateNormal];
    [abnormalButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    abnormalButton.layer.cornerRadius = 6;
    [abnormalButton addTarget:self action:@selector(abnormalButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    // 取消按钮
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(self.textView.maxX - 100, abnormalButton.minY, 100, 40)];
    [self.contentView addSubview:cancelButton];
    [cancelButton setTitle:NSLocalizedString(@"分享", nil) forState:UIControlStateNormal];
    cancelButton.backgroundColor = MainColor;
   // cancelButton.backgroundColor = [UIColor colorWithHexString:@"c8c8c8"];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton.titleLabel setFont:[UIFont systemFontOfSize:18]];
    cancelButton.layer.cornerRadius = 6;
    [cancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    //------- 调整弹窗高度和中心 -------//
    //self.contentView.height = cancelButton.maxY + 10;
    //self.contentView.center = self.center;
    
    //新增的tableview self.contentView.height -CGRectGetMaxY(cancelButton.frame) - 15
    
    self.contentView.userInteractionEnabled = YES;
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(cancelButton.frame) + 10, self.contentView.width , 150  ) style:UITableViewStylePlain];
    MainTableView.userInteractionEnabled = YES;
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.bounces = NO;
    MainTableView.showsVerticalScrollIndicator = NO;
    MainTableView.showsHorizontalScrollIndicator = NO;
    [MainTableView registerClass:[TagFriedTableViewCell class] forCellReuseIdentifier:@"cell"];
    MainTableView.fd_debugLogEnabled = YES;
    
    
    [self.contentView addSubview:MainTableView];
    
}
-(void)dis{
     [self removeFromSuperview];
    
    [self.delegate didCloseWindow];
}
#pragma mark  - tableview的代理方法
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _DataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    TagFriedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CommentModel *model = _DataArray[indexPath.row];

    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
   
    [cell setCModel:model];
    cell.LikeButton.hidden = YES;
    
    cell.UserName.userInteractionEnabled = YES;
    cell.UserImage.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkImformation:)];
    UITapGestureRecognizer *tap1 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkImformation:)];
    
    [cell.UserImage addGestureRecognizer:tap];
    [cell.UserName addGestureRecognizer:tap1];
    
    cell.UserImage.tag = 200 + indexPath.row;
    cell.UserName.tag = 200 + indexPath.row;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentModel *model = _DataArray[indexPath.row];
    return model.cellHeight + 45;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.contentView.width,90 )];
    //view.layer.borderColor = LYColor(248, 248, 248).CGColor;
    view.layer.borderWidth = 1.0f;
    view.layer.borderColor = [UIColor blackColor].CGColor;
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIImageView *userImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 70, 70)];
    userImage.layer.cornerRadius = 35;
    userImage.clipsToBounds = YES;
    userImage.contentMode = UIViewContentModeScaleAspectFit;
    NSString *imageurl = [NSString stringWithFormat:@"%@",self.model.UserImage];
    [userImage sd_setImageWithURL:[NSURL URLWithString:imageurl]];
    [view addSubview:userImage];
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(90, 0, 1, 90)];
    gapView.backgroundColor = [UIColor blackColor];
    [view addSubview:gapView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(93, 10, 150, 20)];
    label.text = NSLocalizedString(@"帖文更新 Status Update", nil);
    label.font = Bold_FONT(13);
    label.textColor = [UIColor blackColor];
    [view addSubview:label];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(93, 40, 150, 20)];
    label1.text = [NSString stringWithFormat:@"by %@",self.model.UserName];
    label1.font = Bold_FONT(11);
    label1.textColor = LYColor(218, 218, 218);
    [view addSubview:label1];
    
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 90;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}
#pragma mark - 获取分享用户的数据
-(void)checkImformation:(UITapGestureRecognizer *)tap{
    CommentModel *model = _DataArray[tap.view.tag - 200];
    [self dismiss];
    if ([self.delegate respondsToSelector:@selector(declareAbnormalAlertView:clickedButtonAtIndex:)]) {
        [self.delegate pushNavagation:model];
    }
}
-(void)loadData{
    if (self.model.post_id == nil) {
        return;
    }
    NSString *geturl = [NSString stringWithFormat:@"%@%@",TestUrl,url];
    NSDictionary *parmDic =@{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":self.model.post_id};
    [PPNetworkHelper POST:geturl parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr.count == 0) {
        
        }else{
            for (NSDictionary *Comdict in dataArr) {
                [_DataArray addObject:[CommentModel parsenWithDictionary:Comdict]];
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
#pragma mark - 弹出此弹窗
/** 弹出此弹窗 */
- (void)show{
    [self loadData];
    UIWindow *keyWindow = [UIApplication sharedApplication].keyWindow;
    [keyWindow addSubview:self];
}

#pragma mark - 移除此弹窗
/** 移除此弹窗 */
- (void)dismiss{
    [self removeFromSuperview];
}

#pragma mark - 申报异常按钮点击
/** 申报异常按钮点击 */
- (void)abnormalButtonClicked{
    if ([self.delegate respondsToSelector:@selector(declareAbnormalAlertView:clickedButtonAtIndex:)]) {
        [self.delegate declareAbnormalAlertView:self clickedButtonAtIndex:AlertButtonLeft];
    }
    [self dismiss];
}

#pragma mark - 取消按钮点击
/** 取消按钮点击 */
- (void)cancelButtonClicked{
    [self.delegate didSendtext:self.textView.text];
    [self dismiss];
}

#pragma mark - UITextView代理方法
- (void)textViewDidChange:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        self.messageLabel.hidden = NO;
    }else{
        self.messageLabel.hidden = YES;
    }
}

/**
 *  键盘将要显示
 *
 *  @param notification 通知
 */
//-(void)keyboardWillShow:(NSNotification *)notification
//{
//    // 获取到了键盘frame
//    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    CGFloat keyboardHeight = frame.size.height;
//
//   // self.contentView.maxY = SCREEN_HEIGHT - keyboardHeight - 10;
//}
///**
// *  键盘将要隐藏
// *
// *  @param notification 通知
// */
//-(void)keyboardWillHidden:(NSNotification *)notification
//{
//    // 弹窗回到屏幕正中
//    self.contentView.centerY = SCREEN_HEIGHT / 2;
//}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.textView resignFirstResponder];
}
-(void)didSendtext:(NSString *)text{
//    if ([self.delegate respondsToSelector:@selector(declareAbnormalAlertView:clickedButtonAtIndex:)]) {
//        [self.delegate declareAbnormalAlertView:self clickedButtonAtIndex:AlertButtonLeft];
//    }
   
}
@end
