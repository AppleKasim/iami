//
//  WebViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"
#import<WebKit/WebKit.h>

@interface WebViewController : XTViewController<UIWebViewDelegate>
{
    UIWebView *mWebView;
    UIActivityIndicatorView *activityIndicator;
    WKWebView *WebV;
}
@property (nonatomic,strong) NSURL *url;
@end
