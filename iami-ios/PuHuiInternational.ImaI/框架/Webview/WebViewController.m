//
//  WebViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WebV = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT )];
    WebV.allowsBackForwardNavigationGestures = YES;
    NSURL *url = [NSURL URLWithString:@"www.baidu.com"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [WebV loadRequest:request];
    WebV.navigationDelegate = self;
    WebV.UIDelegate = self;
    
    [self.view addSubview:WebV];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    //    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    view.tag = 100;
    //    view.backgroundColor = [UIColor blackColor];
    //    view.alpha = 0.5f;
    //    [self.view addSubview:view];
    //
    //    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    //    [activityIndicator setCenter:view.center];
    //    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    //    [view addSubview:activityIndicator];
    //    [activityIndicator startAnimating];
    [CYToast showStatusWithString:@"正在加载"];
}
//页面加载完成之后调用
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    //    [activityIndicator stopAnimating];
    //    UIView *view = (UIView *)[self.view viewWithTag:100];
    //    [view removeFromSuperview];
    [CYToast dismiss];
    
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation {
    //    [activityIndicator stopAnimating];
    //    UIView *view = (UIView *)[self.view viewWithTag:100];
    //    [view removeFromSuperview];
    [CYToast dismiss];
}
//网页开始加载的时候调用
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //创建UIActivityIndicatorView背底半透明view
    //    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
    //    view.tag = 100;
    //    view.backgroundColor = [UIColor blackColor];
    //    view.alpha = 0.5f;
    //    [self.view addSubview:view];
    //
    //    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    //    [activityIndicator setCenter:view.center];
    //    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    //    [view addSubview:activityIndicator];
    //    [activityIndicator startAnimating];
    
    
}

//网页加载完成的时候调用
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    //    [activityIndicator stopAnimating];
    //    UIView *view = (UIView *)[self.view viewWithTag:100];
    //    [view removeFromSuperview];
    //[CYToast dismiss];
}

//网页加载错误的时候调用
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView *)[self.view viewWithTag:100];
    [view removeFromSuperview];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
