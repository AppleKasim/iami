//
//  PHUserModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "PHUserModel.h"
#define UserKey @"userName"
#define Token @"Token"
#define Login @"Login"
#define Imcount @"IMAccount"
#define Title @"title"
#define Content @"Content"
#define IsFirst @"IsFirst"
#define AccToken @"Access Token"
#define MemerId @"member_id"
#define Birth @"birth"
#define Mobile @"mobile"
#define Image @"image"
#define Banner @"banner"
#define Emial @"email"
#define password @"password"
#define infoShow @"info_show"
#define Langu @"language"
@implementation PHUserModel

singleton_implementation(PHUserModel)
-(void)saveUserInfoToSanbox{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:self.nickname forKey:UserKey];
    [defaults setObject:self.csrf_token forKey:Token];
    [defaults setBool:self.isLogin forKey:Login];
    [defaults setObject:self.IMAccount forKey:Imcount];
    [defaults setObject:self.title forKey:Title];
    [defaults setObject:self.content forKey:Content];
    [defaults setObject:self.isFirst forKey:IsFirst];
    [defaults setObject:self.AccessToken forKey:AccToken];
    [defaults setObject:self.member_id forKey:MemerId];
    [defaults setObject:self.Birthday forKey:Birth];
    [defaults setObject:self.mobile forKey:Mobile];
    [defaults setObject:self.UserImage forKey:Image];
    [defaults setObject:self.banner forKey:Banner];
    [defaults setObject:self.email forKey:Emial];
    [defaults setObject:self.Password forKey:password];
    [defaults setObject:self.info_show forKey:infoShow];
    [defaults setObject:self.language forKey:Langu];
    [defaults synchronize];
}
/**
 *  从沙盒里获取用户数据
 */
-(void)loadUserInfoFromSanbox{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.nickname = [defaults objectForKey:UserKey];
    self.csrf_token = [defaults objectForKey:Token];
    self.isLogin = [defaults boolForKey:Login];
    self.IMAccount = [defaults objectForKey:Imcount];
    self.title = [defaults objectForKey:Title];
    self.content = [defaults objectForKey:Content];
    self.isFirst = [defaults objectForKey:IsFirst];
    self.AccessToken = [defaults objectForKey:AccToken];
    self.member_id = [defaults objectForKey:MemerId];
    self.Birthday = [defaults objectForKey:Birth];
    self.mobile = [defaults objectForKey:Mobile];
    self.UserImage = [defaults objectForKey:Image];
    self.banner = [defaults objectForKey:Banner];
    self.email = [defaults objectForKey:Emial];
    self.Password = [defaults objectForKey:password];
    self.info_show = [defaults objectForKey:infoShow];
    self.language = [defaults objectForKey:Langu];
}

/**
 *  顯示目前用戶資料
 */
-(void)showUserInfo{
    NSLog(@"NickName:%@", self.nickname);
    NSLog(@"isLogin:%d", self.isLogin);
    NSLog(@"title:%@", self.title);
    NSLog(@"Birthday:%@", self.Birthday);
    NSLog(@"email:%@", self.email);
    NSLog(@"language:%@", self.language);
}

//-(void)setInfo_show:(NSMutableString *)info_show{
//    _info_show = [_info_show mutableCopy];
//}
-(void)connect{
    NSURL* url = [[NSURL alloc] initWithString:@"http://push.iami-web.me:2120/"];
    _manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @NO, @"forcePolling": @YES,}];
    _socket = _manager.defaultSocket;

    
    //登录
    [_socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
        [_socket emit:@"login" with:@[[PHUserModel sharedPHUserModel].member_id]];
    }];
    
    
    [_socket on:@"currentAmount" callback:^(NSArray* data, SocketAckEmitter* ack) {
        double cur = [[data objectAtIndex:0] floatValue];
        
        [[_socket emitWithAck:@"canUpdate" with:@[@(cur)]] timingOutAfter:0 callback:^(NSArray* data) {
            [_socket emit:@"update" with:@[@{@"amount": @(cur + 2.50)}]];
        }];
        
        [ack with:@[@"Got your currentAmount, ", @"dude"]];
    }];
    //接收新消息
    [_socket on:@"new_msg" callback:^(NSArray *data, SocketAckEmitter *ack) {
        
        NSString *string = [NSString stringWithFormat:@"%@",data[0]];

        NSString *replacedStr = [string stringByReplacingOccurrencesOfString:@"&quot;"withString:@"\""];
        NSString *newreplace1;
        NSLog(@"%@", replacedStr);

        if ([[replacedStr substringWithRange:NSMakeRange(2, 4)] isEqualToString:@"task"]) {
            newreplace1 = replacedStr;
        } else {
            NSString *newreplace = [replacedStr stringByReplacingOccurrencesOfString:@"[" withString:@""];
            newreplace1 = [newreplace stringByReplacingOccurrencesOfString:@"]" withString:@""];
        }

        NSDictionary *json = [self dictionaryWithJsonString:newreplace1];
        
        if (json) {
              [[NSNotificationCenter defaultCenter]postNotificationName:@"newMessage" object:json];
        }
      
    }];
        
    
    [_socket connect];
    
}
-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
    {
        if (jsonString == nil) {
            return nil;
        }
        
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *err;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:NSJSONReadingMutableContainers
                                                              error:&err];
        if(err)
        {
            NSLog(@"json解析失败：%@",err);
            return nil;
        }
        return dic;
    }
@end
