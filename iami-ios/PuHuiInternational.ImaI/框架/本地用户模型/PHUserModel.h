//
//  PHUserModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>
@import SocketIO;

@interface PHUserModel : NSObject
singleton_interface(PHUserModel);

@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *member_id;
@property (nonatomic,copy) NSString *csrf_token;
@property (nonatomic,copy) NSString *AccessToken;
@property (nonatomic,copy) NSString *Birthday;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *UserImage;
@property (nonatomic,copy) NSString *banner;
@property (nonatomic,copy) NSString *email;
@property (nonatomic,copy) NSString *Password;
@property (nonatomic,copy) NSString *language;

@property (nonatomic,assign) BOOL isLogin;
@property (nonatomic,copy) NSString *IMAccount;

@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *content;
@property (nonatomic,copy) NSString *isFirst;
@property (nonatomic,copy) NSString *qiniu_id;

@property (nonatomic,strong) NSMutableString *info_show;

@property (nonatomic,strong) SocketManager *manager;

@property (nonatomic,strong) SocketIOClient *socket;

/**
 *  从沙盒里获取用户数据
 */
-(void)loadUserInfoFromSanbox;
/**
 *  保存用户数据到沙盒
 */
-(void)saveUserInfoToSanbox;
/**
 *  顯示目前用戶資料
 */
-(void)showUserInfo;

//创建socket
-(void)connect;

-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;
@end
