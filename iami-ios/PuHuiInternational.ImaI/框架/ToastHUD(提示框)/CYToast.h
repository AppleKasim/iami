//
//  CYAboutCell.m
//  TianRuiApp
//
//  Created by yaoxc on 15/3/25.
//  Copyright (c) 2015年 yaoxc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"

@interface CYToast : NSObject

+ (instancetype )shareInstance;

+ (void)dismiss;
+ (void)showStatusWithString:(NSString *)string;
+ (void)showErrorWithString:(NSString *)string;
+ (void)showSuccessWithString:(NSString *)string;

+ (void)showStatusWithString:(NSString *)string hideAfterDelay:(CGFloat)delay;
+ (void)showSStatusWithString:(NSString *)string hideAfterDelay:(CGFloat)delay;
@end


@interface MyToast : NSObject

+ (instancetype )shareInstance;

+ (void)showFileWithString:(NSString *)string hideAfterDelay:(CGFloat)delay;



@end
