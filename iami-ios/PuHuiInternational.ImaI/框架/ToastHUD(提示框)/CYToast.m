//
//  CYAboutCell.m
//  TianRuiApp
//
//  Created by yaoxc on 15/3/25.
//  Copyright (c) 2015年 yaoxc. All rights reserved.
//

#import "CYToast.h"


@interface CYToast()

@property (nonatomic,strong) NSTimer *timer;

@end

@implementation CYToast

+ (instancetype )shareInstance {
    static CYToast *g_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        g_instance = [[CYToast alloc] init];
    });
    
    return g_instance;
}

- (void)dealloc
{
    [_timer invalidate];
    _timer = nil;
}

- (void)dismiss
{
    [SVProgressHUD dismiss];
}

+ (void)showStatusWithString:(NSString *)string {
    [SVProgressHUD  setDefaultMaskType:SVProgressHUDMaskTypeClear];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeFlat]
    NSString *str = NSLocalizedString(string, nil);
    if (str) {
        [SVProgressHUD showWithStatus:str];
    }else{
        [SVProgressHUD showWithStatus:string];
    }
    //[SVProgressHUD showWithStatus:string];
}

+ (void)dismiss {
    [SVProgressHUD dismiss];
}

+ (void)showSuccessWithString:(NSString *)string
{
    [SVProgressHUD showSuccessWithStatus:string];
    
    CYToast *tools = [CYToast shareInstance];
    [tools.timer invalidate];
    tools.timer = nil;
    tools.timer = [NSTimer timerWithTimeInterval:1.5 target:tools selector:@selector(dismiss) userInfo:nil repeats:NO];
}

+ (void)showErrorWithString:(NSString *)string
{
    [SVProgressHUD showErrorWithStatus:string];
    CYToast *tools = [CYToast shareInstance];
    [tools.timer invalidate];
    tools.timer = nil;
    tools.timer = [NSTimer timerWithTimeInterval:1.5 target:tools selector:@selector(dismiss) userInfo:nil repeats:NO];
}

+ (void)showStatusWithString:(NSString *)string hideAfterDelay:(CGFloat)delay
{
    [SVProgressHUD showSuccessWithStatus:string];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

+ (void)showSStatusWithString:(NSString *)string hideAfterDelay:(CGFloat)delay
{
    [SVProgressHUD showWithStatus:string];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
    });
}

#pragma mark ---------------安卓版 下方出现提示条-------------------
//获取屏幕宽度
#define KScreenWidth  [UIScreen mainScreen].bounds.size.width
//获取屏幕高度
#define KScreenHeight [UIScreen mainScreen].bounds.size.height

#define FoutOfSize 15

@end

@implementation MyToast {
    UIImageView *_bgImageView;
}

- (UIImageView*)imageView:(NSString*)string delay:(CGFloat)delay {
    if (_bgImageView==nil) {
        _bgImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        _bgImageView.alpha = 1;
        _bgImageView.backgroundColor=[UIColor clearColor];
        
        CGSize labelSize =[MyToast calculateTextHeight:string];
        
        UILabel *label = [[UILabel alloc]init];
        label.bounds =CGRectMake(0, 0, labelSize.width+20, labelSize.height+20);
        label.center = CGPointMake(KScreenWidth/2, KScreenHeight/2.5);
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.9];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:FoutOfSize];
        label.numberOfLines = 0;
        label.text = string;
        label.layer.cornerRadius =5;
        label.clipsToBounds =YES;
        [_bgImageView addSubview:label];
        
        [[MyToast shareInstance] performSelector:@selector(deleteSelfView:) withObject:label afterDelay:delay+0.5];
    }
    return _bgImageView;
}

+ (instancetype )shareInstance {
    static MyToast *progress = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        progress = [[MyToast alloc] init];
    });
    return progress;
}

+ (void)showFileWithString:(NSString *)string hideAfterDelay:(CGFloat)delay {
    [CYToast dismiss];
    UIImageView *imageView = [[MyToast shareInstance] imageView:string delay:delay];
    if (imageView.hidden) {
        imageView.alpha = 1;
        imageView.hidden=NO;
        UILabel *label =(UILabel*)imageView.subviews[0];
        
        CGSize labelSize =[MyToast calculateTextHeight:string];
        label.bounds =CGRectMake(0, 0, labelSize.width+10, labelSize.height+10);
        label.text =string;
        
        [[MyToast shareInstance] performSelector:@selector(deleteSelfView:) withObject:label afterDelay:delay+0.5];
    }
    UIWindow *window =[[UIApplication sharedApplication] keyWindow];
    if (!window) {
        window =[[UIApplication sharedApplication] windows].firstObject;
    }
    if (!window) {
        window =[[UIApplication sharedApplication] windows].lastObject;
    }
    [window addSubview:imageView];

}
-(void)deleteSelfView:(UILabel*)label {
    [UIView animateWithDuration:0.5 animations:^{
        _bgImageView.alpha = 0;
    } completion:^(BOOL finished) {
        _bgImageView.hidden=YES;
    }];
}

+(CGSize)calculateTextHeight:(NSString*)string {
    
    CGRect textRect=[string boundingRectWithSize:CGSizeMake(KScreenWidth-60, CGFLOAT_MAX) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FoutOfSize]} context:nil];
    return textRect.size;
}


@end
