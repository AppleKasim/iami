//
//  NSBundle+Language.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (Language)

+(void)setLanguage:(NSString *)language;


@end
