//
//  QuestContentCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/4.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift

class QuestContentCell: UITableViewCell {

    var timerDisposable: Disposable?

    @IBOutlet weak var numberLabel: UILabel! {
        didSet {
            numberLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var starStackView: UIStackView!
    @IBOutlet weak var successImageView: UIImageView!

    @IBOutlet weak var countdownLabel: UILabel! {
        didSet {
            countdownLabel.textColor = GlobalConstants.grayColorA0
        }
    }

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var difficultyLabel: UILabel! {
        didSet {
            difficultyLabel.textColor = GlobalConstants.grayColorA0
        }
    }

    @IBOutlet weak var bonusLabel: UILabel! {
        didSet {
            bonusLabel.textColor = GlobalConstants.grayColorA0
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func addStar(quantity: Int) {
        for (index, view) in starStackView.subviews.enumerated() where index != 0 {
            view.removeFromSuperview()
        }

        for _ in 1...quantity where quantity >= 1 {
            let starImageView = UIImageView(image: UIImage(named: "QuestDifficulty"))
            starStackView.addArrangedSubview(starImageView)
        }
    }
}
