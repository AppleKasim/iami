//
//  SignInCollectionViewCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/4.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class SignInCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var checkedImageView: UIImageView!

    @IBOutlet weak var dayLabel: UILabel! {
        didSet {
            dayLabel.textColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var dayNumberLabel: UILabel! {
        didSet {
            dayNumberLabel.textColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var receiveLabel: UILabel! {
        didSet {
            receiveLabel.isHidden = true
            receiveLabel.text = "已領取"
        }
    }

    func showReceive() {
        self.receiveLabel.isHidden = false
        self.contentView.backgroundColor = GlobalConstants.grayColorD2
        self.dayLabel.isHidden = true
        self.dayNumberLabel.isHidden = true
        self.checkedImageView.isHidden = true
    }

    func showChecked() {
        self.contentView.backgroundColor = GlobalConstants.grayColorE5
        self.checkedImageView.isHidden = false
        self.checkedImageView.image = #imageLiteral(resourceName: "Checked")
    }

    func showNormalStyle() {
        self.contentView.backgroundColor = GlobalConstants.grayColorE5
        self.dayLabel.isHidden = false
        self.dayNumberLabel.isHidden = false
        self.receiveLabel.isHidden = true
        self.checkedImageView.isHidden = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        self.contentView.layer.borderColor = GlobalConstants.purpleColor.cgColor
        self.contentView.layer.borderWidth = 2.0
        self.contentView.layer.cornerRadius = 5
    }
}
