//
//  SignInTableViewCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/4.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class SignInTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout! {
        didSet {
            let width = (UIScreen.main.bounds.width * 0.95 / 7) - 5

            collectionViewLayout.itemSize = CGSize(width: width, height: width)
            collectionViewLayout.minimumLineSpacing = 5
            collectionViewLayout.minimumInteritemSpacing = 2.5
        }
    }

    var days: [SignInDays] = []

    override func awakeFromNib() {
        super.awakeFromNib()

        let signInNib = UINib(nibName: "SignInCollectionViewCell", bundle: nil)
        collectionView.register(signInNib, forCellWithReuseIdentifier: "SignInCollectionViewCell")

        let rewardNib = UINib(nibName: "RewardCollectionViewCell", bundle: nil)
        collectionView.register(rewardNib, forCellWithReuseIdentifier: "RewardCollectionViewCell")

        self.collectionView.collectionViewLayout.invalidateLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension SignInTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }

    // swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SignInCollectionViewCell", for: indexPath)

        guard let signInCell = cell as? SignInCollectionViewCell else {
            return cell
        }

        let day = days[indexPath.row]

        signInCell.dayNumberLabel.text = String(indexPath.row + 1)

        if day.isRewardDay == "Y", day.rewardType != "not_arrive" {
            signInCell.showReceive()
        } else if day.isChecked == "Y" {
            signInCell.showChecked()
        } else {
            signInCell.showNormalStyle()
        }

        return signInCell
    }
    // swiftlint:enable line_length
}
