//
//  QuestModel.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation

struct MassageModel: Codable {
    var status: String
    var msg: String
}

struct BasicModel<Attributes>: Codable where Attributes: Codable {
    var status: String
    var message: String
    var code: String
    var data: Attributes
}

struct BasicModelNoCode<Attributes>: Codable where Attributes: Codable {
    var status: String
    var message: String
    var data: Attributes
}

struct BasicMessageModel: Codable {
    var status: String
    var message: String
    var code: String
}

struct LoadSignInData: Codable {
    var memberId: String = ""
    var signTime: String = ""
    var signCount: String = ""
    var count: String = ""
    var days: [SignInDays] = []

    enum CodingKeys: String, CodingKey {
        case memberId = "member_id"
        case signTime = "sign_time"
        case signCount = "sign_count"
        case count
        case days
    }
}

struct SignInDays: Codable {
    var day: String
    var isChecked: String
    var isRewardDay: String
    var rewardType: String
}

struct MissionsData: Codable {
    var name: String
    var experience: String
    var checkModule: String
    var star: String
    var times: String
    var rewardModule: String
    var rewardId: String
    var missionId: String
    var status: StatusData
    var startTime: String?
    var endTime: String?
    var timer: Float?

    enum CodingKeys: String, CodingKey {
        case name
        case experience = "exp_value"
        case checkModule = "check_module"
        case star
        case times
        case rewardModule = "reward_module"
        case rewardId = "reward_id"
        case missionId = "mission_id"
        case status
        case startTime = "start_time"
        case endTime = "end_time"
        case timer
    }
}
