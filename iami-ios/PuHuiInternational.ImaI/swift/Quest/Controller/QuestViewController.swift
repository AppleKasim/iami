//
//  QuestViewController.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/4.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import MJRefresh

class QuestViewController: UIViewController, TaskAlertForSocket {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var signInButton: UIButton!

    @IBOutlet weak var backUnderlineView: UIView! {
        didSet {
            backUnderlineView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var underlineView: UIView! {
        didSet {
            underlineView.backgroundColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var underlineLeading: NSLayoutConstraint!

    var segmentedIndex = 0 {
        didSet {
            SVProgressHUD.show()
            switch segmentedIndex {
            case 0:
                signInButton.isHidden = false
                loadSignData()
            case 1:
                getEveryDayMissions()
                signInButton.isHidden = true
            case 2:
                getLimitMissions()
                signInButton.isHidden = true
            case 3:
                getSpecialMissions()
                signInButton.isHidden = true
            default:
                break
            }
        }
    }

    var loadSignInData = LoadSignInData()
    var everyDayMissionsData: [MissionsData] = []
    var limitMissionsData: [MissionsData] = []
    var specialMissionsData: [MissionsData] = []

    var disposeBag = DisposeBag()

    let header = MJRefreshStateHeader()
    let apiManager = ApiManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        let signInTableViewCell = UINib.init(nibName: "SignInTableViewCell", bundle: Bundle.main)
        tableView.register(signInTableViewCell, forCellReuseIdentifier: "SignInTableViewCell")

        removeBorderForSegmentedControl()

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension

        segmentedIndex = 0

        header.setRefreshingTarget(self, refreshingAction: #selector(refreshing))
        header.setTitle("idle", for: .idle)
        header.setTitle("willRefresh", for: .willRefresh)
        header.setTitle("Pulling", for: .pulling)
        header.setTitle("refreshing", for: .refreshing)
        header.lastUpdatedTimeLabel.text = "時間"
        header.lastUpdatedTimeText = { (date) -> String in
            guard let currencyDate = date else {
                return "時間:"
            }
            return "時間:\(String(describing: currencyDate))"
        }

        tableView.mj_header = header

        newMessageNotification()

    }

    @objc func refreshing() {
        switch segmentedIndex {
        case 0:
            loadSignData()
        case 1:
            getEveryDayMissions()
        case 2:
            getLimitMissions()
        case 3:
            getSpecialMissions()
        default:
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func change(_ sender: UISegmentedControl) {
        changeUnderlinePosition()
        segmentedIndex = sender.selectedSegmentIndex

        tableView.reloadData()
    }

    @IBAction func signin(_ sender: UIButton) {
        signInToday()
    }

    func removeBorderForSegmentedControl() {
        segmentedControl.backgroundColor = .clear
        segmentedControl.tintColor = .clear

        segmentedControl.setTitleTextAttributes([
            //NSAttributedStringKey.font : UIFont(name: "DINCondensed-Bold", size: 18),
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
            ], for: .normal)

        segmentedControl.setTitleTextAttributes([
            //NSAttributedStringKey.font : UIFont(name: "DINCondensed-Bold", size: 18),
            NSAttributedStringKey.foregroundColor: GlobalConstants.purpleColor
            ], for: .selected)
    }

    func changeUnderlinePosition() {
        let itemWidth = GlobalConstants.screenWidth * 0.95 / CGFloat(segmentedControl.numberOfSegments)

        UIView.animate(withDuration: 0.3) {
            self.underlineLeading.constant = CGFloat(self.segmentedControl.selectedSegmentIndex) * itemWidth
        }
    }

    func getHoursMinutesSeconds(allSeconds: Float) -> String {
        let allTime: Int = Int(allSeconds)
        var hours = 0
        var minutes = 0
        var seconds = 0
        var hoursText = ""
        var minutesText = ""
        var secondsText = ""

        hours = allTime / 3600
        hoursText = hours > 9 ? "\(hours)" : "0\(hours)"

        minutes = allTime % 3600 / 60
        minutesText = minutes > 9 ? "\(minutes)" : "0\(minutes)"

        seconds = allTime % 3600 % 60
        secondsText = seconds > 9 ? "\(seconds)" : "0\(seconds)"

        return "\(hoursText):\(minutesText):\(secondsText)"
    }

    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: "確認", style: .cancel, handler: nil)

        alertController.addAction(alertActionOk)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension QuestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedIndex {
        case 0:
            return 1
        case 1:
            return everyDayMissionsData.count
        case 2:
            return limitMissionsData.count
        case 3:
            return specialMissionsData.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch segmentedIndex {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignInTableViewCell", for: indexPath)

            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            guard let signInCell = cell as? SignInTableViewCell else {
                return cell
            }

            signInCell.days = loadSignInData.days

            if indexPath.row < loadSignInData.days.count,
                loadSignInData.days[indexPath.row].isRewardDay == "Y" ,
                loadSignInData.days[indexPath.row].rewardType == "not_receive" {
                continueSevenReward(day: indexPath.row + 1)
            }

            signInCell.collectionView.reloadData()

            return signInCell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestContentCell", for: indexPath)

            guard let questContentCell = cell as? QuestContentCell else {
                return cell
            }

            let missionsData = everyDayMissionsData[indexPath.row]

            questContentCell.numberLabel.text = "\(indexPath.row + 1)."
            questContentCell.titleLabel.text = missionsData.name

            let bonusText = "\(missionsData.status.progress)獲得經驗值\(missionsData.experience)分"
            questContentCell.bonusLabel.text = bonusText

            let successImage = missionsData.status.completed == "Y" ? UIImage(named: "注册通过"): UIImage(named: "注册未通过")
            questContentCell.successImageView.image = successImage

            let starInt = Int(missionsData.star)
            questContentCell.addStar(quantity: starInt!)
            questContentCell.countdownLabel.isHidden = true

            return questContentCell

        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestContentCell", for: indexPath)

            guard let questContentCell = cell as? QuestContentCell else {
                return cell
            }

            let missionsData = limitMissionsData[indexPath.row]

            questContentCell.numberLabel.text = "\(indexPath.row + 1)."
            questContentCell.titleLabel.text = missionsData.name

            let bonusText = "\(missionsData.status.progress)獲得經驗值\(missionsData.experience)分"
            questContentCell.bonusLabel.text = bonusText

            let successImage = missionsData.status.completed == "Y" ? UIImage(named: "注册通过"): UIImage(named: "注册未通过")
            questContentCell.successImageView.image = successImage

            let starInt = Int(missionsData.star)
            questContentCell.addStar(quantity: starInt!)
            questContentCell.countdownLabel.isHidden = false

            setTimer(for: questContentCell, starTimer: self.limitMissionsData[indexPath.row].timer!)

            return questContentCell

        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "QuestContentCell", for: indexPath)

            guard let questContentCell = cell as? QuestContentCell else {
                return cell
            }

            let missionsData = specialMissionsData[indexPath.row]

            questContentCell.numberLabel.text = "\(indexPath.row + 1)."
            questContentCell.titleLabel.text = missionsData.name

            let bonusText = "\(missionsData.status.progress)獲得經驗值\(missionsData.experience)分"
            questContentCell.bonusLabel.text = bonusText

            let successImage = missionsData.status.completed == "Y" ? UIImage(named: "注册通过"): UIImage(named: "注册未通过")
            questContentCell.successImageView.image = successImage

            let starInt = Int(missionsData.star)
            questContentCell.addStar(quantity: starInt!)
            questContentCell.countdownLabel.isHidden = false

            setTimer(for: questContentCell, starTimer: self.specialMissionsData[indexPath.row].timer!)

            return questContentCell

        default:
            return UITableViewCell()
        }
    }

    func setTimer(for cell: QuestContentCell, starTimer: Float) {
        cell.timerDisposable?.dispose()
        cell.timerDisposable =
            Observable<Int>.timer(0, period: 1, scheduler: MainScheduler.instance).map { $0 + 1 }
                .subscribe(onNext: { [weak self] (element) in
                    guard let strongSelf = self else { return }
                    let time = starTimer - Float(element)
                    if time > 0.0 {
                        let timeString = strongSelf.getHoursMinutesSeconds(allSeconds: time)
                        cell.countdownLabel.text = timeString
                    } else if time <= 0.0 {
                        cell.countdownLabel.text = "00:00:00"
                    }
                })
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segmentedIndex == 0 {
            return 587 / 736 * GlobalConstants.screenHeight
        } else {
            return UITableViewAutomaticDimension
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if segmentedIndex == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerHeaderCell")
            return cell
        }
        return nil
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if segmentedIndex == 3 {
            return 200
        } else {
            return 0.001
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
}

// MARK: API
extension QuestViewController {
    func loadSignData() {
        apiManager.signInToday(signInTodayApiEnum: .loadSignData()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.loadSignInData = try JSONDecoder().decode(BasicModel<LoadSignInData>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }



//        //SVProgressHUD.show()
//        signInTodayApiProvider.rx.request(.loadSignData())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.loadSignInData = try JSONDecoder().decode(BasicModel<LoadSignInData>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func signInToday() {
        SVProgressHUD.show()
        apiManager.signInToday(signInTodayApiEnum: .signInToday()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                let str = String(data: response.data, encoding: String.Encoding.utf8)
                print("返回的数据是：", str ?? "")

                let massage = try JSONDecoder().decode(MassageModel.self, from: response.data)
                strongSelf.alert(title: "簽到", message: massage.msg)

                if massage.status != "failed" {
                    strongSelf.loadSignData()
                }
            } catch let error as NSError {
                print(error)
            }
        }


//        signInTodayApiProvider.rx.request(.signInToday())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")
//
//                        let massage = try JSONDecoder().decode(MassageModel.self, from: response.data)
//                        strongSelf.alert(title: "簽到", message: massage.msg)
//
//                        if massage.status != "failed" {
//                            strongSelf.loadSignData()
//                        }
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func isSignInToday() {
        apiManager.signInToday(signInTodayApiEnum: .isSignInToday()) { response in
            do {
                let str = String(data: response.data, encoding: String.Encoding.utf8)
                print("返回的数据是：", str ?? "")

                let isSignInToday = try JSONDecoder().decode(BasicModel<String>.self, from: response.data)
                print(isSignInToday.data)

            } catch let error as NSError {
                print(error)
            }
        }


//        signInTodayApiProvider.rx.request(.isSignInToday())
//            .subscribe { event in
//                switch event {
//                case let .success(response):
//                    do {
//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")
//
//                        let isSignInToday = try JSONDecoder().decode(BasicModel<String>.self, from: response.data)
//                        print(isSignInToday.data)
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func continueSevenReward(day: Int) {
        apiManager.signInToday(signInTodayApiEnum: .continueSevenReward(day: day)) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let massageModel = try JSONDecoder().decode(MassageModel.self, from: response.data)

                if massageModel.status != "failed" {
                    strongSelf.alert(title: "完成登入獎勵", message: massageModel.msg)
                }
            } catch let error as NSError {
                print(error)
            }
        }


//        signInTodayApiProvider.rx.request(.continueSevenReward(day: day))
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                switch event {
//                case let .success(response):
//                    do {
//                        let massageModel = try JSONDecoder().decode(MassageModel.self, from: response.data)
//
//                        if massageModel.status != "failed" {
//                            strongSelf.alert(title: "完成登入獎勵", message: massageModel.msg)
//                        }
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func getEveryDayMissions() {
        apiManager.misssions(misssionsApiEnum: .getEveryDayMissions()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.everyDayMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }




        //SVProgressHUD.show()
//        misssionsApiProvider.rx.request(.getEveryDayMissions())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.everyDayMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func getLimitMissions() {
        apiManager.misssions(misssionsApiEnum: .getLimitMissions()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.limitMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }



//        //SVProgressHUD.show()
//        misssionsApiProvider.rx.request(.getLimitMissions())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.limitMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func getSpecialMissions() {
        apiManager.misssions(misssionsApiEnum: .getSpecialMissions()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.specialMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()

            } catch let error as NSError {
                print(error)
            }
        }


        //SVProgressHUD.show()
//        misssionsApiProvider.rx.request(.getSpecialMissions())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.specialMissionsData = try JSONDecoder().decode(BasicModel<[MissionsData]>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }
    
}
