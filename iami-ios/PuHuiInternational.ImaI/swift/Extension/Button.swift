//
//  Button.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/6.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

extension UIButton {

    @IBInspectable var fontForWidth: CGFloat {
        get {
            return (self.titleLabel?.font?.pointSize)!
        }
        set {
            setLightTextSize(float: newValue)
        }
    }

    @IBInspectable var boldFontForWidth: CGFloat {
        get {
            return (self.titleLabel?.font?.pointSize)!
        }
        set {
            setBoldTextSize(float: newValue)
        }
    }

    @IBInspectable var gradationStyle: Bool {
        get {
            return false
        }
        set {
            if newValue {
                setGradationStyle()
            }
        }
    }

    func setGradationStyle() {
        self.layer.masksToBounds = true
        self.setTitleColor(.white, for: .normal)
        self.setBackgroundImage(GlobalConstants.purpleImage, for: .normal)
        let gray = GlobalConstants.grayColorD2
        self.setBackgroundImage(UIImage.imageWithColor(color: gray), for: .disabled)
    }

    func setBackgroundColor(color: UIColor, for state: UIControlState) {
        self.setBackgroundImage(image(withColor: color), for: state)
    }

    private func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()

        context?.setFillColor(color.cgColor)
        context?.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }

    func setLightTextSize(float: CGFloat) {
        self.titleLabel?.font = UIFont(name: "SourceHanSansCN-Light", size: float)
        self.titleLabel?.font = self.titleLabel?.font.withSize(CGFloat(float).adaptiveForWidth)
    }

    func setBoldTextSize(float: CGFloat) {
        self.titleLabel?.font = UIFont(name: "SourceHanSansCN-Bold", size: float)
        self.titleLabel?.font = self.titleLabel?.font.withSize(CGFloat(float).adaptiveForWidth)
    }

}
