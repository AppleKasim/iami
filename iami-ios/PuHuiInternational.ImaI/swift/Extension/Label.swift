//
//  Label.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/6.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

extension UILabel {
    @IBInspectable var sizeForHeight: CGFloat {
        get {
            return (font?.pointSize)!
        }
        set {
            setFontLightForHeight(float: newValue)
        }
    }

    @IBInspectable var sizeForWidth: CGFloat {
        get {
            return (font?.pointSize)!
        }
        set {
            setFontLightForWidth(float: newValue)
        }
    }

    @IBInspectable var regularForHeight: CGFloat {
        get {
            return (font?.pointSize)!
        }
        set {
            setFontRegularForHeight(float: newValue)
        }
    }

    @IBInspectable var regularForWidth: CGFloat {
        get {
            return (font?.pointSize)!
        }
        set {
            setFontRegularForWidth(float: newValue)
        }
    }

    func setFontLightForHeight(float: CGFloat) {
        self.font = UIFont(name: "SourceHanSansCN-Light", size: CGFloat(float).adaptiveForHeight)
    }

    func setFontLightForWidth(float: CGFloat) {
        self.font = UIFont(name: "SourceHanSansCN-Light", size: CGFloat(float).adaptiveForWidth)
    }

    func setFontRegularForHeight(float: CGFloat) {
        self.font = UIFont(name: "SourceHanSansCN-Regular", size: CGFloat(float).adaptiveForHeight)
    }

    func setFontRegularForWidth(float: CGFloat) {
        self.font = UIFont(name: "SourceHanSansCN-Regular", size: CGFloat(float).adaptiveForWidth)
    }
}
