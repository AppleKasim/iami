//
//  ProgressView.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

extension UIProgressView {
    func setProgress(dividend: Float, divisor: Float) {
        if dividend == 0 {
            self.progress = 0
        } else {
            let progress = dividend / divisor
            self.progress = progress > 1 ? 1: progress
        }
    }
}
