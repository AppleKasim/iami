//
//  Float.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/6.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

extension CGFloat {
    var adaptiveForWidth: CGFloat {
        return (self / 375) * UIScreen.main.bounds.width    //375的值，主要是依照目前製作使用iphon6 plus去做比例的放大縮小，使用該解析度的寬度．
    }

    var adaptiveForHeight: CGFloat {
        return (self / 667) * UIScreen.main.bounds.height    //667的值，主要是依照目前製作使用iphon6 plus去做比例的放大縮小，使用該解析度的寬度．
    }
}
