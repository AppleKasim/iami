//
//  ImageView.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

extension UIImageView {
    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
}
