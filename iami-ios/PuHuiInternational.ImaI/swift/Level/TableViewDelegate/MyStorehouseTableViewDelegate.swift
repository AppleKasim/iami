//
//  MyStorehouseTableViewDelegate.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class MyStorehouseTableViewDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {

    var itemSelectAction: (String, String) -> Void = {_, _ in }
    var warehouseDataArray: [WarehouseData] = []

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyStorehouseTableViewCell", for: indexPath)

        cell.frame = tableView.bounds
        cell.layoutIfNeeded()
        guard let myStorehouseCell = cell as? MyStorehouseTableViewCell else {
            return cell
        }

        myStorehouseCell.itemSelectAction = itemSelectAction
        myStorehouseCell.warehouseDataArray = warehouseDataArray

        return myStorehouseCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let collectionCellHeight = (GlobalConstants.screenWidth * 0.95) / 3
        let collectionCellInsets = (GlobalConstants.screenWidth - (GlobalConstants.screenWidth * 0.95)) / 2
        return CGFloat(ceil(Double(warehouseDataArray.count) / 3)) * collectionCellHeight + collectionCellInsets * 2
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01  //tableView style使用Grouped，上方會出先一段空白．給0.01或更小值，可以消除．設定０不會有效果．
    }

}
