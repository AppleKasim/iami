//
//  RobberyTableViewDelegate.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class RobberyTableViewDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {
    let disposeBag = DisposeBag()
    var plunderBoxDataArray: [PlunderBoxData] = []
    weak var tableView: UITableView?

    let apiManager = ApiManager()
    var alertAction: (String, String) -> Void = {_, _ in }

    override init() {
        super.init()
        getPlunder()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plunderBoxDataArray.count
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskPointsBoxCell", for: indexPath)
        guard let taskPointsBoxCell = cell as? TaskPointsBoxCell else {
            return cell
        }

        let remain = plunderBoxDataArray[indexPath.row].remainExperience
        let plunder = plunderBoxDataArray[indexPath.row].plunderExperience

        taskPointsBoxCell.remainLabel.text = "可領取:" + remain
        taskPointsBoxCell.plunderLabel.text = "已被掠奪:" + plunder

        taskPointsBoxCell.progressView.setProgress(dividend: Float(remain)!, divisor: Float(plunder)!)

        taskPointsBoxCell.getLotteryReward = {
            self.receiveExperienceBox(boxId: self.plunderBoxDataArray[indexPath.row].experienceBoxId)
        }

        taskPointsBoxCell.timerDisposable?.dispose()
        taskPointsBoxCell.timerDisposable =
            Observable<Int>.timer(0, period: 1, scheduler: MainScheduler.instance).map { $0 + 1 }
                .subscribe(onNext: { [weak self] (element) in
                    guard let strongSelf = self else { return }
                    let time = strongSelf.plunderBoxDataArray[indexPath.row].timer - Float(element)
                    if time > 0.0 {
                        taskPointsBoxCell.receiveButton.isEnabled = false
                        let buttonString = strongSelf.getHoursMinutesSeconds(allSeconds: time)
                        taskPointsBoxCell.receiveButton.setTitle(buttonString, for: .disabled)
                    } else {
                        taskPointsBoxCell.receiveButton.isEnabled = true
                        taskPointsBoxCell.timerDisposable?.dispose()
                    }
                })

        return taskPointsBoxCell
    }

    func getHoursMinutesSeconds(allSeconds: Float) -> String {
        let allTime: Int = Int(allSeconds)
        var minutes = 0
        var seconds = 0
        var minutesText = ""
        var secondsText = ""

        minutes = allTime % 3600 / 60
        minutesText = minutes > 9 ? "\(minutes)" : "0\(minutes)"

        seconds = allTime % 3600 % 60
        secondsText = seconds > 9 ? "\(seconds)" : "0\(seconds)"

        return "\(minutesText):\(secondsText)"
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPointsBoxCell")
        guard let myPointsBoxCell = cell as? MyPointsBoxCell else {
            return cell
        }

        let allRemainExperience = plunderBoxDataArray.map { Float($0.remainExperience)! }.reduce(0, +)
        let allPlunderExperience = plunderBoxDataArray.map { Float($0.plunderExperience)! }.reduce(0, +)

        myPointsBoxCell.remainLabel.text = "可領取積分:" + String(allRemainExperience)
        myPointsBoxCell.plunderLabel.text = "已被掠奪積分:" + String(allPlunderExperience)

        myPointsBoxCell.progressView.setProgress(dividend: allRemainExperience, divisor: allPlunderExperience)

        return myPointsBoxCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 / 736 * GlobalConstants.screenHeight
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100 / 736 * GlobalConstants.screenHeight
    }
}

// MARK: API
extension RobberyTableViewDelegate {
    func getPlunder() {
        apiManager.treasureBoxes(treasureBoxesApiEnum: .getPlunder()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let str = String(data: response.data, encoding: String.Encoding.utf8)
                print("返回的数据是：", str ?? "")

                strongSelf.plunderBoxDataArray = try JSONDecoder().decode(BasicModel<PlunderBoxModel>.self, from: response.data).data.boxes

                strongSelf.tableView?.reloadData()

            } catch let error as NSError {
                print(error)
            }
        }

//        treasureBoxesApi.rx.request(.getPlunder())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                switch event {
//                case let .success(response):
//                    do {
//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")
//
//                        strongSelf.plunderBoxDataArray = try JSONDecoder().decode(BasicModel<PlunderBoxModel>.self, from: response.data).data.boxes
//
//                        strongSelf.tableView?.reloadData()
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func receiveExperienceBox(boxId: String) {
        apiManager.treasureBoxes(treasureBoxesApiEnum: .receiveExperienceBox(boxId)) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                //                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                //                        print("返回的数据是：", str ?? "")

                let message = try JSONDecoder().decode(BasicMessageModel.self, from: response.data).message
                strongSelf.alertAction("領取", message)

                strongSelf.getPlunder()
            } catch let error as NSError {
                print(error)
            }
        }

//        treasureBoxesApi.rx.request(.receiveExperienceBox(boxId))
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                switch event {
//                case let .success(response):
//                    do {
////                        let str = String(data: response.data, encoding: String.Encoding.utf8)
////                        print("返回的数据是：", str ?? "")
//
//                        let message = try JSONDecoder().decode(BasicMessageModel.self, from: response.data).message
//                        strongSelf.alertAction("領取", message)
//
//                        strongSelf.getPlunder()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }
}
