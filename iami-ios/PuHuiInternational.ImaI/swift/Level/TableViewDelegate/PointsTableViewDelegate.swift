//
//  PointsTableViewDelegate.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import SDWebImage

class PointsTableViewDelegate: NSObject, UITableViewDelegate, UITableViewDataSource {

    let upLevelData = ["經驗值累計達到400(200/400)", "好友數達30人以上(30/30)", "10則以上貼文"]
    let nextLevelData = ["四級專屬相框", "開啟下載免費貼圖功能", "抽獎碎片x1", "獲得20經驗值"]
    var experienceData = ExperienceData()

    var iCurrencyQuantity = 0
    var fragmentQuantity = 0
    var currencyQuantity = 0
    var levelMissionsStatusArray: [LevelMissionsStatusData] = []
    var levelRewardsArray: [LevelRewards] = []

    var upLevelAction: () -> Void = {}

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return levelMissionsStatusArray.count
        case 2:
            return levelRewardsArray.count
        default:
            return 0
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LevelItemCell", for: indexPath)

            guard let levelItemCell = cell as? LevelItemCell else {
                return cell
            }
            levelItemCell.currencyLabel.text = String(currencyQuantity)
            levelItemCell.fragmentLabel.text = String(fragmentQuantity)
            levelItemCell.iCurrencyLabel.text = String(iCurrencyQuantity)
            levelItemCell.iCurrencyView.isHidden = true
            levelItemCell.mallButton.isEnabled = false

            return levelItemCell

        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTextCell", for: indexPath)

            guard let textCell = cell as? LevelContentTextCell else {
                return cell
            }
            textCell.contentLabel.text = levelMissionsStatusArray[indexPath.row].name
            let completed = levelMissionsStatusArray[indexPath.row].status.completed == "Y" ? true: false
            textCell.perfectionImageView.isHidden = !completed
            return textCell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTextCell", for: indexPath)

            guard let textCell = cell as? LevelContentTextCell else {
                return cell
            }
            textCell.contentLabel.text = levelRewardsArray[indexPath.row].name
            textCell.perfectionImageView.isHidden = true
            return textCell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ProfilePictureHeader")
            guard let profilePictureCell = cell as? LevelProfilePictureHeaderView else {
                return cell
            }

            let userDefault = UserDefaults.standard
            let name = userDefault.string(forKey: GlobalConstants.DefaultsKey.userKey)
            profilePictureCell.nameLabel.text = name
            profilePictureCell.levelLabel.text = "Lv." + experienceData.currentLevelInfo.level
            profilePictureCell.scaleLabel.text = experienceData.experience + "/" + experienceData.nextLevelInfo.experience

            if let experience = Float(experienceData.experience),
                let nextExperience = Float(experienceData.nextLevelInfo.experience) {
                let progress = experience / nextExperience
                profilePictureCell.progressView.progress = progress > 1 ? 1: progress
                profilePictureCell.upButton.isEnabled = progress > 1
            }

            profilePictureCell.upLevelAction = upLevelAction

            return profilePictureCell
        case 1, 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "LevelTitleCell")
            guard let titleCell = cell as? LevelTitleCell else {
                return cell
            }
            if section == 1 {
                titleCell.titleLabel.text = "升級條件:"
            } else if section == 2 {
                titleCell.titleLabel.text = "下次升級獎勵:"
            }
            return titleCell
        default:
            return UIView()
        }

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 111 / 736 * GlobalConstants.screenHeight
        default:
            return 22 / 736 * GlobalConstants.screenHeight
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 255 / 736 * GlobalConstants.screenHeight
        default:
            return 44 / 736 * GlobalConstants.screenHeight
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }

}
