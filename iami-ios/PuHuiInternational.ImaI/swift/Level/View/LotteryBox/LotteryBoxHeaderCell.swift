//
//  LotteryBoxHeaderCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LotteryBoxHeaderCell: UITableViewCell {

    @IBOutlet weak var bottomBorderView: UIView! {
        didSet {
            bottomBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var centerBorderView: UIView! {
        didSet {
            centerBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var fragmentLabel: UILabel! {
        didSet {
            fragmentLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var iCurrencyLabel: UILabel! {
        didSet {
            iCurrencyLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var iCurrencyView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
