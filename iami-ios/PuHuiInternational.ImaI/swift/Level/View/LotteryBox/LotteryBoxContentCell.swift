//
//  LotteryBoxContentCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LotteryBoxContentCell: UITableViewCell {
    @IBOutlet weak var boxImageView: UIImageView!

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = GlobalConstants.grayColor31
        }
    }

    var fragmentButtonAction: () -> Void = {}
    @IBOutlet weak var fragmentButton: UIButton!
    @IBOutlet weak var fragmentDistinctlyLabel: UILabel! {
        didSet {
            fragmentDistinctlyLabel.textColor = GlobalConstants.grayColor31
        }
    }

    var iPointsButtonAction: () -> Void = {}
    @IBOutlet weak var iPointsButton: UIButton!
    @IBOutlet weak var iPointsDistinctlyLabel: UILabel! {
        didSet {
            iPointsDistinctlyLabel.textColor = GlobalConstants.grayColor31
        }
    }
    @IBOutlet weak var iPointsStackView: UIStackView!

    override func layoutSubviews() {
        super.layoutSubviews()

        fragmentButton.layer.cornerRadius = fragmentButton.frame.size.height / 2.0
        iPointsButton.layer.cornerRadius = iPointsButton.frame.size.height / 2.0
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func fragmentToSmallMary(_ sender: UIButton) {
        fragmentButtonAction()
    }

    @IBAction func iPointsToSmallMary(_ sender: UIButton) {
        iPointsButtonAction()
    }
}
