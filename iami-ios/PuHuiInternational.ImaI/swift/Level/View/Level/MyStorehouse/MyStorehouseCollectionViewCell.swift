//
//  MyStorehouseCollectionViewCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class MyStorehouseCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.layer.borderWidth = 3
            backView.layer.borderColor = GlobalConstants.purpleColor.cgColor
        }
    }
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel! {
        didSet {
            quantityLabel.textColor = .white
            quantityLabel.backgroundColor = GlobalConstants.purpleColor
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }
}
