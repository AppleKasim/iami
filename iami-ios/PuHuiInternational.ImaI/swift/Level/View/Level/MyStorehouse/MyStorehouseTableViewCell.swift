//
//  MyStorehouseTableViewCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class MyStorehouseTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!

    var itemSelectAction: (String, String) -> Void = {_, _ in }

    var warehouseDataArray: [WarehouseData] = [] {
        didSet {
            collectionView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let nib = UINib(nibName: "MyStorehouseCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "MyStorehouseCollectionViewCell")

        guard let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }

        let width = (GlobalConstants.screenWidth * 0.95) / 3 - 5

        layout.itemSize = CGSize(width: width, height: width)

        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5

        let insets = (GlobalConstants.screenWidth - (GlobalConstants.screenWidth * 0.95)) / 2

        layout.sectionInset = UIEdgeInsets(top: insets, left: insets, bottom: insets, right: insets / 2)

        self.collectionView.collectionViewLayout.invalidateLayout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension MyStorehouseTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return warehouseDataArray.count
    }

// swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyStorehouseCollectionViewCell", for: indexPath)

        guard let myStorehouseCell = cell as? MyStorehouseCollectionViewCell else {
            return cell
        }

        if let imageString = warehouseDataArray[indexPath.row].image {
            let imageUrl = URL(string: GlobalConstants.baseUrl + imageString)
            myStorehouseCell.imageView.sd_setImage(with: imageUrl, completed: nil)
        } else {
            myStorehouseCell.imageView.image = #imageLiteral(resourceName: "占位图")
        }

        myStorehouseCell.nameLabel.text = warehouseDataArray[indexPath.row].name

        let quantity = Int(warehouseDataArray[indexPath.row].treasureCount)! > 99 ? "99+": warehouseDataArray[indexPath.row].treasureCount
        myStorehouseCell.quantityLabel.text = quantity

        myStorehouseCell.layoutIfNeeded()  //處理quantityLabel圓角，沒有正常顯示的問題
        myStorehouseCell.quantityLabel.layer.cornerRadius = myStorehouseCell.quantityLabel.frame.size.height / 2.0
        myStorehouseCell.quantityLabel.layer.masksToBounds = true

        return myStorehouseCell
    }
// swiftlint:enable line_length

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemSelectAction(warehouseDataArray[indexPath.row].name, "123")
    }

}
