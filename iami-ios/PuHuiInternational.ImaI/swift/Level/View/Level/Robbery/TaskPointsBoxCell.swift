//
//  TaskPointsBoxCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TaskPointsBoxCell: UITableViewCell {
    @IBOutlet weak var boxImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var receiveButton: UIButton!

    @IBOutlet weak var progressView: UIProgressView! {
        didSet {
            progressView.layer.masksToBounds = true
            progressView.subviews[1].clipsToBounds = true
            progressView.tintColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var remainLabel: UILabel!
    @IBOutlet weak var plunderLabel: UILabel!

    var getLotteryReward: () -> Void = {}
    let disposeBag = DisposeBag()
    var timerDisposable: Disposable?

    override func layoutSubviews() {
        super.layoutSubviews()

        progressView.layer.cornerRadius = progressView.frame.size.height / 2.0
        progressView.layer.sublayers![1].cornerRadius = progressView.frame.size.height / 2.0

        receiveButton.layer.cornerRadius = receiveButton.frame.size.height / 2.0
    }

    @IBAction func getReward(_ sender: UIButton) {
        getLotteryReward()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
