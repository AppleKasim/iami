//
//  MyPointsBoxCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class MyPointsBoxCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var progressView: UIProgressView! {
        didSet {
            progressView.layer.cornerRadius = (30 / 100 * GlobalConstants.screenHeight) / 15.0
            progressView.layer.masksToBounds = true
            progressView.layer.sublayers![1].cornerRadius = (30 / 100 * GlobalConstants.screenHeight) / 15.0
            progressView.subviews[1].clipsToBounds = true
            progressView.tintColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var remainLabel: UILabel!
    @IBOutlet weak var plunderLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
