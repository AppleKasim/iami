//
//  LevelItemCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LevelItemCell: UITableViewCell {

    @IBOutlet weak var topBorderView: UIView! {
        didSet {
            topBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var bottomBorderView: UIView! {
        didSet {
            bottomBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var leftBorderView: UIView! {
        didSet {
            leftBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var rightBorderView: UIView! {
        didSet {
            rightBorderView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var currencyLabel: UILabel! {
        didSet {
            currencyLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var mallButton: UIButton!

    @IBOutlet weak var fragmentLabel: UILabel! {
        didSet {
            fragmentLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var lotteryButton: UIButton!

    @IBOutlet weak var iCurrencyView: UIView! {
        didSet {
            //iCurrencyView.isHidden = true
        }
    }

    @IBOutlet weak var iCurrencyLabel: UILabel! {
        didSet {
            iCurrencyLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var ipsButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func layoutSubviews() {
        super.layoutSubviews()
        lotteryButton.layer.cornerRadius = lotteryButton.frame.size.height / 2.0
        mallButton.layer.cornerRadius = mallButton.frame.size.height / 2.0
        ipsButton.layer.cornerRadius = ipsButton.frame.size.height / 2.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
