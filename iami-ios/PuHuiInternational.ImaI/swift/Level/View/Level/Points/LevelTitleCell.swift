//
//  LevelTitleCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LevelTitleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.purpleColor
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
