//
//  LevelContentTextCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LevelContentTextCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = GlobalConstants.grayColor31
        }
    }
    
    @IBOutlet weak var perfectionImageView: UIImageView! {
        didSet {
            perfectionImageView.isHidden = true
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
