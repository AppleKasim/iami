//
//  LevelProfilePictureHeaderView.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class LevelProfilePictureHeaderView: UITableViewHeaderFooterView {

    var upLevelAction: () -> Void = {}

    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            let userDefault = UserDefaults.standard
            let userImageUrl = userDefault.string(forKey: GlobalConstants.DefaultsKey.userImage)
            let imageUrl = URL(string: userImageUrl!)
            profileImageView.sd_setImage(with: imageUrl, completed: nil)

            profileImageView.layer.cornerRadius = (140 / 255 * GlobalConstants.screenHeight) / 5.5
            profileImageView.layer.masksToBounds = true
            profileImageView.contentMode = .scaleAspectFit
        }
    }

    @IBOutlet weak var progressView: UIProgressView! {
        didSet {
            progressView.layer.cornerRadius = (30 / 255 * GlobalConstants.screenHeight) / 5.5
            progressView.layer.masksToBounds = true
            progressView.layer.sublayers![1].cornerRadius = (30 / 255 * GlobalConstants.screenHeight) / 5.5
            progressView.subviews[1].clipsToBounds = true
            progressView.tintColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var levelLabel: UILabel! {
        didSet {
            levelLabel.backgroundColor = GlobalConstants.purpleColor
            levelLabel.layer.cornerRadius = (16 / 255 * GlobalConstants.screenHeight) / 5.5
            levelLabel.layer.masksToBounds = true
            levelLabel.textColor = .white
        }
    }

    @IBOutlet weak var upButton: UIButton! {
        didSet {
            upButton.layer.cornerRadius = (30 / 255 * GlobalConstants.screenHeight) / 5.5
        }
    }

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var scaleLabel: UILabel! {
        didSet {
            scaleLabel.textColor = .white
        }
    }

    @IBAction func upLevel(_ sender: UIButton) {
        upLevelAction()
    }

}
