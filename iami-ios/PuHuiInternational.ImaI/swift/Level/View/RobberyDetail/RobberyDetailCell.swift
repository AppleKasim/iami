//
//  RobberyDetailCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift

class RobberyDetailCell: UITableViewCell {

    var timerDisposable: Disposable?

    @IBOutlet weak var dateLabel: UILabel! {
        didSet {
            dateLabel.textColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var reciprocalLabel: UILabel! {
        didSet {
            reciprocalLabel.textColor = GlobalConstants.grayColorE5
        }
    }

    var robberyAction: () -> Void = {}
    @IBOutlet weak var robberyButton: UIButton!

    @IBOutlet weak var levelLabel: UILabel! {
        didSet {
            levelLabel.backgroundColor = GlobalConstants.purpleColor
            levelLabel.layer.masksToBounds = true
            levelLabel.textColor = .white
        }
    }

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var contentLabel: UILabel! {
        didSet {
            contentLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var userImageView: UIImageView!

    override func layoutSubviews() {
        super.layoutSubviews()
        levelLabel.layer.cornerRadius = levelLabel.frame.size.height / 2.0
        robberyButton.layer.cornerRadius = robberyButton.frame.size.height / 2.0
        userImageView.layer.cornerRadius = userImageView.frame.size.height / 2.0
        userImageView.layer.masksToBounds = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func plunder(_ sender: UIButton) {
        robberyButton.isEnabled = false
        robberyAction()
    }

}
