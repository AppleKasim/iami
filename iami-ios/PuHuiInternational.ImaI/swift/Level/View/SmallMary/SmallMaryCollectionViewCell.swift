//
//  SmallMaryCollectionViewCell.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class SmallMaryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = GlobalConstants.grayColor31
        }
    }

    func selectCell() {
        self.contentView.layer.borderWidth = 3.0
        self.contentView.backgroundColor = .white
    }

    func noSelectCell() {
        self.contentView.layer.borderWidth = 0
        self.contentView.backgroundColor = GlobalConstants.grayColorE5
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.borderColor = GlobalConstants.purpleColor.cgColor
        self.contentView.backgroundColor = GlobalConstants.grayColorE5
    }
}
