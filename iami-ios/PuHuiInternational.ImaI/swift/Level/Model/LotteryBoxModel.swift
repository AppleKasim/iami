//
//  LotteryBoxModel.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation

struct LotteryBoxesData: Codable {
    var tatter: Int
    var level: String = ""
    var box5Level: [LotteryBoxesDetailData]
    var box10Level: [LotteryBoxesDetailData]
    var box15Level: [LotteryBoxesDetailData]

    enum CodingKeys: String, CodingKey {
        case tatter
        case level
        case box5Level = "tbox_1"
        case box10Level = "tbox_2"
        case box15Level = "tbox_3"
    }
}

struct LotteryBoxesDetailData: Codable {
    var id: String = ""
    var treasureBoxId: String = ""
    var name: String = ""
    var type: String = ""
    var value: String = ""
    var probability: String = ""
    var image: String?
    var rewardModule: String?

    enum CodingKeys: String, CodingKey {
        case id
        case treasureBoxId = "treasure_box_id"
        case name
        case type
        case value
        case probability = "PR"
        case image
        case rewardModule = "reward_module"
    }
}

struct LotteryWinningItemData: Codable {
    var treasureIndex: Int = 0
    var treasureId: String = ""
    var treasureImage: String = ""
    var treasureName: String = ""

    enum CodingKeys: String, CodingKey {
        case treasureIndex = "treasure_index"
        case treasureId = "treasure_id"
        case treasureImage = "treasure_image"
        case treasureName = "treasure_name"
    }
}
