//
//  LevelModel.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation

struct LevelInfo: Codable {
    var id: String = ""
    var name: String = ""
    var level: String = ""
    var experience: String = ""
    var missions: String = ""
    var levelupReward: String = ""

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case level
        case experience = "exp_value"
        case missions
        case levelupReward = "levelup_reward"
    }
}

struct ExperienceData: Codable {
    var currentLevelInfo: LevelInfo = LevelInfo()
    var nextLevelInfo: LevelInfo = LevelInfo()
    var experience: String = ""
}

struct LevelData: Codable {
    var memberId: String = ""
    var path: String = ""
    var fileName: String = ""
    var fileType: String = ""
    var width: String = ""
    var height: String = ""
    var size: String = ""
    var createTime: String = ""

    enum CodingKeys: String, CodingKey {
        case memberId = "id"
        case path
        case fileName  = "filename"
        case fileType
        case width
        case height
        case size
        case createTime = "create_time"
    }
}

// MARK: Level
struct StatusData: Codable {
    var completed: String = ""
    var progress: String = ""
}

struct LevelMissionsStatusData: Codable {
    var id: String = ""
    var name: String = ""
    var type: String = ""
    var experience: String = ""
    var times: String = ""
    var star: String = ""
    var startTime: String = ""
    var endTime: String = ""
    var status: StatusData
    var checkModule: String = ""

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case experience = "exp_value"
        case times
        case star
        case startTime = "start_time"
        case endTime = "end_time"
        case status
        case checkModule = "check_module"
    }
}

struct LevelStatusDataArray: Codable {
    var array: [LevelMissionsStatusData]
}

struct LevelRewards: Codable {
    var id: String
    var name: String
    var type: String
    var times: String
    var valueSet: String
    var status: String
    var rewardModule: String

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case type
        case times
        case valueSet = "value_set"
        case status
        case rewardModule = "reward_module"
    }
}

struct WarehouseData: Codable {
    var id: String
    var memberId: String
    var treasureId: String
    var createTime: String
    var treasureBoxId: String
    var name: String
    var type: String
    var value: String
    var probability: String
    var image: String?
    var rewardModule: String?
    var treasureCount: String

    enum CodingKeys: String, CodingKey {
        case id
        case memberId = "member_id"
        case treasureId = "treasure_id"
        case createTime = "create_time"
        case treasureBoxId = "treasure_box_id"
        case name
        case type
        case value
        case probability = "PR"
        case image
        case rewardModule = "reward_module"
        case treasureCount = "treasure_count"
    }
}
