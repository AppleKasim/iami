//
//  RobberyModel.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation

struct PlunderBoxModel: Codable {
    var status: String = ""
    var boxes: [PlunderBoxData] = []
}

struct AlreadyPlunderData: Codable {
    var experienceBoxId: String = ""
    var lockExperience: String = ""
    var unlockExperience: String = ""
}

struct PlunderBoxData: Codable {
    var experienceBoxId: String = ""
    var lockExperience: String = ""
    var unlockExperience: String = ""
    var expiredTime: String = ""
    var currentDatetime: String = ""
    var isReceive: String = ""
    var plunderExperience: String = ""
    var remainExperience: String = ""
    var timer: Float

    enum CodingKeys: String, CodingKey {
        case experienceBoxId = "experience_box_id"
        case lockExperience = "lock_exp"
        case unlockExperience  = "unlock_exp"
        case expiredTime = "expired_time"
        case currentDatetime = "current_datetime"
        case isReceive = "is_receive"
        case plunderExperience = "plunder_experience"
        case remainExperience = "remain_experience"
        case timer
    }
}

struct PlunderDate: Codable {
    var memberId: String = ""
    var email: String = ""
    var password: String = ""
    var nickname: String = ""
    var gender: String = ""
    var mobile: String = ""
    var birth: String = ""
    var level: String = ""
    var money: String = ""
    var resume: String = ""
    var city: String = ""
    var countryId: String = ""
    var relationship: String?
    var avatar: String = ""
    var banner: String = ""
    var languageId: String = ""
    var lastLoginTime: String = ""
    var modifyTime: String = ""
    var activeAuth: String = ""
    var resetPasswordAuth: String?
    var isOnline: String = ""
    var status: String = ""
    var streamer: String = ""
    var accountId: String?
    var createTime: String = ""
    var lastNicknameTime: String = ""
    var infoShow: String = ""
    var releasePage: String = ""
    var experienceBoxId: String?
    var plunderExperience: Int?
    var unlockExperience: String?
    var expiredTime: String?
    var experienceValue: String?
    var plunderTime: String?
    var canBePlunder: String?
    var timer: Float?

    enum CodingKeys: String, CodingKey {
        case memberId = "member_id"
        case email
        case password
        case nickname
        case gender
        case mobile
        case birth
        case level
        case money
        case resume
        case city
        case countryId = "country_id"
        case relationship
        case avatar
        case banner
        case languageId = "language_id"
        case lastLoginTime = "last_login_time"
        case modifyTime = "modify_time"
        case activeAuth = "active_auth"
        case resetPasswordAuth = "reset_password_auth"
        case isOnline
        case status
        case streamer
        case accountId = "account_id"
        case createTime = "create_time"
        case lastNicknameTime = "last_nickname_time"
        case infoShow = "info_show"
        case releasePage = "release_page"
        case experienceBoxId = "experience_box_id"
        case plunderExperience = "plunder_experience"
        case unlockExperience = "unlock_exp"
        case expiredTime = "expired_time"
        case experienceValue = "exp_value"
        case plunderTime = "plunder_time"
        case canBePlunder = "can_be_plunder"
        case timer
    }
}
