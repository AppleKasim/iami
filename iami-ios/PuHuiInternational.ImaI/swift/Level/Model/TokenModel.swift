//
//  TokenModel.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/23.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation

struct CsrfTokenModel: Codable {
    var token: String = ""
    var name: String = ""
}

struct AccessTokenModel: Codable {
    var memberId: String = ""
    var email: String = ""
    var password: String = ""
    var nickname: String = ""
    var gender: String = ""
    var mobile: String = ""
    var birth: String = ""
    var level: String = ""
    var money: String = ""
    var resume: String = ""
    var city: String = ""
    var countryId: String = ""
    var countryName: String = ""
    var relationship: String?
    var avatar: String = ""
    var banner: String = ""
    var languageId: String = ""
    var languageName: String = ""
    var lastLoginTime: String = ""
    var lastNicknameTime: String?
    var modifyTime: String = ""
    var activeAuth: String = ""
    var resetPasswordAuth: String?
    var status: String = ""
    var infoShow: String = ""
    var createTime: String = ""
    var isOnline: String = ""
    var tokenExpired: String = ""
    var token: String = ""

    enum CodingKeys: String, CodingKey {
        case memberId = "member_id"
        case email
        case password
        case nickname
        case gender
        case mobile
        case birth
        case level
        case money
        case resume
        case city
        case countryId = "country_id"
        case countryName = "country_name"
        case relationship
        case avatar
        case banner
        case languageId = "language_id"
        case languageName = "language_name"
        case lastLoginTime = "last_login_time"
        case lastNicknameTime = "last_nickname_time"
        case modifyTime = "modify_time"
        case activeAuth = "active_auth"
        case resetPasswordAuth = "reset_password_auth"
        case status
        case infoShow = "info_show"
        case createTime = "create_time"
        case isOnline
        case tokenExpired = "token_expired"
        case token
    }
}
