//
//  LotteryBoxViewController.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SVProgressHUD

class LotteryBoxViewController: UIViewController, TaskAlertForSocket {
    @IBOutlet weak var tableView: UITableView!

    var disposeBag = DisposeBag()
    var lotteryBoxesData: LotteryBoxesData? {
        didSet {
            box5ContentString = (lotteryBoxesData?.box5Level.map {$0.name}.joined(separator: ", "))!
            box10ContentString = (lotteryBoxesData?.box10Level.map {$0.name}.joined(separator: ", "))!
            box15ContentString = (lotteryBoxesData?.box15Level.map {$0.name}.joined(separator: ", "))!
        }
    }
    var box5ContentString = ""
    var box10ContentString = ""
    var box15ContentString = ""

    var fragmentQuantity = 0
    let apiManager = ApiManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension

        SVProgressHUD.show()
        getTreasureBoxes()
        newMessageNotification()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        getTatter()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let smallMaryViewController = segue.destination as? SmallMaryViewController else {
            return
        }
        switch (sender as? IndexPath)?.row {
        case 0:
            smallMaryViewController.lotteryBoxesDetailArray = lotteryBoxesData?.box5Level
            smallMaryViewController.boxLevel = "5"
        case 1:
            smallMaryViewController.lotteryBoxesDetailArray = lotteryBoxesData?.box10Level
            smallMaryViewController.boxLevel = "10"
        case 2:
            smallMaryViewController.lotteryBoxesDetailArray = lotteryBoxesData?.box15Level
            smallMaryViewController.boxLevel = "15"
        default:
            break
        }
    }
}

extension LotteryBoxViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LotteryBoxContentCell", for: indexPath)

        guard let lotteryBoxContentCell = cell as? LotteryBoxContentCell else {
            return cell
        }

        if lotteryBoxesData != nil {
            let level = Int((lotteryBoxesData?.level)!)!
            switch indexPath.row {
            case 0:
                lotteryBoxContentCell.titleLabel.text = "5級寶箱內容物"
                lotteryBoxContentCell.contentLabel.text = box5ContentString
                lotteryBoxContentCell.boxImageView?.image = #imageLiteral(resourceName: "Level5Box")
                lotteryBoxContentCell.fragmentDistinctlyLabel.text = "消耗4碎片"

                if level < 5 {
                    lotteryBoxContentCell.fragmentButton.setTitle("等級不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }

                if fragmentQuantity < 4 {
                    lotteryBoxContentCell.fragmentButton.setTitle("碎片不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }
            case 1:
                lotteryBoxContentCell.titleLabel.text = "10級寶箱內容物"
                lotteryBoxContentCell.contentLabel.text = box10ContentString
                lotteryBoxContentCell.boxImageView?.image = #imageLiteral(resourceName: "Level10Box")
                lotteryBoxContentCell.fragmentDistinctlyLabel.text = "消耗6碎片"

                if level < 10 {
                    lotteryBoxContentCell.fragmentButton.setTitle("等級不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }

                if fragmentQuantity < 6 {
                    lotteryBoxContentCell.fragmentButton.setTitle("碎片不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }
            case 2:
                lotteryBoxContentCell.titleLabel.text = "15級寶箱內容物"
                lotteryBoxContentCell.contentLabel.text = box15ContentString
                lotteryBoxContentCell.boxImageView?.image = #imageLiteral(resourceName: "Level15Box")
                lotteryBoxContentCell.fragmentDistinctlyLabel.text = "消耗10碎片"

                if level < 15 {
                    lotteryBoxContentCell.fragmentButton.setTitle("等級不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }

                if fragmentQuantity < 10 {
                    lotteryBoxContentCell.fragmentButton.setTitle("碎片不足", for: .disabled)
                    lotteryBoxContentCell.fragmentButton.isEnabled = false
                }
            default:
                break
            }
        }

        lotteryBoxContentCell.iPointsStackView.isHidden = true

        lotteryBoxContentCell.fragmentButtonAction = {
            self.performSegue(withIdentifier: "FragmentToSmallMarySegue", sender: indexPath)
        }

        lotteryBoxContentCell.iPointsButtonAction = {
            self.performSegue(withIdentifier: "IPointsToSmallMarySegue", sender: indexPath)
        }

        return lotteryBoxContentCell
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LotteryBoxHeaderCell")
        guard let lotteryBoxHeaderCell = cell as? LotteryBoxHeaderCell else {
            return cell
        }

        lotteryBoxHeaderCell.fragmentLabel.text = String(fragmentQuantity)
        lotteryBoxHeaderCell.iCurrencyView.isHidden = true

        return lotteryBoxHeaderCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 74 / 736 * GlobalConstants.screenHeight
    }

}

// MARK: API
extension LotteryBoxViewController {
    func getTreasureBoxes() {
        //SVProgressHUD.show()
        apiManager.treasureBoxes(treasureBoxesApiEnum: .getTreasureBoxes()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                //                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                //                        print("返回的数据是：", str ?? "")

                strongSelf.lotteryBoxesData = try JSONDecoder().decode(BasicModel<LotteryBoxesData>.self, from: response.data).data
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }

//        treasureBoxesApi.rx.request(.getTreasureBoxes())
//            .subscribe { event in
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
////                        let str = String(data: response.data, encoding: String.Encoding.utf8)
////                        print("返回的数据是：", str ?? "")
//
//                        self.lotteryBoxesData = try JSONDecoder().decode(BasicModel<LotteryBoxesData>.self, from: response.data).data
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func getTatter() {
        apiManager.level(levelApiEnum: .getTatter()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                let fragmentQuantity = try JSONDecoder().decode(BasicModel<Int>.self, from: response.data).data
                strongSelf.fragmentQuantity = fragmentQuantity

                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }

//        //SVProgressHUD.show()
//        levelApiProvider.rx.request(.getTatter())
//            .subscribe { event in
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        let fragmentQuantity = try JSONDecoder().decode(BasicModel<Int>.self, from: response.data).data
//                        self.fragmentQuantity = fragmentQuantity
//
//                        self.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }
}
