//
//  LevelViewController.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import MJRefresh

class LevelViewController: UIViewController, TaskAlertForSocket {

    let pointsTableViewDelegate = PointsTableViewDelegate()
    let robberyTableViewDelegate = RobberyTableViewDelegate()
    let myStorehouseTableViewDelegate = MyStorehouseTableViewDelegate()
    let apiManager = ApiManager()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var robberyButtonView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var backUnderlineView: UIView! {
        didSet {
            backUnderlineView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    @IBOutlet weak var underlineView: UIView! {
        didSet {
            underlineView.backgroundColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var underlineLeading: NSLayoutConstraint!

    var disposeBag = DisposeBag()
    var experienceData = ExperienceData()
    let header = MJRefreshStateHeader()

    var segmentedIndex = 0 {
        didSet {
            switch segmentedIndex {
            case 0:
                pointsTableViewDelegate.upLevelAction = {
                    self.levelup()
                }
                self.tableView.delegate = pointsTableViewDelegate
                self.tableView.dataSource = pointsTableViewDelegate
                self.robberyButtonView.isHidden = true
                self.tableView.separatorStyle = .none

                if let level = Int(pointsTableViewDelegate.experienceData.currentLevelInfo.level), level >= 5 {
                    self.segmentedControl.setEnabled(true, forSegmentAt: 1)
                } else {
                    self.segmentedControl.setEnabled(false, forSegmentAt: 1)
                }
            case 1:
                robberyTableViewDelegate.alertAction = alert
                self.tableView.delegate = robberyTableViewDelegate
                self.tableView.dataSource = robberyTableViewDelegate
                self.robberyButtonView.isHidden = false
                self.tableView.separatorStyle = .singleLine
                self.tableView.separatorColor = GlobalConstants.grayColorE5
                robberyTableViewDelegate.tableView = self.tableView
            case 2:
                myStorehouseTableViewDelegate.itemSelectAction = alert
                self.tableView.delegate = myStorehouseTableViewDelegate
                self.tableView.dataSource = myStorehouseTableViewDelegate
                self.robberyButtonView.isHidden = true
                self.tableView.separatorStyle = .none
            default:
                break
            }
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        let nib = UINib(nibName: "LevelProfilePictureTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ProfilePictureCell")

        let profilePictureHeader = UINib.init(nibName: "LevelProfilePictureHeaderView", bundle: Bundle.main)
        tableView.register(profilePictureHeader, forHeaderFooterViewReuseIdentifier: "ProfilePictureHeader")

        let myStorehouseTableViewCell = UINib.init(nibName: "MyStorehouseTableViewCell", bundle: Bundle.main)
        tableView.register(myStorehouseTableViewCell, forCellReuseIdentifier: "MyStorehouseTableViewCell")

        segmentedIndex = 0

        removeBorderForSegmentedControl()

        header.setRefreshingTarget(self, refreshingAction: #selector(getDataForApi))
        header.setTitle("idle", for: .idle)
        header.setTitle("willRefresh", for: .willRefresh)
        header.setTitle("Pulling", for: .pulling)
        header.setTitle("refreshing", for: .refreshing)
        header.lastUpdatedTimeLabel.text = "時間"
        header.lastUpdatedTimeText = { (date) -> String in
            guard let currencyDate = date else {
                return "時間:"
            }
            return "時間:\(String(describing: currencyDate))"
        }

        tableView.mj_header = header

        newMessageNotification()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        getDataForApi()
    }

    @objc func getDataForApi() {
        getExperience()
        getTatter()
        getShield()

        getLevelMissionsStatus()
        getLevelRewards()

        getWarehouse()
        robberyTableViewDelegate.getPlunder()
    }

    @objc func countdown() {
        robberyTableViewDelegate.plunderBoxDataArray = robberyTableViewDelegate.plunderBoxDataArray.map {
            var data = $0
            data.timer -= 1
            return data
        }

        self.tableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BeingPlunderedSegue" {
            guard let detailViewController = segue.destination as? RobberyDetailViewController else {
                return
            }
            detailViewController.mode = .beingPlundered
        } else if segue.identifier == "CanPlunderSegue" {
            guard let detailViewController = segue.destination as? RobberyDetailViewController else {
                return
            }
            detailViewController.mode = .canPlunder
        } else if segue.identifier == "LotteryBoxSegue" {
            guard let lotteryBoxViewController = segue.destination as? LotteryBoxViewController else {
                return
            }
            lotteryBoxViewController.fragmentQuantity = self.pointsTableViewDelegate.fragmentQuantity
        }
    }

    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: "確認", style: .cancel, handler: nil)

        alertController.addAction(alertActionOk)
        present(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }

    @IBAction func changed(_ sender: UISegmentedControl) {
        segmentedIndex = sender.selectedSegmentIndex
        changeUnderlinePosition()
    }

    func removeBorderForSegmentedControl() {
        segmentedControl.backgroundColor = .clear
        segmentedControl.tintColor = .clear

        segmentedControl.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
            ], for: .normal)

        segmentedControl.setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor: GlobalConstants.purpleColor
            ], for: .selected)
    }

    func changeUnderlinePosition() {
        let itemWidth = GlobalConstants.screenWidth * 0.95 / CGFloat(segmentedControl.numberOfSegments)

        UIView.animate(withDuration: 0.3) {
            self.underlineLeading.constant = CGFloat(self.segmentedControl.selectedSegmentIndex) * itemWidth
        }
    }

}

// MARK: API
// swiftlint:disable line_length
extension LevelViewController {
    func getExperience() {
        apiManager.level(levelApiEnum: .getExperience()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let experienceData = try JSONDecoder().decode(BasicModel<ExperienceData>.self, from: response.data).data

                strongSelf.pointsTableViewDelegate.experienceData = experienceData
                strongSelf.tableView.reloadData()

            } catch let error as NSError {
                print(error)
            }
        }
    }

    func getLevelMissionsStatus() {
        apiManager.level(levelApiEnum: .getLevelMissionsStatus()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let levelMissionsStatusData = try JSONDecoder().decode(BasicModel<[LevelMissionsStatusData]>.self, from: response.data).data
                strongSelf.pointsTableViewDelegate.levelMissionsStatusArray = levelMissionsStatusData

                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }

    func getLevelRewards() {
        apiManager.level(levelApiEnum: .getLevelRewards()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let levelRewardsArray = try JSONDecoder().decode(BasicModel<[LevelRewards]>.self, from: response.data).data

                strongSelf.pointsTableViewDelegate.levelRewardsArray = levelRewardsArray
                strongSelf.tableView.reloadData()

            } catch let error as NSError {
                print("error:\(error.localizedDescription)")
            }
        }
    }

    func levelup() {
        apiManager.level(levelApiEnum: .levelup()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let levelResponse = try JSONDecoder().decode(BasicMessageModel.self, from: response.data)
                strongSelf.alert(title: "升級", message: levelResponse.message)

                if levelResponse.status != "failed" {
                    strongSelf.getDataForApi()
                }

            } catch let error as NSError {
                print(error)
            }
        }
    }

    func getTatter() {
        apiManager.level(levelApiEnum: .getTatter()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let fragmentQuantity = try JSONDecoder().decode(BasicModel<Int>.self, from: response.data).data
                strongSelf.pointsTableViewDelegate.fragmentQuantity = fragmentQuantity

                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }

    func getShield() {
        apiManager.level(levelApiEnum: .getShield()) { [weak self] response in
            guard let strongSelf = self else { return }
            do {
                let currencyQuantity = try JSONDecoder().decode(BasicModel<Int>.self, from: response.data).data
                strongSelf.pointsTableViewDelegate.currencyQuantity = currencyQuantity

            } catch let error as NSError {
                print(error)
            }
        }
    }

    func getWarehouse() {
        apiManager.level(levelApiEnum: .getWarehouse()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                let warehouseDataArray = try JSONDecoder().decode(BasicModel<[WarehouseData]>.self, from: response.data).data

                strongSelf.myStorehouseTableViewDelegate.warehouseDataArray = warehouseDataArray
                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }
    }

}
// swiftlint:enable line_length
