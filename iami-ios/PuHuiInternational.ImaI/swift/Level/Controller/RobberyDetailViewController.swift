//
//  RobberyDetailViewController.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD
import MJRefresh

enum RobberyDetailMode {
    case canPlunder
    case beingPlundered
}

class RobberyDetailViewController: UIViewController, TaskAlertForSocket {

    var mode: RobberyDetailMode = .canPlunder
    var disposeBag = DisposeBag()
    var plunderDate: [PlunderDate] = []
    let header = MJRefreshStateHeader()

    let apiManager = ApiManager()
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.tableView.separatorColor = GlobalConstants.grayColorE5

        SVProgressHUD.show()
        getDataByMode()

        header.setRefreshingTarget(self, refreshingAction: #selector(getDataByMode))
        header.setTitle("idle", for: .idle)
        header.setTitle("willRefresh", for: .willRefresh)
        header.setTitle("Pulling", for: .pulling)
        header.setTitle("refreshing", for: .refreshing)
        header.lastUpdatedTimeLabel.text = "時間"
        header.lastUpdatedTimeText = { (date) -> String in
            guard let currencyDate = date else {
                return "時間:"
            }
            return "時間:\(String(describing: currencyDate))"
        }

        tableView.mj_header = header

        newMessageNotification()
    }

    @objc func getDataByMode() {
        if mode == .canPlunder {
            getCanPlunder()
        } else if mode == .beingPlundered {
            getWhoPlunderMe()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: "確認", style: .cancel, handler: nil)

        alertController.addAction(alertActionOk)
        present(alertController, animated: true, completion: nil)
    }

}

extension RobberyDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return plunderDate.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RobberyDetailCell", for: indexPath)
        guard let detailCell = cell as? RobberyDetailCell else {
            return cell
        }

        if mode == .canPlunder {
            detailCell.dateLabel.isHidden = true
            detailCell.robberyAction = { [weak self] in
                guard let strongSelf = self else { return }
                let plunderExperience = strongSelf.plunderDate[indexPath.row].plunderExperience!
                let experienceBoxId = strongSelf.plunderDate[indexPath.row].experienceBoxId!

                strongSelf.plunderExperience(boxId: experienceBoxId, experience: String(plunderExperience))
            }

            detailCell.contentLabel.text = "可掠奪\(plunderDate[indexPath.row].plunderExperience!)積分"

            if plunderDate[indexPath.row].canBePlunder != "Y" {
                detailCell.robberyButton.setTitle("今日已掠奪", for: .disabled)
                detailCell.robberyButton.isEnabled = false
                detailCell.robberyButton.setLightTextSize(float: 10)
            }

            detailCell.timerDisposable?.dispose()
            detailCell.timerDisposable =
                Observable<Int>.timer(0, period: 1, scheduler: MainScheduler.instance).map { $0 + 1 }
                    .subscribe(onNext: { [weak self] (element) in
                        guard let strongSelf = self else { return }
                        let time = strongSelf.plunderDate[indexPath.row].timer! - Float(element)
                        if time > 0.0 {
                            let timeString = strongSelf.getHoursMinutesSeconds(allSeconds: time)
                            detailCell.reciprocalLabel.text = timeString
                        } else {
                            detailCell.reciprocalLabel.text = "00:00"
                            detailCell.robberyButton.isEnabled = false
                        }
                    })

        } else if mode == .beingPlundered {
            detailCell.reciprocalLabel.isHidden = true
            detailCell.robberyButton.isHidden = true
            if let plunderTime = plunderDate[indexPath.row].plunderTime {
                detailCell.dateLabel.text = plunderTime
            }
        }

        detailCell.nameLabel.text = plunderDate[indexPath.row].nickname
        detailCell.levelLabel.text = "Lv." + plunderDate[indexPath.row].level

        let userImageUrl = plunderDate[indexPath.row].avatar
        let imageUrl = URL(string: userImageUrl)
        detailCell.userImageView?.sd_setImage(with: imageUrl, completed: nil)

        return detailCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 81 / 736 * GlobalConstants.screenHeight
    }

    func getHoursMinutesSeconds(allSeconds: Float) -> String {
        let allTime: Int = Int(allSeconds)
        var minutes = 0
        var seconds = 0
        var minutesText = ""
        var secondsText = ""

        minutes = allTime % 3600 / 60
        minutesText = minutes > 9 ? "\(minutes)" : "0\(minutes)"

        seconds = allTime % 3600 % 60
        secondsText = seconds > 9 ? "\(seconds)" : "0\(seconds)"

        return "\(minutesText):\(secondsText)"
    }

}

// MARK: API
extension RobberyDetailViewController {
    func getCanPlunder() {
        apiManager.plunder(plunderApiEnum: .getWhoCanPlunderByMe()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.plunderDate = try JSONDecoder().decode(BasicModel<[PlunderDate]>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }


//        //SVProgressHUD.show()
//        plunderApiProvider.rx.request(.getWhoCanPlunderByMe())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.plunderDate = try JSONDecoder().decode(BasicModel<[PlunderDate]>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func plunderExperience(boxId: String, experience: String) {
        SVProgressHUD.show()
        apiManager.plunder(plunderApiEnum: .plunderExperience(boxId)) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                let str = String(data: response.data, encoding: String.Encoding.utf8)
                print("返回的数据是：", str ?? "")

                let basicModel = try JSONDecoder().decode(BasicMessageModel.self, from: response.data)

                if basicModel.status == "success" {
                    strongSelf.alert(title: "掠奪成功", message: "\(experience)積分")
                } else {
                    strongSelf.alert(title: "掠奪失敗", message: "掠奪失敗")
                }

                strongSelf.getCanPlunder()

            } catch let error as NSError {
                print(error)
            }
        }




//        plunderApiProvider.rx.request(.plunderExperience(boxId))
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")
//
//                        let basicModel = try JSONDecoder().decode(BasicMessageModel.self, from: response.data)
//
//                        if basicModel.status == "success" {
//                            strongSelf.alert(title: "掠奪成功", message: "\(experience)積分")
//                        } else {
//                            strongSelf.alert(title: "掠奪失敗", message: "掠奪失敗")
//                        }
//
//                        strongSelf.getCanPlunder()
//
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }

    func getWhoPlunderMe() {
        apiManager.plunder(plunderApiEnum: .whoPlunderMe()) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.plunderDate = try JSONDecoder().decode(BasicModel<[PlunderDate]>.self, from: response.data).data

                strongSelf.tableView.mj_header.endRefreshing()
                strongSelf.tableView.reloadData()
            } catch let error as NSError {
                print(error)
            }
        }



        //SVProgressHUD.show()
//        plunderApiProvider.rx.request(.whoPlunderMe())
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.plunderDate = try JSONDecoder().decode(BasicModel<[PlunderDate]>.self, from: response.data).data
//
//                        strongSelf.tableView.mj_header.endRefreshing()
//                        strongSelf.tableView.reloadData()
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }
}
