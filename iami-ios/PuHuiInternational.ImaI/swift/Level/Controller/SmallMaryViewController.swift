//
//  SmallMaryViewController.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SVProgressHUD

class SmallMaryViewController: UIViewController, TaskAlertForSocket {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var starButton: UIButton!

    @IBOutlet weak var collectionViewLayout: UICollectionViewFlowLayout! {
        didSet {
            let collectionWidth = floor(120 / 414 * GlobalConstants.screenWidth)

            collectionViewLayout.itemSize = CGSize(width: collectionWidth, height: collectionWidth)
            collectionViewLayout.minimumInteritemSpacing = 10
            collectionViewLayout.minimumLineSpacing = 10
        }
    }

    var disposeBag = DisposeBag()
    var timer = Timer()
    let itemSequence = [0, 1, 2, 5, 8, 7, 6, 3]
    let itemIndexSequence = [0, 1, 2, 7, 3, 6, 5, 4]
    var current = 0

    var startItemIndex = 0
    var endItemIndex = 3
    var lotteryBoxesDetailArray: [LotteryBoxesDetailData]!
    var lotteryWinningItemData = LotteryWinningItemData() {
        didSet {
            endItemIndex = lotteryWinningItemData.treasureIndex
        }
    }

    var numberOfTurns = 5
    var timeInterval = 0.05
    var boxLevel = ""

    let apiManager = ApiManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        startItemIndex = Int(arc4random_uniform(8))
        current = startItemIndex
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.navigationBar.tintColor = UIColor.white

        newMessageNotification()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func start(_ sender: UIButton) {
        starButton.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isUserInteractionEnabled = false
        self.navigationController?.navigationBar.tintColor = UIColor.lightGray
        probabilityDraw()
    }

    @objc func lotteryAnimation() {
        if current == itemSequence.last {
            current = itemSequence.first!
            numberOfTurns -= 1
            timer.invalidate()
            timeInterval += 0.05
            // swiftlint:disable line_length
            timer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(lotteryAnimation), userInfo: nil, repeats: true)
            // swiftlint:enable line_length
        } else {
            current = itemSequence[itemSequence.index(of: current)! + 1]
        }
        collectionView.reloadData()
    }

    func animageEnd(itemIndex: Int) {
        timer.invalidate()
        numberOfTurns = 5
        timeInterval = 0.05

        let imageAlertView = ImageAlertView()
        imageAlertView.frame = self.view.bounds

        //---之後要簡化
        if let urlString = lotteryBoxesDetailArray[itemIndex].image {
            let imageUrl = URL(string: GlobalConstants.baseUrl + urlString)
            imageAlertView.imageView.sd_setImage(with: imageUrl, completed: nil)
        } else {
            imageAlertView.imageView.image = #imageLiteral(resourceName: "占位图")
        }
        imageAlertView.nameLabel.text = lotteryBoxesDetailArray[itemIndex].name
        //---

        imageAlertView.buttonAction = {
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.navigationController?.navigationBar.tintColor = UIColor.white
        }

        self.navigationController?.view.addSubview(imageAlertView)
    }
}

extension SmallMaryViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
// swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 4 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallMaryCenterImageCell", for: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallMaryCollectionViewCell", for: indexPath)
            guard let smallMaryCell = cell as? SmallMaryCollectionViewCell else {
                return cell
            }

            let index = indexPath.row < 4 ? indexPath.row: indexPath.row - 1
            let itemIndex = itemIndexSequence[index]
//            let item = lotteryBoxesDetailArray.filter {
//                $0
//            }

            smallMaryCell.nameLabel.text = lotteryBoxesDetailArray[itemIndex].name

            if let urlString = lotteryBoxesDetailArray[itemIndex].image {
                let imageUrl = URL(string: GlobalConstants.baseUrl + urlString)
                smallMaryCell.imageView.sd_setImage(with: imageUrl, completed: nil)
            } else {
                smallMaryCell.imageView.image = #imageLiteral(resourceName: "占位图")
            }

            if indexPath.row == current {
                smallMaryCell.selectCell()
                if indexPath.row == endItemIndex && numberOfTurns == 0 {
                    animageEnd(itemIndex: itemIndex)
                }
            } else {
                smallMaryCell.noSelectCell()
            }

            return smallMaryCell
        }
    }
// swiftlint:enable line_length
}

// MARK: API
extension SmallMaryViewController {
    func probabilityDraw() {
        SVProgressHUD.show()
        apiManager.treasureBoxes(treasureBoxesApiEnum: .probabilityDraw(boxLevel)) { [weak self] response in
            SVProgressHUD.dismiss()
            guard let strongSelf = self else { return }
            do {
                strongSelf.lotteryWinningItemData = try JSONDecoder().decode(BasicModel<LotteryWinningItemData>.self, from: response.data).data

                // swiftlint:disable line_length
                strongSelf.timer = Timer.scheduledTimer(timeInterval: strongSelf.timeInterval, target: strongSelf, selector: #selector(strongSelf.lotteryAnimation), userInfo: nil, repeats: true)
                // swiftlint:enable line_length
            } catch let error as NSError {
                print(error)
            }
        }

//        treasureBoxesApi.rx.request(.probabilityDraw(boxLevel))
//            .subscribe { [weak self] event in
//                guard let strongSelf = self else { return }
//                SVProgressHUD.dismiss()
//                switch event {
//                case let .success(response):
//                    do {
//                        strongSelf.lotteryWinningItemData = try JSONDecoder().decode(BasicModel<LotteryWinningItemData>.self, from: response.data).data
//
//                        // swiftlint:disable line_length
//                        strongSelf.timer = Timer.scheduledTimer(timeInterval: strongSelf.timeInterval, target: strongSelf, selector: #selector(strongSelf.lotteryAnimation), userInfo: nil, repeats: true)
//                        // swiftlint:enable line_length
//                    } catch let error as NSError {
//                        print(error)
//                    }
//                case let .error(error):
//                    print("数据请求失败!错误原因：", error)
//                }
//            }.disposed(by: disposeBag)
    }
}
