//
//  TaskAlertForSocket.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/22.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol TaskAlertForSocket {
    var disposeBag: DisposeBag { get set }

}

extension TaskAlertForSocket where Self: UIViewController {
    func newMessageNotification() {
        let notificationName = NSNotification.Name(rawValue: "newMessage")

        NotificationCenter.default.rx.notification(notificationName)
            .map { notification -> String in
                guard let object = notification.object as? [String: Any],
                    let task = object["task"] as? [[String: String]] else {
                        return ""
                }

                let message = task.map {
                    guard let name = $0["name"], let reward = $0["reward"] else {
                        return ""
                    }

                    return "\(name) \(reward) \n"
                    }.reduce("", +)

                return message
        }.observeOn(MainScheduler.instance)
            .subscribe(onNext: { (message) in
                self.alert(title: "完成任務", message: message)
            })
        .disposed(by: disposeBag)
    }

    func alert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: "確認", style: .cancel, handler: nil)

        alertController.addAction(alertActionOk)
        self.present(alertController, animated: true, completion: nil)
    }
}
