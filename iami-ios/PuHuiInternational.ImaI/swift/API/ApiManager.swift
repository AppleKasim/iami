//
//  ApiManager.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/22.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya

class ApiManager {

    let disposeBag = DisposeBag()

//    func levelApiToken(levelApiEnum: LevelApi, responseAction: @escaping (Moya.Response) -> Void) {
//        getCsrfToken(responseAction: responseAction)
////        levelApiProvider.rx.request(levelApiEnum)
////            .subscribe { [weak self] event in
////                guard let strongSelf = self else { return }
////                switch event {
////                case let .success(response):
////                    strongSelf.getCsrfToken(responseAction: responseAction(response))
////
////                case let .error(error):
////                    print("数据请求失败!错误原因：", error)
////                }
////            }.disposed(by: disposeBag)
//    }

    func level(levelApiEnum: LevelApi, responseAction: @escaping (Moya.Response) -> Void) {
        getCsrfAndAccessToken { [weak self] in
            guard let strongSelf = self else { return }
            levelApiProvider.rx.request(levelApiEnum)
                .subscribe { event in
                    switch event {
                    case let .success(response):
                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                        print("返回的数据是：", str ?? "")

                        responseAction(response)
                    case let .error(error):
                        print("数据请求失败!错误原因：", error)
                    }
                }.disposed(by: strongSelf.disposeBag)
        }
    }

    func plunder(plunderApiEnum: PlunderApi, responseAction: @escaping (Moya.Response) -> Void) {
        getCsrfAndAccessToken { [weak self] in
            guard let strongSelf = self else { return }
            plunderApiProvider.rx.request(plunderApiEnum)
                .subscribe { event in
                    switch event {
                    case let .success(response):
                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                        print("返回的数据是：", str ?? "")

                        responseAction(response)
                    case let .error(error):
                        print("数据请求失败!错误原因：", error)
                    }
                }.disposed(by: strongSelf.disposeBag)
        }
    }

    func treasureBoxes(treasureBoxesApiEnum: TreasureBoxesApi, responseAction: @escaping (Moya.Response) -> Void) {
        getCsrfAndAccessToken { [weak self] in
            guard let strongSelf = self else { return }
            treasureBoxesApiProvider.rx.request(treasureBoxesApiEnum)
                .subscribe { event in
                    switch event {
                    case let .success(response):
                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                        print("返回的数据是：", str ?? "")

                        responseAction(response)
                    case let .error(error):
                        print("数据请求失败!错误原因：", error)
                    }
                }.disposed(by: strongSelf.disposeBag)
        }
    }

    func signInToday(signInTodayApiEnum: SignInTodayApi, responseAction: @escaping (Moya.Response) -> Void) {
        getCsrfAndAccessToken { [weak self] in
            guard let strongSelf = self else { return }
            signInTodayApiProvider.rx.request(signInTodayApiEnum)
                .subscribe { event in
                    switch event {
                    case let .success(response):
                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                        print("返回的数据是：", str ?? "")

                        responseAction(response)
                    case let .error(error):
                        print("数据请求失败!错误原因：", error)
                    }
                }.disposed(by: strongSelf.disposeBag)
        }
    }

    func misssions(misssionsApiEnum: MisssionsApi, responseAction: @escaping (Moya.Response) -> Void) {
        getCsrfAndAccessToken { [weak self] in
            guard let strongSelf = self else { return }
            misssionsApiProvider.rx.request(misssionsApiEnum)
                .subscribe { event in
                    switch event {
                    case let .success(response):
                        let str = String(data: response.data, encoding: String.Encoding.utf8)
                        print("返回的数据是：", str ?? "")

                        responseAction(response)
                    case let .error(error):
                        print("数据请求失败!错误原因：", error)
                    }
                }.disposed(by: strongSelf.disposeBag)
        }
    }

    private func getCsrfAndAccessToken(responseAction: @escaping () -> Void) {
        tokenApi.rx.request(.getCsrfToken())
            .subscribe { [weak self] event in
                guard let strongSelf = self else { return }
                switch event {
                case let .success(response):
                    do {

//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")

                        let csrfToken = try JSONDecoder().decode(BasicModel<CsrfTokenModel>.self, from: response.data).data.token

                        let userDefault = UserDefaults.standard
                        userDefault.set(csrfToken, forKey: GlobalConstants.DefaultsKey.token)
                        userDefault.synchronize()

                        strongSelf.getAccessToken(responseAction: responseAction)
                    } catch let error as NSError {
                        print(error)
                    }
                case let .error(error):
                    print("数据请求失败!错误原因：", error)
                }
            }.disposed(by: disposeBag)
    }

    private func getAccessToken(responseAction: @escaping () -> Void) {
        tokenApi.rx.request(.getAccessToken())
            .subscribe { event in
                switch event {
                case let .success(response):
                    do {

//                        let str = String(data: response.data, encoding: String.Encoding.utf8)
//                        print("返回的数据是：", str ?? "")

                        let token = try JSONDecoder().decode(BasicModelNoCode<AccessTokenModel>.self, from: response.data).data.token
                        let userDefault = UserDefaults.standard
                        userDefault.set(token, forKey: GlobalConstants.DefaultsKey.accessToken)
                        userDefault.synchronize()

                        responseAction()
                    } catch let error as NSError {
                        print(error)
                    }
                case let .error(error):
                    print("数据请求失败!错误原因：", error)
                }
            }.disposed(by: disposeBag)
    }
}
