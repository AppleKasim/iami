//
//  MissionsApi.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import Moya

let misssionsApiProvider = MoyaProvider<MisssionsApi>()

public enum MisssionsApi {
    case getEveryDayMissions()
    case getLimitMissions()
    case getSpecialMissions()
}

extension MisssionsApi: TargetType {
    public var baseURL: URL {
        return URL(string: GlobalConstants.baseUrl)!
    }

    public var path: String {
        switch self {
        case .getEveryDayMissions:
            return "/api/getEveryDayMissions"
        case .getLimitMissions:
            return "/api/getLimitMissions"
        case .getSpecialMissions:
            return "/api/getSpecialMissions"
        }
    }

    public var method: Moya.Method {
        return .post
    }

    public var task: Task {
        let userDefault = UserDefaults.standard
        let token = userDefault.string(forKey: GlobalConstants.DefaultsKey.token)
        let memberId = userDefault.string(forKey: GlobalConstants.DefaultsKey.memberId)
        let accessToken = userDefault.string(forKey: GlobalConstants.DefaultsKey.accessToken)

        let parameters = ["csrf_token_name": token!, "member_id": memberId!, "token": accessToken!]
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
