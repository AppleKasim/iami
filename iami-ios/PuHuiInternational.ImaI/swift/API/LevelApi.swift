//
//  LevelApi.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import Moya

let levelApiProvider = MoyaProvider<LevelApi>()

public enum LevelApi {
    case getLevelMissionsStatus()
    case getExperience()
    case getLevelRewards()
    case levelup()
    case getTatter()
    case getShield()
    case getWarehouse()
}

extension LevelApi: TargetType {
    public var baseURL: URL {
        return URL(string: GlobalConstants.baseUrl)!
    }

    public var path: String {
        switch self {
        case .getLevelMissionsStatus:
            return "/api/getLevelMissionsStatus"
        case .getExperience:
            return "/api/getExperience"
        case .getLevelRewards:
            return "/api/getLevelRewards"
        case .levelup:
            return "/api/levelup"
        case .getTatter:
            return "/api/getTatter"
        case .getShield:
            return "/api/getShield"
        case .getWarehouse:
            return "/api/getWarehouse"
        }
    }

    public var method: Moya.Method {
        return .post
    }

    public var task: Task {
        let userDefault = UserDefaults.standard
        let token = userDefault.string(forKey: GlobalConstants.DefaultsKey.token)
        let memberId = userDefault.string(forKey: GlobalConstants.DefaultsKey.memberId)
        let accessToken = userDefault.string(forKey: GlobalConstants.DefaultsKey.accessToken)

        let parameters = ["csrf_token_name": token!, "member_id": memberId!, "token": accessToken!]
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
