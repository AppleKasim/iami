//
//  SignInApi.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import Moya

let signInTodayApiProvider = MoyaProvider<SignInTodayApi>()

public enum SignInTodayApi {
    case signInToday()
    case isSignInToday()
    case loadSignData()
    case continueSevenReward(day: Int)
}

extension SignInTodayApi: TargetType {
    public var baseURL: URL {
        return URL(string: GlobalConstants.baseUrl)!
    }

    public var path: String {
        switch self {
        case .signInToday:
            return "/api/signInToday"
        case .isSignInToday:
            return "/api/isSignInToday"
        case .loadSignData:
            return "/api/loadSignData"
        case .continueSevenReward:
            return "/api/continueSevenReward"
        }
    }

    public var method: Moya.Method {
        return .post
    }

    public var task: Task {
        let userDefault = UserDefaults.standard
        let token = userDefault.string(forKey: GlobalConstants.DefaultsKey.token)
        let memberId = userDefault.string(forKey: GlobalConstants.DefaultsKey.memberId)
        let accessToken = userDefault.string(forKey: GlobalConstants.DefaultsKey.accessToken)

        var parameters = ["csrf_token_name": token!, "member_id": memberId!, "token": accessToken!]

        if case .continueSevenReward(let day) = self {
            parameters["day"] = String(day)
        }

        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
