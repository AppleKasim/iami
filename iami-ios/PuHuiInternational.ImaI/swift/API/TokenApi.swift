//
//  TokenApi.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import Moya

let tokenApi = MoyaProvider<TokenApi>()

public enum TokenApi {
    case getCsrfToken()
    case getAccessToken()
}

extension TokenApi: TargetType {
    public var baseURL: URL {
        return URL(string: GlobalConstants.baseUrl)!
    }

    public var path: String {
        switch self {
        case .getCsrfToken:
            return "/api/get_csrf_token"
        case .getAccessToken:
            return "/api/doLogin"
        }
    }

    public var method: Moya.Method {
        switch self {
        case .getCsrfToken:
            return .get
        case .getAccessToken:
            return .post
        }
    }

    public var task: Task {
        switch self {
        case .getCsrfToken:
            return .requestPlain
        case .getAccessToken:
            let userDefault = UserDefaults.standard
            let email = userDefault.string(forKey: GlobalConstants.DefaultsKey.email)
            let password = userDefault.string(forKey: GlobalConstants.DefaultsKey.password)
            let token = userDefault.string(forKey: GlobalConstants.DefaultsKey.token)

            let parameters = ["email": email!, "password": password!, "csrf_token_name": token!]
            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }

    //是否执行Alamofire验证
    public var validate: Bool {
        return false
    }

    //这个就是做单元测试模拟的数据，只会在单元测试文件中有作用
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }

    public var headers: [String: String]? {
        return nil
    }
}
