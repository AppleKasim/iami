//
//  GlobalConstant.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

import Foundation
import UIKit

struct GlobalConstants {

    #if DEBUG
    static let baseUrl = "http://test.iami-web.me"
    //static let baseUrl = "http://jerry.iami-web.me"
    #else
    static let baseUrl = "http://www.iami-web.me"
    #endif

    static let purpleColor = UIColor(rgb: 0x8361c2)
    static let grayColorD2 = UIColor(rgb: 0xd2d2d2)
    static let grayColor31 = UIColor(rgb: 0x313131)
    static let grayColorA0 = UIColor(rgb: 0xA0A0A0)
    static let grayColorE5 = UIColor(rgb: 0xE5E5E5)
    static let grayColor64 = UIColor(rgb: 0x646464)
    static let screenWidth = UIScreen.main.bounds.width
    static let screenHeight = UIScreen.main.bounds.height
    static let purpleImage = #imageLiteral(resourceName: "Gradation")

    struct DefaultsKey {
        static let userKey = "userName"
        static let token = "Token"
        static let login = "Login"
        static let imAccount = "IMAccount"
        static let title = "title"
        static let content = "Content"
        static let isFirst = "IsFirst"
        static let accessToken = "Access Token"
        static let memberId = "member_id"
        static let birth = "birth"
        static let mobile = "mobile"
        static let userImage = "image"
        static let banner = "banner"
        static let email = "email"
        static let password = "password"
        static let infoShow = "info_show"
        static let language = "language"
    }

}
