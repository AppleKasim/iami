//
//  ImageAlertView.swift
//  PuHuiInternational.ImaI
//
//  Created by Apple on 2018/8/3.
//  Copyright © 2018年 ljq. All rights reserved.
//

import UIKit

class ImageAlertView: UIView {

    @IBOutlet weak var contentView: UIView! {
        didSet {
            contentView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }
    }
    
    @IBOutlet weak var alertView: UIView! {
        didSet {
            alertView.layer.cornerRadius = 5
            alertView.layer.masksToBounds = true
        }
    }

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = GlobalConstants.grayColor64
        }
    }

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var nameLabel: UILabel! {
        didSet {
            nameLabel.textColor = GlobalConstants.grayColor31
        }
    }

    @IBOutlet weak var confirmButton: UIButton! {
        didSet {
            confirmButton.tintColor = GlobalConstants.purpleColor
        }
    }

    @IBOutlet weak var confirmButtonTopView: UIView! {
        didSet {
            confirmButtonTopView.backgroundColor = GlobalConstants.grayColorE5
        }
    }

    var buttonAction: () -> Void = {}

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initFromXIB()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initFromXIB()
    }

    func initFromXIB() {

        let bundle = Bundle(for: type(of: self))

        let nib = UINib(nibName: "ImageAlertView", bundle: bundle)

        guard let nibView = nib.instantiate(withOwner: self, options: nil)[0] as? UIView else {
            return
        }

        contentView = nibView
        contentView.frame = bounds
        self.addSubview(contentView)

    }

    @IBAction func confirm(_ sender: UIButton) {
        buttonAction()
        self.removeFromSuperview()
    }
}
