//
//  LoginViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "LoginViewController.h"
#import "RegistViewController.h"
#import "WelcomeView.h"


@interface LoginViewController ()<UITextFieldDelegate,UITextViewDelegate>
{
    //SZTextView *UserTld;
    UITextField *UserTld;
    UITextField *PasswordTld;
    UIButton *ChargeButton;
    
    UIButton *QQButton;
    UIButton *WecheatButton;
    UIButton *WeiboButton;
    
    UIButton *RegistButton;
    UIButton *ForgetButton;
    //白色view
    UIView *WhiteView;
    
//    忘记密码的VIew
    UIView *ForgetPwdView;
    
    UIView *SubView;
    
    UITextField *forEmilTld;
    NSString *language_id;

    
}
@property (nonatomic,copy) NSMutableArray *BirthArray;
#define cornerR 6

#define LoginUrl @"/api/doLogin"
#define fotUrl @"/api/doForget"
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.view.backgroundColor = LYColor(237, 204, 105);
     [self getCSRF_Token];
    _BirthArray = [[NSMutableArray alloc]init];
    [_BirthArray addObject:@"1990▼"];
    [_BirthArray addObject:@"1月▼"];
    [_BirthArray addObject:@"1月▼"];
    [self creatUI];
    // Do any additional setup after loading the view.
}

-(void)creatUI{
    UIImageView *MainImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    MainImage.image = [UIImage imageNamed:@"iami_splashbg.jpg"];
    MainImage.userInteractionEnabled = YES;
    [self.view addSubview:MainImage];
    
    CGFloat height = 0;
    if (IsiPhoneX) {
        height= 40;
    }else{
        height = 28;
    }
    UIImageView *lImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 35, height, 70, 19)];
    lImage.image = [UIImage imageNamed:@"logo280x78"];
    lImage.contentMode = UIViewContentModeScaleAspectFit;
    [MainImage addSubview:lImage];
    
    
    
    
    WhiteView = [[UIView alloc]initWithFrame:CGRectMake(10, 150, SCREEN_WIDTH - 20, 400)];
    WhiteView.backgroundColor = [UIColor whiteColor];
    WhiteView.layer.cornerRadius = 6;
    
    CGFloat width = [self getLabelWidthWithText:NSLocalizedString(@"登录", nil) width:SCREEN_WIDTH - 20 font:Bold_FONT(32)];
    
    UILabel *loginLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH /2 - width / 2 - 50 - 10, 34, width, 40)];
    loginLabel.text = NSLocalizedString(@"登录", nil);
    loginLabel.layer.cornerRadius = 8;
    loginLabel.font = Bold_FONT(32);
    loginLabel.textColor = MainColor;
    [WhiteView addSubview:loginLabel];
    
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(loginLabel.frame) + 5, 42, 100, 27)];
    logoImage.image = [UIImage imageNamed:@"logo copy"];
    [WhiteView addSubview:logoImage];
    
    UserTld = [[UITextField alloc]initWithFrame:CGRectMake(30, 100, WhiteView.frame.size.width - 60, 36)];
    if (![[PHUserModel sharedPHUserModel].email isEqualToString:@""] && [PHUserModel sharedPHUserModel].email.length > 0) {
        UserTld.text = [PHUserModel sharedPHUserModel].email;
    }else{
        UserTld.placeholder = NSLocalizedString(@"电子邮箱", nil);
    }
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    UserTld.leftView=emptyView;
    UserTld.leftViewMode=UITextFieldViewModeAlways;
    UserTld.font = FONT(20);
    UserTld.keyboardType  = UIKeyboardTypeEmailAddress;
    UserTld.layer.borderWidth = 1.0f;
    UserTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    UserTld.layer.cornerRadius = cornerR;
    UserTld.autocorrectionType = UITextAutocorrectionTypeNo;
    UserTld.delegate = self;
    [WhiteView addSubview:UserTld];
   
   
    PasswordTld = [[UITextField alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(UserTld.frame) + 20, WhiteView.frame.size.width - 60, 36)];
    UIView *emptyView1 =[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    PasswordTld.leftView=emptyView1;
    PasswordTld.leftViewMode=UITextFieldViewModeAlways;
    
    PasswordTld.secureTextEntry = YES;
    PasswordTld.placeholder = [NSString stringWithFormat:@"%@",NSLocalizedString(@"密码", nil)];
    PasswordTld.layer.borderWidth = 1.0f;
    PasswordTld.font = FONT(20);
    PasswordTld.delegate = self;
    PasswordTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    PasswordTld.layer.cornerRadius = cornerR;
    [WhiteView addSubview:PasswordTld];
   

    
    

    
    UIButton *ForgetBut = [UIButton buttonWithType:UIButtonTypeCustom];
    ForgetBut.frame =CGRectMake(33, CGRectGetMaxY(PasswordTld.frame)  + 20, 200, 20);
    [ForgetBut setTitle:NSLocalizedString(@"忘记密码？", nil) forState:UIControlStateNormal];
    ForgetBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [ForgetBut setTitleColor:MainColor forState:UIControlStateNormal];
    ForgetBut.titleLabel.font = FONT(15);
    [ForgetBut addTarget:self action:@selector(forgetPswd) forControlEvents:UIControlEventTouchUpInside];
    [WhiteView addSubview:ForgetBut];
    
    ChargeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [ChargeButton setBackgroundImage:[UIImage imageWithColor:MainColor] forState:UIControlStateNormal];
    ChargeButton.frame = CGRectMake(30, CGRectGetMaxY(ForgetBut.frame)+ 20, WhiteView.frame.size.width - 60, 40);
    ChargeButton.clipsToBounds = YES;
    [ChargeButton setTitle:NSLocalizedString(@"登录", nil) forState:UIControlStateNormal];
    [ChargeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ChargeButton addTarget:self action:@selector(LogIn) forControlEvents:UIControlEventTouchUpInside];
    ChargeButton.layer.cornerRadius = 8;
    [WhiteView addSubview:ChargeButton];
    
    RegistButton = [UIButton buttonWithType:UIButtonTypeCustom];
    RegistButton.frame = CGRectMake(30, CGRectGetMaxY(ChargeButton.frame) + 20, WhiteView.frame.size.width - 60, 40);
    [RegistButton setTitle:NSLocalizedString(@"注册", nil) forState:UIControlStateNormal];
    [RegistButton setBackgroundImage:[UIImage imageWithColor:LYColor(223, 223, 223)] forState:UIControlStateNormal];
    [RegistButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [RegistButton addTarget:self action:@selector(Regist) forControlEvents:UIControlEventTouchUpInside];
    RegistButton.layer.cornerRadius = 8;
    RegistButton.clipsToBounds = YES;
    [WhiteView addSubview:RegistButton];
    
    [self.view addSubview:WhiteView];
}

/**
 登陆r
 */
-(void)LogIn{
    if ([[self getPreferredLanguage] isEqualToString:@"zh-Hans-CN"]){
        [[NSUserDefaults standardUserDefaults] setObject:@[@"zh-Hans-CN"] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"zh-CN";
    }
    else if ([[self getPreferredLanguage] isEqualToString:@"zh-Hant-CN"]){
        NSArray *lans = @[@"zh-Hant-CN"];
        [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"zh-TW";
    }
    else {
        [[NSUserDefaults standardUserDefaults] setObject:@[@"en"] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"english";
    }
    
    NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,LoginUrl];
    NSString *user = UserTld.text;
    NSString *pass = PasswordTld.text;
    NSDictionary *paramDic = @{@"email":user,@"password":pass,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    
    [PPNetworkHelper setValue:language_id forHTTPHeaderField:@"set-lg"];
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"message"] isEqualToString:@"success"]) {
            NSDictionary *newDict = dic[@"data"];
            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [PHUserModel sharedPHUserModel].isLogin = YES;
            [PHUserModel sharedPHUserModel].AccessToken = [NetDataCommon stringFromDic:newDict forKey:@"token"];
            [PHUserModel sharedPHUserModel].member_id = [NetDataCommon stringFromDic:newDict forKey:@"member_id"];
            [PHUserModel sharedPHUserModel].nickname = [NetDataCommon stringFromDic:newDict forKey:@"nickname"];;
            [PHUserModel sharedPHUserModel].mobile = [NetDataCommon stringFromDic:newDict forKey:@"mobile"];;
            [PHUserModel sharedPHUserModel].Birthday = [NetDataCommon stringFromDic:newDict forKey:@"birth"];;
            [PHUserModel sharedPHUserModel].UserImage = [NetDataCommon stringFromDic:newDict forKey:@"avatar"];;
            [PHUserModel sharedPHUserModel].banner = [NetDataCommon stringFromDic:newDict forKey:@"banner"];;
            [PHUserModel sharedPHUserModel].email =[NetDataCommon stringFromDic:newDict forKey:@"email"];;
            [PHUserModel sharedPHUserModel].Password = pass;
            [PHUserModel sharedPHUserModel].info_show= [[NSMutableString alloc]init];
            [[PHUserModel sharedPHUserModel].info_show appendFormat:@"%@",[NetDataCommon stringFromDic:newDict forKey:@"info_show"]];
            [PHUserModel sharedPHUserModel].language = [NetDataCommon stringFromDic:newDict forKey:@"language_name"];
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            [appdelegate initTabBarVC];

        }else{
            [CYToast showErrorWithString:[NSString stringWithFormat:@"%@",dic[@"message"]]];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);

    } getToken:false];

}
//忘记密码
-(void)sendEmial{
//    [ForgetPwdView removeFromSuperview];
//    for (UIView *subView in SubView.subviews) {
//        [subView removeFromSuperview];
//    }
    [[QWAlertView sharedMask]dismiss];
    NSDictionary *parmDic =@{@"email":forEmilTld.text};
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,fotUrl];
    [PPNetworkHelper GET:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [self sendEmailSuccess];
        } else {

            UIAlertController* alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"电子邮件错误", nil)
                                                                           message:dic[@"message"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"确定", nil) style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];

            [alert addAction:defaultAction];
            alert.view.tintColor = LYColor(131, 97, 194);
            [self presentViewController:alert animated:YES completion:nil];




//            NSString * cancel = NSLocalizedString(@"确定", nil);
//
//            [cancel colorWithHexString:@"8361c2"];
//            [PXAlertView showAlertWithTitle:NSLocalizedString(@"电子邮件错误", nil) message:dic[@"message"] cancelTitle:NSLocalizedString(@"确定", nil) completion:nil];

//            [PXAlertView showAlertWithTitle:NSLocalizedString(@"电子邮件错误", nil) message:dic[@"message"] cancelTitle:NSLocalizedString(@"确定", nil) completion: nil];
            //NSLog(@"%@", dic[@"message"]);
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
/**
 注册
 */
-(void)Regist{
    RegistViewController *controller = [[RegistViewController alloc]init];
//    [controller setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
//    [self presentViewController:controller animated:YES completion:^{
//
//    }];
    [self.navigationController pushViewController:controller animated:YES];
}

/**
 忘记密码
 */
-(void)forgetPswd{
    [self ShowFortView];
}

/**
 弹出忘记密码的View
 */
-(void)ShowFortView{
    ForgetPwdView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    ForgetPwdView.backgroundColor = LYColor(33, 28, 16);
    
    
    SubView  = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 260 - 250, SCREEN_WIDTH - 20, 210)];
    if (SCREEN_WIDTH < 370) {
        SubView  = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 260 - 280, SCREEN_WIDTH - 20, 210)];
    } else if (IsiPhoneX) {
        SubView  = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 260 - 300, SCREEN_WIDTH - 20, 210)];
    }


    SubView.backgroundColor = [UIColor whiteColor];
    SubView.layer.cornerRadius = 8;
    
    UIButton *closeBut = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBut.frame = CGRectMake(SubView.frame.size.width + 10 - 30, 5, 15, 15);
    [closeBut setImage:[UIImage imageNamed:@"icon-关闭按钮"] forState:UIControlStateNormal];
    [closeBut addTarget:self action:@selector(closeForgetView) forControlEvents:UIControlEventTouchUpInside];
    
    [SubView addSubview:closeBut];
    
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 22, SCREEN_WIDTH, 20)];
    titleLabel.text = NSLocalizedString(@"忘记密码？", nil);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = Bold_FONT(22);
    [SubView addSubview:titleLabel];
    
    UILabel *subsLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,50 , SubView.frame.size.width - 20, 40)];
    subsLabel.text = NSLocalizedString(@"如忘记密码,请输入电子邮件,生日,手机号码,iami会将您的密码发送到您的电子邮件箱里。", nil);
    subsLabel.font = FONT(13);
    subsLabel.numberOfLines = 2;
    subsLabel.textColor = LYColor(188, 188, 188);
    [SubView addSubview:subsLabel];

   forEmilTld = [[UITextField alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(subsLabel.frame) + 10, SubView.frame.size.width - 20, 36)];
    forEmilTld.placeholder = NSLocalizedString(@"电子邮箱", nil);
    //forEmilTld.layer.borderWidth = 1.0f;
    forEmilTld.delegate = self;
    forEmilTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    forEmilTld.layer.cornerRadius = cornerR;
    forEmilTld.autocorrectionType = UITextAutocorrectionTypeNo;
    forEmilTld.keyboardType = UIKeyboardTypeEmailAddress;
    forEmilTld.borderStyle = UITextBorderStyleRoundedRect;
    [SubView addSubview:forEmilTld];
    

    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendButton setBackgroundImage:[UIImage imageWithColor:MainColor] forState:UIControlStateNormal];
    sendButton.frame = CGRectMake(10, CGRectGetMaxY(forEmilTld.frame)+ 20, SubView.frame.size.width - 20, 40);
    sendButton.clipsToBounds = YES;
    [sendButton setTitle:NSLocalizedString(@"发送", nil) forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(forgetSend) forControlEvents:UIControlEventTouchUpInside];
    sendButton.layer.cornerRadius = 8;
    [SubView addSubview:sendButton];


    [[QWAlertView sharedMask]show:SubView withType:QWAlertViewStyleActionSheetDown];
    
    

    
}

/**
 关闭忘记密码View
 */
-(void)closeForgetView{
//    [UIView animateWithDuration:0.3f animations:^{
//        [ForgetPwdView removeFromSuperview];
//    }];
    [[QWAlertView sharedMask]dismiss];
    
}
/**
 忘记密码时生日选择按钮点击事件
 */
-(void)BirthChoose{
    DatePickerTool *datePicker = [[DatePickerTool alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 250, WhiteView.frame.size.width -20, 200)];
    __block DatePickerTool *blockPick = datePicker;
    datePicker.callBlock = ^(NSString *pickDate) {
        
        NSLog(@"%@",pickDate);
        
        if (pickDate) {
            [_BirthArray removeAllObjects];
            NSArray *array =   [pickDate componentsSeparatedByString:@"-"];
            UIButton *but1 = [self.view viewWithTag:100];
            UIButton *but2 = [self.view viewWithTag:101];
            UIButton *but3 = [self.view viewWithTag:102];
            
            [but1 setTitle:[NSString stringWithFormat:@"%@▼",array[0]] forState:UIControlStateNormal];
            [but2 setTitle:[NSString stringWithFormat:@"%@月▼",array[1]] forState:UIControlStateNormal];
            [but3 setTitle:[NSString stringWithFormat:@"%@日▼",array[2]] forState:UIControlStateNormal];
        }
        
        [blockPick removeFromSuperview];
    };
    
    [self.view addSubview:datePicker];
}

/**
 忘记密码，点击发送
 */
-(void)forgetSend{
    if (forEmilTld.text.length == 0 || [forEmilTld.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"请输入邮箱"];
        return;
    }
    
    [self sendEmial];
    
   
}
-(void)sendEmailSuccess{
    SubView  = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 260 - 250, SCREEN_WIDTH - 20, 210)];
    SubView.backgroundColor = [UIColor whiteColor];
    SubView.layer.cornerRadius = 8;
       // SubView.frame = CGRectMake(10, 150, SCREEN_WIDTH - 20, 170);
        
        UIButton *closeBut = [UIButton buttonWithType:UIButtonTypeCustom];
        closeBut.frame = CGRectMake(SubView.frame.size.width + 10 - 30, 5, 15, 15);
        [closeBut setImage:[UIImage imageNamed:@"icon-关闭按钮"] forState:UIControlStateNormal];
        [closeBut addTarget:self action:@selector(closeForgetView) forControlEvents:UIControlEventTouchUpInside];
        
        [SubView addSubview:closeBut];
        
        UILabel *tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH, 20)];
        tipLabel.text = NSLocalizedString(@"密码已送出", nil);
        tipLabel.font = Bold_FONT(22);
        tipLabel.textAlignment = NSTextAlignmentCenter;
        [SubView addSubview:tipLabel];
        
        UILabel *subLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, SubView.frame.size.width - 20, 40)];
        subLabel.text = NSLocalizedString(@"系统已将您的密码发送至您的电子邮件信箱,并请您妥善保管您的密码", nil);
        subLabel.font = FONT(13);
        subLabel.numberOfLines = 2;
        subLabel.textColor = LYColor(188, 188, 188);
        [SubView addSubview:subLabel];
        
        UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendButton setBackgroundImage:[UIImage imageWithColor:MainColor] forState:UIControlStateNormal];
        sendButton.frame = CGRectMake(10, CGRectGetMaxY(subLabel.frame)+ 20, SubView.frame.size.width - 20, 40);
        sendButton.clipsToBounds = YES;
        [sendButton setTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
        [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sendButton addTarget:self action:@selector(closeForgetView) forControlEvents:UIControlEventTouchUpInside];
        sendButton.layer.cornerRadius = 8;
        [SubView addSubview:sendButton];
        
    [[QWAlertView sharedMask]show:SubView withType:QWAlertViewStyleActionSheetDown];
}
-(void)remember:(UIButton *)button{
    button.selected = !button.selected;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;
}
//获取token
-(void)getCSRF_Token{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@/api/get_csrf_token",TestUrl];
    [PPNetworkHelper GET:url parameters:nil success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [PHUserModel sharedPHUserModel].csrf_token = dic[@"data"][@"token"];
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            [[PHUserModel sharedPHUserModel]showUserInfo];
        }else{
            [CYToast showErrorWithString:@"token获取失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    } getToken:false];
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == UserTld) {
        if (textField.text.length == 0) {
             UserTld.placeholder = NSLocalizedString(@" 电子邮箱", nil);
        }
    }
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        self.view.frame = frame;
        
    }];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        self.view.frame = frame;
        
    }];
    return YES;
}
@end
