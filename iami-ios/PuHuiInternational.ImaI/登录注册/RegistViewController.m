//
//  RegistViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "RegistViewController.h"
#import "WSAuthCode.h"
#import "HTLinkLabel.h"
#import "TermsOfOursViewController.h"//服务条款
#import "PrivateViewController.h"//隐私声明


@interface RegistViewController ()<UITableViewDelegate,UITableViewDataSource,HTLinkLabelDelegate>
{
    UIView *WhiteView;
    UITableView *MainTableView;
    UIButton *RegistButton;//注册按钮
    
    UIButton *boyBut;
    UIButton *girlBut;
     NSString *Date;
    
    WSAuthCode *wsCode;
    UITextField *currentField;
    
    UIButton *currentButton;
    
    UIView *ForgetPwdView;
    
    dispatch_group_t downloadGroup;
    
    UIImageView *maskImage;
    
    UILabel *ValidView;
    
    BOOL isOk;
    
    UIButton *privateBut;//电子邮箱的打勾按钮
    
    UIButton *selectButton;
    
   // UIButton *RegistButton;
    
    NSString *language_id;
    
    DatePickerTool *datePicker;

}
@property (nonatomic,copy) NSMutableArray *BirthArray;
#define cornerR 6
#define registUrl @"/api/doRegister"
#define validRegist @"/member/validateRegister"
@end

@implementation RegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self creatBack];
    _BirthArray = [[NSMutableArray alloc]init];
    [_BirthArray addObject:@"1990▼"];
    [_BirthArray addObject:@"1月▼"];
    [_BirthArray addObject:@"1日▼"];
     Date = @"1990-01-01";
    UIImageView *MainImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    MainImage.image = [UIImage imageNamed:@"iami_splashbg.jpg"];
    MainImage.userInteractionEnabled = YES;
    [self.view addSubview:MainImage];
    
    
    UIImageView *lImage = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 35, Height_StatusBar , 70, 19)];
    lImage.image = [UIImage imageNamed:@"logo280x78"];
    lImage.contentMode = UIViewContentModeScaleAspectFit;
    [MainImage addSubview:lImage];
    // Do any additional setup after loading the view.
    [self creatTable];
    
    UITapGestureRecognizer *tappp = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(maskDimiss:)];
    tappp.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tappp];
   
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)maskDimiss:(UITapGestureRecognizer *)tap{

    if (currentField.tag == 104 || currentField.tag == 105) {
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            
            self.view.frame = frame;
            
        }];
    }
    
    [maskImage removeFromSuperview];
    [currentField resignFirstResponder];
    [currentField endEditing:YES];
    [self.view endEditing:YES];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = YES;

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBar.hidden = NO;
     [datePicker removeFromSuperview];
}
-(void)creatBack{
    
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(5, Height_StatusBar - 5, 40, 31)];
    rightButton.titleLabel.font = Bold_FONT(17);
    rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -10);
    [rightButton setImage:[UIImage imageNamed:@"白色返回"] forState:UIControlStateNormal];
    rightButton.titleLabel.frame = rightButton.frame;
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
   
    [self.view addSubview:rightButton];
}
-(void)backBtnPressed{

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)creatTable{
    CGFloat height = 530;
    if (SCREEN_HEIGHT < 667) {
        height = 440;
    }else if (SCREEN_HEIGHT > 700){
        height = SCREEN_HEIGHT - 200;
    }
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, Height_StatusBar + 40, SCREEN_WIDTH - 20, height   ) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.layer.cornerRadius = 6;
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.bounces = NO;
    MainTableView.showsVerticalScrollIndicator = NO;
    MainTableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:MainTableView];
     [self creatBack];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //return MainTableView.frame.size.height;
    return 600;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    WhiteView = [[UIView alloc]initWithFrame:CGRectMake(20, 0, SCREEN_WIDTH - 40, 600 )];
    WhiteView.backgroundColor = [UIColor whiteColor];
    
    CGFloat textWidth = WhiteView.frame.size.width -35;
    //注册账号
    UILabel *registLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH - 20, 30)];
    registLabel.text = NSLocalizedString(@"注册账号", nil);
    registLabel.textColor = MainColor;
    registLabel.font = Bold_FONT(28);
    registLabel.textAlignment = NSTextAlignmentCenter;
    [WhiteView addSubview:registLabel];
    
  
    
    //电子邮箱textfield
    UITextField *emialTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(registLabel.frame) + 16, textWidth, 36)];
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    emialTld.leftView=emptyView;
    emialTld.leftViewMode=UITextFieldViewModeAlways;
    emialTld.placeholder = NSLocalizedString(@"电子邮箱", nil);
    emialTld.keyboardType = UIKeyboardTypeEmailAddress;
    emialTld.layer.borderWidth = 1.0f;
    emialTld.tag = 100 ;
    emialTld.font = FONT(18);
    emialTld.delegate = self;
    emialTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    emialTld.layer.cornerRadius = cornerR;
    emialTld.autocorrectionType = UITextAutocorrectionTypeNo;
    
    ValidView = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(emialTld.frame)  + 3, textWidth, 5)];
    ValidView.textColor = [UIColor redColor];
    ValidView.font = FONT(10);
    
    
    
    UIButton *alarmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [alarmButton setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton.tag = 70;
    alarmButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
   
    alarmButton.frame = CGRectMake(emialTld.frame.size.width - 50, 9, 40, 18);
    emialTld.rightView = alarmButton;
    emialTld.rightViewMode = UITextFieldViewModeAlways;
    
    privateBut = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) + 5, emialTld.frame.origin.y+ 7, 22, 22);
    [privateBut setImage:[UIImage imageNamed:@"注册未通过"] forState:UIControlStateNormal];
    privateBut.tag = 80;
    [privateBut setImage:[UIImage imageNamed:@"注册通过"] forState:UIControlStateSelected];
    [WhiteView addSubview:privateBut];
    
    //密码textfield
    UITextField *PassWdTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(emialTld.frame) + 16, textWidth, 36)];
    UIView *emptyView1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    PassWdTld.leftView=emptyView1;
    PassWdTld.leftViewMode=UITextFieldViewModeAlways;
    PassWdTld.placeholder = NSLocalizedString(@"密码", nil);
    PassWdTld.secureTextEntry = YES;
    PassWdTld.layer.borderWidth = 1.0f;
    PassWdTld.tag = 101;
    PassWdTld.font = FONT(18);
    PassWdTld.delegate = self;
    [PassWdTld addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    PassWdTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    PassWdTld.layer.cornerRadius = cornerR;
    
    UIButton *alarmButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    //alarmButton1.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) - 50, 9, 18, 18);
    [alarmButton1 setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton1 setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton1.tag = 71;
    alarmButton1.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton1 addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
   // [PassWdTld addSubview:alarmButton1];
    
    alarmButton1.frame = CGRectMake(emialTld.frame.size.width - 50, 9, 40, 18);
    PassWdTld.rightView = alarmButton1;
    PassWdTld.rightViewMode = UITextFieldViewModeAlways;
    
    UIButton *privateBut1 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut1.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) + 5, PassWdTld.frame.origin.y+ 7, 22, 22);
    [privateBut1 setImage:[UIImage imageNamed:@"注册未通过"] forState:UIControlStateNormal];
    [privateBut1 setImage:[UIImage imageNamed:@"注册通过"] forState:UIControlStateSelected];
    privateBut1.tag = 81;
    [WhiteView addSubview:privateBut1];
    
    UITextField *RecheckTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(PassWdTld.frame) + 16, textWidth, 36)];
    UIView *emptyView2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    RecheckTld.leftView=emptyView2;
    RecheckTld.leftViewMode=UITextFieldViewModeAlways;
    RecheckTld.placeholder = NSLocalizedString(@"重新输入新密码", nil);
    RecheckTld.layer.borderWidth = 1.0f;
    RecheckTld.tag = 102;
    RecheckTld.secureTextEntry = YES;
    RecheckTld.font = FONT(18);
    RecheckTld.delegate = self;
     [RecheckTld addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    RecheckTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    RecheckTld.layer.cornerRadius = cornerR;
    
    UIButton *alarmButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    //alarmButton2.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) - 50, 9, 18, 18);
    [alarmButton2 setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton2 setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton2.tag = 72;
    alarmButton2.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton2 addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
    
    alarmButton2.frame = CGRectMake(emialTld.frame.size.width - 50, 9, 40, 18);
    RecheckTld.rightView = alarmButton2;
    RecheckTld.rightViewMode = UITextFieldViewModeAlways;
   // [RecheckTld addSubview:alarmButton2];
    
    UIButton *privateBut2 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut2.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) + 5, RecheckTld.frame.origin.y+ 7, 22, 22);
    [privateBut2 setImage:[UIImage imageNamed:@"注册未通过"] forState:UIControlStateNormal];
    privateBut2.tag = 82;
    [privateBut2 setImage:[UIImage imageNamed:@"注册通过"] forState:UIControlStateSelected];
    [WhiteView addSubview:privateBut2];
    
    UITextField *NickNameTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(RecheckTld.frame) + 16, textWidth, 36)];
    UIView *emptyView3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    NickNameTld.leftView=emptyView3;
    NickNameTld.leftViewMode=UITextFieldViewModeAlways;
    NickNameTld.placeholder = NSLocalizedString(@"昵称", nil);
    NickNameTld.layer.borderWidth = 1.0f;
    NickNameTld.font = FONT(18);
    NickNameTld.tag = 103;
    NickNameTld.delegate = self;
    NickNameTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    NickNameTld.layer.cornerRadius = cornerR;
    
    UIButton *alarmButton3 = [UIButton buttonWithType:UIButtonTypeCustom];
   // alarmButton3.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) - 50, 9, 18, 18);
    [alarmButton3 setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton3 setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton3.tag = 73;
    alarmButton3.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton3 addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
    
    alarmButton3.frame = CGRectMake(emialTld.frame.size.width - 50, 9, 40, 18);
    NickNameTld.rightView = alarmButton3;
    NickNameTld.rightViewMode = UITextFieldViewModeAlways;
    
    UIButton *privateBut3 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut3.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) + 5, NickNameTld.frame.origin.y+ 7, 22, 22);
    [privateBut3 setImage:[UIImage imageNamed:@"注册未通过"] forState:UIControlStateNormal];
    privateBut3.tag = 83;
    [privateBut3 setImage:[UIImage imageNamed:@"注册通过"] forState:UIControlStateSelected];
    [WhiteView addSubview:privateBut3];
    
    [WhiteView addSubview:emialTld];
    [WhiteView addSubview:PassWdTld];
    [WhiteView addSubview:RecheckTld];
    [WhiteView addSubview:NickNameTld];
    
    //性别选择
    UILabel *SexLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(NickNameTld.frame) + 16, 40, 20)];
    SexLabel.text = NSLocalizedString(@"性别", nil);
    SexLabel.textColor = LYColor(140, 140, 140);
    
    boyBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [boyBut setImage:[UIImage imageNamed:@"icon-选择框"] forState:UIControlStateNormal];
    [boyBut setImage:[UIImage imageNamed:@"icon-选择框选中1"] forState:UIControlStateSelected];
    boyBut.selected = YES;
    [boyBut addTarget:self action:@selector(SexClick:) forControlEvents:UIControlEventTouchUpInside];
    boyBut.frame = CGRectMake(70, CGRectGetMaxY(NickNameTld.frame) + 23, 15, 15);
    
    
    UILabel *boyLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, CGRectGetMaxY(NickNameTld.frame) + 20, 30, 20)];
    boyLabel.text = NSLocalizedString(@"男", nil);
    boyLabel.textColor = LYColor(140, 140, 140);
    boyLabel.font = FONT(15);
    
    girlBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [girlBut setImage:[UIImage imageNamed:@"icon-选择框"] forState:UIControlStateNormal];
    [girlBut setImage:[UIImage imageNamed:@"icon-选择框选中1"] forState:UIControlStateSelected];
    [girlBut addTarget:self action:@selector(SexClick:) forControlEvents:UIControlEventTouchUpInside];
    girlBut.frame = CGRectMake(115, CGRectGetMaxY(NickNameTld.frame) + 23, 15, 15);
    
    UILabel *girlLabel = [[UILabel alloc]initWithFrame:CGRectMake(135, CGRectGetMaxY(NickNameTld.frame) + 20, 30, 20)];
    girlLabel.text = NSLocalizedString(@"女", nil);
    girlLabel.textColor = LYColor(140, 140, 140);
    girlLabel.font = FONT(15);
    
    //生日
    UILabel *birthLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(SexLabel.frame) + 30, 40, 20)];
    birthLabel.text = NSLocalizedString(@"生日", nil);
    birthLabel.textColor = LYColor(106, 106, 106);
    
    
     CGFloat width = (SCREEN_WIDTH -90 - 75) / 3;
    
    for (NSInteger i = 0; i < 3; i++) {
        UIButton *birButton = [UIButton buttonWithType:UIButtonTypeCustom];
       // birButton.frame = CGRectMake(90 * i +70, CGRectGetMaxY(SexLabel.frame) + 20, 75, 40);
         birButton.frame = CGRectMake((width + 10) * i +75, CGRectGetMaxY(SexLabel.frame) + 20, width, 40);
        [birButton setTitle:[NSString stringWithFormat:@"%@",_BirthArray[i]] forState:UIControlStateNormal];
        [birButton setTitleColor:LYColor(106, 106, 106) forState:UIControlStateNormal];
        birButton.layer.borderWidth = 1.0f;
        birButton.layer.cornerRadius = cornerR;
        birButton.layer.borderColor = LYColor(223, 223, 223).CGColor;
        [birButton addTarget:self action:@selector(BirthChoose) forControlEvents:UIControlEventTouchUpInside];
        birButton.tag = 200 + i;
        [WhiteView addSubview:birButton];
    }
    
    //手机
    UITextField *PhoneTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(SexLabel.frame) + 60 + 16, textWidth, 36)];
    UIView *emptyView4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    PhoneTld.leftView=emptyView4;
    PhoneTld.leftViewMode=UITextFieldViewModeAlways;
    PhoneTld.placeholder = NSLocalizedString(@"手机", nil);
    PhoneTld.layer.borderWidth = 1.0f;
    PhoneTld.tag= 104;
    PhoneTld.font = FONT(18);
    PhoneTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    PhoneTld.layer.cornerRadius = cornerR;
    PhoneTld.delegate = self;
    
    UIButton *alarmButton4 = [UIButton buttonWithType:UIButtonTypeCustom];
   // alarmButton4.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) - 50, 9, 18, 18);
    [alarmButton4 setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton4 setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton4.tag = 74;
    alarmButton4.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton4 addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
   // [PhoneTld addSubview:alarmButton4];
    
    alarmButton4.frame = CGRectMake(emialTld.frame.size.width - 50, 9, 40, 18);
    PhoneTld.rightView = alarmButton4;
    PhoneTld.rightViewMode = UITextFieldViewModeAlways;
    
    UIButton *privateBut4 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut4.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) + 7, PhoneTld.frame.origin.y+ 7, 22, 22);
    [privateBut4 setImage:[UIImage imageNamed:@"注册未通过"] forState:UIControlStateNormal];
    [privateBut4 setImage:[UIImage imageNamed:@"注册通过"] forState:UIControlStateSelected];
    privateBut4.tag = 84;
    [WhiteView addSubview:privateBut4];
    
    
    //验证码
    UITextField *CodeTld = [[UITextField alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(PhoneTld.frame) + 16, textWidth - 140, 36)];
    UIView *emptyView5=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    CodeTld.leftView=emptyView5;
    CodeTld.leftViewMode=UITextFieldViewModeAlways;
    CodeTld.placeholder = NSLocalizedString(@"验证码", nil);
    CodeTld.layer.borderWidth = 1.0f;
    CodeTld.tag= 105;
    CodeTld.font = FONT(18);
    CodeTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    CodeTld.layer.cornerRadius = cornerR;
    CodeTld.delegate = self;
    
    wsCode = [[WSAuthCode alloc]initWithFrame:CGRectMake(CGRectGetMaxX(CodeTld.frame) + 5, CGRectGetMaxY(PhoneTld.frame) + 15, 130, 40)];
    [WhiteView addSubview:wsCode];
    
    
    UIButton *alarmButton5 = [UIButton buttonWithType:UIButtonTypeCustom];
    alarmButton5.frame = CGRectMake(CGRectGetMaxX(emialTld.frame) - 50, 9, 18, 18);
    [alarmButton5 setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
    [alarmButton5 setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
    alarmButton5.tag = 75;
    
//    alarmButton5.imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [alarmButton5 addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
//    [CodeTld addSubview:alarmButton5];
    
    [WhiteView addSubview:CodeTld];
    [WhiteView addSubview:PhoneTld];
    [WhiteView addSubview:SexLabel];
    [WhiteView addSubview:boyLabel];
    [WhiteView addSubview:boyBut];
    [WhiteView addSubview:girlLabel];
    [WhiteView addSubview:girlBut];
    [WhiteView addSubview:birthLabel];
    
    //注册按钮
    RegistButton = [UIButton buttonWithType:UIButtonTypeCustom];
    RegistButton.frame = CGRectMake(20, CGRectGetMaxY(CodeTld.frame) +20, textWidth, 50);
    RegistButton.layer.cornerRadius = 10;
    RegistButton.clipsToBounds = YES;
    RegistButton.enabled = NO;
   // [RegistButton setBackgroundColor:LYColor(230, 183, 94)];
    [RegistButton setBackgroundColor:LYColor(223, 223, 223)];
    [RegistButton setTitle:NSLocalizedString(@"注册", nil) forState:UIControlStateNormal];
    [RegistButton addTarget:self action:@selector(regist) forControlEvents:UIControlEventTouchUpInside];
    [RegistButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [WhiteView addSubview:RegistButton];
    //注意事项和条款
//    UILabel *WaringLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, CGRectGetMaxY(RegistButton.frame) + 10, textWidth, 40)];
//    WaringLabel.numberOfLines = 2;
    
    selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectButton.frame = CGRectMake(24, CGRectGetMaxY(RegistButton.frame) + 13, 13, 13);
    [selectButton setBackgroundImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [selectButton setBackgroundImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [selectButton addTarget:self action:@selector(primiteTerm) forControlEvents:UIControlEventTouchUpInside];
    [WhiteView addSubview:selectButton];
    
    
 
    HTLinkLabel *label = [[HTLinkLabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(selectButton.frame) + 3, CGRectGetMaxY(RegistButton.frame) + 12, textWidth + 10, 50)];
    label.textColor =  LYColor(106, 106, 106);
    label.delegate   = self;
    HTAttributedString * attributedStr = [[HTAttributedString alloc] init];
    //label.font = FONT(18);
    NSMutableAttributedString * attrString = nil;
    
    NSString * string = [NSString stringWithFormat:NSLocalizedString(@"如果注册，即表示你同意服务条款，隐私政策\n其他人将可以透过提供的电子邮件或电话号码找到你",nil)];
    
    attrString = [[NSMutableAttributedString alloc] initWithString:string];
    [attrString addAttribute:NSFontAttributeName value:FONT(12) range:NSMakeRange(0, string.length)];
    [attrString addAttributes:@{NSKernAttributeName:@(0.5f)} range:NSMakeRange(0, [attrString length])];
    
  
    
    NSString *language = [NSLocale preferredLanguages].firstObject;
     if ([language hasPrefix:@"zh"]) {
        
         [attrString addAttribute:NSFontAttributeName
                            value:Bold_FONT(13)
                            range:NSMakeRange(11, 4)];
         [attrString addAttribute:NSForegroundColorAttributeName
                            value:[UIColor blackColor]
                            range:NSMakeRange(11, 4)];
         
         [attrString addAttribute:NSFontAttributeName
                            value:Bold_FONT(13)
                            range:NSMakeRange(16, 4)];
         [attrString addAttribute:NSForegroundColorAttributeName
                            value:[UIColor blackColor]
                            range:NSMakeRange(16, 4)];
         
         
         HTLink *link1 = [[HTLink alloc] init];
         link1.linkRange = NSMakeRange(11, 4);
         link1.linkValue = @"1";
         
         HTLink *link2 = [[HTLink alloc] init];
         link2.linkRange = NSMakeRange(16, 4);
         link2.linkValue = @"2";
         
     attributedStr.links = @[link1,link2];
    }
    
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];//行间距
    label.textAlignment = NSTextAlignmentCenter;
    [attrString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [attrString length])];
    
    attributedStr.AttributedString = attrString;
    
    label.AttributedString = attributedStr;
    [WhiteView addSubview:label];
    
            return WhiteView;
   
}
-(void)primiteTerm{
    selectButton.selected = !selectButton.selected;
    
    if (selectButton.selected) {
        [RegistButton setBackgroundColor:MainColor];
        RegistButton.enabled = YES;
    }else{
        [RegistButton setBackgroundColor:LYColor(223, 223, 223)];
        RegistButton.enabled = NO;
    }
}
-(void)didClickLink:(HTLink *)link{
    if ([link.linkValue isEqualToString:@"1"]) {
        TermsOfOursViewController *con = [[TermsOfOursViewController alloc]init];
        con.m_showBackBt = YES;
        con.type = @"1";
        [self.navigationController pushViewController:con animated:YES];
    }else{
        PrivateViewController *con = [[PrivateViewController alloc]init];
        con.m_showBackBt = YES;
         con.type = @"1";
        [self.navigationController pushViewController:con animated:YES];
    }
}
/**
 性别选择
 */
-(void)SexClick:(UIButton *)button{
    
    if (button.selected) {
        
    }else{
        boyBut.selected = !boyBut.selected;
        girlBut.selected = !girlBut.selected;
    }
    
}
-(void)BirthChoose{
    datePicker = [[DatePickerTool alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 250, WhiteView.frame.size.width -20, 200)];
    __block DatePickerTool *blockPick = datePicker;
    datePicker.callBlock = ^(NSString *pickDate) {
        
        NSLog(@"%@",pickDate);
        
        if (pickDate) {
             Date = pickDate;
            [_BirthArray removeAllObjects];
            MainTableView.scrollEnabled = YES;

            NSArray *array =   [pickDate componentsSeparatedByString:@"-"];
            UIButton *but1 = [self.view viewWithTag:200];
            UIButton *but2 = [self.view viewWithTag:201];
            UIButton *but3 = [self.view viewWithTag:202];
            
            [but1 setTitle:[NSString stringWithFormat:@"%@年▼",array[0]] forState:UIControlStateNormal];
            [but2 setTitle:[NSString stringWithFormat:@"%@月▼",array[1]] forState:UIControlStateNormal];
            [but3 setTitle:[NSString stringWithFormat:@"%@日▼",array[2]] forState:UIControlStateNormal];
        }else{
           // [[QWAlertView sharedMask]dismiss];
            MainTableView.scrollEnabled = YES;
        }
        
        [blockPick removeFromSuperview];
    };
    [self.view addSubview:datePicker];
    MainTableView.scrollEnabled = NO;

    
}
-(void)regist{
    
    UITextField *textF = [self.view viewWithTag:105];
    BOOL isOk = [wsCode startAuthWithString:textF.text];

    if (isOk) {

    }else{
        [CYToast showErrorWithString:@"验证码错误"];
        //这里面写验证失败之后的动作.
        [wsCode reloadAuthCodeView];
        textF.text = @"";
        return;
    }

    UITextField *emialTld = [self.view viewWithTag:100];
    UITextField *passwdTld = [self.view viewWithTag:101];
    UITextField *recheckTld = [self.view viewWithTag:102];
    UITextField *nicknameTld = [self.view viewWithTag:103];
    UITextField *phoneTld = [self.view viewWithTag:104];

    NSString *phoneRegex = @"^1[3|4|5|7|8][0-9]\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];//匹配是否为手机号
    BOOL isPhoneNumber = [phoneTest evaluateWithObject:phoneTld.text];


    if ([emialTld.text isEqualToString:@""] ||[passwdTld.text isEqualToString:@""]||[recheckTld.text isEqualToString:@""]||[nicknameTld.text isEqualToString:@""]||[phoneTld.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"请填写正确个人信息"];
        return;
    }

    else if(![passwdTld.text isEqualToString:recheckTld.text]){
        [CYToast showErrorWithString:@"确认密码错误"];
        return;
    }

    NSString *gender = nil;
    if (boyBut.selected) {
        gender = @"M";
    }else{
        gender = @"F";
    }
   
    if (!language_id || language_id.length == 0) {
        NSString *current = [self getPreferredLanguage];
        if ([current hasPrefix:@"zh"]) {
            language_id = @"zh-TW";
        }else{
            language_id = @"english";
        }
    }
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,registUrl];
    NSDictionary *paramDic =@{@"email":emialTld.text,@"password":passwdTld.text,@"repassword":recheckTld.text,@"nickname":nicknameTld.text,@"gender":gender,@"mobile":phoneTld.text,@"birth":Date,@"language_id":language_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"%@",dic);
        if ([dic[@"status"] isEqualToString:@"success"]) {
//            [CYToast showSuccessWithString:@"注册成功"];
//            [self.navigationController popViewControllerAnimated:YES];
            [self showRegistMessage];
        }else{
             [CYToast showErrorWithString:dic[@"message"]];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)showRegistMessage{
    /**
     弹出忘记密码的View
     */
    
        ForgetPwdView = [[UIView alloc]initWithFrame:CGRectMake(10,SCREEN_HEIGHT  / 2 - 100, SCREEN_WIDTH -20, 180)];
        
        ForgetPwdView.backgroundColor = [UIColor whiteColor];
      ForgetPwdView.layer.cornerRadius = 8;
    
        UIButton *closeBut = [UIButton buttonWithType:UIButtonTypeCustom];
        closeBut.frame = CGRectMake(ForgetPwdView.frame.size.width + 10 - 30, 5, 15, 15);
        [closeBut setImage:[UIImage imageNamed:@"icon-关闭按钮"] forState:UIControlStateNormal];
        [closeBut addTarget:self action:@selector(forgetSend) forControlEvents:UIControlEventTouchUpInside];
        
        [ForgetPwdView addSubview:closeBut];
        
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, 20)];
        titleLabel.text = @"注册成功";
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = Bold_FONT(22);
        [ForgetPwdView addSubview:titleLabel];
        
        UILabel *subsLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,55 , ForgetPwdView.frame.size.width - 20, 40)];
        subsLabel.text = @"您已经完成注册\n请登陆帐号开始使用最佳的社交软件IAMI";
        subsLabel.font = FONT(13);
        subsLabel.numberOfLines = 0;
    subsLabel.textAlignment = NSTextAlignmentCenter;
        subsLabel.textColor = LYColor(188, 188, 188);
        [ForgetPwdView addSubview:subsLabel];
        
        SZTextView *forEmilTld = [[SZTextView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(subsLabel.frame) + 10, ForgetPwdView.frame.size.width - 20, 36)];
        forEmilTld.placeholder = @" 电子邮件";
        forEmilTld.layer.borderWidth = 1.0f;
        forEmilTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
        forEmilTld.layer.cornerRadius = cornerR;
        //[SubView addSubview:forEmilTld];
    
        UILabel *birthLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(forEmilTld.frame) + 22, 40, 20)];
        birthLabel.text = @"生日";
        birthLabel.textColor = LYColor(106, 106, 106);
        
        
//        for (NSInteger i = 0; i < 3; i++) {
//            UIButton *birButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            birButton.frame = CGRectMake(90 * i +60, CGRectGetMaxY(forEmilTld.frame) + 15, 75, 40);
//            [birButton setTitle:[NSString stringWithFormat:@"%@",_BirthArray[i]] forState:UIControlStateNormal];
//            [birButton setTitleColor:LYColor(106, 106, 106) forState:UIControlStateNormal];
//            birButton.layer.borderWidth = 1.0f;
//            birButton.layer.cornerRadius = cornerR;
//            birButton.layer.borderColor = LYColor(223, 223, 223).CGColor;
//            [birButton addTarget:self action:@selector(BirthChoose) forControlEvents:UIControlEventTouchUpInside];
//            birButton.tag = 100 + i;
//            //[SubView addSubview:birButton];
//        }
        // [SubView addSubview:birthLabel];
        
        SZTextView *forPhoneTld = [[SZTextView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(birthLabel.frame) + 22, ForgetPwdView.frame.size.width - 20, 36)];
        forPhoneTld.placeholder = @" 手机";
        forPhoneTld.layer.borderWidth = 1.0f;
        forPhoneTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
        forPhoneTld.layer.cornerRadius = cornerR;
        forPhoneTld.delegate = self;
        // [SubView addSubview:forPhoneTld];
        
        UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendButton setBackgroundImage:[UIImage imageWithColor:MainColor] forState:UIControlStateNormal];
        sendButton.frame = CGRectMake(10, CGRectGetMaxY(forEmilTld.frame)-30, ForgetPwdView.frame.size.width - 20, 40);
        sendButton.clipsToBounds = YES;
        [sendButton setTitle:@"确定" forState:UIControlStateNormal];
        [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sendButton addTarget:self action:@selector(forgetSend) forControlEvents:UIControlEventTouchUpInside];
        sendButton.layer.cornerRadius = 8;
       // [SubView addSubview:sendButton];
        
        [ForgetPwdView addSubview:sendButton];
    
    [[QWAlertView sharedMask]show:ForgetPwdView withType:QWAlertViewStyleAlert];
    
    
    
}
-(void)forgetSend{
    [[QWAlertView sharedMask]dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - 注册添加提醒事项
-(void)showNickAlarm:(UIButton *)button{
    if (maskImage) {
        [maskImage removeFromSuperview];
        maskImage = nil;
        return;
    }
   

    
    [maskImage removeFromSuperview];
    NSInteger buttontag = button.tag;
    NSString *string = nil;
    switch (buttontag) {
        case 70:
            string = NSLocalizedString(@"請填寫可收驗證信的信箱，切勿隨意填寫", nil);
            break;
        case 71:
            string = NSLocalizedString(@"請填寫6~12位數密碼、需含英文大小寫、數字", nil);
            break;
        case 72:
            string = NSLocalizedString(@"請再次確認您的密碼", nil);
            break;
        case 73:
            string = NSLocalizedString(@"此為您的IAMI個人暱稱", nil);
            break;
        case 74:
            string = NSLocalizedString(@"請填寫您的手機號碼", nil);
            break;
        case 75:
            string = NSLocalizedString(@"请输入验证码", nil);
            break;
            
        default:
            break;
    }
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect:button.bounds toView:window];
    
   maskImage = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x - 116, rect.origin.y + 20, 150, 60)];
    maskImage.image = [UIImage imageNamed:@"黑_96X280"];
    maskImage.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 140, 50)];
    label.text = string;
    label.textColor = [UIColor whiteColor];
    label.font = FONT(13);
    label.numberOfLines = 0;
    [maskImage addSubview:label];
    
    
    [self.view addSubview:maskImage];
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    [UIView animateWithDuration:0.3 animations:^{
      
                CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
       
               self.view.frame = frame;
        
    }];
    return YES;
    
}
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
//    //获取textfield当前的位置，判断是否需要偏移view
//    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
//    CGRect rect=[textField convertRect: textField.bounds toView:window];
//
//    if (rect.origin.y > SCREEN_HEIGHT - 258 ) {
//        [UIView animateWithDuration:0.25f animations:^{
//            [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y- 258, self.view.frame.size.width, self.view.frame.size.height)];
//        }];
//    }
//
//    return YES;
//
//}

//-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    currentField.layer.borderColor = LYColor(223, 223, 223).CGColor;
//    currentField = textField;
//    currentField.layer.borderColor =LYColor(230, 183, 94).CGColor;
//
//    UIButton *button = [self.view viewWithTag:textField.tag - 30];
//    currentButton.selected = NO;
//    currentButton = button;
//    currentButton.selected = YES;
//
//}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    //_currentTF = textField;
     self.currentTF = textField;
    currentField.layer.borderColor = LYColor(223, 223, 223).CGColor;
    currentField = textField;
    currentField.layer.borderColor =MainColor.CGColor;
   

    UIButton *button = [self.view viewWithTag:textField.tag - 30];
    currentButton.selected = NO;
    currentButton = button;
    currentButton.selected = YES;
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    switch (textField.tag) {
        case 100:
        {
            //UIButton *button = [self.view viewWithTag:80];
            [self valRegistWith:textField.text];
           // button.selected = isOk;
            break;
        }
        case 101:
        {
            UIButton *button = [self.view viewWithTag:81];
            button.selected =   [self judgePassWordLegal:textField.text];
            if (button.selected == NO) {
                  [wsCode reloadData];
            }
            break;
        }
        case 102:
        {
            UIButton *button = [self.view viewWithTag:82];
            UITextField *text = [self.view viewWithTag:101];
            
            UIButton *passbut = [self.view viewWithTag:81];
            if (passbut.selected == NO ) {
                button.selected = NO;
                  [wsCode reloadData];
            }
            
            if ([textField.text isEqualToString:text.text] == YES && text.text.length > 0) {
                button.selected = YES;
            }else{
                button.selected = NO;
                  [wsCode reloadData];
            }
            break;
        }
        case 103:
        {
            UIButton *button = [self.view viewWithTag:83];
            if (textField.text.length > 0) {
                
                button.selected = YES;
            }else{
                button.selected = NO;
            }
            break;
        }
        case 104:
        {
            UIButton *button = [self.view viewWithTag:84];
            if (textField.text.length > 0) {
                button.selected = YES;
            }else{
                button.selected = NO;
            }
            break;
        }
        default:
            break;
    }
    
    
//    if (currentField.tag == 104 || currentField.tag == 105) {
////        if (currentField.tag == 104|| currentField.tag == 105) {
////            NSLog(@"1");
////
////        }else{
////             NSLog(@"2");
////            [UIView animateWithDuration:0.3 animations:^{
////
////                CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
////
////                self.view.frame = frame;
////
////            }];
////        }
//    }else{
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            
            self.view.frame = frame;
            
        }];
   // }
   
    return YES;
}
//限制输入字符不能超过12个
-(void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.tag == 101 || textField.tag == 102) {
        CGFloat maxLength = 12;
        NSString *toBeString = textField.text;
        
        //获取高亮部分
        UITextRange *selectedRange = [textField markedTextRange];
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        if (!position || !selectedRange)
        {
            if (toBeString.length > maxLength)
            {
                NSRange rangeIndex = [toBeString rangeOfComposedCharacterSequenceAtIndex:maxLength];
                if (rangeIndex.length == 1)
                {
                    textField.text = [toBeString substringToIndex:maxLength];
                }
                else
                {
                    NSRange rangeRange = [toBeString rangeOfComposedCharacterSequencesForRange:NSMakeRange(0, maxLength)];
                    textField.text = [toBeString substringWithRange:rangeRange];
                }
            }
        }
    }
   
}

-(BOOL)judgePassWordLegal:(NSString *)pass{
    
    BOOL result ;
    
    // 判断长度大于8位后再接着判断是否同时包含数字和大小写字母
    
    NSString * regex =@"(?![0-9A-Z]+$)(?![0-9a-z]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$";
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    result = [pred evaluateWithObject:pass];
    
    //NSLog(@"%hhd",result);
    return result;
    
}
-(void)valRegistWith:(NSString *)string{
    
    
    if ([[self getPreferredLanguage] isEqualToString:@"zh-Hans-CN"]){
        
        [[NSUserDefaults standardUserDefaults] setObject:@[@"zh-Hans-CN"] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"zh-CN";
    }
    else if ([[self getPreferredLanguage] isEqualToString:@"zh-Hant-CN"]){
        NSArray *lans = @[@"zh-Hant-CN"];
        [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"zh-TW";
    }// ([[self getPreferredLanguage] isEqualToString:@"en"])
    else{
        [[NSUserDefaults standardUserDefaults] setObject:@[@"en"] forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        language_id=@"english";
    }
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"email":string};
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,validRegist];
    [PPNetworkHelper setValue:language_id forHTTPHeaderField:@"set-lg"];

    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"]isEqualToString:@"failed"]) {
            ValidView.text = dic[@"message"];
            [UIView animateWithDuration:1.0f animations:^{
              [WhiteView addSubview:ValidView];
            }];
            
        NSTimer  *myTimer =  [NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(dismissV) userInfo:nil repeats:NO];
            [[NSRunLoop currentRunLoop]addTimer:myTimer forMode:NSDefaultRunLoopMode];
            [wsCode reloadData];
        }
        else{
            privateBut.selected = YES;
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];

  
}
-(void)dismissV{
    [UIView animateWithDuration:1.0f animations:^{
        [ValidView removeFromSuperview];
        ValidView = nil;
    }];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    MainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
//    if (scrollView != searchTableview) {
//        [_searchBar resignFirstResponder];
//        [searchTableview removeFromSuperview];
////    }
//    [UIView animateWithDuration:1.0f animations:^{
//        [ValidView removeFromSuperview];
//    }];
    
        [UIView animateWithDuration:0.3f animations:^{
            [maskImage removeFromSuperview];
            maskImage = nil;
            [self.view endEditing:YES];
        }];
    

    
}
@end
