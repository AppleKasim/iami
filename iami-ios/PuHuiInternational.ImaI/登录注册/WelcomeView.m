//
//  WelcomeView.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/7.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "WelcomeView.h"

@interface WelcomeView ()
{
     UIView *WhiteView;
    
    UIButton *ChargeButton;
    
    UIButton *RegistButton;
}
@end

@implementation WelcomeView

-(id)initWithFrame:(CGRect)frame{
    //[super initWithFrame:frame];
    self = [super initWithFrame:frame];
    [self creatUI];
    return self;
}
-(void)creatUI{
    UIImageView *MainImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    MainImage.image = [UIImage imageNamed:@"iami_splashbg.jpg"];
    MainImage.userInteractionEnabled = YES;
    
    WhiteView = [[UIView alloc]initWithFrame:CGRectMake(10, 100, SCREEN_WIDTH - 20, 330)];
    WhiteView.backgroundColor = [UIColor whiteColor];
    WhiteView.layer.cornerRadius = 8;
    
    UIImageView *topView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH / 2 - 20, 20, 40, 15)];
    topView.image = [UIImage imageNamed:@"logo_shadow"];
    
    [self addSubview:MainImage];
    [MainImage addSubview:WhiteView];
    [MainImage addSubview:topView];
    [self creatSubView];
}
-(void)creatSubView{
    UILabel *tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH - 20, 30)];
    tipLabel.text = @"欢迎回来";
    tipLabel.textColor = LYColor(243, 163, 59);
    tipLabel.textAlignment = NSTextAlignmentCenter;
    [WhiteView addSubview:tipLabel];
    
    UIImageView *mainImage = [[UIImageView alloc]initWithFrame:CGRectMake(WhiteView.frame.size.width / 2 -35, CGRectGetMaxY(tipLabel.frame) + 15, 70, 70)];
    mainImage.image = [UIImage imageNamed:@"欢迎头像"];
    [WhiteView addSubview:mainImage];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(WhiteView.frame.size.width / 2 - 120, CGRectGetMaxY(mainImage.frame) + 15, 240, 20)];
    nameLabel.text = @"萌萌哒";
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = Bold_FONT(18);
    [WhiteView addSubview:nameLabel];
    
    UILabel *tipLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(40, CGRectGetMaxY(nameLabel.frame)  + 6, 50, 10)];
    tipLabel1.text = @"记住我";
    tipLabel1.textColor = LYColor(223, 223, 223);
    tipLabel1.font = FONT(13);
    [WhiteView addSubview:tipLabel1];
    
    UILabel *tipLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(90, CGRectGetMaxY(nameLabel.frame)  + 6, 100, 10)];
    tipLabel2.text = @"忘记密码";
    tipLabel2.textColor = LYColor(244, 169, 69);
    tipLabel2.font = FONT(13);
    [WhiteView addSubview:tipLabel2];
    
    ChargeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [ChargeButton setBackgroundImage:[UIImage imageWithColor:LYColor(244, 169, 69)] forState:UIControlStateNormal];
    ChargeButton.frame = CGRectMake(30, CGRectGetMaxY(tipLabel1.frame)+ 20, SCREEN_WIDTH - 80, 40);
    ChargeButton.clipsToBounds = YES;
    [ChargeButton setTitle:@"进入iami" forState:UIControlStateNormal];
    [ChargeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [ChargeButton addTarget:self action:@selector(LogIn) forControlEvents:UIControlEventTouchUpInside];
    [WhiteView addSubview:ChargeButton];
    
    RegistButton = [UIButton buttonWithType:UIButtonTypeCustom];
    RegistButton.frame = CGRectMake(30, CGRectGetMaxY(ChargeButton.frame) + 20, SCREEN_WIDTH - 80, 40);
    [RegistButton setTitle:@"登出" forState:UIControlStateNormal];
    [RegistButton setBackgroundImage:[UIImage imageWithColor:LYColor(223, 223, 223)] forState:UIControlStateNormal];
    [RegistButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [RegistButton addTarget:self action:@selector(Regist) forControlEvents:UIControlEventTouchUpInside];
    [WhiteView addSubview:RegistButton];
    
    
}
@end
