//
//  InfoTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InfoModel.h"

@interface InfoTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *MainLabel;
@property (nonatomic,strong) UILabel *TimeLabel;
@property (nonatomic,strong) UIButton *TypeButton;

-(void)setInfomodel:(InfoModel *)model;
@end
