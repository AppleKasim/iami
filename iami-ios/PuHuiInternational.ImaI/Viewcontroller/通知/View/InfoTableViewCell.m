//
//  InfoTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InfoTableViewCell.h"

@implementation InfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setInfomodel:(InfoModel *)model{
    self.backgroundColor = LYColor(223, 223, 223);

    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 16, 50)];
    view.layer.cornerRadius = 8;
    view.clipsToBounds = YES;
    view.backgroundColor = [UIColor whiteColor];
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 30, 30)];
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",model.UserImage];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 15;
    _UserImage.clipsToBounds = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    
    _MainLabel = [[UILabel alloc]initWithFrame:CGRectMake(48, 10, SCREEN_WIDTH - 70, 30)];
//    if ([model.MainLabel isEqualToString:@"Comment"]) {
//        model.MainLabel = NSLocalizedString(@"在你的帖文下留言", nil);
//    }
    _MainLabel.font  = FONT(13);
    _MainLabel.numberOfLines = 2;
    _MainLabel.text = [NSString stringWithFormat:@"%@ %@",model.nickname,model.MainLabel];
    
    _TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 115, 30, 90, 15)];
    _TimeLabel.textAlignment = NSTextAlignmentRight;
    _TimeLabel.font = FONT(9);
    _TimeLabel.text = [NSString stringWithFormat:@"%@",model.TimeLabel];
    _TimeLabel.textColor = LYColor(223, 223, 223);
    
    [view addSubview:_UserImage];
    [view addSubview:_MainLabel];
    [view addSubview:_TimeLabel];
    
    [self.contentView addSubview:view];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



















@end
