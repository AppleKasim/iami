//
//  PostDetailViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/5.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "PostDetailViewController.h"
#import "TagFriedTableViewCell.h"
#import "MainTableViewCell.h"
#import "VideoTableViewCell.h"//video的cell
#import "FriendInformationViewController.h"//好友个人主页
#import "TagFriendViewController.h"//tag好友选择页面
#import "ShareFriendViewController.h"//分享页面
#import "ShortMovieViewController.h"//五秒影片
#import "MainUserViewController.h"//个人主页
#import "SearchViewController.h"//search
#import "ShareTableViewCell.h"//分享cell
#import "DeclareAbnormalAlertView.h"//弹出分享框

@interface PostDetailViewController ()<UITableViewDelegate,UITableViewDataSource,DeclareAbnormalAlertViewDelegate,UITextFieldDelegate>
{
    UITableView *MainTableView;
    
    UITextField *_CommentTld;
    
    SZTextView *editText;
    
    UIView *view;
    
    CGFloat _currentKeyboardH;
    CGFloat _transformY;

}
@property (nonatomic,copy) NSMutableArray *DataArray;
@property (nonatomic,copy) NSMutableArray *CommentArray;

#define getPost @"/post/getPosts"
#define GetComment @"/post/getComments"//获得所有评论
#define LikeCommetn @"/like/toggleCommentLike"//评论点赞
#define CommentUrl @"/post/doComment"//评论
#define LikeUrl @"/like/togglePostLike"//贴文按赞
#define CommentUrl @"/post/doComment"//评论
#define PostUrl @"/post/doPost" //发布消息
#define CollectPost @"/post/collectPost"//收藏帖文
#define DeletePost @"/post/delPost"//删除帖文
#define EDPost @"/post/editPost"//编辑帖文

@end

@implementation PostDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _DataArray = [[NSMutableArray alloc]init];
    _CommentArray = [[NSMutableArray alloc]init];
    self.view.backgroundColor = LYColor(223, 223, 223);
    
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)dealloc{
     [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)keyboardWasShown:(NSNotification *) notif{
    NSValue *keyBoardBeginBounds=[[notif userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
    
    //获取键盘弹出后的Rect
    NSValue *keyBoardEndBounds=[[notif
                                 userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect  endRect=[keyBoardEndBounds CGRectValue];
    
    CGFloat curkeyBoardHeight = [[[notif userInfo] objectForKey:@"UIKeyboardBoundsUserInfoKey"] CGRectValue].size.height;
    
    CGFloat height = endRect.size.height;
     CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
    //CGFloat deltaY=curkeyBoardHeight-beginRect.origin.y;
    NSLog(@"%f--%f--%f",beginRect.size.height,endRect.size.height,curkeyBoardHeight);
    

   
    
    if (IsiPhoneX) {
        [UIView animateWithDuration:0.3f animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x, curkeyBoardHeight + 22, view.frame.size.width, view.frame.size.height)];
        }];
    }else{
        [UIView animateWithDuration:0.3f animations:^{
            [view setFrame:CGRectMake(view.frame.origin.x, curkeyBoardHeight +Height_TabBar+1, view.frame.size.width, view.frame.size.height)];
        }];
    }
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.1 animations:^{

        CGRect frame = self.view.frame;

        frame.origin.y = Height_NavBar;

        view.frame = CGRectMake(0, SCREEN_HEIGHT - 36 - Height_TabBar- Height_NavBar, SCREEN_WIDTH, 36) ;

    }];
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch

{
    // 过滤掉UIButton，也可以是其他类型
    if ( [touch.view isKindOfClass:[UIButton class]])
    {
        return NO;
    }
   
        return YES;
        
    
}
- (void)keyboardHide:(UITapGestureRecognizer *)tap{
    
}
-(void)dimissK:(UITapGestureRecognizer *)tap{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.1 animations:^{
        
        CGRect frame = self.view.frame;
        
        frame.origin.y = Height_NavBar;
        
        view.frame = CGRectMake(0, SCREEN_HEIGHT - 36 - Height_TabBar- Height_NavBar, SCREEN_WIDTH, 36) ;
        
    }];
}
-(void)didCloseWindow{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.1 animations:^{
        
        CGRect frame = self.view.frame;
        
        frame.origin.y = Height_NavBar;
        
        view.frame = CGRectMake(0, SCREEN_HEIGHT - 36 - Height_TabBar- Height_NavBar, SCREEN_WIDTH, 36) ;
        
    }];
}
-(void)creatCommentView{
    view = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT - 36 - Height_TabBar- Height_NavBar, SCREEN_WIDTH, 36)];
    view.backgroundColor = [UIColor whiteColor];
    
    _CommentTld = [[UITextField alloc]initWithFrame:CGRectMake(15, 3, SCREEN_WIDTH - 80 - 15, 30)];
    _CommentTld.placeholder = NSLocalizedString(@"请输入评论", nil);
    _CommentTld.font = FONT(15);
    _CommentTld.layer.borderWidth = 1.0f;
    _CommentTld.layer.borderColor = MainColor.CGColor;
    _CommentTld.layer.cornerRadius = 8;
    _CommentTld.delegate = self;
    [view addSubview:_CommentTld];
    
    UIButton *_CommentBut = [UIButton buttonWithType:UIButtonTypeCustom];
    _CommentBut.frame = CGRectMake(SCREEN_WIDTH - 70, 6, 60, 25);
    [_CommentBut setTitle:NSLocalizedString(@"评论", nil) forState:UIControlStateNormal];
    _CommentBut.backgroundColor = MainColor;
    [_CommentBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _CommentBut.titleLabel.font = FONT(15);
    _CommentBut.layer.cornerRadius = 10;
    _CommentBut.clipsToBounds = YES;
    [_CommentBut addTarget:self action:@selector(postComment) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:_CommentBut];
    
    [self.view addSubview:view];
   // [self.view bringSubviewToFront:view];
}
#pragma  mark UITableView && Delegate
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH   , SCREEN_HEIGHT - Height_NavBar - Height_TabBar - 40 ) style:UITableViewStylePlain];
    MainTableView.backgroundColor =LYColor(223, 223, 223);
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.bounces = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dimissK:)];
    [MainTableView addGestureRecognizer:tap];
    
    [MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[VideoTableViewCell class] forCellReuseIdentifier:@"videocell"];
    [MainTableView registerClass:[ShareTableViewCell class] forCellReuseIdentifier:@"sharecell"];
    [MainTableView registerClass:[TagFriedTableViewCell class] forCellReuseIdentifier:@"commcell"];
    
    [self.view addSubview:MainTableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return _CommentArray.count ;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MainModel *main =_DataArray[indexPath.row];
//        if (main.isShowCommentV) {
//            return main.cellHeight + 70;
//        }else{
//            return   main.cellHeight;
//        }
        return main.cellHeight ;
    }
    else if (indexPath.section ==1){
            CommentModel *model = _CommentArray[indexPath.row];
            return model.cellHeight + 45;
        
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MainModel *main =_DataArray[indexPath.row];
//        if (main.isShowCommentV) {
//            return main.cellHeight + 70;
//        }else{
//            return   main.cellHeight;
//        }
        return main.cellHeight  ;
    }
    else if (indexPath.section ==1){
        CommentModel *model = _CommentArray[indexPath.row];
        return model.cellHeight + 45;
        
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MainModel *model = _DataArray[indexPath.row];
        if ([model.post_type isEqualToString:@"video"]) {
            
            VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videocell" forIndexPath:indexPath];
            
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            
            cell.opaque = YES;
            [cell setMode:model];
            
            cell.CommentView.hidden = YES;

            cell.CommentTld.delegate = self;
            
            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.MoreButton.hidden = YES;
           // [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
            
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap1];
            
            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
            cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            cell.CommentTld.tag = 300 + indexPath.section;
            cell.MoreCommentBut.tag = 200 + indexPath.section;
            cell.CommentBut.tag = 200 + indexPath.section;
            cell.CommentButton.tag = 200 + indexPath.section;
            cell.LikeButton.tag = 200 + indexPath.section;
            cell.UserImage.tag = 200 + indexPath.section;
            cell.ForwardButton.tag = 200 + indexPath.section;
            cell.CollectButton.tag = 200 + indexPath.section;
            cell.UserName.tag = 200 +indexPath.section;
            cell.MoreButton.tag = 200 + indexPath.section;
            return cell;
        }
        else if ([model.post_type isEqualToString:@"share"]){
            ShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sharecell" forIndexPath:indexPath];
            
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            cell.opaque = YES;
            
            if ([model.shareType isEqualToString:@"video"]) {
                [cell setVideoMode:model];
            }else{
                [cell setMode:model];
            }
            
            cell.CommentView.hidden = YES;

            cell.CommentTld.delegate = self;
            
            
            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.MoreButton.hidden = YES;

            
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
            [cell.shareImageView addGestureRecognizer:tap1];
            UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
            [cell.shareLabel addGestureRecognizer:tap2];
            
            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
            cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
            UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap3];
            
            
            
            cell.CommentTld.tag = 300 + indexPath.section;
            cell.MoreCommentBut.tag = 200 + indexPath.section;
            cell.CommentBut.tag = 200 + indexPath.section;
            cell.CommentButton.tag = 200 + indexPath.section;
            cell.LikeButton.tag = 200 + indexPath.section;
            cell.UserImage.tag = 200 + indexPath.section;
            // cell.UserImage.tag = 200 + indexPath.row;
            cell.shareLabel.tag = 200 + indexPath.section;
            cell.UserName.tag = 200 + indexPath.section;
            cell.shareImageView.tag = 200 + indexPath.section;
            cell.ForwardButton.tag = 200 + indexPath.section;
            cell.CollectButton.tag = 200 + indexPath.section;
            cell.MoreButton.tag = 200 + indexPath.section;
            return cell;
        }
        else{
            
            MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            
            cell.opaque = YES;
            
            //[cell setMode:model];
            cell.model = model;

            cell.CommentView.hidden = YES;
            cell.CommentTld.delegate = self;
            
            
            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//发布评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
            cell.MoreButton.hidden = YES;

            
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap];

            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap1];

            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
            cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            cell.CommentTld.tag = 300 + indexPath.section;
            cell.MoreCommentBut.tag = 200 + indexPath.section;
            cell.CommentBut.tag = 200 + indexPath.section;
            cell.CommentButton.tag = 200 + indexPath.section;
            cell.LikeButton.tag = 200 + indexPath.section;
            cell.UserImage.tag = 200 + indexPath.section;
            cell.UserName.tag = 200 +indexPath.section;
            cell.ForwardButton.tag = 200 + indexPath.section;
            cell.CollectButton.tag = 200 + indexPath.section;
            cell.MoreButton.tag = 200 + indexPath.section;
            return cell;
        }
    }
    else if (indexPath.section == 1){
        TagFriedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commcell" forIndexPath:indexPath];
        CommentModel *model = _CommentArray[indexPath.row];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        [cell setNewCModel:model];
        [cell.LikeButton addTarget:self action:@selector(likecellCom:) forControlEvents:UIControlEventTouchUpInside];
         [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];
        cell.LikeButton.tag = 100 + indexPath.row;
        cell.ReportButton.tag = 100 + indexPath.row;
        
        return cell;
    }

    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

#pragma mark LoadData
-(void)loadData{
    [_DataArray removeAllObjects];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *member_id = nil;
    if (self.member_id && ![self.member_id isEqualToString:@""]) {
        member_id = self.member_id;
    }else{
        member_id = [PHUserModel sharedPHUserModel].member_id;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getPost];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":member_id,@"post_id":self.PostId,@"Page":@"0"};
    
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                MainModel *model =[MainModel pareWithDictionary:parmDic];
               // model setValue:@"1" forKey:<#(nonnull NSString *)#>
                model.isNoti = @"1";
                
                [_DataArray addObject:model];
            }
            [self creatTable];
            [self LoadComment];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
-(void)LoadComment{
     [CYToast showStatusWithString:@"正在加载"];
    //[_CommentArray removeAllObjects];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,GetComment];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":self.PostId};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
            [_CommentArray removeAllObjects];
            for (NSDictionary *Comdict in dataArr) {
                [_CommentArray addObject:[CommentModel parsenWithDictionary:Comdict]];
            }
            [MainTableView reloadData];
         [self creatCommentView];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
-(void)likeCom:(UIButton *)button{
    CommentModel *model = _DataArray[0];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeCommetn];
    NSDictionary *parmDic  = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"comment_id":model.comment_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([model.isLike isEqualToString:@"Y"]) {
                model.isLike = @"N";
            }else{
                model.isLike = @"Y";
            }
//            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -100 inSection:0];
//            [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
            [MainTableView reloadData];
        }
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
-(void)postComment{
    [_CommentTld resignFirstResponder];
    [_CommentTld endEditing:YES];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CommentUrl];
    if (_CommentTld.text.length == 0 || [_CommentTld.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"评论不能为空"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":self.PostId,@"content":_CommentTld.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"评论成功"];
          

            NSDictionary *comDic = @{@"post_id":self.PostId,@"count":[NSString stringWithFormat:@"%ld",_CommentArray.count +1]};
            [[NSNotificationCenter defaultCenter]postNotificationName:@"CommentSuccess" object:nil userInfo:comDic];
            
            MainModel *model = _DataArray[0];
            NSInteger count = [model.Comment integerValue] + 1;
            model.Comment = [NSString stringWithFormat:@"%ld",count];
            
              [self LoadComment];
            [MainTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_DataArray.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }else{
            [CYToast showErrorWithString:@"评论失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
#pragma mark -cell上的点击事件
//-(void)EditPost:(UIButton *)button{
//    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
//    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"编辑帖文", nil)];
//    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"删除帖文", nil)];
//
//    [actionItems addObject:item];
//    [actionItems addObject:item1];
//
//    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
//    [actionSheet show:^{
//
//    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
//        if (selectedIndex == 0) {
//            [self editPost:button.tag - 200];
//        }else if (selectedIndex == 1){
//            [self deletePost:button.tag - 200];
//        }
//    }];
//}
//#pragma mark - 帖文编辑
//-(void)editPost:(NSInteger )index{
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, 50, SCREEN_WIDTH - 20, 130)];
//    view.layer.cornerRadius = 10;
//    view.backgroundColor = [UIColor whiteColor];
//
//    MainModel *model =_DataArray[index];
//    editText = [[SZTextView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 100)];
//    editText.text = model.content;
//    editText.layer.borderWidth = 2.0f;
//    editText.layer.borderColor = LYColor(223, 223, 223).CGColor;
//    editText.layer.cornerRadius = 8;
//    [view addSubview:editText];
//
//    UIButton *PostButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    PostButton.frame = CGRectMake(SCREEN_WIDTH- 65, 105, 40, 20);
//    PostButton.backgroundColor =LYColor(237, 204, 105);
//    [PostButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
//    PostButton.titleLabel.font = FONT(13);
//    PostButton.layer.cornerRadius = 8;
//    [PostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [PostButton addTarget:self action:@selector(sendEditPost:)
//         forControlEvents:UIControlEventTouchUpInside];
//    PostButton.tag = index;
//    [view addSubview:PostButton];
//
//    [[QWAlertView sharedMask] show:view withType:QWAlertViewStyleActionSheetDown];
//
//
//}
//#pragma mark - 发布帖文编辑
//-(void)sendEditPost:(UIButton *)button{
//    [[QWAlertView sharedMask]dismiss];
//    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,EDPost];
//    NSInteger index = button.tag;
//    MainModel *model =_DataArray[index];
//    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":editText.text};
//    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
//        NSDictionary *dic = responseObject;
//        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
//       // [self load];
//    } failure:^(NSError *error) {
//        NSLog(@"%@",error);
//        [CYToast dismiss];
//    }];
//
//}
//#pragma mark - 删除帖文
//-(void)deletePost:(NSInteger )index{
//    [CYToast showStatusWithString:@"正在加载"];
//    MainModel *model =_DataArray[index];
//    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,DeletePost];
//    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
//
//    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
//        NSDictionary *dic = responseObject;
//        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
//        if ([dic[@"status"] isEqualToString:@"success"]) {
//            [_DataArray removeObjectAtIndex:index];
//            [MainTableView reloadData];
//        }
//    } failure:^(NSError *error) {
//        NSLog(@"%@",error);
//        [CYToast dismiss];
//    }];
//}
-(void)reporeComment:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
    }];
}
-(void)gotoReport{
    ReportViewController *controller = [[ReportViewController alloc]init];
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 收藏贴文
 */
-(void)collectPost:(UIButton *)button{
    
    [CYToast showStatusWithString:@"正在加载"];
    
    MainModel *model =_DataArray[0];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CollectPost];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([model.isCollect isEqualToString:@"Y"]) {
            model.isCollect = @"N";
        }else{
            model.isCollect = @"Y";
        }
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//        [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
//评论按赞
-(void)likecellCom:(UIButton *)button{
    CommentModel *model = _CommentArray[button.tag - 100];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeCommetn];
    NSDictionary *parmDic  = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"comment_id":model.comment_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            //[CYToast showSuccessWithString:@""]
            if ([model.isLike isEqualToString:@"Y"]) {
                model.isLike = @"N";
            }else{
                model.isLike = @"Y";
            }
            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -100 inSection:1];
            [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
/**
 分享按钮点击
 */
-(void)shareClick:(UIButton *)button{
    
    MainModel *model = _DataArray[0];
    NSMutableString *share = [[NSMutableString alloc]initWithString:NSLocalizedString(@"已有人分享了这则帖文", nil)];
    [share insertString:model.Share atIndex:1];
    DeclareAbnormalAlertView *alertView = [[DeclareAbnormalAlertView alloc]initWithTitle:@"分享帖文" message:share delegate:self leftButtonTitle:@"确定" rightButtonTitle:@"取消"];
    //currentShareModel = model;
    alertView.model = model;
    alertView.delegate = self;
    [alertView show];
    
}

-(void)didSendtext:(NSString *)text{
    
    MainModel *model = _DataArray[0];

    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    NSDictionary *parmDic =@{@"target":[PHUserModel sharedPHUserModel].member_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_type":@"share",@"share_id":model.post_id,@"content":[NSString stringWithFormat:@"%@",text]};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 点击查看贴文的赞
 */
-(void)checkLike:(UIButton *)button{
    
    MainModel *model =_DataArray[0];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeUrl];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSString *data = dic[@"data"];
        if ([data isEqualToString:@"yes"]) {
            model.isLike = @"Y";
            NSInteger likecount = [model.Like integerValue] + 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }else{
            model.isLike = @"N";
            NSInteger likecount = [model.Like integerValue] - 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }
        [MainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:button.tag - 200 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
/**
 点击查看评论
 */
-(void)comment:(UIButton *)button{
    MainModel *main =_DataArray[0];
    main.isShowCommentV = !main.isShowCommentV;
    NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
    [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationFade];
}
/**
 发布评论
 */
-(void)postComment:(UIButton *)button{
    MainModel *model = _DataArray[0];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CommentUrl];
    if (_CommentTld.text.length == 0 || [_CommentTld.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"评论不能为空"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":_CommentTld.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"评论成功"];
            MainModel *main =_DataArray[0];
            main.isShowCommentV = !main.isShowCommentV;
            NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
            [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [CYToast showSuccessWithString:@"评论失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
//-(void)textViewDidBeginEditing:(UITextView *)textView{
//    CurText = (SZTextView *)textView;
//}
//-(void)textViewDidEndEditing:(UITextView *)textView{
//
//    [CurText resignFirstResponder];
//    [CurText endEditing:YES];
//}
/**
 查看更多评论
 */
-(void)checkMoreComment:(UIButton *)button{
    MainModel *model = _DataArray[0];
    ShareFriendViewController *controller = [[ShareFriendViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.post_id = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[0];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.BannerImage = model.banner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckShareInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[0];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.shareID;
    controller.Username = model.shareName;
    controller.ImageUrl = model.shareImage;
    controller.BannerImage = model.shareBanner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)checkWhoInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[0];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.ForWhoMember_id;
    controller.Username = model.ForWhoName;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma  mark other
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
