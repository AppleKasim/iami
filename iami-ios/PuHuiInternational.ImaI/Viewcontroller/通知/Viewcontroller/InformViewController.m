//
//  InformViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InformViewController.h"
#import "InfoTableViewCell.h"
#import "ShortMovieViewController.h"
#import "PostDetailViewController.h"

@interface InformViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *MainTableView;
}
@property (nonatomic,copy) NSMutableArray *dataArray;
#define getNotifi @"/api/notifies"
#define readNotifi @"/page/readNotifies"

@end

@implementation InformViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [[NSMutableArray alloc]init];
    //self.view.backgroundColor = LYColor(223, 223, 223);
    [self loadData];
    [self creatTable];
    [self creatTabButton];
    [self readNoti];
    // Do any additional setup after loading the view.
}
#pragma =================== TableView delegeta && datasource =============================
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(8, 10, SCREEN_WIDTH  -16, SCREEN_HEIGHT - Height_NavBar - Height_TabBar - 18) style:UITableViewStylePlain];
    MainTableView.backgroundColor =LYColor(223, 223, 223);
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.bounces = NO;
    [MainTableView registerClass:[InfoTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:MainTableView];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    InfoModel *model = _dataArray[indexPath.section];
    [cell setInfomodel:model];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5)];
    view.backgroundColor =LYColor(223, 223, 223);
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    
    InfoModel *model = _dataArray[indexPath.section];
    
    if ([model.type isEqualToString:@"post"]) {
        PostDetailViewController *controller = [[PostDetailViewController alloc]init];
        controller.m_showBackBt = YES;
        if (model.PostId == nil || [model.PostId isEqualToString:@""]) {
            return;
        }
        controller.PostId = model.PostId;
        [self.navigationController pushViewController:controller animated:YES];
    }else{
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.member_id;
        controller.Username = model.nickname;
        controller.ImageUrl = model.UserImage;
        controller.BannerImage = model.BannerImage;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
   
}
-(void)creatTabButton{
    UIImage *rightImage = [[UIImage imageNamed:@"直播.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(0, 0, 40, 40);
    [liveButton setImage:[UIImage imageNamed:@"五秒视频.png"] forState:UIControlStateNormal];
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    [liveButton addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *but1 = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(live)];
    UIBarButtonItem *but3 = [[UIBarButtonItem alloc]initWithCustomView:liveButton];
    
    
    
    self.navigationItem.leftBarButtonItems = @[but1,but3];
    //self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
}
-(void)live{
    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:controller animated:YES];
}

//-(void)setting{
//    ShortMovieViewController *controller = [[ShortMovieViewController alloc]init];
//    controller.m_showBackBt = YES;
//    [self.navigationController pushViewController:controller animated:YES];
//}
#pragma ========== 网络请求 ==========
-(void)loadData{
   
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getNotifi];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        [CYToast dismiss];
        NSArray *dataArr = dic[@"data"];
         [_dataArray removeAllObjects];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_dataArray addObject:[InfoModel pareWithDictionary:parmDic]];
            }
        }
        [self creatTable];
        [MainTableView reloadData];

        if (_dataArray.count > 0) {

        } else {

        }


    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)readNoti{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,readNotifi];
    [CYToast showStatusWithString:@"正在加载"];

    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        self.tabBarItem.badgeValue = 0;
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"newMessage" object:nil];
     self.tabBarItem.badgeValue = 0;
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)new:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
//    if ([[dic allKeys]containsObject:@"notice"]) {
//
//    }
      [self loadData];
}
@end
