//
//  PostDetailViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/5.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"

@interface PostDetailViewController : XTViewController

@property (nonatomic,strong) NSString *PostId;
@property (nonatomic,strong) NSString *member_id;

@end
