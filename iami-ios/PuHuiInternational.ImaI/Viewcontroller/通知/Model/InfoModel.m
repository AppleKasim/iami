//
//  InfoModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InfoModel.h"

@implementation InfoModel
+(InfoModel *)pareWithDictionary:(NSDictionary *)dict{
    InfoModel *model = [[InfoModel alloc]init];
    NSDictionary *memebrDic = dict[@"member"];
    model.UserImage = [NetDataCommon stringFromDic:memebrDic forKey:@"avatar"];
    model.MainLabel = [NetDataCommon stringFromDic:dict forKey:@"text"];
    model.TimeLabel = [NetDataCommon stringFromDic:dict forKey:@"createTime"];
    model.nickname = [NetDataCommon stringFromDic:memebrDic forKey:@"nickname"];
    NSString *path = [NetDataCommon stringFromDic:dict forKey:@"target_path"];
    model.PostId = [model getIdString:path];
    model.type = [model getType:path];
    model.member_id =[NetDataCommon stringFromDic:dict forKey:@"member_id"];
    model.BannerImage = [NetDataCommon stringFromDic:memebrDic forKey:@"banner"];
   // model.memberImage = [NetDataCommon stringFromDic:memebrDic forKey:@""];
    return model;
}
-(NSString *)getType:(NSString *)string{
    NSString *type = nil;
    if ([string rangeOfString:@"/info"].location ==NSNotFound) {
        type = @"post";
    }else{
        type = @"info";
    }
    return type;
}
-(NSString *)getIdString:(NSString *)string{
    
    if ([string rangeOfString:@"/info"].location ==NSNotFound) {
        NSRange range = [string rangeOfString:@"="];
        NSString *new = [string substringFromIndex:range.location + 1];
        return new;
    }
    return nil;
}
@end
