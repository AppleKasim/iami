//
//  InfoModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoModel : NSObject
@property (nonatomic,strong) NSString *BackColorString;

@property (nonatomic,copy) NSString *UserImage;
@property (nonatomic,copy) NSString *MainLabel;
@property (nonatomic,copy) NSString *TimeLabel;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *PostId;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *member_id;
@property (nonatomic,copy) NSString *memberImage;
@property (nonatomic,copy) NSString *BannerImage;
+(InfoModel *)pareWithDictionary:(NSDictionary *)dict;
@end
