//
//  InviteModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InviteModel : NSObject
@property (nonatomic,strong) NSString *UserImage;
@property (nonatomic,strong) NSString *UserName;
@property (nonatomic,strong) NSString *MeseeageLabel;
@property (nonatomic,strong) NSString *LevelLabel;
@property (nonatomic,strong) NSString *FreInvitButton;
@property (nonatomic,strong) NSString *FollowButton;
@property (nonatomic,strong) NSString *invitee_id;
@property (nonatomic,strong) NSString *isFriend;
@property (nonatomic,strong) NSString *isTrace;
@property (nonatomic,strong) NSString *relationship;
@property (nonatomic,strong) NSString *BannerImage;
//@property (nonatomic,strong) NSString *member_id;

+(InviteModel *)pareWithDictionary:(NSDictionary *)dict;
@end
