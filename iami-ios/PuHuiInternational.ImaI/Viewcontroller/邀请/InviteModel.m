//
//  InviteModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InviteModel.h"

@implementation InviteModel
+(InviteModel *)pareWithDictionary:(NSDictionary *)dict{
    InviteModel *model = [[InviteModel alloc]init];
    model.UserImage = [NetDataCommon stringFromDic:dict forKey:@"avatar"];
    model.UserName = [NetDataCommon stringFromDic:dict forKey:@"nickname"];
    model.LevelLabel = [NetDataCommon stringFromDic:dict forKey:@"level"];
    model.invitee_id = [NetDataCommon stringFromDic:dict forKey:@"member_id"];
    //model.isTrace = [NetDataCommon stringFromDic:dict forKey:@"isTrace"];
    //model.isFriend = [NetDataCommon stringFromDic:dict forKey:@"isFriend"];
    model.relationship  = [NetDataCommon stringFromDic:dict forKey:@"relationship"];
    model.isTrace = [NetDataCommon stringFromDic:dict forKey:@"is_trace" orKey:@"isTrace"];
     model.isFriend = [NetDataCommon stringFromDic:dict forKey:@"is_friend" orKey:@"isFriend"];
    model.BannerImage = [NetDataCommon stringFromDic:dict forKey:@"banner"];
    return model;
}
@end
