//
//  InviteTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InviteTableViewCell.h"

@implementation InviteTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(InviteModel *)model{
    CGFloat LeadingSpace = 10;
    
    self.backgroundColor = LYColor(223, 223, 223);
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(3, 0, SCREEN_WIDTH - 6, 80)];
    view.layer.cornerRadius = 10;
    view.clipsToBounds = YES;
    view.backgroundColor = [UIColor whiteColor];
    
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(20-imageW, 10 - imageW, 60 + imageW * 2, 60+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
   
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 60, 60)];
    //_UserImage.image = [UIImage imageNamed:@"avatar"];
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",model.UserImage];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 30;
    _UserImage.clipsToBounds = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    
    
     CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH - LeadingSpace * 2 font:Bold_FONT(13)];
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leveImage.frame) + 10, 14, namelHeght, 20)];
    _UserName.text = model.UserName;
    _UserName.font = Bold_FONT(13);
    _UserName.textColor = NameColor;
    _UserName.textAlignment = NSTextAlignmentLeft;
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 10, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:memberImage];
    
    _LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) +2, memberImage.frame.origin.y + 5, 29, 13)];
    _LevelLabel.backgroundColor = MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [view addSubview:_LevelLabel];
    
    _MeseeageLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserImage.frame) + 10, 40, 130, 20)];
    //_MeseeageLabel.text = @"你们在iami上有100位共同好友";
    _MeseeageLabel.font = FONT(9);
    
    _FreInvitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _FreInvitButton.frame = CGRectMake(SCREEN_WIDTH - 100, 20, 90, 20);
    [_FreInvitButton setTitle:NSLocalizedString(@"接受好友邀请", nil) forState:UIControlStateNormal];
    _FreInvitButton.backgroundColor = MainColor;
    [_FreInvitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _FreInvitButton.layer.cornerRadius = 8;
    _FreInvitButton.titleLabel.font = FONT(12);
    
    _FollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _FollowButton.frame = CGRectMake(SCREEN_WIDTH - 100, 45, 90, 20);
    [_FollowButton setTitle:NSLocalizedString(@"已追踪", nil) forState:UIControlStateNormal];
    _FollowButton.backgroundColor = [UIColor whiteColor];
    _FollowButton.layer.borderWidth = 1.0f;
    _FollowButton.layer.cornerRadius = 8;
    _FollowButton.layer.borderColor =MainColor.CGColor;
    [_FollowButton setTitleColor:MainColor forState:UIControlStateNormal];
    _FollowButton.titleLabel.font = FONT(12);
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 78, SCREEN_WIDTH, 2)];
    gapView.backgroundColor =LYColor(221, 221, 221);
    
     [view addSubview:leveImage];
    [leveImage addSubview:_UserImage];
    [view addSubview:_UserName];
    [view addSubview:_MeseeageLabel];
    [view addSubview:_FreInvitButton];
    [view addSubview:_FollowButton];
    [view addSubview:gapView];
    
    [self.contentView addSubview:view];
}

-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}















- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
