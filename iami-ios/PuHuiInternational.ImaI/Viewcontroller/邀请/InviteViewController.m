//
//  InviteViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "InviteViewController.h"
#import "InviteTableViewCell.h"
#import "ShortMovieViewController.h"
#import "SearchTableViewCell.h"
@interface InviteViewController ()<UITableViewDelegate,UITableViewDataSource,PST_MenuViewDelegate,UISearchBarDelegate>
{
    UITableView *MainTableView ;
    UILabel *noDataLabel;

    NSInteger currentIndex;

    UISearchBar *_searchBar;
    BOOL searchShow;
    UITableView *searchTableview;
}
@property (nonatomic,copy) NSMutableArray *dataArray;

@property (nonatomic,copy) NSMutableArray *SearchdataArr;
#define invitUrl @"/api/invite_notifies"
#define statusUrl @"/invite/setInviteStatus"
#define searchurl @"/page/memberQuery"
@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [[NSMutableArray alloc]init];
    [self creatTabButton];

    _SearchdataArr = [[NSMutableArray alloc]init];
   // [self loadData];
    // Do any additional setup after loading the view.
    [self createNoDataLabel];
}

-(void)createNoDataLabel{
    CGRect labelRect = CGRectMake(0, 25, SCREEN_WIDTH, 100);
    noDataLabel = [[UILabel alloc] initWithFrame: labelRect];
    noDataLabel.text = NSLocalizedString(@"请点选右上角放大镜，搜寻好友", nil);
    noDataLabel.textAlignment = NSTextAlignmentCenter;
    noDataLabel.numberOfLines = 0;
}

-(void)viewWillDisappear:(BOOL)animated{
    [_searchBar removeFromSuperview];
}

#pragma mark -  TableView delegeta && datasource
-(void)creatTable{
    searchShow = NO;

    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH, SCREEN_HEIGHT -  70 ) style:UITableViewStylePlain];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.layer.cornerRadius = 10;

    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.bounces = NO;
    [MainTableView registerClass:[InviteTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == searchTableview) {
        return _SearchdataArr.count;
    }

    if (_dataArray.count == 0) {
        [self.view addSubview:noDataLabel];
    } else {
        [noDataLabel removeFromSuperview];
    }

    return _dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual: searchTableview]){
        SearchModel *model = _SearchdataArr[indexPath.row];

        static NSString *CellIdentifier = @"search";
        SearchTableViewCell  *cell = (SearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        if (cell == nil)
        {
            cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if (tableView == searchTableview) {
            [cell setModel:model];
        }

        return cell;
    } else {
        InviteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }

        InviteModel *model =  _dataArray[indexPath.row];
        [cell setModel:model];

        cell.FreInvitButton.tag = 200 + indexPath.row;
        [cell.FreInvitButton addTarget:self action:@selector(friendInvi:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }

    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == searchTableview){
        return 50;
    } else {
        return 80;
    }

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    if (tableView == searchTableview) {
        if (_SearchdataArr.count == 0) {
            return;
        }
        SearchModel *model =_SearchdataArr[indexPath.row];
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.member_id;
        controller.Username = model.nickname;
        controller.ImageUrl = model.avatar;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];

    } else {
        InviteModel *model =  _dataArray[indexPath.row];
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.invitee_id;
        controller.BannerImage = model.BannerImage;
        controller.ImageUrl = model.UserImage;
        controller.Username = model.UserName;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
}
//创建tabbar按钮
-(void)creatTabButton{
    UIImage *rightImage = [[UIImage imageNamed:@"直播.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(0, 0, 40, 40);
    [liveButton setImage:[UIImage imageNamed:@"五秒视频.png"] forState:UIControlStateNormal];
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    [liveButton addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *but1 = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(live)];
    UIBarButtonItem *but3 = [[UIBarButtonItem alloc]initWithCustomView:liveButton];
    
    
    
    self.navigationItem.leftBarButtonItems = @[but1,but3];
    UIImage *searchImage = [[UIImage imageNamed:@"搜索.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:searchImage style:UIBarButtonItemStylePlain target:self action:@selector(creatSearchBar)];
}
-(void)live{
    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:controller animated:YES];
}
//-(void)setting{
//    ShortMovieViewController *controller = [[ShortMovieViewController alloc]init];
//    controller.m_showBackBt = YES;
//    [self.navigationController pushViewController:controller animated:YES];
//}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"newMessage" object:nil];
    
    [self loadData];


    if (searchShow) {
        [self showSearch];
    }
    [_searchBar resignFirstResponder];
    [searchTableview removeFromSuperview];

}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)new:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
     if ([[dic allKeys]containsObject:@"invite"]){
         [self loadData];
    }
}
-(void)friendInvi:(UIButton *)button{
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect: button.bounds toView:window];
    
    currentIndex = button.tag - 200;
    PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[NSLocalizedString(@"拒绝", nil),NSLocalizedString(@"同意", nil)] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
    menuView.titleColor = MainColor;
    menuView.lineColor = MainColor;
}
-(void)didSelectRowAtIndex:(NSInteger)index title:(NSString *)title img:(NSString *)img{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,statusUrl];
    InviteModel *model = _dataArray[currentIndex];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"invitee_id":model.invitee_id,@"status":[NSString stringWithFormat:@"%ld",index]};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        [CYToast showSuccessWithString:[NSString stringWithFormat:@"%@",dict[@"message"]]];
        [self loadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark - searchBar相关

/**
 *  searchBar
 */
-(void)showSearch{
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(120, 2, SCREEN_WIDTH - 175, 40)];
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.showsCancelButton = NO;
    _searchBar.tintColor = [UIColor orangeColor];
    _searchBar.placeholder = @"搜索感兴趣的内容";
    _searchBar.delegate = self;



    for (UIView *subView in _searchBar.subviews) {
        if ([subView isKindOfClass:[UIView  class]]) {
            [[subView.subviews objectAtIndex:0] removeFromSuperview];
            if ([[subView.subviews objectAtIndex:0] isKindOfClass:[UITextField class]]) {
                UITextField *textField = [subView.subviews objectAtIndex:0];
                textField.backgroundColor = [UIColor whiteColor];

                //设置默认文字颜色
                UIColor *color = [UIColor grayColor];
                [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"搜索好友", nil)
                                                                                    attributes:@{NSForegroundColorAttributeName:color}]];
            }
        }
    }

    [self.navigationController.navigationBar addSubview:_searchBar];
    searchShow = YES;
}

-(void)creatSearchBar{

    if (_searchBar) {
        [_searchBar removeFromSuperview];
        _searchBar = nil;
        searchShow = NO;
        return;
    }

    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(120, 2, SCREEN_WIDTH - 175, 40)];
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.showsCancelButton = NO;
    _searchBar.tintColor = [UIColor orangeColor];
    _searchBar.placeholder = @"搜索感兴趣的内容";
    _searchBar.delegate = self;


    for (UIView *subView in _searchBar.subviews) {
        if ([subView isKindOfClass:[UIView  class]]) {
            [[subView.subviews objectAtIndex:0] removeFromSuperview];
            if ([[subView.subviews objectAtIndex:0] isKindOfClass:[UITextField class]]) {
                UITextField *textField = [subView.subviews objectAtIndex:0];
                //                textField.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
                textField.backgroundColor = [UIColor whiteColor];


                //设置默认文字颜色
                UIColor *color = [UIColor grayColor];
                [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"搜索好友", nil)
                                                                                    attributes:@{NSForegroundColorAttributeName:color}]];
            }
        }
    }

    [self.navigationController.navigationBar addSubview:_searchBar];
    searchShow = YES;
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if (searchBar.text.length > 0) {
        [self loadData:searchBar.text];
    }
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if (searchBar.text.length == 0) {
        [_searchBar resignFirstResponder];
        [searchTableview removeFromSuperview];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        [_searchBar resignFirstResponder];
        [searchTableview removeFromSuperview];
        return;
    }else{
        [self loadData:searchText];
    }

}

-(void)creatSearchTableview{
    if (searchTableview) {
        [searchTableview removeFromSuperview];
    }
    searchTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200) style:UITableViewStylePlain];
    [searchTableview registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"search"];

    searchTableview.delegate = self;
    searchTableview.dataSource = self;
    searchTableview.separatorStyle = NO;
    searchTableview.scrollEnabled = YES;
    [self.view addSubview:searchTableview];
}


#pragma mark - 网络请求
-(void)loadData{
   
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,invitUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        NSArray *dataArr = dic[@"data"];
         [_dataArray removeAllObjects];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_dataArray addObject:[InviteModel pareWithDictionary:parmDic]];
            }
        }

        [self creatTable];
        [MainTableView reloadData];

        if (_dataArray.count > 0) {
            self.tabBarItem.badgeValue = (_dataArray.count > 99) ? @"99+" : [NSString stringWithFormat:@"%ld",_dataArray.count];
        }else{
            self.tabBarItem.badgeValue = 0;
        }
         [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

-(void)loadData:(NSString *)string{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,searchurl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"keyword":string};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];

        if (dataArr && dataArr.count > 0) {
            [_SearchdataArr removeAllObjects];

            for (NSDictionary *parm in dataArr) {
                [_SearchdataArr addObject:[SearchModel parsenWith:parm]];
            }
            [self creatSearchTableview];
        }
        //        if (searchTableview) {
        //            [searchTableview reloadData];
        //        }else{
        //
        //        }


        //  [searchDisplayController.searchResultsTableView reloadData];
        // [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
