//
//  InviteTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InviteModel.h"

@interface InviteTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) UILabel *MeseeageLabel;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UIButton *FreInvitButton;
@property (nonatomic,strong) UIButton *FollowButton;

-(void)setModel:(InviteModel *)model;
@end
