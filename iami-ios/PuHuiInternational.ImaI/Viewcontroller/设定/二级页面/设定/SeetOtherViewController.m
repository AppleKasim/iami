//
//  SeetOtherViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/25.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "SeetOtherViewController.h"
#import "PrivateViewController.h"//隐私权
#import "TermsOfOursViewController.h"//服务条款
#import "ChangePwdViewController.h"//更改密码
#import "UserInformationViewController.h"//修改密码
#import "AddLiveViewController.h"//我的直播
#import "ChangeHeadViewController.h"//更新头像
#import "HotPostViewController.h"//人气帖文
#import "LanguageSetViewController.h"//语系设定

@interface SeetOtherViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
}
@property (nonatomic,copy) NSArray *dataArray;

@end

@implementation SeetOtherViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = @[@"",NSLocalizedString(@"更新头像／封面照", nil),NSLocalizedString(@"基本资料编辑", nil),NSLocalizedString(@"更改密码", nil),NSLocalizedString(@"语系设定", nil),NSLocalizedString(@"隐私权声明", nil),NSLocalizedString(@"使用条款", nil)];
    
    [self creatTable];
    
   
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
#pragma mark  - 获取当前时间当版本号
-(NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyMMdd"];
    NSString*dateTime = [formatter stringFromDate:[NSDate  date]];
    self.CurrentTime = dateTime;
    // NSLog(@"当前时间是===%@",_CurrentTime);
    return _CurrentTime;
}
#pragma mark =======TableView delegeta && datasource =========================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT -  Height_NavBar - Height_TabBar ) style:UITableViewStylePlain];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DemoTableViewCell";
    UITableViewCell  *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    if (indexPath.row == 0) {
        UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,  20, SCREEN_WIDTH - 60, 20)];
        _LevelLabel.textColor =MainColor;
        _LevelLabel.text = NSLocalizedString(@"设定", nil);
        _LevelLabel.textAlignment = NSTextAlignmentLeft;

        _LevelLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];

        [cell.contentView addSubview:_LevelLabel];
    }else{
        UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(30,  20, SCREEN_WIDTH - 60, 20)];
        _LevelLabel.textColor =[UIColor blackColor];
        _LevelLabel.text = [NSString stringWithFormat:@"%@",_dataArray[indexPath.row]];
        _LevelLabel.textAlignment = NSTextAlignmentLeft;
        //_LevelLabel.font = FONT(17);
        UIFont *font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];
        _LevelLabel.font = font;
        [cell.contentView addSubview:_LevelLabel];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        
    }else if (indexPath.row == 1){
                ChangeHeadViewController *controller = [[ChangeHeadViewController alloc]init];
                controller.m_showBackBt = YES;
                [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 2){
                UserInformationViewController *controller = [[UserInformationViewController alloc]init];
                controller.m_showBackBt=  YES;
                [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 3){
        ChangePwdViewController *controller = [[ChangePwdViewController alloc]init];
                controller.m_showBackBt=  YES;
                [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 4){
        LanguageSetViewController *controller = [[LanguageSetViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 5){
        PrivateViewController *controller = [[PrivateViewController alloc]init];
        controller.m_showBackBt=  YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 6){
                TermsOfOursViewController *controller = [[TermsOfOursViewController alloc]init];
                controller.m_showBackBt = YES;
                [self.navigationController pushViewController:controller animated:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
