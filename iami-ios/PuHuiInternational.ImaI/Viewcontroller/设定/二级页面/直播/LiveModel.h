//
//  LiveModel.h
//  PHSJIamILive
//
//  Created by user on 2018/3/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveModel : NSObject
@property (nonatomic,copy) NSString *qiniu_id;
@property (nonatomic,copy) NSString *push_url;
@property (nonatomic,copy) NSString *nickname;
@property (nonatomic,copy) NSString *avatar;
+(LiveModel *)Instance;

+(LiveModel *)parser:(NSDictionary *)mInfo;
@end
