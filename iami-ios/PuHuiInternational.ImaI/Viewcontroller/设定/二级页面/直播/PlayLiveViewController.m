//
//  PlayLiveViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "PlayLiveViewController.h"
#import <PLMediaStreamingKit/PLMediaStreamingKit.h>

@interface PlayLiveViewController ()<PLMediaStreamingSessionDelegate, PLStreamingSendingBufferDelegate>
{
    // PLStreamingSessionConstructor *_sessionConstructor;
    PLMediaStreamingSession *_streamingSession;
    //视频配置信息
    PLVideoCaptureConfiguration *videoCaptureConfiguration;
    UISlider *_zoomSlider;
    
}
@property (nonatomic, strong) PLMediaStreamingSession *session;
@property (nonatomic,copy) NSString *push_url;
@property (nonatomic) BOOL isShow;
@end

@implementation PlayLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    [self creatLive];
    [self creatUI];
    [self creatSettingBut];
    [self creatBack];
   
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.tabBarController.tabBar.hidden = YES;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
//    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
//    if(self.navigationController.childViewControllers.count == 1)
//    {
//        return NO;
//    }
    
    return NO;
}
-(void)back{
    [self deleteLive];
}
/**
 开始直播
 */
-(void)startLive{
    if (_push_url) {
        NSURL *pushURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",_push_url]];
        
        [self.session startStreamingWithPushURL:pushURL feedback:^(PLStreamStartStateFeedback feedback) {
            if (feedback == PLStreamStartStateSuccess) {
                NSLog(@"Streaming started.");
            }
            else {
                NSLog(@"Oops.");
            }
        }];
    }
}
-(void)creatLive{
    
    videoCaptureConfiguration = [PLVideoCaptureConfiguration defaultConfiguration];
    PLAudioCaptureConfiguration *audioCaptureConfiguration = [PLAudioCaptureConfiguration defaultConfiguration];
    PLVideoStreamingConfiguration *videoStreamingConfiguration = [PLVideoStreamingConfiguration defaultConfiguration];
    PLAudioStreamingConfiguration *audioStreamingConfiguration = [PLAudioStreamingConfiguration defaultConfiguration];
    
    self.session = [[PLMediaStreamingSession alloc] initWithVideoCaptureConfiguration:videoCaptureConfiguration audioCaptureConfiguration:audioCaptureConfiguration videoStreamingConfiguration:videoStreamingConfiguration audioStreamingConfiguration:audioStreamingConfiguration stream:nil];
    self.session.delegate = self;
    
    
    [self.view addSubview:self.session.previewView];
    
    [self requestAccessForVideo];
    
}
#pragma mark  直播相关的方法
-(NSString *)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyMMddHHmmss"];
    NSString*dateTime = [formatter stringFromDate:[NSDate  date]];
    self.CurrentTime = dateTime;
    // NSLog(@"当前时间是===%@",_CurrentTime);
    return _CurrentTime;
}
/**
 新增直播
 */
-(void)getLive{
    NSString *str = [NSString stringWithFormat:@"%@/live/addLive",TestUrl];
    NSString *live_name = [NSString stringWithFormat:@"iami_%@",[PHUserModel sharedPHUserModel].member_id];
    NSDictionary *paramDic = @{@"live_name":live_name,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    
    
    
    [CYToast showStatusWithString:@"正在加载"];
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSLog(@"%@",dic);
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [PHUserModel sharedPHUserModel].qiniu_id = dic[@"data"][@"qiniu_id"];
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            _push_url = dic[@"data"][@"push_url"];
        }else{
            
       [PHUserModel sharedPHUserModel].qiniu_id = dic[@"qiniu_id"];
         [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            [self RegetLive];
        }

        NSLog(@"%@",dic);
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [self startLive];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 重新获取推流地址
 */
-(void)RegetLive{
    NSString *str = [NSString stringWithFormat:@"%@/live/getNewPushURL",TestUrl];
    NSString *live_name = [PHUserModel sharedPHUserModel].qiniu_id;
    NSDictionary *paramDic = @{@"qiniu_id":live_name,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
             _push_url = dic[@"data"];
            
             [self startLive];
        }
        
        //NSLog(@"%@",dic);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
/**
 删除直播
 */
-(void)deleteLive{
    [CYToast showStatusWithString:NSLocalizedString(@"正在退出", nil)];
    NSString *str = [NSString stringWithFormat:@"%@/live/delLive",TestUrl];
    if (![PHUserModel sharedPHUserModel].qiniu_id || [[PHUserModel sharedPHUserModel].qiniu_id isEqualToString:@""]) {
          [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    NSDictionary *paramDic = @{@"qiniu_id":[PHUserModel sharedPHUserModel].qiniu_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"退出成功"];
        }
        [self.navigationController popViewControllerAnimated:YES];
        //NSLog(@"%@",dic);
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 更换摄像头
 */
- (void)_pressedChangeCameraButton:(UIButton *)button
{
    if (self.session.captureDevicePosition == 1) {
        self.session.captureDevicePosition = 2;
    }else{
        self.session.captureDevicePosition = 1;
    }
}
/**
 断开直播
 */
-(void)stopLive{
    [self deleteLive];
    [self.session stopStreaming];
    
}
- (void)mediaStreamingSession:(PLMediaStreamingSession *)session streamStateDidChange:(PLStreamState)state{
    NSLog(@"====%ld",state);
    if (state == 2) {
        [CYToast showSuccessWithString:NSLocalizedString(@"连接成功，直播开始", nil)];
    }else if (state == 4){
        [CYToast showSuccessWithString:NSLocalizedString(@"连接中断，直播结束", nil)];
        //[self deleteLive];
    }
}
/***
 六个按钮的点击事件
 */
-(void)buttonClick:(UIButton *)Button{
    if (Button.tag == 200) {
        //[self getLive];
    }else if (Button.tag == 201){
        //[self reGetLive];
    }else if (Button.tag == 202){
        //[self deleteLive];
    }
}


#pragma mark     =======  UI  =====
-(void)creatBack{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 45, 50, 30,30)];
    leftButton.layer.cornerRadius = 15;
    leftButton.backgroundColor = [UIColor whiteColor];
    [leftButton setImage:[UIImage imageNamed:@"关闭直播"] forState:UIControlStateNormal];
    [leftButton setImage:[UIImage imageNamed:@"关闭直播"] forState:UIControlStateSelected];
    [leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    leftButton.alpha = 0.5;
    [self.view addSubview:leftButton];
    
}
-(void)creatUI{
   
    
    
    UIButton *chnageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    chnageButton.frame = CGRectMake(SCREEN_WIDTH - 45, SCREEN_HEIGHT - 45, 30,30);
    [chnageButton setImage:[UIImage imageNamed:@"更换相机"] forState:UIControlStateNormal];
    [chnageButton addTarget:self action:@selector(_pressedChangeCameraButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:chnageButton];

    
    
    SZTextView *text = [[SZTextView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 40, 130, 30)];
    text.placeholder = @"你想说什么？";
    text.layer.cornerRadius = 8;
    text.layer.borderWidth = 0.2f;
    text.layer.borderColor = LYColor(237, 204, 105).CGColor;
    text.backgroundColor = [UIColor whiteColor];
    //[self.view addSubview:text];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = CGRectMake(150, SCREEN_HEIGHT - 40, 60, 25);
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    sendButton.titleLabel.font  =FONT(14);
    sendButton.backgroundColor = LYColor(237, 204, 105);
    sendButton.layer.cornerRadius = 10;
    sendButton.clipsToBounds = YES;
    [sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   // [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    //[self.view addSubview:sendButton];
    
    CGFloat MaxWidth = [self getLabelWidthWithText:[PHUserModel sharedPHUserModel].nickname width:SCREEN_WIDTH - 20 font:FONT(16)];
    
    
    
    UIView *yellowBaV = [[UIView alloc]initWithFrame:CGRectMake(10, 50, 120+MaxWidth, 60)];
    //UIView *yellowBaV = [[UIView alloc]initWithFrame:CGRectMake(10, 50, 200, 60)];
    yellowBaV.backgroundColor =MainColor;
    yellowBaV.alpha = 0.5f;
    yellowBaV.layer.cornerRadius = 10;

    
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 50, 50)];
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",[PHUserModel sharedPHUserModel].UserImage];
    [image sd_setImageWithURL:[NSURL URLWithString:url]];
    image.clipsToBounds = YES;
    image.layer.cornerRadius = 25;
    [yellowBaV addSubview:image];
    
//    if (MaxWidth > 70) {
//        MaxWidth = 70;
//    }
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(65, 20, MaxWidth, 20)];
    label.text = [NSString stringWithFormat:@"%@",[PHUserModel sharedPHUserModel].nickname];
    label.font = FONT(16);
    [yellowBaV addSubview:label];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label.frame) , 18, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [yellowBaV addSubview:memberImage];
    
   UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) +2, memberImage.frame.origin.y + 5, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [yellowBaV addSubview:_LevelLabel];
    [self.view addSubview:yellowBaV];
    
    UILabel *CountLabel = [[UILabel alloc]initWithFrame:CGRectMake(yellowBaV.frame.origin.x, CGRectGetMaxY(yellowBaV.frame) + 10 , 80, 20)];
    CountLabel.backgroundColor = LYColor(237, 204, 105);
    CountLabel.alpha = 0.3f;
    CountLabel.layer.cornerRadius = 6;
    CountLabel.clipsToBounds = YES;
    CountLabel.text  = @" 10.5k";
    CountLabel.textColor = [UIColor whiteColor];
    CountLabel.font = FONT(14);
   // [self.view addSubview:CountLabel];
}

-(void)creatSettingBut{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20,20)];
    [leftButton setImage:[UIImage imageNamed:@"设置"] forState:UIControlStateNormal];
    [leftButton setImage:[UIImage imageNamed:@"设置"] forState:UIControlStateSelected];
    [leftButton addTarget:self action:@selector(Setting) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.rightBarButtonItem = leftItem;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.session stopStreaming];
}
#pragma mark -- Public Method
- (void)requestAccessForVideo {
    __weak typeof(self) _self = self;
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusNotDetermined: {
            // 许可对话没有出现，发起授权许可
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[_self.session setRunning:YES];
                         [_self getLive];//直接开始直播
                    });
                }
            }];
            break;
        }
        case AVAuthorizationStatusAuthorized: {
            // 已经开启授权，可继续
            dispatch_async(dispatch_get_main_queue(), ^{
               // [_self.session setRunning:YES];
                 [_self getLive];//直接开始直播
            });
            break;
        }
        case AVAuthorizationStatusDenied:
        case AVAuthorizationStatusRestricted:
            // 用户明确地拒绝授权，或者相机设备无法访问
            
            break;
        default:
            break;
    }
}
@end
