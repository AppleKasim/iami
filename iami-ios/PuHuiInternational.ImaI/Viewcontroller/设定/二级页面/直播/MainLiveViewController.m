//
//  MainLiveViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/9.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainLiveViewController.h"
#import "MainCollectionViewCell.h"
#import "LiveModel.h"
#import "PlayLiveViewController.h"
#import "PlayViewController.h"//观看直播

@interface MainLiveViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *mainCollectionView;
}
@property (nonatomic,copy) NSMutableArray *DataArray;
#define cellHeight 110
@end

@implementation MainLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _DataArray = [[NSMutableArray alloc]init];
    [self loadData];
    [self creatCollect];
    [self creatBottmBut];
    // Do any additional setup after loading the view.
}
/**
 底部拍摄按钮
 */
-(void)creatBottmBut{
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(15, SCREEN_HEIGHT - 60 - Height_TabBar - Height_NavBar, SCREEN_WIDTH - 30, 55);
    [liveButton addTarget:self action:@selector(Start) forControlEvents:UIControlEventTouchUpInside];
    liveButton.backgroundColor = MainColor;
    [liveButton setTitle:NSLocalizedString(@"开始直播", nil) forState:UIControlStateNormal];
    [liveButton setImage:[UIImage imageNamed:@"直播相机"] forState:UIControlStateNormal];
    liveButton.layer.cornerRadius = 8;
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -25, 0, 0);
    [liveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:liveButton];
}
-(void)Start{
    PlayLiveViewController *controller = [[PlayLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)creatCollect{
    //1.初始化layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置collectionView滚动方向
    //    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //设置headerView的尺寸大小
    layout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 0);
    //该方法也可以设置itemSize
    layout.itemSize =CGSizeMake(cellHeight - 10, cellHeight + 10);
    
    //2.初始化collectionView
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar - 70) collectionViewLayout:layout];
    [self.view addSubview:mainCollectionView];
    mainCollectionView.backgroundColor = [UIColor clearColor];
    
    //mjrefresh
    MJRefreshStateHeader *header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadData)];
    mainCollectionView.mj_header = header;
    
    
    //3.注册collectionViewCell
    //注意，此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致 均为 cellId
    [mainCollectionView registerClass:[MainCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    //注册headerView  此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致  均为reusableView
    [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
    
    //4.设置代理
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    
    
}
#pragma mark collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _DataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    MainCollectionViewCell *cell = (MainCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.item < _DataArray.count) {
        LiveModel *model =_DataArray[indexPath.item];
        cell.NameLabel.text = model.nickname;
        NSString *url = [NSString stringWithFormat:@"%@%@",@"",model.avatar];
        [cell.MainImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图.png"]];
        //cell.MainImage.image = [UIImage imageNamed:@"直播图.jpg"];
    }else{
        cell.MainImage.image = [UIImage imageNamed:@"占位图.png"];
        cell.NameLabel.text = @"";
    }
    //cell.NameLabel.text =
    //CALayer *cellImageLayer = cell.MainImage.layer;
    //
    //    //        [cellImageLayer setCornerRadius:cell.imageView.frame.size.width / 2];
    //
    //    [cellImageLayer setCornerRadius:55.0];
    //
    //    [cellImageLayer setMasksToBounds:YES];
    
    
    
    
    return cell;
}
//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellHeight - 10, cellHeight + 15);
}
//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 25;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PlayViewController *controller = [[PlayViewController alloc]init];
    controller.m_showBackBt = YES;
    if (indexPath.item < _DataArray.count) {
        LiveModel *mode = _DataArray[indexPath.item];
        controller.qiniu_id = mode.qiniu_id;
        controller.avatar = mode.avatar;
        controller.nickName = mode.nickname;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else{
        [CYToast showErrorWithString:@"暂无直播!"];
    }
    
}
#pragma  =========== 网络请求 ============
-(void)loadData{
    if (_DataArray.count > 0  ) {
        [_DataArray removeAllObjects];
    }
    [CYToast showStatusWithString:@"正在加载"];
     NSString *str = [NSString stringWithFormat:@"%@/live/getLive",TestUrl];
    NSDictionary *parmDic = @{@"status":@"connected",@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:str parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSArray *array = dic[@"data"];
        if (array && array.count != 0) {
            for (NSDictionary *dict in array) {
                [_DataArray addObject:[LiveModel parser:dict]];
            }
        }
        NSLog(@"%@",dic);
        [mainCollectionView reloadData];
        [mainCollectionView.mj_header endRefreshing];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        [mainCollectionView.mj_header endRefreshing];
        
        NSLog(@"%@",error);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
