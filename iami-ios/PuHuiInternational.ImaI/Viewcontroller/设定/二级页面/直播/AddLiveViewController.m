//
//  AddLiveViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/8.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "AddLiveViewController.h"
#import "MainLiveViewController.h"

@interface AddLiveViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
    UIButton *confirmBut;
}
/** 蒙版 */
@property (nonatomic, strong) UIView *cover;


#define kWindow [UIApplication sharedApplication].keyWindow

@end

@implementation AddLiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.view.backgroundColor =LYColor(223, 223, 223);
    [self creatTable];
    // Do any additional setup after loading the view.
}
#pragma mrak - 蒙版
-(UIView *)cover{
    if (!_cover) {
        _cover = [[UIView alloc] initWithFrame:kWindow.bounds];
        _cover.backgroundColor = [UIColor blackColor];
        _cover.alpha = 0.98;
    }
    return _cover;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
#pragma =======TableView delegeta && datasource ===================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, SCREEN_HEIGHT - 30) style:UITableViewStyleGrouped];
    //MainTableView.backgroundColor = LYColor(255, 255, 255);
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.layer.cornerRadius = 10;
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return SCREEN_HEIGHT - 20;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 10, SCREEN_HEIGHT - 20)];
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 20, 50)];
    backView.backgroundColor =LYColor(231, 194, 99);
    backView.layer.cornerRadius = 10;
    [view addSubview:backView];
    //view.backgroundColor = [UIColor whiteColor];
    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH - 10, 20)];
    TitleLabel.text = @"申请直播主";
    TitleLabel.textColor = [UIColor whiteColor];
    TitleLabel.font = FONT(20);
    TitleLabel.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:TitleLabel];
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 60, 120, 20)];
    nameLabel.text = @"昵称";
    nameLabel.textColor =LYColor(231, 194, 99);
    nameLabel.font = FONT(15);
    
    UILabel *NameL = [[UILabel alloc]initWithFrame:CGRectMake(20, 85, 120, 20)];
    NameL.text = @"David";
    NameL.textColor = [UIColor blackColor];
    NameL.font = FONT(15);
    
    [view addSubview:nameLabel];
    [view addSubview:NameL];
    
    UILabel *emailLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 120, 120, 20)];
    emailLabel.text = @"电子邮件";
    emailLabel.textColor =LYColor(231, 194, 99);
    emailLabel.font = FONT(15);
    
    UILabel *email = [[UILabel alloc]initWithFrame:CGRectMake(20, 145, 120, 20)];
    email.text = @"12346@qq.com";
    email.textColor = [UIColor blackColor];
    email.font = FONT(15);
    
    [view addSubview:emailLabel];
    [view addSubview:email];
    
    UILabel *SexLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 180, 120, 20)];
    SexLabel.text = @"性别";
    SexLabel.textColor =LYColor(231, 194, 99);
    SexLabel.font = FONT(15);
    
    UILabel *Sexl = [[UILabel alloc]initWithFrame:CGRectMake(20, 205, 120, 20)];
    Sexl.text = @"男";
    Sexl.textColor = [UIColor blackColor];
    Sexl.font = FONT(15);
    
    [view addSubview:SexLabel];
    [view addSubview:Sexl];
    
    UILabel *BirthLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 240, 120, 20)];
    BirthLabel.text = @"生日";
    BirthLabel.textColor =LYColor(231, 194, 99);
    BirthLabel.font = FONT(15);
    
    UILabel *Birthl = [[UILabel alloc]initWithFrame:CGRectMake(20, 265, 120, 20)];
    Birthl.text = @"1975年5月2日";
    Birthl.textColor = [UIColor blackColor];
    Birthl.font = FONT(15);
    
    [view addSubview:BirthLabel];
    [view addSubview:Birthl];
    
    UILabel *PhoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 300, 120, 20)];
    PhoneLabel.text = @"手机";
    PhoneLabel.textColor =LYColor(231, 194, 99);
    PhoneLabel.font = FONT(15);
    
    UILabel *Phonel = [[UILabel alloc]initWithFrame:CGRectMake(20, 325, 120, 20)];
    Phonel.text = @"+12345678";
    Phonel.textColor = [UIColor blackColor];
    Phonel.font = FONT(15);
    
    [view addSubview:PhoneLabel];
    [view addSubview:Phonel];
    
    UILabel *StatuLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 360, 120, 20)];
    StatuLabel.text = @"申请直播主状态";
    StatuLabel.textColor =LYColor(231, 194, 99);
    StatuLabel.font = FONT(15);
    
    UILabel *Statul = [[UILabel alloc]initWithFrame:CGRectMake(20, 385, 120, 20)];
    Statul.text = @"尚未申请";
    Statul.textColor = [UIColor blackColor];
    Statul.font = FONT(15);
    
    [view addSubview:StatuLabel];
    [view addSubview:Statul];
    
    confirmBut = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBut.frame = CGRectMake(SCREEN_WIDTH / 2 -100, CGRectGetMaxY(Statul.frame) + 25, 200, 35);
    confirmBut.layer.cornerRadius = 10;
    confirmBut.clipsToBounds = YES;
    [confirmBut setTitle:@"申请" forState:UIControlStateNormal];
    confirmBut.backgroundColor = LYColor(231, 194, 99);
    [confirmBut addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [confirmBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirmBut.titleLabel.font = FONT(17);
    [view addSubview:confirmBut];
    return view;
}
-(void)confirm{
    [self creatBGview];
//    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
//    controller.m_showBackBt = YES;
//    [self.navigationController pushViewController:controller animated:YES];
}
-(void)creatBGview{
    
    UIView *WhiteView = [[UIView alloc]initWithFrame:CGRectMake(10, 100, SCREEN_WIDTH - 20, 250)];
    WhiteView.layer.cornerRadius = 6;
    WhiteView.backgroundColor = [UIColor whiteColor];
   
    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH -20, 20)];
    label.text = @"申请直播主确认";
    label.textAlignment = NSTextAlignmentCenter;
    label.font = FONT(15);
    label.textColor = LYColor(231, 194, 99);
    [WhiteView addSubview:label];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(40, 40, SCREEN_WIDTH- 100, 100)];
    label1.text = @"请确认个人资料是否真实，如需修改请至【设定-基本资料编辑】，确认无误后按下【确认按钮】";
    label1.font = FONT(19);
    label1.numberOfLines = 0;
    label1.textAlignment = NSTextAlignmentCenter;
    [WhiteView addSubview:label1];

    
    CGFloat buttonwidth = (SCREEN_WIDTH - 20 - 45) / 2;
   UIButton  *But = [UIButton buttonWithType:UIButtonTypeCustom];
    But.frame = CGRectMake(SCREEN_WIDTH / 2 - buttonwidth - 20, CGRectGetMaxY(label1.frame) + 25, buttonwidth, 45);
    [But setTitle:@"确认" forState:UIControlStateNormal];
    But.backgroundColor = LYColor(231, 194, 99);
    [But addTarget:self action:@selector(creatLiveShow) forControlEvents:UIControlEventTouchUpInside];
    But.layer.cornerRadius = 6;
    But.clipsToBounds = YES;
    [But setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    But.titleLabel.font = FONT(17);
    [WhiteView addSubview:But];

    
    UIButton  *But1 = [UIButton buttonWithType:UIButtonTypeCustom];
    But1.frame = CGRectMake(CGRectGetMaxX(But.frame) + 5, CGRectGetMaxY(label1.frame) + 25, buttonwidth, 45);
    [But1 setTitle:@"取消" forState:UIControlStateNormal];
    But1.backgroundColor = LYColor(223, 223, 223);
    [But1 addTarget:self action:@selector(removeMenuList) forControlEvents:UIControlEventTouchUpInside];
    [But1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    But1.titleLabel.font = FONT(17);
    But1.layer.cornerRadius = 6;
    But1.clipsToBounds = YES;
    [WhiteView addSubview:But1];

    
    [self.cover addSubview:WhiteView];
    [kWindow addSubview:self.cover];//蒙版添加到主窗口
    //蒙版添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeMenuList)];
    [self.cover addGestureRecognizer:tap];
    self.view.backgroundColor = [UIColor clearColor];
    //[kWindow addSubview:self.view];
}
-(void)removeMenuList{
    //[self removeFromSuperview];
    [self.cover removeFromSuperview];
}
-(void)creatLiveShow{
    [self.cover removeFromSuperview];
        MainLiveViewController *controller = [[MainLiveViewController alloc]init];
        controller.m_showBackBt = YES;
        controller.hidesBottomBarWhenPushed = YES;

        [self.navigationController pushViewController:controller animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
