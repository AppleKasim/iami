//
//  PlayViewController.m
//  PHSJIamILive
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "PlayViewController.h"
#import <PLPlayerKit/PLPlayerKit.h>

@interface PlayViewController ()<PLPlayerDelegate>
{
    UIView *AlertView;
}
@property (nonatomic, strong) PLPlayer  *player;
//@property (nonatomic,strong)  NSString *push_url;

@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self initPlayer];
  
      [self reGetLive];

    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = YES;
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.player stop];
}

-(void)initPlayer{
    PLPlayerOption *option = [PLPlayerOption defaultOption];
    [option setOptionValue:@10 forKey:PLPlayerOptionKeyTimeoutIntervalForMediaPackets];
    if (_push_url) {
        NSURL *url = [NSURL URLWithString:self.push_url];
        self.player = [PLPlayer playerWithURL:url option:option];
    }
    self.player.delegate = self;
    [self.view addSubview:self.player.playerView];

    [self.player play];
    
    [self creatUI];

}
-(void)creatUI{
    SZTextView *text = [[SZTextView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 40-Height_NavBar, 130, 30)];
    text.placeholder = @"你想说什么？";
    text.layer.cornerRadius = 8;
    text.layer.borderWidth = 0.2f;
    text.layer.borderColor = LYColor(237, 204, 105).CGColor;
    text.backgroundColor = [UIColor whiteColor];
    //[self.player.playerView addSubview:text];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = CGRectMake(150, SCREEN_HEIGHT - 40, 60, 25);
    [sendButton setTitle:@"发送" forState:UIControlStateNormal];
    sendButton.titleLabel.font  =FONT(14);
    sendButton.backgroundColor = LYColor(237, 204, 105);
    sendButton.layer.cornerRadius = 10;
    sendButton.clipsToBounds = YES;
    [sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   // [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
    //[self.player.playerView addSubview:sendButton];
    
    CGFloat MaxWidth = [self getLabelWidthWithText:self.nickName width:SCREEN_WIDTH - 20 font:FONT(16)];
    //160
    
    
    UIView *yellowBaV = [[UIView alloc]initWithFrame:CGRectMake(10, 25, 120+MaxWidth, 60)];
    yellowBaV.backgroundColor = MainColor;
    yellowBaV.alpha = 0.5f;
    yellowBaV.layer.cornerRadius = 10;
    
    
    //60
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 50, 50)];
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",self.avatar];
    [image sd_setImageWithURL:[NSURL URLWithString:url]];
    image.clipsToBounds = YES;
    image.layer.cornerRadius = 25;
    [yellowBaV addSubview:image];
    

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(65, 20, MaxWidth, 20)];
    label.text = [NSString stringWithFormat:@"%@",self.nickName];
    label.font = FONT(16);
    [yellowBaV addSubview:label];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label.frame) , 18, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [yellowBaV addSubview:memberImage];
    
    UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) +2, memberImage.frame.origin.y + 5, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [yellowBaV addSubview:_LevelLabel];
    [self.view addSubview:yellowBaV];
    
    
    UILabel *CountLabel = [[UILabel alloc]initWithFrame:CGRectMake(yellowBaV.frame.origin.x, CGRectGetMaxY(yellowBaV.frame) + 10 , 80, 20)];
    CountLabel.backgroundColor = LYColor(237, 204, 105);
    CountLabel.alpha = 0.3f;
    CountLabel.layer.cornerRadius = 6;
    CountLabel.clipsToBounds = YES;
    CountLabel.text  = @"10.5k";
    CountLabel.textColor = [UIColor whiteColor];
    CountLabel.font = FONT(14);
   // [self.player.playerView addSubview:CountLabel];
}
//-(void)getLiu{
//    [PPNetworkHelper GET:@"http://dev.iamiweb.com/api/get_csrf_token" parameters:nil success:^(id responseObject) {
//        NSLog(@"%@",responseObject);
//      
//        [self reGetLive];
//    } failure:^(NSError *error) {
//        NSLog(@"%@",error);
//    }];
//}
-(void)reGetLive{
  
    NSString *url = [NSString stringWithFormat:@"%@/live/getNewPushURL",TestUrl];
    NSDictionary *paramDic = @{@"qiniu_id":self.qiniu_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [CYToast showStatusWithString:@"正在加载"];
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        _push_url = dic[@"data"];
        NSLog(@"%@",dic);
        [self initPlayer];

    } failure:^(NSError *error) {
         [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)player:(PLPlayer *)player statusDidChange:(PLPlayerStatus)state{
    NSLog(@"%ld",state);
    if (player.playing) {
        NSLog(@"11playing");
    }else{
        NSLog(@"11play end");
    }
    if (state == PLPlayerStatusCaching) {
       
    }
}
- (void)player:(nonnull PLPlayer *)player stoppedWithError:(nullable NSError *)error {
    // 当发生错误，停止播放时，会回调这个方法
    if (player.playing) {
        NSLog(@"playing");
    }else{
        NSLog(@"play end");
    }
    NSLog(@"%@",error);
     [self showEndLive];
}
-(void)showEndLive{
    AlertView = [[UIView alloc]initWithFrame:CGRectMake(10, (SCREEN_HEIGHT - Height_NavBar) / 2   - 80, SCREEN_WIDTH - 20, 150)];
   // AlertView.center = self.view.center;
    AlertView.backgroundColor = [UIColor whiteColor];
    AlertView.layer.cornerRadius = 10;
    
    UILabel *mainLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, 20, AlertView.frame.size.width - 60, 20)];
    mainLabel.textAlignment = NSTextAlignmentCenter;
    mainLabel.text = NSLocalizedString(@"直播已结束，回到首页", nil);
    mainLabel.textColor = [UIColor blackColor];
    mainLabel.font = FONT(17);
    [AlertView addSubview:mainLabel];
    
    UIButton *confimrBut = [UIButton buttonWithType:UIButtonTypeCustom];
    confimrBut.frame = CGRectMake((SCREEN_WIDTH - 20)/ 2 - 50, 90, 100, 40);
    [confimrBut setTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
    confimrBut.backgroundColor = MainColor;
    [confimrBut addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    confimrBut.layer.cornerRadius = 10;
    [confimrBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AlertView addSubview:confimrBut];
    
//    UIButton *cancelBut = [UIButton buttonWithType:UIButtonTypeCustom];
//    cancelBut.frame = CGRectMake(CGRectGetMaxX(confimrBut.frame) + 20, 90, 100, 40);
//    [cancelBut setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
//    cancelBut.backgroundColor = LYColor(223, 223, 223);
//    [cancelBut addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
//    cancelBut.layer.cornerRadius = 10;
//    [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [AlertView addSubview:cancelBut];
    
    [self.player.playerView addSubview:AlertView];
    //[[QWAlertView sharedMask]show:AlertView withType:QWAlertViewStyleActionSheetDown];
    
}
//-(void)timer{
//     [self showEndLive];
//}
-(void)cancel{
   // [[QWAlertView sharedMask]dismiss];
    AlertView.frame = CGRectMake(0, 0, 0, 0);
    [AlertView removeFromSuperview];
    
}
-(void)back{
     //[[QWAlertView sharedMask]dismiss];
     [AlertView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
