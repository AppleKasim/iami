//
//  LiveModel.m
//  PHSJIamILive
//
//  Created by user on 2018/3/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "LiveModel.h"

@implementation LiveModel

+(LiveModel *)Instance{
    return [[LiveModel alloc]init];
}

+(LiveModel *)parser:(NSDictionary *)mInfo{
    LiveModel *model = [LiveModel Instance];
    model.qiniu_id = [NetDataCommon stringFromDic:mInfo forKey:@"qiniu_id"];
    model.push_url = [NetDataCommon stringFromDic:mInfo forKey:@"push_url"];
    model.nickname = [NetDataCommon stringFromDic:mInfo forKey:@"nickname"];
    model.avatar = [NetDataCommon stringFromDic:mInfo forKey:@"avatar"];
    return model;
}
@end
