//
//  PlayViewController.h
//  PHSJIamILive
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"

@interface PlayViewController : XTViewController
@property (nonatomic,copy) NSString *qiniu_id;
@property (nonatomic,copy) NSString *push_url;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *nickName;
@end
