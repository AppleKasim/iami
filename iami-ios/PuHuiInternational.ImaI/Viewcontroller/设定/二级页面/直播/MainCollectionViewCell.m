//
//  MainCollectionViewCell.m
//  PHSJIamILive
//
//  Created by user on 2018/3/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainCollectionViewCell.h"
#define cellHeight 110
@implementation MainCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        _MainImage  = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, cellHeight, cellHeight)];
        _MainImage.backgroundColor = [UIColor redColor];
        _MainImage.clipsToBounds = YES;
        _MainImage.layer.cornerRadius = 55;
        _MainImage.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_MainImage];
        
        _NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, cellHeight + 5, cellHeight, 20)];
        _NameLabel.font = Bold_FONT(19);
        _NameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_NameLabel];
       
        
    }
    
    return self;
}


@end
