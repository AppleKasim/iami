//
//  MainCollectionViewCell.h
//  PHSJIamILive
//
//  Created by user on 2018/3/14.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong) UIImageView *MainImage;
@property (nonatomic,strong) UILabel *NameLabel;
@end
