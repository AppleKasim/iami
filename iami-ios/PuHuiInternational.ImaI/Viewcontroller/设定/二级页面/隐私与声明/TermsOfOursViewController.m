//
//  TermsOfOursViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/23.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "TermsOfOursViewController.h"

@interface TermsOfOursViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
    
   // CGFloat size;
}
@property (nonatomic,copy) NSString *MainString;
@property (nonatomic,copy) NSArray *DataArray;
@property (nonatomic,copy) NSArray *HeadArray;
@end

@implementation TermsOfOursViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _HeadArray = @[NSLocalizedString(@"服务条款（即权利与义务宣告", nil),NSLocalizedString(@"隐私设定", nil),NSLocalizedString(@"安全性", nil),NSLocalizedString(@"注册和帐号安保", nil),NSLocalizedString(@"保护其他人的权利", nil),NSLocalizedString(@"手机与其他装置", nil),NSLocalizedString(@"付款", nil)];
    
    
    _DataArray = @[NSLocalizedString(@"服务条款（即权利与义务宣告 内容", nil),NSLocalizedString(@"隐私设定 内容", nil),NSLocalizedString(@"安全性 内容", nil),NSLocalizedString(@"注册和帐号安保 内容", nil),NSLocalizedString(@"保护其他人的权利 内容", nil),NSLocalizedString(@"手机与其他装置 内容", nil),NSLocalizedString(@"付款 内容", nil)];
    self.view.backgroundColor =LYColor(223, 223, 223);
    [self creatTable];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
#pragma mark - TableView delegeta && datasource
-(void)creatTable{
    
   // NSInteger i = self.navigationController.viewControllers.count;
//    if () {
//        <#statements#>
//    }
    
    if ([self.type isEqualToString:@"1"]) {
        MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH -20 , SCREEN_HEIGHT -Height_NavBar - 20) style:UITableViewStyleGrouped];
    }else{
        MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH -20 , SCREEN_HEIGHT - Height_TabBar-Height_NavBar - 20) style:UITableViewStyleGrouped];
    }
//    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH , SCREEN_HEIGHT - Height_TabBar-Height_NavBar -20) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    //MainTableView.bounces = NO;
    [self.view addSubview:MainTableView];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2+_DataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.1f;
    }
    else if (section == 1) {
        return 30;
    }
    else{
       return  [self getLabelHeightWithText:[NSString stringWithFormat:@"%@",_HeadArray[section - 2]] width:SCREEN_WIDTH - 30 font:Bold_FONT(20)] + 20;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView *viwe = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 0.1f)];
        //viwe.backgroundColor =LYColor(223, 223, 223);
        return viwe;
    }
    else if (section == 1) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 30)];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 5, 200, 20)];
        label.text = NSLocalizedString(@"使用条款", nil);
        label.font = Bold_FONT(24);
        label.textColor = MainColor;
        [view addSubview:label];
        return view;
    }else {
        CGFloat height =   [self getLabelHeightWithText:[NSString stringWithFormat:@"%@",_HeadArray[section - 2]] width:SCREEN_WIDTH - 30 font:Bold_FONT(20)] ;
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 20, 20)];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, SCREEN_WIDTH - 20, height)];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.text = [NSString stringWithFormat:@"%@",_HeadArray[section - 2]];
        label.font = Bold_FONT(20);
        label.textColor =LYColor(142, 142, 142);
        [view addSubview:label];
        return view;
    }
    return nil;
}
-(CGFloat )tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section > 1) {
        return   [self getLabelHeightWithText:[NSString stringWithFormat:@"%@",_DataArray[section - 2]] width:SCREEN_WIDTH - 30 font:FONT(18)] ;
    }
    return 5.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section > 1) {
        CGFloat size =    [self getLabelHeightWithText:[NSString stringWithFormat:@"%@",_DataArray[section - 2]] width:SCREEN_WIDTH - 30 font:FONT(18)] ;
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 20, size)];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 30, size)];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.text = [NSString stringWithFormat:@"%@",_DataArray[section - 2]];
        label.font = FONT(18);
        label.textColor =LYColor(142, 142, 142);
        [view addSubview:label];
        return view;
    }else{
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 5.0f)];
        return view;
    }
    return nil;
}
-(void)rightlogo{
    if ([PHUserModel sharedPHUserModel].isLogin == NO) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [UIView animateWithDuration:0.25f animations:^{
            self.tabBarController.selectedIndex = 0;
        }];
    }
}
@end
