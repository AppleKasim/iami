//
//  UserInformationViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"
#import "CountryModel.h"

@interface UserInformationViewController : XTViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIView * mPickView;
    UIPickerView * pick;
    UIView *mGrayView;
}
@end
