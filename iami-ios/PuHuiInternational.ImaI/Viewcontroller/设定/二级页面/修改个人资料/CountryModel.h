//
//  CountryModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryModel : NSObject
#pragma mark - 国家的
@property (nonatomic,strong) NSString *cname;
@property (nonatomic,strong) NSString *country_id;

#pragma mark - 语系的
@property (nonatomic,strong) NSString *language_id;
@property (nonatomic,strong) NSString *language_name;

@property (nonatomic,strong) NSString *type;

+(CountryModel *)parsenWithDic:(NSDictionary *)dict;
+(CountryModel *)parsenWithLanguageDic:(NSDictionary *)dict;
@end
