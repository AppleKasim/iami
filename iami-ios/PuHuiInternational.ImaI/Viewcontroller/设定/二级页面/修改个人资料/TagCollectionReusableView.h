//
//  TagCollectionReusableView.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol TagReuseDelegate  <NSObject>

-(void)returenText:(NSString *)text;
@end
@interface TagCollectionReusableView : UICollectionReusableView<UITextViewDelegate,UITextFieldDelegate>
{
    UIImageView *maskImage;

}
@property (nonatomic,strong) UITextField *TagText;

@property (nonatomic,strong) UIButton *AddButton;

@property (nonatomic,weak) id<TagReuseDelegate> delegate;
@end
