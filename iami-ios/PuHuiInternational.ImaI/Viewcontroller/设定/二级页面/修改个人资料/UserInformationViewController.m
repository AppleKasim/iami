//
//  UserInformationViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "UserInformationViewController.h"
#import "TagModel.h"
#import "TagSelectViewController.h"
#import "LPTagModel.h"
#import "LPTagCollectionView.h"


@interface UserInformationViewController ()<UITableViewDelegate,UITableViewDataSource,TaggDeleaget,LPSwitchTagDelegate>
{
    UITableView *MainTableView;
    UIButton *confirmBut;
    NSString *Date;
    NSString *countryId;
    NSString *langId;
    
    UIButton *boyBut;
    UIButton *girlBut;
    
    NSString   *city;
    NSString   *company;
    NSString   *country;
    NSString   *department;
    NSString   *position;
    NSString   *relationship;
    NSString   *resume;
    NSString   *school;
    NSString   *birthday;
    NSString   *gender;
    NSString   *phone;
    
    UIView *backView;

    
    TagSelectViewController *controller;
    
    LPTagCollectionView *_tagCollectionView;

    UIScrollView *tagScroll;
    CGFloat tagHeight;
    
    UIButton *privateBut;
    NSString *slectType;
    NSInteger RelationIndex;
    
    //队列
    dispatch_group_t downloadGroup;
    
    UITextView *_currentTF;
    
    DatePickerTool *datePicker;
}
@property (nonatomic,copy) NSMutableArray *BirthArray;
@property (nonatomic,copy) NSMutableArray *CountryArray;
@property (nonatomic,copy) NSMutableArray *LanguageArray;

@property (nonatomic,copy) NSMutableArray *TagArray;
@property (nonatomic,copy) NSArray *RelationArray;
@property (nonatomic,copy) NSArray *NewRelaArray;

@property (nonatomic,copy) NSMutableArray *ShowMessageArray;
#define cornerR 10
#define Headerheight 750

#define editInfo @"/api/editMember"//旧的编辑接口
#define editMember @"/api/editMember2"
#define getCountries @"/api/getCountries"
#define getself @"/member/getMemberInfo"
#define getAllLabel @"/api/getAllLabels"//取得所有的标签
#define getSelfLabel @"/api/getLabels"//取得会员所有的标签
#define AddLabel @"/api/doLabel"//增加标签
#define AddSelfLabel @"/label/addLabel"
#define DeleteLabel @"/label/delLabel"//删除标签
#define getLanguage @"/language/getLanguage"//获取语系列表
@end

@implementation UserInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _TagArray = [[NSMutableArray alloc]init];
     controller = [[TagSelectViewController alloc]init];
    tagHeight = 0;
    controller.delegate = self;
    self.view.backgroundColor =LYColor(223, 223, 223);
    _BirthArray = [[NSMutableArray alloc]init];
    _CountryArray = [[NSMutableArray alloc]init];
    _LanguageArray = [[NSMutableArray alloc]init];
    _ShowMessageArray = [[NSMutableArray alloc]init];
    //_ShowMessageArray addObject:<#(nonnull id)#>
    
    _RelationArray = @[NSLocalizedString(@"已婚", nil),NSLocalizedString(@"离婚", nil),NSLocalizedString(@"交往中", nil),NSLocalizedString(@"单身", nil)];
    _NewRelaArray = @[@"married",@"devorce",@"dating",@"single"];
    [self creatTable];
    [self getSelfLabe];
    [self loadselfInfo];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self animationStoped];
    [datePicker removeFromSuperview];
}
-(void)selectArray:(NSMutableArray *)SelectArray{
    [_TagArray removeAllObjects];
    for (LPTagModel *model in SelectArray) {
        [_TagArray addObject:model];
    }
    tagHeight = 50;
    [MainTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}
#pragma =======TableView delegeta && datasource ===================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 50, SCREEN_WIDTH - 20, SCREEN_HEIGHT - Height_NavBar  - Height_TabBar  - 55) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.layer.cornerRadius = 10;
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.bounces = NO;
    MainTableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:MainTableView];
    
     [self.view addSubview:[self creatHeader]];
}
-(UIView *)creatHeader{
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH - 20, 50)];
    backView.backgroundColor =MainColor;
    backView.layer.cornerRadius = 10;
    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH - 10, 20)];
    TitleLabel.text = NSLocalizedString(@"基本资料编辑", nil);
    
    TitleLabel.textColor = [UIColor whiteColor];
    TitleLabel.font = FONT(20);
    TitleLabel.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:TitleLabel];
    return backView;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return Headerheight + tagHeight;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    [_ShowMessageArray removeAllObjects];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 20, Headerheight)];
    
    UIImageView *imag = [[UIImageView alloc]initWithFrame:CGRectMake(5, 20, 20, 20)];
    imag.image = [UIImage imageNamed:@"昵称"];
    imag.clipsToBounds = YES;
    imag.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imag];
    
    SZTextView *nickTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, 15, SCREEN_WIDTH - 90, 30)];
    nickTld.delegate = self;
    nickTld.tag = 70;
    nickTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    nickTld.text = [PHUserModel sharedPHUserModel].nickname;
    nickTld.layer.borderWidth = 1.0f;
    nickTld.layer.cornerRadius =cornerR;
    nickTld.font = FONT(14);

    nickTld.clipsToBounds = YES;
    nickTld.userInteractionEnabled=YES;
    //nickTld.editable = NO;
    nickTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    
    privateBut = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, nickTld.frame.origin.y+ 5, 20, 20);
    [privateBut setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    privateBut.tag = 600 ;
    [privateBut addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
   
    
    UIButton *alarmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    alarmButton.frame = CGRectMake(SCREEN_WIDTH - 125, 5, 20, 20);
    [alarmButton setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateNormal];
    alarmButton.tag = 80;
    alarmButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [alarmButton addTarget:self action:@selector(showNickAlarm) forControlEvents:UIControlEventTouchUpInside];
    [nickTld addSubview:alarmButton];
     [view addSubview:nickTld];
    
    //emial
    UIImageView *imageemial = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(nickTld.frame) + 25, 20, 20)];
    imageemial.image = [UIImage imageNamed:@"邮箱"];
    imageemial.clipsToBounds = YES;
    imageemial.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imageemial];
    
    UILabel *emialLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, CGRectGetMaxY(nickTld.frame) + 20, SCREEN_WIDTH - 90, 30)];
    emialLabel.text = [PHUserModel sharedPHUserModel].email;
    emialLabel.font = FONT(17);
    [view addSubview:emialLabel];
    
    
    UIButton *privateButemial = [UIButton buttonWithType:UIButtonTypeCustom];
    privateButemial.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, emialLabel.frame.origin.y+ 5, 20, 20);
    privateButemial.tag = 601 ;
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"email"]) {
         privateButemial.selected = YES;
    }
    [privateButemial setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateButemial setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateButemial addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    //[view addSubview:privateButemial];
    
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(emialLabel.frame) + 25, 20, 20)];
    image1.image = [UIImage imageNamed:@"个人简历"];
    image1.clipsToBounds = YES;
    image1.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image1];
    
    SZTextView *MainText = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(emialLabel.frame) + 25, SCREEN_WIDTH - 90, 80)];
    if (resume.length > 0) {
        MainText.text = resume;
    }else{
        MainText.placeholder = NSLocalizedString(@"请输入您个人的简历", nil);
    }
    MainText.delegate = self;
    MainText.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    MainText.layer.borderWidth = 1.0f;
    MainText.layer.cornerRadius =cornerR;
    MainText.tag = 200;
    MainText.font = FONT(17);
    MainText.clipsToBounds = YES;
    MainText.layer.borderColor = LYColor(223, 223, 223).CGColor;
    [view addSubview:MainText];
    
    UIButton *privateBut1 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut1.frame = CGRectMake(CGRectGetMaxX(emialLabel.frame) + 5, MainText.frame.origin.y+ 5, 20, 20);
    privateBut1.tag = 602;
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"resume"]) {
        privateBut1.selected = YES;
        [_ShowMessageArray addObject:@"resume"];
    }
    [privateBut1 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut1 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut1 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut1];
    
    
    UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(MainText.frame) + 25, 20, 20)];
    image2.image = [UIImage imageNamed:@"生日"];
    image2.clipsToBounds = YES;
    image2.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image2];
    
    UILabel *birthLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(MainText.frame) + 25, 40, 20)];
    birthLabel.text = NSLocalizedString(@"生日", nil);
    birthLabel.textColor = LYColor(140, 140, 140);
    birthLabel.font = FONT(14);
    [view addSubview:birthLabel];
    
    CGFloat width = (SCREEN_WIDTH -90 - 75) / 3;
    
    for (NSInteger i = 0; i < _BirthArray.count; i++) {
        UIButton *birButton = [UIButton buttonWithType:UIButtonTypeCustom];
        birButton.frame = CGRectMake((width + 10) * i +75, CGRectGetMaxY(MainText.frame) + 20, width, 30);
        [birButton setTitle:[NSString stringWithFormat:@"%@",_BirthArray[i]] forState:UIControlStateNormal];
        [birButton setTitleColor:LYColor(106, 106, 106) forState:UIControlStateNormal];
        birButton.layer.borderWidth = 1.0f;
        birButton.layer.cornerRadius = cornerR;
        birButton.layer.borderColor = LYColor(223, 223, 223).CGColor;
        [birButton addTarget:self action:@selector(BirthChoose) forControlEvents:UIControlEventTouchUpInside];
        birButton.tag = 400 + i;
        [view addSubview:birButton];
    }
    
    UIButton *privateButBirth = [UIButton buttonWithType:UIButtonTypeCustom];
    privateButBirth.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, CGRectGetMaxY(MainText.frame) + 25, 20, 20);
    [privateButBirth setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    privateButBirth.tag = 603;
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"birth"]) {
        privateButBirth.selected = YES;
        [_ShowMessageArray addObject:@"birth"];

    }
    [privateButBirth setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateButBirth addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateButBirth];
    
    //性别选择
    UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(birthLabel.frame) + 25, 20, 20)];
    image3.image = [UIImage imageNamed:@"性别"];
    image3.clipsToBounds = YES;
    image3.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image3];
    
    UILabel *SexLabel = [[UILabel alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(birthLabel.frame) + 25, 40, 20)];
    SexLabel.text = NSLocalizedString(@"性别", nil);
    SexLabel.textColor = LYColor(140, 140, 140);
    [view addSubview:SexLabel];
    
    boyBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [boyBut setImage:[UIImage imageNamed:@"icon-选择框"] forState:UIControlStateNormal];
    [boyBut setImage:[UIImage imageNamed:@"icon-选择框选中"] forState:UIControlStateSelected];
    boyBut.selected = NO;
    [boyBut addTarget:self action:@selector(SexClick:) forControlEvents:UIControlEventTouchUpInside];
    boyBut.frame = CGRectMake(80, CGRectGetMaxY(birthLabel.frame) + 25, 15, 15);
    
    
    UILabel *boyLabel = [[UILabel alloc]initWithFrame:CGRectMake(105, CGRectGetMaxY(birthLabel.frame) + 23, 30, 20)];
    boyLabel.text = NSLocalizedString(@"男", nil);
    boyLabel.textColor = LYColor(140, 140, 140);
    boyLabel.font = FONT(15);
    [view addSubview:boyBut];
    [view addSubview:boyLabel];
    
    girlBut = [UIButton buttonWithType:UIButtonTypeCustom];
    [girlBut setImage:[UIImage imageNamed:@"icon-选择框"] forState:UIControlStateNormal];
    [girlBut setImage:[UIImage imageNamed:@"icon-选择框选中"] forState:UIControlStateSelected];
    girlBut.selected = NO;
    [girlBut addTarget:self action:@selector(SexClick:) forControlEvents:UIControlEventTouchUpInside];
    girlBut.frame = CGRectMake(CGRectGetMaxX(boyLabel.frame), CGRectGetMaxY(birthLabel.frame) + 25, 15, 15);
    
    UILabel *girlLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(girlBut.frame) + 10, CGRectGetMaxY(birthLabel.frame) + 23, 30, 20)];
    girlLabel.text = NSLocalizedString(@"女", nil);
    girlLabel.textColor = LYColor(140, 140, 140);
    girlLabel.font = FONT(15);
    [view addSubview:girlLabel];
    [view addSubview:girlBut];
    
    if ([gender isEqualToString:@"F"]) {
        girlBut.selected = YES;
    }else{
        boyBut.selected = YES;
    }
    
    UIButton *privateBut2 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut2.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, SexLabel.frame.origin.y+ 5, 20, 20);
    [privateBut2 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut2 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    privateBut2.tag = 604;
//    for (NSString *dict in [PHUserModel sharedPHUserModel].info_show) {
//        if ([dict isEqualToString:@"gender"]) {
//            privateBut2.selected = YES;
//        }
//    }
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"gender"]) {
        privateBut2.selected = YES;
        [_ShowMessageArray addObject:@"gender"];

    }
    [privateBut2 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut2];
    
    //手机
    UIImageView *imagephone = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(girlLabel.frame) + 25, 20, 20)];
    imagephone.image = [UIImage imageNamed:@"手机"];
    imagephone.clipsToBounds = YES;
    imagephone.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:imagephone];
    
    SZTextView *phoneTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(girlLabel.frame) + 20, SCREEN_WIDTH - 90, 30)];
    phoneTld.font = FONT(12);
    phoneTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    phoneTld.delegate = self;
    phoneTld.layer.cornerRadius = cornerR;
    phoneTld.layer.borderWidth = 1.0f;
    phoneTld.tag = 209;
    phoneTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (phone.length > 0) {
        phoneTld.text = phone;
    }else{
        phoneTld.placeholder = NSLocalizedString(@"手机", nil);
    }
    [view addSubview:phoneTld];
    
    UIButton *phoneBut = [UIButton buttonWithType:UIButtonTypeCustom];
    phoneBut.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, phoneTld.frame.origin.y+ 5, 20, 20);
    phoneBut.tag = 605;

    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"mobile"]) {
        phoneBut.selected = YES;
        [_ShowMessageArray addObject:@"mobile"];
    }
    [phoneBut setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [phoneBut setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [phoneBut addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:phoneBut];
    
    
    //任职公司
    UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(phoneTld.frame) + 25, 20, 20)];
    image4.image = [UIImage imageNamed:@"任职公司"];
    image4.clipsToBounds = YES;
    image4.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image4];

    CGFloat bussinessWidth = SCREEN_WIDTH - 90;
    
    SZTextView *BussinessTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(phoneTld.frame) + 20, bussinessWidth * 0.65, 30)];
    BussinessTld.font = FONT(12);
    BussinessTld.delegate = self;
    BussinessTld.showsVerticalScrollIndicator = NO;
    BussinessTld.scrollEnabled = NO;
    BussinessTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    BussinessTld.layer.cornerRadius = cornerR;
    BussinessTld.layer.borderWidth = 1.0f;
    BussinessTld.tag = 201;
    BussinessTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (company.length > 0) {
        BussinessTld.text = company;
    }else{
         BussinessTld.placeholder = NSLocalizedString(@"任职公司", nil);
    }
    [view addSubview:BussinessTld];
    
    SZTextView *jobTld = [[SZTextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(BussinessTld.frame) + bussinessWidth *0.1, CGRectGetMaxY(phoneTld.frame) + 20, bussinessWidth *0.25, 30)];
    jobTld.layer.borderWidth = 1.0f;
    jobTld.scrollEnabled = NO;
    jobTld.showsVerticalScrollIndicator = NO;
    jobTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    jobTld.layer.cornerRadius =  cornerR;
    jobTld.delegate = self;
    jobTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (position.length > 0) {
        jobTld.text = position;
    }else{
        jobTld.placeholder = NSLocalizedString(@"职务", nil);
    }
    jobTld.tag = 202;
    jobTld.font = FONT(12);
    [view addSubview:jobTld];
    
    UIButton *privateBut3 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut3.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, jobTld.frame.origin.y+ 5, 20, 20);
    privateBut3.tag = 606;
//    for (NSString *dict in [PHUserModel sharedPHUserModel].info_show) {
//        if ([dict isEqualToString:@"company"]) {
//            privateBut3.selected = YES;
//        }
//    }
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"company"] ||[[PHUserModel sharedPHUserModel].info_show containsString:@"companies_0"]) {
        privateBut3.selected = YES;
        [_ShowMessageArray addObject:@"companies_0"];

    }
    [privateBut3 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut3 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut3 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut3];
    
    UIImageView *image5 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(jobTld.frame) + 25, 20, 20)];
    image5.image = [UIImage imageNamed:@"学校"];
    image5.clipsToBounds = YES;
    image5.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image5];
    
    SZTextView *SchoolTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(jobTld.frame) + 20, bussinessWidth * 0.65, 30)];
    SchoolTld.font = FONT(12);
    SchoolTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    SchoolTld.showsVerticalScrollIndicator = NO;
    SchoolTld.scrollEnabled = NO;
    SchoolTld.tag = 203;
    SchoolTld.layer.borderWidth = 1.0f;
    SchoolTld.layer.cornerRadius = cornerR;
    SchoolTld.delegate = self;
    SchoolTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (school.length > 0) {
         SchoolTld.text = school;
    }else{
         SchoolTld.placeholder = NSLocalizedString(@"学校", nil);
    }
    [view addSubview:SchoolTld];
    
    SZTextView *MajorTld = [[SZTextView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(BussinessTld.frame) + bussinessWidth * 0.1, CGRectGetMaxY(jobTld.frame) + 20, bussinessWidth * 0.25, 30)];
    MajorTld.layer.borderWidth = 1.0f;
    MajorTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    MajorTld.showsVerticalScrollIndicator = NO;
    MajorTld.scrollEnabled = NO;
    MajorTld.layer.cornerRadius  =cornerR;
    MajorTld.tag = 204;
    MajorTld.delegate = self;
    MajorTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (department.length > 0) {
        MajorTld.text = department;
    }else{
        MajorTld.placeholder = NSLocalizedString(@"科系", nil);

    }
    MajorTld.font = FONT(12);
    [view addSubview:MajorTld];
    
    UIButton *privateBut4 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut4.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, MajorTld.frame.origin.y+ 5, 20, 20);
    privateBut4.tag = 607;

    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"school"]||[[PHUserModel sharedPHUserModel].info_show containsString:@"schools_0"]) {
        privateBut4.selected = YES;
        [_ShowMessageArray addObject:@"schools_0"];

    }
    [privateBut4 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut4 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut4 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut4];
    
    UIImageView *image6 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(MajorTld.frame) + 25, 20, 20)];
    image6.image = [UIImage imageNamed:@"居住地"];
    image6.clipsToBounds = YES;
    image6.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image6];
    
    SZTextView *addressTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(MajorTld.frame) + 20, SCREEN_WIDTH - 90, 30)];
    addressTld.layer.borderWidth = 1.0f;
    addressTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);
    addressTld.layer.cornerRadius  =cornerR;
    addressTld.tag = 205;
    addressTld.delegate = self;
    addressTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (city.length > 0) {
         addressTld.text = city;
    }else{
        addressTld.placeholder = NSLocalizedString(@"居住地", nil);
    }
    addressTld.font = FONT(12);
    [view addSubview:addressTld];
    
    UIButton *privateBut5 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut5.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, addressTld.frame.origin.y+ 5, 20, 20);
    privateBut5.tag = 608;
//    for (NSString *dict in [PHUserModel sharedPHUserModel].info_show) {
//        if ([dict isEqualToString:@"city"]) {
//            privateBut5.selected = YES;
//        }
//    }
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"city"]) {
        privateBut5.selected = YES;
        [_ShowMessageArray addObject:@"city"];

    }
    [privateBut5 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut5 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut5 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut5];
    
    UIImageView *image7 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(addressTld.frame) + 25, 20, 20)];
    image7.image = [UIImage imageNamed:@"国籍"];
    image7.clipsToBounds = YES;
    image7.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image7];
    
    UIButton *CountryBut = [UIButton buttonWithType:UIButtonTypeCustom];
    CountryBut.frame =CGRectMake(30, CGRectGetMaxY(addressTld.frame) + 20, SCREEN_WIDTH - 90, 30);
    CountryBut.layer.borderWidth = 1.0f;
    CountryBut.tag = 206;
    CountryBut.layer.cornerRadius  =cornerR;
    CountryBut.layer.borderColor = LYColor(223, 223, 223).CGColor;
    CountryBut.titleLabel.font = FONT(13);
    CountryBut.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
    CountryBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [CountryBut addTarget:self action:@selector(getCountri) forControlEvents:UIControlEventTouchUpInside];
    if (country.length > 0) {
        [CountryBut setTitle:country forState:UIControlStateNormal];
        [CountryBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    }else{
       [CountryBut setTitle:NSLocalizedString(@"请选择国籍", nil) forState:UIControlStateNormal];
        [CountryBut setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];

    }
    
    SZTextView *nationTld = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(addressTld.frame) + 20, SCREEN_WIDTH - 90, 30)];
    nationTld.layer.borderWidth = 1.0f;
    nationTld.textContainerInset = UIEdgeInsetsMake(7,10, 5, 0);

    nationTld.layer.cornerRadius  =cornerR;
    nationTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    if (country.length > 0) {
        nationTld.text = country;
    }else{
        nationTld.placeholder = NSLocalizedString(@"国籍", nil);
    }
    
    nationTld.font = FONT(12);
    [view addSubview:CountryBut];
    
    UIButton *privateBut6 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut6.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, nationTld.frame.origin.y+ 5, 20, 20);
    privateBut6.tag = 609;

    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"country"]) {
        privateBut6.selected = YES;
        [_ShowMessageArray addObject:@"country_id"];

    }
    [privateBut6 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut6 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut6 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut6];
    
    UIImageView *image8 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(nationTld.frame) + 25, 20, 20)];
    image8.image = [UIImage imageNamed:@"感情状态"];
    image8.clipsToBounds = YES;
    image8.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image8];
    

    UIButton *LoveBut = [UIButton buttonWithType:UIButtonTypeCustom];
    LoveBut.frame =CGRectMake(30, CGRectGetMaxY(nationTld.frame) + 20, SCREEN_WIDTH - 90, 30);
    LoveBut.layer.borderWidth = 1.0f;
    LoveBut.tag = 207;
    LoveBut.layer.cornerRadius  =cornerR;
    LoveBut.layer.borderColor = LYColor(223, 223, 223).CGColor;
    LoveBut.titleLabel.font = FONT(13);
    LoveBut.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);

    LoveBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [LoveBut addTarget:self action:@selector(getrelation) forControlEvents:UIControlEventTouchUpInside];
  
    if (relationship.length > 0) {
        [LoveBut setTitle:[NSString stringWithFormat:@"%@",relationship] forState:UIControlStateNormal];
          [LoveBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
        [LoveBut setTitle:NSLocalizedString(@"感情状态", nil) forState:UIControlStateNormal];
          [LoveBut setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    }
    [view addSubview:LoveBut];
    
    UIButton *privateBut7 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut7.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, LoveBut.frame.origin.y+ 5, 20, 20);
    privateBut7.tag = 610;
//    for (NSDictionary *dict in [PHUserModel sharedPHUserModel].info_show) {
//        if ([[dict allKeys]containsObject:@"relationship"]) {
//            privateBut7.selected = YES;
//        }
//    }
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"relationship"]) {
        privateBut7.selected = YES;
        [_ShowMessageArray addObject:@"relationship"];

    }
    [privateBut7 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut7 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut7 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:privateBut7];
    
    
    UIImageView *image9 = [[UIImageView alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(LoveBut.frame) + 25, 20, 20)];
    image9.image = [UIImage imageNamed:@"语系设定"];
    image9.clipsToBounds = YES;
    image9.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image9];
    
    UIButton *LanguageBut = [UIButton buttonWithType:UIButtonTypeCustom];
    LanguageBut.frame =CGRectMake(30, CGRectGetMaxY(LoveBut.frame) + 20, SCREEN_WIDTH - 90, 30);
    LanguageBut.layer.borderWidth = 1.0f;
    LanguageBut.tag = 208;
    LanguageBut.layer.cornerRadius  =cornerR;
    LanguageBut.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);

    LanguageBut.layer.borderColor = LYColor(223, 223, 223).CGColor;
    LanguageBut.titleLabel.font = FONT(13);
    
    LanguageBut.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [LanguageBut addTarget:self action:@selector(getLang) forControlEvents:UIControlEventTouchUpInside];
  
    NSString *langguag = [PHUserModel sharedPHUserModel].language;
    if (langguag.length > 0) {
        [LanguageBut setTitle:[NSString stringWithFormat:@"%@",langguag] forState:UIControlStateNormal];
          [LanguageBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else{
         [LanguageBut setTitle:NSLocalizedString(@"请选择语系", nil) forState:UIControlStateNormal];
          [LanguageBut setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    }
    [view addSubview:LanguageBut];
    
    UIButton *privateBut8 = [UIButton buttonWithType:UIButtonTypeCustom];
    privateBut8.frame = CGRectMake(CGRectGetMaxX(nickTld.frame) + 5, LanguageBut.frame.origin.y+ 5, 20, 20);
    [privateBut8 setImage:[UIImage imageNamed:@"隐私未勾选"] forState:UIControlStateNormal];
    [privateBut8 setImage:[UIImage imageNamed:@"隐私勾选"] forState:UIControlStateSelected];
    [privateBut8 addTarget:self action:@selector(clickPriBut:) forControlEvents:UIControlEventTouchUpInside];
    //[view addSubview:privateBut8];
    
    
    
    
    UILabel *selTag = [[UILabel alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(LanguageBut.frame) +20, 90, 20)];
    selTag.text = NSLocalizedString(@"个性化标签", nil);
    selTag.textColor = MainColor;
    [selTag sizeToFit];
    selTag.font = FONT(17);
    
    UIButton *selectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectButton.frame = CGRectMake(CGRectGetMaxX(selTag.frame)
                                    + 10, CGRectGetMaxY(LanguageBut.frame) +17, 30, 30);
    [selectButton setBackgroundImage:[UIImage imageNamed:@"添加标签"] forState:UIControlStateNormal];
    [selectButton addTarget:self action:@selector(getAllLabels) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:selTag];
    [view addSubview:selectButton];
    
    
    CGFloat maxWdith = 5;
    if (_TagArray && _TagArray.count > 0) {
        tagScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(selTag.frame) +20, SCREEN_WIDTH - 20, 40)];
        tagScroll.showsVerticalScrollIndicator = NO;
        tagScroll.showsHorizontalScrollIndicator = NO;
       // tagScroll.contentSize = CGSizeMake(5+75 * _TagArray.count, 40);
       
        
        
        for (NSInteger i = 0 ; i < _TagArray.count; i++) {
            
            LPTagModel *model = _TagArray[i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTitle:[NSString stringWithFormat:@"%@",model.name] forState:UIControlStateNormal];
            CGFloat width = [self getLabelWidthWithText:[NSString stringWithFormat:@"%@",model.name] width:SCREEN_WIDTH  font:FONT(13)];
            button.frame = CGRectMake(maxWdith, 0 , width + 20, 40);
            button.layer.cornerRadius=8;
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

            [button setBackgroundImage:[UIImage imageNamed:@"标签背景图"] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button.titleLabel.font = FONT(12);
            [tagScroll addSubview:button];
            
            maxWdith = maxWdith + width + 30;


        }
         tagScroll.contentSize = CGSizeMake(maxWdith + 10, 40);
       [view addSubview:tagScroll];
    }
    
    confirmBut = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBut.frame = CGRectMake(view.frame.size.width/2 - 150, CGRectGetMaxY(selTag.frame) +40 + tagHeight, 300, 35);
    [confirmBut setBackgroundColor:MainColor];
    confirmBut.layer.cornerRadius = 6;
    confirmBut.clipsToBounds = YES;
    [confirmBut setTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
    [confirmBut addTarget:self action:@selector(regist) forControlEvents:UIControlEventTouchUpInside];
    [confirmBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [view addSubview:confirmBut];
    return view;
}

/**
 删除标签
 button
 */
-(void)deleteTag:(UIButton *)button{
    
    
    NSInteger i = button.tag - 500;
    LPTagModel *model = _TagArray[i];
    
    if ([model.ident isEqualToString:@""] || model.ident.length == 0) {
        [_TagArray removeObjectAtIndex:i];
        [MainTableView reloadData];
        return;
    }
    
    
   
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,DeleteLabel];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"id":model.ident};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [_TagArray removeObjectAtIndex:i];
            [MainTableView reloadData];
        }

    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 性别选择
 */
-(void)SexClick:(UIButton *)button{
    
    if (button.selected) {
        
    }else{
        boyBut.selected = !boyBut.selected;
        girlBut.selected = !girlBut.selected;
        
    }
    
}
-(void)BirthChoose{
    datePicker = [[DatePickerTool alloc] initWithFrame:CGRectMake(10, SCREEN_HEIGHT - 250, SCREEN_WIDTH - 20, 200)];
    __block DatePickerTool *blockPick = datePicker;
    __weak  NSMutableArray *weakArr = _BirthArray;
    __weak UITableView *weakTable = MainTableView;
    __weak UserInformationViewController *weakself = self;
    

    datePicker.callBlock = ^(NSString *pickDate) {
        
        NSLog(@"%@",pickDate);
        
        if (pickDate) {
            Date = pickDate;
            [weakArr removeAllObjects];
            weakTable.scrollEnabled = YES;

            NSArray *array =   [pickDate componentsSeparatedByString:@"-"];
            UIButton *but1 = [weakself.view viewWithTag:400];
            UIButton *but2 = [weakself.view viewWithTag:401];
            UIButton *but3 = [weakself.view viewWithTag:402];
            
            [but1 setTitle:[NSString stringWithFormat:@"%@▼",array[0]] forState:UIControlStateNormal];
            [but2 setTitle:[NSString stringWithFormat:@"%@▼",array[1]] forState:UIControlStateNormal];
            [but3 setTitle:[NSString stringWithFormat:@"%@▼",array[2]] forState:UIControlStateNormal];
            [weakArr addObject:array[0]];
            [weakArr addObject:array[1]];
            [weakArr addObject:array[2]];
        }else{
            weakTable.scrollEnabled = YES;

        }
        
        [blockPick removeFromSuperview];
    };
    
     AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
     [delegate.window addSubview:datePicker];
    MainTableView.scrollEnabled = NO;

}

/**
 取得所有标签
 */
-(void)getAllLabels{
    NSString *url  = [NSString stringWithFormat:@"%@%@",TestUrl,getAllLabel];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        controller.dataarray = dic[@"data"];
        controller.selectArray = _TagArray;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
/**
 确认修改资料
 */
-(void)regist{

     downloadGroup = dispatch_group_create();
    for (LPTagModel *model in _TagArray) {
        if ([model.ident isEqualToString:@""] || model.ident == nil) {
            [self addPersonalTag:model];
        }
    }
    
    dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
         [self addLabel];
        NSLog(@"end");
    });
   
    [CYToast showStatusWithString:@"正在加载"];

   
    NSMutableArray *Info_showArray = [NSMutableArray array];
        for (NSString *info in _ShowMessageArray) {
            [Info_showArray addObject:info];
        }
    [Info_showArray addObject:@"nickname"];
   
    
    SZTextView *nickTld = [self.view viewWithTag:70];
    SZTextView *personTld= [self.view viewWithTag:200];
    SZTextView *compantTld = [self.view viewWithTag:201];
    SZTextView *positionTld = [self.view viewWithTag:202];
    SZTextView *schoolTld = [self.view viewWithTag:203];
    SZTextView *departmentTld = [self.view viewWithTag:204];
    SZTextView *cityTld = [self.view viewWithTag:205];
    //SZTextView *relationship = [self.view viewWithTag:207];
    //UIButton *relationship = [self.view viewWithTag:207];
    SZTextView *phoneTld = [self.view viewWithTag:209];

    NSMutableDictionary *muDic = [[NSMutableDictionary alloc]init];
    [muDic setObject:[PHUserModel sharedPHUserModel].AccessToken forKey:@"token"];
    [muDic setObject:[PHUserModel sharedPHUserModel].csrf_token forKey:@"csrf_token_name"];

    if (nickTld.text.length> 0) {
        [muDic setObject:[NSString stringWithFormat:@"%@",nickTld.text] forKey:@"nickname"];
    }

    if (personTld.text.length > 0) {
        [muDic setObject:[NSString stringWithFormat:@"%@",personTld.text] forKey:@"resume"];
    }

     [muDic setObject:@[[NSString stringWithFormat:@"%@",compantTld.text]] forKey:@"company"];

    [muDic setObject:@[[NSString stringWithFormat:@"%@",positionTld.text]] forKey:@"position"];

     [muDic setObject:@[[NSString stringWithFormat:@"%@",schoolTld.text]] forKey:@"school"];
    
    [muDic setObject:[NSString stringWithFormat:@"%@",Date] forKey:@"birth"];
    
     [muDic setObject:@[[NSString stringWithFormat:@"%@",departmentTld.text]] forKey:@"department"];
    if (cityTld.text.length > 0) {
        [muDic setObject:[NSString stringWithFormat:@"%@",cityTld.text] forKey:@"city"];
    }
    if (phoneTld.text.length > 0) {
        [muDic setObject:[NSString stringWithFormat:@"%@",phoneTld.text] forKey:@"mobile"];
        
    }
    if (RelationIndex && relationship.length> 0) {
        NSString *string = _NewRelaArray[RelationIndex];
        [muDic setObject:string forKey:@"relationship"];
    }
    

     [muDic setObject:Info_showArray forKey:@"info_show"];
    
    if (countryId && countryId.length>0) {
         [muDic setObject:countryId forKey:@"country_id"];
    }
    
    if (langId && langId.length > 0) {
        [muDic setObject:langId forKey:@"language_id"];
    }
    
    if (girlBut.selected) {
        [muDic setObject:@"F" forKey:@"gender"];
    }else{
        [muDic setObject:@"M" forKey:@"gender"];
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,editInfo];

    [PPNetworkHelper POST:url parameters:muDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"更改资料成功"];
            [self requestIsHaveReviewMessage];
            
        }else{
            [CYToast showErrorWithString:@"修改失败"];
        }
      [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
#pragma mark - 获得语系
-(void)getLang{
    slectType = @"cou";
    [_CountryArray removeAllObjects];
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getLanguage];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        //NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSArray *dataArr = responseObject;
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_CountryArray addObject:[CountryModel parsenWithLanguageDic:parmDic]];
            }
            [self showPicker];
        }
        
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
#pragma mark - 感情状态选择
-(void)getrelation{
    slectType = @"rel";
    [self showPicker];
}
#pragma mark - 获取国家集合
/**
获取国家集合
 */
-(void)getCountri{
    
    UIButton *countryButton = [self.view viewWithTag:206];
    //250
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[countryButton convertRect:countryButton.bounds toView:window];
    
    CGFloat height = rect.origin.y - (SCREEN_HEIGHT - 250);
    NSLog(@"%f-",height);
    if (height > 0 ) {
        [MainTableView setContentOffset:CGPointMake(0,height + 32 )];
    }
    
    [_CountryArray removeAllObjects];
    slectType = @"cou";
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getCountries];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_CountryArray addObject:[CountryModel parsenWithDic:parmDic]];
            }
        }
        [self showPicker];
    } failure:^(NSError *error) {
         [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 获得个人资料
 */
-(void)loadselfInfo{

    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getself];
   
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id};
   
   
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
         [CYToast dismiss];
        NSDictionary *dic = responseObject;
        city = [NetDataCommon stringFromDic:dic forKey:@"city"];
        NSArray *companArr = dic[@"companies"];
        if (companArr && companArr.count > 0) {
            company = [NetDataCommon stringFromDic:companArr[0] forKey:@"company"];
             position = [NetDataCommon stringFromDic:companArr[0] forKey:@"position"];
        }
        NSArray *schoolArr = dic[@"schools"];
        if (schoolArr && schoolArr.count > 0) {
            department = [NetDataCommon stringFromDic:schoolArr[0] forKey:@"department"];
            school = [NetDataCommon stringFromDic:schoolArr[0] forKey:@"school"];
        }
        birthday = [NetDataCommon stringFromDic:dic forKey:@"birth"];
        country = [NetDataCommon stringFromDic:dic forKey:@"country"];
        phone = [NetDataCommon stringFromDic:dic forKey:@"phone"];
        gender = [NetDataCommon stringFromDic:dic forKey:@"gender"];
        relationship = [NetDataCommon stringFromDic:dic forKey:@"relationship"];
        resume = [NetDataCommon stringFromDic:dic forKey:@"resume"];
        [self setBirthday];
        
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)setBirthday{
//    [_BirthArray addObject:@"1990▼"];
//    [_BirthArray addObject:@"1月▼"];
//    [_BirthArray addObject:@"1日▼"];
//    Date = @"1990-01-01";
    
    NSArray *birArray = [birthday componentsSeparatedByString:@"-"];
    for (NSString *string in birArray) {
        [_BirthArray addObject:[NSString stringWithFormat:@"%@▼",string]];
    }
    Date = birthday;
    [MainTableView reloadData];
}
/**
 取得会员所有标签
 */
-(void)getSelfLabe{
    NSString *url =[NSString stringWithFormat:@"%@%@",TestUrl,getSelfLabel];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
       
        NSDictionary *dic = responseObject;
        NSArray *array= dic[@"data"];
        if (array && array.count> 0) {
            for (NSDictionary *dict in array) {
                LPTagModel *model =    [[LPTagModel alloc]init];
                model.name = dict[@"labelname"];
                model.ident = dict[@"id"];
                model.isChoose = YES;
                [_TagArray addObject:model];
            }
             tagHeight = 50;
        }
       
        [MainTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
         [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 增加标签
 */
-(void)addLabel{
    NSString *url  = [NSString stringWithFormat:@"%@%@",TestUrl,AddLabel];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    for (LPTagModel  *model in _TagArray) {
        [array addObject:model.ident];
    }
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"label":array};
    
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSLog(@"addlabel success");
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 增加个人标签
 */
-(void)addPersonalTag:(LPTagModel *)model{
    dispatch_group_enter(downloadGroup);
    
    NSString *url  = [NSString stringWithFormat:@"%@%@",TestUrl,AddSelfLabel];
 
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"label":model.name};
    
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            model.ident = dic[@"data"][@"id"];
            [CYToast dismiss];
            
        }
       dispatch_group_leave(downloadGroup);
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)showPicker{
    if (mPickView == nil) {
        mPickView = [[UIView alloc] init];
        mPickView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 250);
        
        mPickView.backgroundColor = [UIColor whiteColor];
        
        pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 34, SCREEN_WIDTH, 216)];
        pick.delegate = self;
        pick.dataSource = self;
        pick.showsSelectionIndicator = YES;
        [mPickView addSubview:pick];
        
        UIButton * leftBt = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        leftBt.frame = CGRectMake(10, 5, 61, 30);
        leftBt.tag = 100;
        [leftBt setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        leftBt.titleLabel.font = Bold_FONT(15);
        [leftBt addTarget:self action:@selector(hidePickView:) forControlEvents:UIControlEventTouchUpInside];
        [mPickView addSubview:leftBt];
        
        UIButton * rightBt = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        rightBt.frame = CGRectMake(SCREEN_WIDTH-10-71, 5, 71, 30);
        rightBt.tag = 101;
        [rightBt setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
        rightBt.titleLabel.font = Bold_FONT(15);
        [rightBt addTarget:self action:@selector(hidePickView:) forControlEvents:UIControlEventTouchUpInside];
        [mPickView addSubview:rightBt];
        
        mGrayView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        mGrayView.backgroundColor = [UIColor lightGrayColor];
        mGrayView.alpha = 0.f;
        
        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate.window addSubview:mGrayView];
        
        [delegate.window addSubview:mPickView];
        
    }
    
    [self.view setUserInteractionEnabled:NO];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    mGrayView.alpha = 0.6;
    mPickView.frame = CGRectMake(0, SCREEN_HEIGHT-250, SCREEN_WIDTH, 250);
    
    [UIView commitAnimations];
}
-(void)hidePickView:(id)sender{
     UIButton * tmpBt = (UIButton*)sender;
    if (tmpBt.tag == 101) {
        if ([slectType isEqualToString:@"rel"]) {
            NSInteger index = [pick selectedRowInComponent:0];
            NSString *string = _RelationArray[index];
            UIButton *relBut = [self.view viewWithTag:207];
            RelationIndex = index;
            [relBut setTitle:string forState:UIControlStateNormal];
             [relBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }else{
            NSInteger index = [pick selectedRowInComponent:0];
            CountryModel *model = _CountryArray[index];
            UIButton *countryBut = nil;
            
            if ([model.type isEqualToString:@"coun"]) {
                
                countryBut = [self.view viewWithTag:206];
                [countryBut setTitle:[NSString stringWithFormat:@"%@",model.cname] forState:UIControlStateNormal];
                countryId = model.country_id;
            }else{
                countryBut = [self.view viewWithTag:208];
                [countryBut setTitle:[NSString stringWithFormat:@"%@",model.language_name] forState:UIControlStateNormal];
                langId= model.language_id;
            }
            [countryBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
        
       
    }
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationStoped)];
    //mPickView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 250);
    //mGrayView.alpha = 0.0;
    [self animationStoped];
    [UIView commitAnimations];
    [self.view setUserInteractionEnabled:YES];
}
- (void)animationStoped
{
    if (mPickView) {
        [mPickView removeFromSuperview];
        mPickView = nil;
        [mGrayView removeFromSuperview];
        
        mGrayView = nil;
        pick.delegate = nil;
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
//returns the # of rows in each component.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if ([slectType isEqualToString:@"rel"]) {
        return _RelationArray.count;
    }else{
        return _CountryArray.count;
    }
    
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if ([slectType isEqualToString:@"rel"]) {
        return [NSString stringWithFormat:@"%@",_RelationArray[row]];
    }
    else{
        CountryModel *model = _CountryArray[row];
        if ([model.type isEqualToString:@"coun"]) {
            return model.cname;
        }else{
            return model.language_name;
        }
    }
    return nil;
    
}
#pragma mark - 点击提示昵称最大字数
-(void)showNickAlarm{
    
    UIButton *nickt = [self.view viewWithTag:80];
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[nickt convertRect:nickt.bounds toView:window];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x - 130, rect.origin.y + 20, 150, 60)];
    image.image = [UIImage imageNamed:@"黑_96X280"];
    image.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 140, 50)];
    label.text = @"昵称的长度为10个中文字，不能包含不雅文字，修正后七天内不能再修正";
    label.textColor = [UIColor whiteColor];
    label.font = FONT(10);
    label.numberOfLines = 0;
    [image addSubview:label];
    
    [[QWAlertView sharedMask]show:image withType:QWAlertViewStyleActionSheetDown];
    
}
#pragma mark - 隐私按钮点击事件
-(void)clickPriBut:(UIButton *)button{
    button.selected = !button.selected;
    NSString *string = nil;
    switch (button.tag) {
        case 600:
            string = @"";
            break;
        case 601:
            string = @"email";
            break;
        case 602:
            string = @"resume";
            break;
        case 603:
            string = @"birth";
            break;
         case 604:
            string = @"gender";
            break;
        case 605:
            string = @"mobile";
            break;
        case 606:
            string = @"companies_0";
            break;
        case 607:
            string = @"school_0";
            break;
        case 608:
            string = @"city";
            break;
        case 609:
            string = @"country_id";
            break;
        case 610:
            string = @"relationship";
            break;

        default:
            break;
    }
    if (button.selected) {
        [_ShowMessageArray addObject:string];
    }else{
        [_ShowMessageArray removeObject:string];
    }
    
}
-(void)requestIsHaveReviewMessage{
    if ([PHUserModel sharedPHUserModel].isLogin == NO) {
        return;
    }
    NSDictionary *paramDic = @{@"email":[PHUserModel sharedPHUserModel].email,@"password":[PHUserModel sharedPHUserModel].Password,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/doLogin"];
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        if ([dic[@"message"] isEqualToString:@"success"]) {
            NSDictionary *newDict = dic[@"data"];

            [PHUserModel sharedPHUserModel].isLogin = YES;
              [PHUserModel sharedPHUserModel].nickname = [NetDataCommon stringFromDic:newDict forKey:@"nickname"];
            [PHUserModel sharedPHUserModel].info_show= [[NSMutableString alloc]init];
            [[PHUserModel sharedPHUserModel].info_show appendFormat:@"%@",[NetDataCommon stringFromDic:newDict forKey:@"info_show"]];
            [PHUserModel sharedPHUserModel].language = [NetDataCommon stringFromDic:newDict forKey:@"language_name"];
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            
            if ([dic[@"data"][@"language_id"] isEqualToString:@"english"] && ![[self getPreferredLanguage] isEqualToString:@"en"]) {
                
                
                [[NSUserDefaults standardUserDefaults] setObject:@[@"en"] forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [NSBundle setLanguage:@"en"];
                
                AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                appdelegate.window.rootViewController = nil;
                [appdelegate.window resignKeyWindow];
                
                [appdelegate initTabBarVC];
                [appdelegate.window makeKeyAndVisible];
            }else if ([dic[@"data"][@"language_id"] isEqualToString:@"zh-CN"] && ![[self getPreferredLanguage] isEqualToString:@"zh-Hans-CN"]){
                
                
                [[NSUserDefaults standardUserDefaults] setObject:@[@"zh-Hans-CN"] forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [NSBundle setLanguage:@"zh-Hans-CN"];
                
                AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                appdelegate.window.rootViewController = nil;
                [appdelegate.window resignKeyWindow];
                
                [appdelegate initTabBarVC];
                [appdelegate.window makeKeyAndVisible];
            }
            else if ([dic[@"data"][@"language_id"] isEqualToString:@"zh-TW"] && ![[self getPreferredLanguage] isEqualToString:@"zh-Hant-CN"]){
                
                
                NSArray *lans = @[@"zh-Hant-CN"];
                [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [NSBundle setLanguage:@"zh-Hant-CN"];
                
                AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                appdelegate.window.rootViewController = nil;
                [appdelegate.window resignKeyWindow];
                
                [appdelegate initTabBarVC];
                [appdelegate.window makeKeyAndVisible];
            }
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [CYToast showErrorWithString:@"账号或密码错误"];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        
    }];
}
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    _currentTF = textView;
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [UIView animateWithDuration:0.3 animations:^{
        
        CGRect frame = self.view.bounds;
        
        frame.origin.y = Height_TabBar;
        
        self.view.frame = frame;
        
    }];
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
//        //在这里做你响应return键的代码
//        [_currentSZTF resignFirstResponder];
//        [_currentSZTF endEditing:YES];
//        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
//    }

    if ([text isEqualToString:@"\n"] && textView.tag != 200) {
        return NO;
    }
    
    return YES;
}

- (void)keyboardWasShown:(NSNotification *) notif
{
    NSValue *keyBoardBeginBounds=[[notif userInfo]objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect beginRect=[keyBoardBeginBounds CGRectValue];
    
    //获取键盘弹出后的Rect
    NSValue *keyBoardEndBounds=[[notif userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect  endRect=[keyBoardEndBounds CGRectValue];
    
    
    //获取键盘位置变化前后纵坐标Y的变化值
    CGFloat deltaY=endRect.origin.y-beginRect.origin.y;
    NSLog(@"看看这个变化的Y值:%f",deltaY);
    
    //获取textfield当前的位置，判断是否需要偏移view
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    
    //CGRect rect=[_currentSZTF convertRect: _currentSZTF.bounds toView:window];
    CGRect rect1 = [_currentTF convertRect: _currentTF.bounds toView:window];
    //|| rect1.origin.y < SCREEN_HEIGHT - 258
    if (rect1.origin.y < SCREEN_HEIGHT + deltaY -Height_TabBar ) {
        return;
    }
    
    //在0.25s内完成self.view的Frame的变化，等于是给self.view添加一个向上移动deltaY的动画
    [UIView animateWithDuration:0.25f animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+deltaY + 42, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}
@end
