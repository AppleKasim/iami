//
//  TagModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/18.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "TagModel.h"

@implementation TagModel
+(TagModel *)parsenWithDictionary:(NSDictionary *)dic{
    TagModel *model = [[TagModel alloc]init];
    model.ident = [NetDataCommon stringFromDic:dic forKey:@"id"];
    model.labelname = [NetDataCommon stringFromDic:dic forKey:@"labelname"];
    return model;
}
@end
