//
//  CountryModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "CountryModel.h"

@implementation CountryModel

+(CountryModel *)parsenWithDic:(NSDictionary *)dict{
    CountryModel *model = [[CountryModel alloc]init];
    model.type = @"coun";
    model.country_id = [NetDataCommon stringFromDic:dict forKey:@"country_id"];
    model.cname = [NetDataCommon stringFromDic:dict forKey:@"cname"];
    return model;
}
+(CountryModel *)parsenWithLanguageDic:(NSDictionary *)dict{
    CountryModel *model = [[CountryModel alloc]init];
    model.type = @"lang";
    model.language_id = [NetDataCommon stringFromDic:dict forKey:@"language_id"];
    model.language_name = [NetDataCommon stringFromDic:dict forKey:@"language_name"];
    return model;
}
@end
