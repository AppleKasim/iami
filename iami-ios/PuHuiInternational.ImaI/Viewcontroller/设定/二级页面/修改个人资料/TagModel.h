//
//  TagModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/18.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagModel : NSObject

@property (nonatomic,strong) NSString *ident;
@property (nonatomic,strong) NSString *labelname;

+(TagModel *)parsenWithDictionary:(NSDictionary *)dic;
@end
