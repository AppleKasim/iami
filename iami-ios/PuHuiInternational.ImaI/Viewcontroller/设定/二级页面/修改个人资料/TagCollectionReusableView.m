//
//  TagCollectionReusableView.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "TagCollectionReusableView.h"

@implementation TagCollectionReusableView

-(id)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    
    if (self) {
       
       // self.backgroundColor=[UIColor greenColor];
        
        self.TagText = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, 200, 30)];
        self.TagText.placeholder = @"自行输入";
        UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
        self.TagText .leftView=emptyView;
        self.TagText .leftViewMode=UITextFieldViewModeAlways;
        self.TagText.delegate = self;
        self.TagText.layer.borderWidth = 1.0f;
        self.TagText.layer.borderColor = LYColor(223, 223, 223).CGColor;
        self.TagText.layer.cornerRadius = 8;
        [self addSubview:self.TagText];
        
        UIButton *alarmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        alarmButton.frame = CGRectMake(CGRectGetMaxX(self.TagText.frame) - 50, 5, 20, 20);
        [alarmButton setImage:[UIImage imageNamed:@"提示感叹号"] forState:UIControlStateSelected];
        [alarmButton setImage:[UIImage imageNamed:@"提示感叹号灰"] forState:UIControlStateNormal];
        alarmButton.tag = 70;
        alarmButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [alarmButton addTarget:self action:@selector(showNickAlarm:) forControlEvents:UIControlEventTouchUpInside];
        [self.TagText addSubview:alarmButton];
        
        
       self.AddButton  = [UIButton buttonWithType:UIButtonTypeCustom];
        self.AddButton.frame = CGRectMake(CGRectGetMaxX(self.TagText.frame) + 3, 10, 30, 30);
        [self.AddButton setBackgroundImage:[UIImage imageNamed:@"添加标签"] forState:UIControlStateNormal];
        [self.AddButton addTarget:self action:@selector(AddTag) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.AddButton];
       
    }
    return self;
    
}
-(void)AddTag{
    [self.TagText resignFirstResponder];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"Addtag" object:[NSString stringWithFormat:@"%@",self.TagText.text]];
    
    self.TagText.text = @"";
    
}
-(void)showNickAlarm:(UIButton *)button{
   // [maskImage removeFromSuperview];
    if (maskImage) {
        [maskImage removeFromSuperview];
        maskImage = nil;
        return;
    }
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect:button.bounds toView:window];
    
    NSString *string = NSLocalizedString(@"tag提示信息", nil);
    //(CGRectGetMaxX(self.TagText.frame) - 50, 9, 18, 18)
    maskImage = [[UIImageView alloc]initWithFrame:CGRectMake(rect.origin.x - 130, 30, 150, 60)];
    maskImage.image = [UIImage imageNamed:@"黑_96X280"];
    maskImage.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 140, 50)];
    label.text = string;
    label.textColor = [UIColor whiteColor];
    label.font = FONT(13);
    label.numberOfLines = 0;
    [maskImage addSubview:label];
    
    [self addSubview:maskImage];
}
@end
