//
//  TagSelectViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/18.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"
@protocol TaggDeleaget <NSObject>

-(void)selectArray:(NSMutableArray *)SelectArray;

@end

@interface TagSelectViewController : XTViewController


@property (nonatomic,copy) NSMutableArray *DataArray;
@property (nonatomic,copy) NSArray *dataarray;
@property (nonatomic,copy) NSArray *selectArray;
@property (nonatomic,weak) id <TaggDeleaget> delegate;
@end
