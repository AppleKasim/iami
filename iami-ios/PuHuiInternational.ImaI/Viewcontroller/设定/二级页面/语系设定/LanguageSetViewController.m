//
//  LanguageSetViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "LanguageSetViewController.h"

@interface LanguageSetViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
    
    UIButton *currentButton;
    
    UIButton *confirmBut;
    
    UIButton *currentLabelBut;
}
@property (nonatomic,copy) NSArray *LanguageArray;
#define editInfo @"/api/editMember"//旧的编辑接口

@end

@implementation LanguageSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =LYColor(223, 223, 223);
    _LanguageArray = @[@"繁體中文",@"简体中文",@"English"];
    
    [self creatTable];
  
    // Do any additional setup after loading the view.
}
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH -30, 300) style:UITableViewStyleGrouped];
    MainTableView.layer.cornerRadius = 10;
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = NO;
    MainTableView.bounces = NO;
    [self.view addSubview:MainTableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 50;
    }else if (section == 1){
        return 250;
    }
    return 0;
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0){
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH -30 , 50)];
        
        UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 50)];
        backView.layer.cornerRadius = 10;
        backView.clipsToBounds = YES;
        backView.backgroundColor =MainColor;
        [view addSubview:backView];
        
        //view.backgroundColor = [UIColor whiteColor];
        UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH - 30, 20)];
        TitleLabel.text = NSLocalizedString(@"语系设定", nil);
        TitleLabel.textColor = [UIColor whiteColor];
        TitleLabel.font = FONT(20);
        TitleLabel.textAlignment = NSTextAlignmentCenter;
        [backView addSubview:TitleLabel];
        
        return view;
    }else if (section == 1){
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 30, 250)];
        for (NSInteger i = 0 ; i < _LanguageArray.count; i++) {
            UIButton *langBut = [UIButton buttonWithType:UIButtonTypeCustom];
            langBut.frame = CGRectMake(view.frame.size.width / 2 - 40, 10 + 40 * i, 80, 30);
            langBut.contentHorizontalAlignment= 0;
            langBut.imageView.contentMode = UIViewContentModeScaleAspectFit;
             [langBut setTitle:[NSString stringWithFormat:@"%@",_LanguageArray[i]] forState:UIControlStateNormal];
            [langBut setTitleColor:LYColor(223, 223, 223) forState:UIControlStateNormal];
             [langBut setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
            langBut.titleLabel.textAlignment = NSTextAlignmentLeft;;
            
            UIButton *seleButton = [UIButton buttonWithType:UIButtonTypeCustom];
            seleButton.frame = CGRectMake(langBut.frame.origin.x - 30, langBut.frame.origin.y + 7, 16, 16);
                        [seleButton setImage:[UIImage imageNamed:@"icon-选择框"] forState:UIControlStateNormal];
                        [seleButton setImage:[UIImage imageNamed:@"icon-选择框选中"] forState:UIControlStateSelected];
             [seleButton addTarget:self action:@selector(changeLanguage:) forControlEvents:UIControlEventTouchUpInside];
           
            langBut.tag = i * 10 + 1 ;
            seleButton.tag = i * 10 + 2;
            
            [view addSubview:seleButton];
            [view addSubview:langBut];
            
        }
        confirmBut = [UIButton buttonWithType:UIButtonTypeCustom];
        confirmBut.frame = CGRectMake(MainTableView.frame.size.width / 2 - 120, 180, 240, 35);
        confirmBut.layer.cornerRadius = 6;
        [confirmBut setTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
        confirmBut.backgroundColor = MainColor;
        [confirmBut addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
        [confirmBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        confirmBut.titleLabel.font = FONT(17);
        [view addSubview:confirmBut];
        
        return view;
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(void)changeLanguage:(UIButton *)button{
    currentButton.selected = NO;
    currentButton = button;
    currentButton.selected = YES;
    
    UIButton *titleButton = (UIButton *)[self.view viewWithTag:currentButton.tag - 1];
    currentLabelBut.selected = NO;
    currentLabelBut = titleButton;
    currentLabelBut.selected = YES;
//    UIButton *but = [self.view viewWithTag:button.tag - 1];
//    but.selected = currentButton.selected;
    
//    if([button.titleLabel.text isEqualToString:@"简体中文"]){
//        NSArray *lans = @[@"zh-Hans"];
//        [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
//        [NSBundle setLanguage:@"zh-Hans"];
//    }else if ([button.titleLabel.text isEqualToString:@"English"]){
//        NSArray *lans = @[@"en"];
//        [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
//        [NSBundle setLanguage:@"en"];
//    }
//    AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [appdelegate initTabBarVC];

  
}
-(void)confirm{
    if (currentButton == nil) {
        return;
    }
   
    UIButton *button = (UIButton *)[self.view viewWithTag:currentButton.tag - 1];
    
        if([button.titleLabel.text isEqualToString:@"简体中文"]){
            NSArray *lans = @[@"zh-Hans-CN"];
            [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [NSBundle setLanguage:@"zh-Hans-CN"];
            [self UploadLanguage:@"zh-CN"];
         
        }else if ([button.titleLabel.text isEqualToString:@"English"]){
            NSArray *lans = @[@"en"];
            [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
             [[NSUserDefaults standardUserDefaults]synchronize];
            [NSBundle setLanguage:@"en"];
            [self UploadLanguage:@"english"];

        }else if([button.titleLabel.text isEqualToString:@"繁體中文"]){
            //zh-Hant
            NSArray *lans = @[@"zh-Hant-CN"];
            [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
             [[NSUserDefaults standardUserDefaults]synchronize];
            [NSBundle setLanguage:@"zh-Hant-CN"];
            [self UploadLanguage:@"zh-TW"];
            
            
        }

}
-(void)UploadLanguage:(NSString *)string{
    NSMutableDictionary *muDic = [NSMutableDictionary dictionary];
   
    [muDic setObject:[PHUserModel sharedPHUserModel].AccessToken forKey:@"token"];
    [muDic setObject:[PHUserModel sharedPHUserModel].csrf_token forKey:@"csrf_token_name"];
    if (string && string.length > 0) {
        [muDic setObject:string forKey:@"language_id"];
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,editInfo];
    [PPNetworkHelper POST:url parameters:muDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//            appdelegate.window.rootViewController = nil;
//            [appdelegate.window resignKeyWindow];
            
            [appdelegate initTabBarVC];
           // [appdelegate.window makeKeyAndVisible];
        }
       
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(NSArray *)StringtoDictionary:(NSString *)string{
    NSString *newString = [string stringByReplacingOccurrencesOfString:@"[" withString:@""];
    NSString *newstring = [newString stringByReplacingOccurrencesOfString:@"]" withString:@""];
    NSString *new = [newstring stringByReplacingOccurrencesOfString:@"\""        withString:@""];

    NSArray *array = [newstring componentsSeparatedByString:@","];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    for (NSString *dicSring in array) {
        NSDictionary *dic = [[PHUserModel sharedPHUserModel] dictionaryWithJsonString:dicSring];
        [arr addObject:dic];
    }
    //NSDictionary *dic = [[PHUserModel sharedPHUserModel] dictionaryWithJsonString:newstring];
    return arr;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
