//
//  HotPostViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/12.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "HotPostViewController.h"
#import "VideoTableViewCell.h"
#import "MainTableViewCell.h"
#import "ShareFriendViewController.h"
#import "MainUserViewController.h"
#import "ShareTableViewCell.h"//分享cell
#import "DeclareAbnormalAlertView.h"
#define CollectPost @"/post/collectPost"//收藏帖文


@interface HotPostViewController ()<UITableViewDelegate,UITableViewDataSource,DeclareAbnormalAlertViewDelegate,TTTAttributedLabelDelegate,mainVideoCellDelegate,mainCellDelegate,ShareCellDelegate>
{
    UITableView *MainTableView;
    
     NSInteger Page;
    
    SZTextView *CurText;
    MainModel *currentShareModel;
    
    dispatch_group_t downloadGroup;
    
    SZTextView *editText;


}
@property (nonatomic,copy) NSMutableArray *DataArray;

@property (nonatomic,strong) ZFPlayerView *playView;

#define getHotUrl @"/api/hot_post"
#define LikeUrl @"/like/togglePostLike"//贴文按赞
#define CommentUrl @"/post/doComment"//评论
#define PostUrl @"/post/doPost" //发布消息
#define DeletePost @"/post/delPost"//删除帖文
#define EDPost @"/post/editPost"//编辑帖文

@end

@implementation HotPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    Page = 1;
    _DataArray = [[NSMutableArray alloc]init];
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadComment:) name:@"CommentSuccess" object:nil
     ];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
//    [self.playView pause];
//    [self.playView resetPlayer];
    
}
-(void)reloadComment:(NSNotification *)noti{
    NSDictionary *dic = noti.userInfo;
    for (MainModel *model in _DataArray) {
        if ([model.post_id isEqualToString:dic[@"post_id"]]) {
            model.Comment = [NSString stringWithFormat:@"%@",dic[@"count"]];
            NSLog(@"%@",model.Comment);
            [MainTableView reloadData];
        }
    }
}
-(void)creatTable{
    [self.view addSubview:[self creatHeader]];
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50, SCREEN_WIDTH , SCREEN_HEIGHT -  Height_NavBar - Height_TabBar ) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor whiteColor];

    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.layer.cornerRadius = 10;
    MainTableView.showsVerticalScrollIndicator = NO;
    MainTableView.showsHorizontalScrollIndicator = NO;

    [MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[VideoTableViewCell class] forCellReuseIdentifier:@"videocell"];
    [MainTableView registerClass:[ShareTableViewCell class] forCellReuseIdentifier:@"sharecell"];
    MainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    MainTableView.scrollEnabled = YES;
    
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadDataMore)];
    MainTableView.mj_footer = footer;
    [self.view addSubview:MainTableView];
}
-(UIView *)creatHeader{
    
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH - 10, 50)];
        view.layer.cornerRadius = 10;
        view.backgroundColor = MainColor;
        UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH - 10, 20)];
        TitleLabel.text = NSLocalizedString(@"人气帖文", nil);
        TitleLabel.textColor = [UIColor whiteColor];
        TitleLabel.font = FONT(18);
        TitleLabel.textAlignment = NSTextAlignmentCenter;
        [view addSubview:TitleLabel];
        
        return view;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainModel *model = _DataArray[indexPath.row];
    if ([model.post_type isEqualToString:@"video"]) {
        VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videocell" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        [cell setMode:model];

        
        cell.CommentTld.delegate = self;
        cell.MainLabel.delegate = self;
        cell.delegate =self;

        [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
        [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
        [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
        [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
         //[cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];

        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserImage addGestureRecognizer:tap];
        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserName addGestureRecognizer:tap1];
        
        UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
        [cell.ForWhoLabel addGestureRecognizer:forWho];
        cell.ForWhoLabel.tag = 200 + indexPath.row;
        
        cell.CommentTld.tag = 300 + indexPath.row;
        cell.MoreCommentBut.tag = 200 + indexPath.row;
        cell.CommentBut.tag = 200 + indexPath.row;
        cell.CommentButton.tag = 200 + indexPath.row;
        cell.LikeButton.tag = 200 + indexPath.row;
        cell.UserImage.tag = 200 + indexPath.row;
        cell.ForwardButton.tag = 200 + indexPath.row;
        cell.CollectButton.tag = 200 + indexPath.row;
        cell.MoreButton.tag = 200 + indexPath.row;
        cell.ReportButton.tag = 200 + indexPath.row;
        cell.UserName.tag = 200 + indexPath.row;

        
        return cell;
    }
    else if ([model.post_type isEqualToString:@"share"]){
        ShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sharecell" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        
        if ([model.shareType isEqualToString:@"video"]) {
            [cell setVideoMode:model];
        }else{
            [cell setMode:model];
        }
        
//        if (model.isShowCommentV == YES) {
//            cell.CommentView.hidden = NO;
//        }else{
//            cell.CommentView.hidden = YES;
//        }
        cell.CommentTld.delegate = self;
        cell.MainLabel.delegate = self;
        cell.delegate =self;


        
        [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
        [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
        [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
        [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
        // [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];

        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
        [cell.shareImageView addGestureRecognizer:tap1];
        
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
        [cell.shareLabel addGestureRecognizer:tap3];
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserImage addGestureRecognizer:tap];
        
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserName addGestureRecognizer:tap2];
        
        UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
        [cell.ForWhoLabel addGestureRecognizer:forWho];
        cell.ForWhoLabel.tag = 200 + indexPath.row;
        

        
        cell.CommentTld.tag = 300 + indexPath.row;
        cell.shareImageView.tag = 200 + indexPath.row;
        cell.shareLabel.tag = 200 + indexPath.row;
        cell.MoreCommentBut.tag = 200 + indexPath.row;
        cell.CommentBut.tag = 200 + indexPath.row;
        cell.CommentButton.tag = 200 + indexPath.row;
        cell.LikeButton.tag = 200 + indexPath.row;
        cell.UserImage.tag = 200 + indexPath.row;
        cell.ForwardButton.tag = 200 + indexPath.row;
        cell.CollectButton.tag = 200 + indexPath.row;
        cell.MoreButton.tag = 200 + indexPath.row;
        cell.ReportButton.tag = 200 + indexPath.row;
        cell.UserName.tag = indexPath.row + 200;
       // [self comment:cell.CommentButton];

        
        return cell;
    }
    else{
        MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        
        //[cell setMode:model];
        cell.model = model;
//        if (model.isShowCommentV == YES) {
//            cell.CommentView.hidden = NO;
//        }else{
//            cell.CommentView.hidden = YES;
//        }
        cell.CommentTld.delegate = self;
        cell.MainLabel.delegate = self;
        cell.delegate =self;

        
        [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
        [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//发布评论
        [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
        [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
        // [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserImage addGestureRecognizer:tap];
        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
        [cell.UserName addGestureRecognizer:tap1];
        
        UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
        [cell.ForWhoLabel addGestureRecognizer:forWho];
        cell.ForWhoLabel.tag = 200 + indexPath.row;
        
        cell.CommentTld.tag = 300 + indexPath.row;
        cell.MoreCommentBut.tag = 200 + indexPath.row;
        cell.CommentBut.tag = 200 + indexPath.row;
        cell.CommentButton.tag = 200 + indexPath.row;
        cell.LikeButton.tag = 200 + indexPath.row;
        cell.UserImage.tag = 200 + indexPath.row;
        cell.ForwardButton.tag = 200 + indexPath.row;
        cell.CollectButton.tag = 200 + indexPath.row;
        cell.MoreButton.tag = 200 + indexPath.row;
        cell.ReportButton.tag = 200 + indexPath.row;
        cell.UserName.tag = 200 + indexPath.row;
       // [self comment:cell.CommentButton];

        return cell;
    }
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _DataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainModel *main =_DataArray[indexPath.row];
     return main.cellHeight + main.CommentHeight;
//    if (main.isShowCommentV) {
//        return main.cellHeight + main.CommentHeight;
//    }else{
//        return   main.cellHeight;
//    }
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == MainTableView) {
        MainModel *main =_DataArray[indexPath.row];
//        if (main.isShowCommentV) {
//            return main.cellHeight + main.CommentHeight;
//        }else{
//            return   main.cellHeight;
//        }
         return main.cellHeight + main.CommentHeight;
    }
    else{
        return 40;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //return Headerheight;
    return 0.1f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 10, 0.1f)];
    //    view.layer.cornerRadius = 10;
    //    view.backgroundColor = LYColor(231, 194, 99);
    //    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH - 10, 20)];
    //    TitleLabel.text = @"我的收藏";
    //    TitleLabel.textColor = [UIColor whiteColor];
    //    TitleLabel.font = FONT(18);
    //    TitleLabel.textAlignment = NSTextAlignmentCenter;
    //    [view addSubview:TitleLabel];
    
    return view;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}
#pragma mark 点击网页
- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
//    ViewController *web = [[ViewController alloc]init];
//
//    NSString *stering = url.absoluteString;
//    web.url = stering;
//    //self.viewC = [[MainViewController alloc]init];
//    [self.navigationController pushViewController:web animated:YES];
     [[UIApplication sharedApplication ] openURL: url];
    
}
/**
 编辑贴文点击事件
 */
#pragma mark - 帖文编辑弹窗
-(void)EditPost:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"编辑帖文", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"删除帖文", nil)];
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    MainModel *model =_DataArray[button.tag - 200];
    if (model.shareID && model.shareID != nil ) {
        if ([model.shareID isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
            [actionItems addObject:item2];
        }
    }else if (model.member_id ){
        if ([model.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
            [actionItems addObject:item2];
        }
    }
    
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (actionItems.count > 2) {
            if (selectedIndex == 0) {
                [self editPost:button.tag - 200];
            }else if (selectedIndex == 2){
                [self deletePost:button.tag - 200];
            }else if (selectedIndex == 1){
                [self gotoReport];
            }
        }else if (actionItems.count == 1){
            [self gotoReport];
        }
        
        
    }];
}
-(void)gotoReport{
    ReportViewController *controller = [[ReportViewController alloc]init];
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)reporeComment:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
    }];
}
-(void)clickMore{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
        
    }];
}
#pragma mark - 帖文编辑
-(void)editPost:(NSInteger )index{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT / 2 -70, SCREEN_WIDTH - 20, 130)];
    view.layer.cornerRadius = 10;
    view.backgroundColor = [UIColor whiteColor];
    
    MainModel *model =_DataArray[index];
    editText = [[SZTextView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 100)];
    editText.text = model.content;
    editText.layer.borderWidth = 2.0f;
    editText.layer.borderColor = LYColor(223, 223, 223).CGColor;
    editText.layer.cornerRadius = 8;
    [view addSubview:editText];
    
    UIButton *PostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    PostButton.frame = CGRectMake(SCREEN_WIDTH- 65, 105, 40, 20);
    PostButton.backgroundColor =LYColor(237, 204, 105);
    [PostButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
    PostButton.titleLabel.font = FONT(13);
    PostButton.layer.cornerRadius = 8;
    [PostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [PostButton addTarget:self action:@selector(sendEditPost:)
         forControlEvents:UIControlEventTouchUpInside];
    PostButton.tag = index;
    [view addSubview:PostButton];
    
    [[QWAlertView sharedMask] show:view withType:QWAlertViewStyleActionSheetDown];
    
    
}
#pragma mark - 发布帖文编辑
-(void)sendEditPost:(UIButton *)button{
    [[QWAlertView sharedMask]dismiss];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,EDPost];
    NSInteger index = button.tag;
    MainModel *model =_DataArray[index];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":editText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        //        if (dic[@"status" isEqualToString:@"success"]) {
        //             model.content = editText.text;
        //        }
        if ([dic[@"status"] isEqualToString:@"success"]) {
            model.content = editText.text;
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
    
}
#pragma mark - 删除帖文
-(void)deletePost:(NSInteger )index{
    [PXAlertView showAlertWithTitle:NSLocalizedString(@"确认要删除?", nil) message:nil cancelTitle:NSLocalizedString(@"取消", nil) otherTitle:NSLocalizedString(@"确定", nil) completion:^(BOOL cancelled) {
        if (!cancelled) {
            [CYToast showStatusWithString:@"正在加载"];
            MainModel *model =_DataArray[index];
            NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,DeletePost];
            NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
            
            [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
                NSDictionary *dic = responseObject;
                [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
                if ([dic[@"status"] isEqualToString:@"success"]) {
                    [_DataArray removeObjectAtIndex:index];
                    [MainTableView reloadData];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [CYToast dismiss];
            }];
        }
    }];
}
/**
 收藏贴文
 */
-(void)collectPost:(UIButton *)button{
    
    [CYToast showStatusWithString:@"正在加载"];
    
    MainModel *model =_DataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CollectPost];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([model.isCollect isEqualToString:@"Y"]) {
            model.isCollect = @"N";
        }else{
            model.isCollect = @"Y";
        }
        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
        [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
/**
 分享按钮点击
 */
-(void)shareClick:(UIButton *)button{
    
    MainModel *model = _DataArray[button.tag - 200];
    NSMutableString *share = [[NSMutableString alloc]initWithString:NSLocalizedString(@"已有人分享了这则帖文", nil)];
    [share insertString:model.Share atIndex:1];
    DeclareAbnormalAlertView *alertView = [[DeclareAbnormalAlertView alloc]initWithTitle:@"分享帖文" message:share delegate:self leftButtonTitle:@"确定" rightButtonTitle:@"取消"];
    currentShareModel = model;
    alertView.model = model;
    alertView.delegate = self;
    [alertView show];
    
}
-(void)didSendtext:(NSString *)text{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    NSDictionary *parmDic =@{@"target":[PHUserModel sharedPHUserModel].member_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_type":@"share",@"share_id":currentShareModel.post_id,@"content":[NSString stringWithFormat:@"%@",text]};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            currentShareModel.content = text;
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
- (void)declareAbnormalAlertView:(DeclareAbnormalAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
}
-(void)pushNavagation:(CommentModel *)model{
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.m_showBackBt = YES;
    //controller.BannerImage = model.BannerImage;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 点击查看贴文的赞
 */
-(void)checkLike:(UIButton *)button{
    
    MainModel *model =_DataArray[button.tag - 200];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeUrl];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSString *data = dic[@"data"];
        if ([data isEqualToString:@"yes"]) {
            model.isLike = @"Y";
            NSInteger likecount = [model.Like integerValue] + 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }else{
            model.isLike = @"N";
            NSInteger likecount = [model.Like integerValue] - 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }
        [MainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:button.tag - 200 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
/**
 点击查看评论
 */
-(void)comment:(UIButton *)button{

    MainModel *model = _DataArray[button.tag - 200];
    PostDetailViewController *controller = [[PostDetailViewController alloc]init];
    controller.m_showBackBt = YES;
    if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
        return;
    }
    controller.member_id = model.member_id;
    controller.PostId = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
//
//    MainModel *main =_DataArray[button.tag - 200];
//
//    if (main.isShowCommentV) {
//        main.isShowCommentV =! main.isShowCommentV;
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//
//        [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationNone];
//        return;
//    }
//
//    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/post/getComments"];
//    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":main.post_id};
//   // [CYToast showStatusWithString:@"正在加载"];
//    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
//        NSDictionary *dic = responseObject;
//        NSArray *dataArr = dic[@"data"];
//        [main.CommentArray removeAllObjects];
//        if (dataArr && dataArr.count > 0) {
//            for (NSDictionary *Comdict in dataArr) {
//                CommentModel *mode =[CommentModel parsenWithDictionary:Comdict];
//                [main.CommentArray addObject:mode];
//            }
//        }
//        main.isShowCommentV = !main.isShowCommentV;
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//
//        [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationNone];
//        [CYToast dismiss];
//    } failure:^(NSError *error) {
//        [CYToast dismiss];
//    }];
}
-(void)didCloseWindow{
    
}
-(void)playWith:(ZFPlayerView *)PlView{
    self.playView = PlView;
}
/**
 发布评论
 */
-(void)postComment:(UIButton *)button{
    MainModel *model = _DataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CommentUrl];
    if (CurText.text.length == 0 || [CurText.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"评论不能为空"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":CurText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"评论成功"];
            MainModel *main =_DataArray[button.tag - 200];
            main.isShowCommentV = !main.isShowCommentV;
            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
            [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [CYToast showSuccessWithString:@"评论失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CurText = (SZTextView *)textView;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
    [CurText resignFirstResponder];
    [CurText endEditing:YES];
}
/**
 查看更多评论
 */
-(void)checkMoreComment:(UIButton *)button{

    MainModel *model = _DataArray[button.tag - 200];
    PostDetailViewController *controller = [[PostDetailViewController alloc]init];
    controller.m_showBackBt = YES;
    if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
        return;
    }
    controller.member_id = model.member_id;
    controller.PostId = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.BannerImage = model.banner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckShareInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.shareID;
    controller.Username = model.shareName;
    controller.ImageUrl = model.shareImage;
    controller.BannerImage = model.shareBanner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)checkWhoInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.ForWhoMember_id;
    controller.Username = model.ForWhoName;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)loadData{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getHotUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"decode":@"1"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_DataArray addObject:[MainModel pareWithDictionary:parmDic]];
            }
           
        }
        downloadGroup = dispatch_group_create();
        
        for (MainModel *model in _DataArray) {
            [self loadComment:model];
        }
        
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
            
            [self creatTable];
            NSLog(@"end");
        });
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
         [CYToast dismiss];
    }];
}
-(void)loadDataMore{
    Page = Page + 1;
    //[CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getHotUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"page":[NSString stringWithFormat:@"%ld",Page],@"decode":@"1"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        //[CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        
        NSMutableArray *moreArray = [[NSMutableArray alloc]init];

        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                MainModel *newModel = [MainModel pareWithDictionary:parmDic];
                [_DataArray addObject:newModel];
                [moreArray addObject:newModel];

            }

            downloadGroup = dispatch_group_create();
            for (MainModel *model in moreArray) {
                [self loadComment:model];
            }
            dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{

                [MainTableView reloadData];
            });
        }else{
            [MainTableView.mj_footer resetNoMoreData];
            Page -- ;
        }

        [MainTableView.mj_footer endRefreshing];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
         [CYToast dismiss];
    }];
}
-(void)loadComment:(MainModel  *)CoModel{
    dispatch_group_enter(downloadGroup);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/post/getComments"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":CoModel.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        
        if (dataArr && dataArr.count > 0) {
            [CoModel.CommentArray removeAllObjects];
            for (NSDictionary *Comdict in dataArr) {
                CommentModel *mode =[CommentModel parsenWithDictionary:Comdict];
                [CoModel.CommentArray addObject:mode];
            }
            NSLog(@"1");
        }
        
        dispatch_group_leave(downloadGroup);
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
@end
