//
//  UserFriTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InviteModel.h"

@interface UserFriTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) UILabel *MeseeageLabel;
@property (nonatomic,strong) UILabel *LevelLabel;

@property (nonatomic,strong) UIButton *FreInvitButton;
@property (nonatomic,strong) UIButton *FollowButton;

-(void)setModel:(InviteModel *)model;
@end
