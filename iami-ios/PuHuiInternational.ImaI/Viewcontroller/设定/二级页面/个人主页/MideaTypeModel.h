//
//  MideaTypeModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MideaTypeModel : NSObject
@property (nonatomic,strong) NSString *path;
@property (nonatomic,strong) NSString *fileType;
@property (nonatomic,strong) NSString *picture_id;

+(MideaTypeModel *)parseDict:(NSDictionary *)dic;
@end
