//
//  UserFriTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "UserFriTableViewCell.h"

@implementation UserFriTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(InviteModel *)model{
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(20-imageW, 10 - imageW, 60 + imageW * 2, 60+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 60, 60)];
     [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 30;
    _UserImage.clipsToBounds = YES;
    _UserImage.userInteractionEnabled = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    
    //限制最多显示10个汉字
    CGFloat width = [self getWidthWithText:model.UserName height:20 font:15];
    if (width > 170) {
        width =  170;
    }
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leveImage.frame) + 20, 10, width, 20)];
    _UserName.text = [NSString stringWithFormat:@"%@",model.UserName];
    _UserName.font = FONT(15);
    _UserName.textColor = NameColor;
    [_UserName sizeToFit];
    _UserName.textAlignment = NSTextAlignmentLeft;
    
    _MeseeageLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserImage.frame) + 20, 30, 130, 20)];
    _MeseeageLabel.font = FONT(9);
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame)  , 11, 30, 20)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    
    _LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) -1, 14, 24, 14)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = [NSString stringWithFormat:@"lv%@",model.LevelLabel];
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    
    NSString *isfriend = nil;
    if ([model.isFriend isEqualToString:@"1"]) {
        isfriend = NSLocalizedString(@"好友", nil);
    }else{
        isfriend = NSLocalizedString(@"添加好友", nil);
    }
    

    _FreInvitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _FreInvitButton.frame = CGRectMake( 100, 50, 90, 20);
    [_FreInvitButton setTitle:isfriend forState:UIControlStateNormal];
    _FreInvitButton.backgroundColor = [UIColor whiteColor];
    _FreInvitButton.layer.cornerRadius = 6;
    [_FreInvitButton setTitleColor:MainColor forState:UIControlStateNormal];
    _FreInvitButton.layer.borderColor = MainColor.CGColor;
    _FreInvitButton.layer.borderWidth = 1.0f;
    _FreInvitButton.titleLabel.font = FONT(12);
    
    NSString *istrace = nil;
    if ([model.isTrace isEqualToString:@"1"]) {
        istrace = NSLocalizedString(@"已追踪", nil);
    }else{
        istrace = NSLocalizedString(@"未追踪", nil);
    }
    _FollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _FollowButton.frame = CGRectMake(200, 50, 90, 20);
    [_FollowButton setTitle:istrace forState:UIControlStateNormal];
    _FollowButton.backgroundColor = [UIColor whiteColor];
    _FollowButton.layer.borderWidth = 1.0f;
    _FollowButton.layer.cornerRadius = 6;

    _FollowButton.layer.borderColor =MainColor.CGColor;
    [_FollowButton setTitleColor:MainColor forState:UIControlStateNormal];
    _FollowButton.titleLabel.font = FONT(12);
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 78, SCREEN_WIDTH, 2)];
    gapView.backgroundColor =LYColor(221, 221, 221);
    
    
    [leveImage addSubview:_UserImage];
    [self.contentView addSubview:leveImage];
    //[self.contentView addSubview:_UserImage];
    [self.contentView addSubview:_UserName];
    [self.contentView addSubview:_MeseeageLabel];
    [self.contentView addSubview:_FreInvitButton];
    [self.contentView addSubview:_FollowButton];
    [self.contentView addSubview:_LevelLabel];
    [self.contentView addSubview:memberImage];
    [self.contentView addSubview:gapView];
    
    
}
//根据高度度求宽度  text 计算的内容  Height 计算的高度 font字体大小
- (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(CGFloat)font{
    
    CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, height)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]}
                                     context:nil];
    
    return rect.size.width;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
