//
//  MainUserViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"

@interface MainUserViewController : XTViewController

@property (nonatomic,strong) NSString *member_id;

@property (nonatomic,strong) NSString *ImageUrl;

@property (nonatomic,strong) NSString *Username;

@property (nonatomic,strong) NSString *BannerImage;
@end
