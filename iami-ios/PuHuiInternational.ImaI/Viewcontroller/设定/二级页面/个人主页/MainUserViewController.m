//
//  MainUserViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/2.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainUserViewController.h"
#import "ViewController.h"
#import "MainTableViewCell.h"
#import "VideoTableViewCell.h"//video的cell
#import "UserFriTableViewCell.h"//好友的cell
#import "ImageCollectionViewCell.h"//图片的cell
#import "ShareTableViewCell.h"//分享cell
#import "MyfriendViewController.h"//查看粉丝和好友
#import "DeclareAbnormalAlertView.h"
#import "ChatroomViewController.h"

@interface MainUserViewController ()<UITableViewDelegate,UITableViewDataSource,PST_MenuViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,DeclareAbnormalAlertViewDelegate,XLPhotoBrowserDelegate, XLPhotoBrowserDatasource,TTTAttributedLabelDelegate,mainVideoCellDelegate,mainCellDelegate,ShareCellDelegate>
{
    UITableView *MainTableView;
    UICollectionView *mainCollectionView;//图片的collectionview
    
    UITableView *ClickTableview;
    CALToolBarView *toolBarTwo;
    NSInteger selectIndex;
    NSInteger Page;
    SZTextView *Maintext;
    
   NSString   *collectCount ;
    NSString   *fansCount ;
    NSString   *postCount ;
    NSString   *traceCount ;
    
    NSString   *city;
    NSString   *company;
    NSString   *country;
    NSString   *department;
    NSString   *position;
    NSString   *relationship;
    NSString   *resume;
    NSString   *school;
    
    CGFloat beginContentY;          //开始滑动的位置
    CGFloat endContentY;            //结束滑动的位置
     CGFloat sectionHeaderHeight;    //section的header高度
    
    PST_MenuView *currentMenuView;
    
    NSString *isFriend;
    
    NSString *isTrace;
    
    NSString *isInvite;
    
    NSString *isInvited;//是否被邀请
    
    
    
       ZYQAssetPickerController *picker;
    
    //主页发帖文相关
   UIView *UploadView;
    NSString *MainString;
    UIImagePickerController *_imagePickerController;
    MainModel *currentShareModel;
    SZTextView *CurText;
    SZTextView *editText;

    UIScrollView *scroll;
    
    dispatch_group_t downloadGroup;



}
@property(nonatomic,strong)UIImageView *strechableImageView;
@property(nonatomic,assign)CGRect originFrame;

@property (nonatomic,copy) NSMutableArray *dataArray;//取得自己所有帖文
@property (nonatomic,copy) NSMutableArray *friendArray;//取得自己所有的好友
@property (nonatomic,copy) NSMutableArray *imageArray;//取得所有的图片
@property (nonatomic,copy) NSMutableArray *imageDataArray;//上传媒体库的图片
@property (nonatomic,copy) NSMutableArray *videoDataArray;//上传媒体库的影片

@property (nonatomic,copy) NSMutableArray *OnlyImage;//自己媒体库中，全部的图片

@property (nonatomic,copy) NSURL *FilePath;//本地视频地址

@property (nonatomic,copy) NSMutableArray *postNewImageArray;
@property (nonatomic,copy) NSMutableArray *postImageArray;
@property (nonatomic,copy) NSMutableArray *postVideoArray;

@property (nonatomic,copy) NSMutableArray *MideaPicArray;

@property (nonatomic, strong) NSMutableArray *mediaUrls;
@property (nonatomic, strong) ZFPlayerController *mediaPlayer;
@property (nonatomic, strong) ZFPlayerControlView *mediaControlView;
@property (nonatomic, strong) ZFAVPlayerManager *mediaPlayerManager;

@property (nonatomic, strong) ZFPlayerController *postPlayer;
@property (nonatomic, strong) ZFPlayerControlView *postControlView;
@property (nonatomic, strong) NSMutableArray *postUrls;
@property (nonatomic, strong) ZFAVPlayerManager *postPlayerManager;


#define RATIO 2.5
#define NaviH (SCREEN_HEIGHT == 812 ? 88 : 64) // 812是iPhoneX的高度
#define cellHeight1 120
#define cellectionHeight (SCREEN_WIDTH - 40) /3

#define getSelfPosts @"/post/getSelfPosts"
#define getSelfFriend @"/friend/getFriendList"
#define getSeflPicture @"/picture/getPictures"
#define addTrace @"/trace/addTrace" //追踪
#define delTrace @"/trace/delTrace"//取消追踪
#define getSelfInfo @"/api/getInfo"//获取帖文，粉丝，追踪数
#define getMemberBrief @"/member/getMemberInfo"//获得会员资料简介/member/getMemberBrief
#define deleteFriend @"/friend/delFriend"//删除好友
#define delTraceFriend @"/trace/delTrace"//取消追踪
#define TraceFriend @"/trace/addTrace"//追踪好友
#define AddFriend @"/invite/addInvite"//邀请好友
#define Isfriend @"/friend/isFriend"//是否为好友
#define Istrace @"/trace/isTrace"//是否追踪
#define uploadPicAndMov @"/post/uploadPictureAndMovie"//上传媒体库
#define FriendStatus @"/invite/isInvite"//是否已邀请
#define FriendInvited @"/invite/isInvited"//是否已被邀请

#define PostUrl @"/post/doPost" //发布消息
#define UploadPic @"/post/uploadPicture"//上传图片
#define UploadMov @"/post/uploadMovie"//上传影片
#define LikeUrl @"/like/togglePostLike"//贴文按赞
#define CommentUrl @"/post/doComment"//评论
#define CollectPost @"/post/collectPost"//收藏帖文
#define DeletePost @"/post/delPost"//删除帖文
#define EDPost @"/post/editPost"//编辑帖文
@end

@implementation MainUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    selectIndex = 0;
    if ([self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        sectionHeaderHeight = SCREEN_WIDTH / RATIO + 110;
    }else{
        sectionHeaderHeight =SCREEN_WIDTH / RATIO + 155;
    }
   
    _MideaPicArray = [[NSMutableArray alloc]init];
    _dataArray = [[NSMutableArray alloc]init];
    _friendArray = [[NSMutableArray alloc]init];
    _imageArray = [[NSMutableArray alloc]init];
    _imageDataArray = [[NSMutableArray alloc]init];
    _videoDataArray = [[NSMutableArray alloc]init];
    _postNewImageArray = [[NSMutableArray alloc]init];
    _postImageArray = [[NSMutableArray alloc]init];
    _postVideoArray = [[NSMutableArray alloc]init];
    _OnlyImage = [[NSMutableArray alloc]init];
    Page = 1;
    [self getMemeberBrief];
    [self loadInfo];
    [self creatTable];
    [self loadFriendRelation];
    [self loadTraceRelation];
    [self getFriendStatus];
    [self getFriendIsInvuted];

    _postPlayerManager = [[ZFAVPlayerManager alloc] init];

    [self setPostPlayer];
    //[self addObserver:scroll forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
   // [self startUserLoop];
    // Do any additional setup after loading the view.
}

-(void)addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(void *)context{
//    
//    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
//    [[SDImageCache sharedImageCache]clearMemory];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache]clearMemory];

    self.postPlayer = nil;
    self.mediaPlayer = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteNo:) name:@"deletePicture" object:nil];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"newMessage" object:nil];
    if (selectIndex == 1) {
        [self LoadSelf];
    }

    [self setPostPlayer];
    [self setMediaPlayer];
}

-(void)setPostPlayer{
    self.postPlayer = [ZFPlayerController playerWithScrollView:MainTableView playerManager:_postPlayerManager containerViewTag:100];
    self.postPlayer.controlView = self.postControlView;
    self.postPlayer.shouldAutoPlay = NO;
    self.postPlayer.assetURLs = self.postUrls;
    self.postPlayer.playerDisapperaPercent = 1.0;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)deleteNo:(NSNotification *)noti{
    //NSDictionary *dict = [noti userInfo];
    [self loadPicture];
}
-(void)new:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
    if ([[dic allKeys]containsObject:@"notice"]) {
       [self getFriendStatus];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:nil];
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache]clearMemory];
    
    // Dispose of any resources that can be recreated.
}
#pragma =======TableView delegeta && datasource ===================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT - Height_TabBar- Height_NavBar) style:UITableViewStylePlain];
    MainTableView.backgroundColor = LYColor(204, 204, 204);
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.rowHeight = UITableViewAutomaticDimension;
    
    [MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[VideoTableViewCell class] forCellReuseIdentifier:@"videocell"];
    [MainTableView registerClass:[UserFriTableViewCell class] forCellReuseIdentifier:@"friendcell"];
    [MainTableView registerClass:[ShareTableViewCell class] forCellReuseIdentifier:@"sharecell"];
    MainTableView.fd_debugLogEnabled = YES;
    

    [self.view addSubview:MainTableView];
}
//查看图片的UI
-(UIView *)creatImageUI{
    
        //1.初始化layout
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        //设置collectionView滚动方向
          //  [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        //设置headerView的尺寸大小
    
        //该方法也可以设置itemSize
        layout.itemSize =CGSizeMake(cellectionHeight, cellectionHeight);
  //  [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    CGFloat height = (_imageArray.count / 3+ 1) * (cellectionHeight + 10) ;
        //2.初始化collectionView
//        mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, height +10 ) collectionViewLayout:layout];
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, height  ) collectionViewLayout:layout];
        mainCollectionView.backgroundColor = [UIColor clearColor];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, 0, SCREEN_WIDTH - 20, 40)];
    view.backgroundColor = MainColor;
    UILabel *addMedia = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, SCREEN_WIDTH - 20, 30)];
    addMedia.text = @"新增照片/影片";
    view.layer.cornerRadius = 10;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(upLoadPicMov:)];
    addMedia.userInteractionEnabled = YES;
    [addMedia addGestureRecognizer:tap];
    addMedia.textAlignment = NSTextAlignmentCenter;
    addMedia.textColor = [UIColor whiteColor];
    [view addSubview:addMedia];
    
    
#pragma mark -如果不是自己的主页，不能添加媒体库
    if ([self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        layout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 40);
        mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, height + 50 ) collectionViewLayout:layout];
        [mainCollectionView addSubview:view];
        
    }
    
    
        //3.注册collectionViewCell
        //注意，此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致 均为 cellId
        [mainCollectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"collectioncell"];
        
        //注册headerView  此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致  均为reusableView
        [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
        
        //4.设置代理
        mainCollectionView.delegate = self;
        mainCollectionView.dataSource = self;
        mainCollectionView.bounces = NO;
        mainCollectionView.pagingEnabled = NO;
        mainCollectionView.scrollEnabled = YES;
        mainCollectionView.backgroundColor = [UIColor whiteColor];
        
        return mainCollectionView;
}
#pragma mark  - collectionView代理方法
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectioncell" forIndexPath:indexPath];

    
    [cell setModel:_imageArray[indexPath.item]];
    MideaTypeModel *model = _imageArray[indexPath.item];
    [cell.MainImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImageView:)]];
    cell.picture_id = model.picture_id;
    cell.MainImage.tag = 2000 + indexPath.item;

    @weakify(self)
    cell.playBlock = ^(UIButton *sender) {
        @strongify(self)
        [self mediaPlayTheVideoAtIndexPath:indexPath scrollToTop:NO];
    };

    return cell;
}
//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat widthAndHeight = floor(cellectionHeight * 10000) / 10000; //保留4位小数，舍弃后面所有位数。(地板函数)
    return CGSizeMake(widthAndHeight, widthAndHeight);
}
//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

- (void)clickImageView:(UITapGestureRecognizer *)tap
{
   
   
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    scroll.backgroundColor = [UIColor blackColor];
    scroll.bounces = NO;
    scroll.pagingEnabled = YES;

     MideaTypeModel *model = _imageArray[tap.view.tag - 2000];
    NSString *url = model.path;

    NSInteger index = 0 ;

    for (NSInteger i = 0; i < _OnlyImage.count; i++) {
        MideaTypeModel *modelnew = _OnlyImage[i];
        if ([url isEqualToString:modelnew.path]) {
            index = i;
        }
    }
    scroll.contentOffset = CGPointMake(index * SCREEN_WIDTH, 0);
    scroll.contentSize = CGSizeMake(SCREEN_WIDTH * _OnlyImage.count, SCREEN_HEIGHT);
    scroll.delegate = self;

    for (NSInteger i = 0; i < _OnlyImage.count; i++) {

            UIImageView *image = [[FLAnimatedImageView alloc]initWithFrame:CGRectMake(i * SCREEN_WIDTH, 0, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar - Height_TabBar)];

            UITapGestureRecognizer *tapsc = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissScroll:)];
            image.userInteractionEnabled = YES;
            [image addGestureRecognizer:tapsc];

            MideaTypeModel *model = _OnlyImage[i];


            NSString *url = [NSString stringWithFormat:@"%@",model.path];
            [image sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图.png"]];
            image.contentMode = UIViewContentModeScaleAspectFit;


            if ([self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {

                UIButton *buton =[UIButton buttonWithType:UIButtonTypeCustom];
                buton.frame = CGRectMake(SCREEN_WIDTH - 40, 10, 31, 31);
                [buton setImage:[UIImage imageNamed:@"删除图片"] forState:UIControlStateNormal];
                buton.tag = 3001 + i;
                buton.imageView.contentMode = UIViewContentModeScaleAspectFit;
                [buton setTitleColor:LYColor(237, 204, 105) forState:UIControlStateNormal];
                [buton addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
                [image addSubview:buton];
            }

            [scroll addSubview:image];

    }
    [self.view addSubview:scroll];

}
-(void)dismissScroll:(UITapGestureRecognizer *)tap{
    if (scroll) {
        [UIView animateWithDuration:0.5f animations:^{
             [scroll removeFromSuperview];
        }];
    }
   
}
-(void)deleteImage:(UIButton *)button{
   MideaTypeModel *model = _OnlyImage[button.tag - 3001];
        [PXAlertView showAlertWithTitle:@"是否删除图片" message:nil cancelTitle:@"取消" otherTitle:@"确定" completion:^(BOOL cancelled) {
            
            if (!cancelled) {
               
                [CYToast showStatusWithString:@"删除中..."];
                NSString *url = [NSString stringWithFormat:@"%@/picture/delPicture",TestUrl];
                NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"picture_id":model.picture_id};
                [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
                    [CYToast dismiss];
                    NSDictionary *dic = responseObject;
                    if ([dic[@"status"] isEqualToString:@"success"]) {
                        [UIView animateWithDuration:0.3f animations:^{
                            [scroll removeFromSuperview];
                        }];
                        [CYToast showStatusWithString:@"删除成功" hideAfterDelay:0.5f];
                     
                        [self loadPicture];
                    }
                } failure:^(NSError *error) {
                    [CYToast dismiss];
                    NSLog(@"%@",error);
                }];
                
            }
        }];
        
        
  
}
-(void)deletePostImage:(UIButton *)button{
    [_imageDataArray removeObjectAtIndex:button.tag - 900];
    [MainTableView reloadData];
}

//- (UIImage *)photoBrowser:(XLPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
//{
//    NSMutableArray *imageArry = [[NSMutableArray alloc]init];
//    for (MideaTypeModel *model in _imageArray) {
//        [imageArry addObject:model.path];
//    }
//    return imageArry[index];
//}
//
//- (UIView *)photoBrowser:(XLPhotoBrowser *)browser sourceImageViewForIndex:(NSInteger)index
//{
//    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:index inSection:0];
//    UICollectionViewCell *cell = [mainCollectionView cellForItemAtIndexPath:indexpath];
//    return cell.subviews[0];
//}
#pragma mark - headerview的创建
-(UIView *)creatInfomation{
    UIView  *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 3)];
    gapView.backgroundColor = LYColor(223, 223, 223);
    //[view addSubview:gapView];
    
    
    CGFloat height = 30;
    UILabel *MainLabel = [[UILabel alloc]init];
  
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"resume"] || ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
         MainLabel.text = resume;
         height = [self getLabelHeightWithText:resume width:SCREEN_WIDTH - 10 font:FONT(17)];
        MainLabel.frame = CGRectMake(10, 8, SCREEN_WIDTH - 10, height);
    }
    MainLabel.frame = CGRectMake(10, 8, SCREEN_WIDTH - 10, height);
    MainLabel.numberOfLines = 0;
    MainLabel.textColor = LYColor(179, 179, 179);
    MainLabel.font = FONT(17);
    [view addSubview:MainLabel];
    
    UIView *gapViewnew = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(MainLabel.frame) + 20, SCREEN_WIDTH, 1)];
    gapViewnew.backgroundColor = LYColor(223, 223, 223);
    [view addSubview:gapViewnew];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(gapViewnew.frame) + 5, 20, 20)];
    image1.image = [UIImage imageNamed:@"职位灰色"];
    image1.clipsToBounds = YES;
    image1.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image1];
    
    
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(60, CGRectGetMaxY(gapViewnew.frame) + 5, 200, 20)];
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"companies_0"]|| ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
          label1.text = company;
    }
    label1.textColor = LYColor(223, 223, 223);
    label1.font = FONT(16);
    [view addSubview:label1];
    
    
    
    UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(image1.frame) + 5, 20, 20)];
    image2.image = [UIImage imageNamed:@"学校灰色"];
    image2.clipsToBounds = YES;
    image2.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image2];
    
    UILabel *label2= [[UILabel alloc]initWithFrame:CGRectMake(60, CGRectGetMaxY(image1.frame) + 5, 200, 15)];
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"schools_0"]|| ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        label2.text = school;
    }
   
    label2.textColor = LYColor(223, 223, 223);
    label2.font = FONT(16);
    [view addSubview:label2];
    
    
    
    UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(image2.frame) + 5, 20, 20)];
    image3.image = [UIImage imageNamed:@"居住地灰色"];
    image3.clipsToBounds = YES;
    image3.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image3];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(60, CGRectGetMaxY(image2.frame) + 5, 200, 15)];
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"city"]|| ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        label3.text = city;
    }
    label3.textColor = LYColor(223, 223, 223);
    label3.font = FONT(16);
    [view addSubview:label3];
    
    
    
    UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(image3.frame) + 5, 20, 20)];
    image4.image = [UIImage imageNamed:@"国籍灰色"];
    image4.clipsToBounds = YES;
    image4.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image4];
    
    UILabel *label4= [[UILabel alloc]initWithFrame:CGRectMake(60, CGRectGetMaxY(image3.frame) + 5, 200, 15)];
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"country_id"]|| ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        label4.text = country;
        
    }
    label4.textColor = LYColor(223, 223, 223);
    label4.font = FONT(16);
    [view addSubview:label4];
    
    
    
    UIImageView *image5 = [[UIImageView alloc]initWithFrame:CGRectMake(10,CGRectGetMaxY(image4.frame) + 5, 20, 20)];
    image5.image = [UIImage imageNamed:@"感情灰色"];
    image5.clipsToBounds = YES;
    image5.contentMode = UIViewContentModeScaleAspectFit;
    [view addSubview:image5];
    
    UILabel *label5 = [[UILabel alloc]initWithFrame:CGRectMake(60, CGRectGetMaxY(image4.frame) + 5, 200, 15)];
    if ([[PHUserModel sharedPHUserModel].info_show containsString:@"relationship"]|| ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        label5.text = relationship;
    }
    label5.textColor = LYColor(223, 223, 223);
    label5.font = FONT(16);
    [view addSubview:label5];
    
    return view;
}
//发送帖文的header
-(UIView *)creatPostUI{
    
    if ( _imageDataArray.count > 0 || _videoDataArray.count > 0) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
        view.backgroundColor = [UIColor whiteColor];
        Maintext = [[SZTextView alloc]initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH - 30, 60)];
        if (MainString && MainString.length > 0) {
            Maintext.text = MainString;
        }else{
            Maintext.placeholder = NSLocalizedString(@"有什么新鲜事", nil);
        }
        Maintext.delegate = self;
        Maintext.autocapitalizationType = UITextAutocapitalizationTypeNone;
        Maintext.layer.borderWidth = 1.0f;
        Maintext.font = FONT(19);
        Maintext.layer.borderColor = MainColor.CGColor;
        Maintext.layer.cornerRadius = 8;
        [view addSubview:Maintext];
        
        UploadView = [[UIView alloc]initWithFrame:CGRectMake(15, 72, 0, 3)];
        UploadView.backgroundColor = LYColor(94, 194, 248);
        [view addSubview:UploadView];
        
        UIButton *cameraBut = [UIButton buttonWithType:UIButtonTypeCustom];
        cameraBut.frame = CGRectMake(15, 77, 30, 30);
        [cameraBut setImage:[UIImage imageNamed:@"相机"] forState:UIControlStateNormal];
        [cameraBut addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:cameraBut];
        
        UIButton *camera1But = [UIButton buttonWithType:UIButtonTypeCustom];
        camera1But.frame = CGRectMake(CGRectGetMaxX(cameraBut.frame) + 10, 77, 34, 25);
        [camera1But setImage:[UIImage imageNamed:@"摄像"] forState:UIControlStateNormal];
        [camera1But addTarget:self action:@selector(Video) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:camera1But];
        
        UIButton *pagBut = [UIButton buttonWithType:UIButtonTypeCustom];
        pagBut.frame = CGRectMake(CGRectGetMaxX(camera1But.frame) + 10, 77, 25, 25);
        [pagBut setImage:[UIImage imageNamed:@"标签"] forState:UIControlStateNormal];
       // [pagBut addTarget:self action:@selector(tagfriend) forControlEvents:UIControlEventTouchUpInside];
        //[view addSubview:pagBut];
        
        UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sendButton.frame = CGRectMake(SCREEN_WIDTH - 70, 77, 60, 25);
        [sendButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
        sendButton.titleLabel.font  =FONT(14);
        sendButton.backgroundColor = MainColor;
        sendButton.layer.cornerRadius = 10;
        sendButton.clipsToBounds = YES;
        [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:sendButton];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.frame = CGRectMake(SCREEN_WIDTH - 140, 77, 60, 25);
        [cancelButton setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.titleLabel.font  =FONT(14);
        cancelButton.backgroundColor = LYColor(223, 223, 223);
        cancelButton.layer.cornerRadius = 10;
        cancelButton.clipsToBounds = YES;
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [view addSubview:cancelButton];
        
        
        
        CGFloat imageWidth = (SCREEN_WIDTH - 20 - 45) / 9;
       // _SmallimageHeight = imageWidth;
        
        if (_videoDataArray.count > 0) {
            UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(55 , 115, imageWidth , imageWidth )];
            image.image = [UIImage imageNamed:@"默认播放按钮"];
            [view addSubview:image];
        }else{
            
            for (NSInteger i = 0; i < _imageDataArray.count; i++) {
                UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10 + (imageWidth + 5) * i  , 115, imageWidth , imageWidth )];
                id dataArray = _imageDataArray[i];
                
                image.image = [UIImage imageWithData:dataArray];
                image.clipsToBounds = YES;
                image.userInteractionEnabled = YES;
                image.backgroundColor = [UIColor blackColor];
                image.contentMode = UIViewContentModeScaleAspectFit;
                image.userInteractionEnabled = YES;
                image.tag = 920 + i;
               // [image addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(watchImage:)]];
                [view addSubview:image];
                
                UIButton *deleBut = [UIButton buttonWithType:UIButtonTypeCustom];
                deleBut.frame = CGRectMake(CGRectGetMaxX(image.frame) - 10,image.frame.origin.y - 5, 15, 15);
                [deleBut setImage:[UIImage imageNamed:@"图片删除按钮"] forState:UIControlStateNormal];
                deleBut.imageView.contentMode = UIViewContentModeScaleAspectFit;
                [deleBut addTarget:self action:@selector(deletePostImage:)
                  forControlEvents:UIControlEventTouchUpInside];
                deleBut.tag = 900 + i;
                [view addSubview:deleBut];
            }
        }
        
        return view;
    }
    else{
        
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
        view.backgroundColor = [UIColor whiteColor];
        Maintext = [[SZTextView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, 60)];
        Maintext.placeholder = @"有什么新鲜事？";
        Maintext.layer.borderWidth = 1.0f;
        Maintext.font = FONT(19);
        Maintext.delegate = self;
        Maintext.layer.borderColor = MainColor.CGColor;
        Maintext.layer.cornerRadius = 8;
        [view addSubview:Maintext];
        
        UIButton *cameraBut = [UIButton buttonWithType:UIButtonTypeCustom];
        cameraBut.frame = CGRectMake(15, 77, 25, 25);
        [cameraBut setImage:[UIImage imageNamed:@"相机"] forState:UIControlStateNormal];
        [cameraBut addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:cameraBut];
        
        UIButton *camera1But = [UIButton buttonWithType:UIButtonTypeCustom];
        camera1But.frame = CGRectMake(15+30, 77, 25, 25);
        [camera1But setImage:[UIImage imageNamed:@"摄像"] forState:UIControlStateNormal];
        [camera1But addTarget:self action:@selector(Video) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:camera1But];
        
        UIButton *pagBut = [UIButton buttonWithType:UIButtonTypeCustom];
        pagBut.frame = CGRectMake(15+30 + 30, 77, 25, 25);
        [pagBut setImage:[UIImage imageNamed:@"标签"] forState:UIControlStateNormal];
        // [pagBut addTarget:self action:@selector(tagfriend) forControlEvents:UIControlEventTouchUpInside];
        //[view addSubview:pagBut];
        
        UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
        sendButton.frame = CGRectMake(SCREEN_WIDTH - 70, 77, 60, 25);
        [sendButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
        sendButton.titleLabel.font  =FONT(14);
        sendButton.backgroundColor = MainColor;
        sendButton.layer.cornerRadius = 10;
        sendButton.clipsToBounds = YES;
        [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:sendButton];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.frame = CGRectMake(SCREEN_WIDTH - 140, 77, 60, 25);
        [cancelButton setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        cancelButton.titleLabel.font  =FONT(14);
        cancelButton.backgroundColor = LYColor(223, 223, 223);
        cancelButton.layer.cornerRadius = 10;
        cancelButton.clipsToBounds = YES;
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [view addSubview:cancelButton];
        
        return view;
    }
        
    
  
}
//个人页面的UI
-(UIView *)creatHeader{
    CGRect frame = CGRectMake(0, 0, SCREEN_WIDTH, sectionHeaderHeight );
    CGRect imageFrame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_WIDTH / RATIO  );
    
    
    self.strechableImageView = [[UIImageView alloc]init];
    NSString *Url = [NSString stringWithFormat:@"%@%@",@"",self.BannerImage];
    [self.strechableImageView sd_setImageWithURL:[NSURL URLWithString:Url] placeholderImage:[UIImage imageNamed:@"cover"]];
    self.strechableImageView.contentMode = UIViewContentModeScaleToFill
    ;
    self.strechableImageView.frame = imageFrame;
    [self.strechableImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickstrechableImageView:)]];
    self.strechableImageView.userInteractionEnabled = YES;
    self.originFrame = imageFrame;
    
    
    UIView *headerView = [[UIView alloc] initWithFrame: frame];
    headerView.backgroundColor = [UIColor whiteColor];
    [headerView addSubview:self.strechableImageView];
    
//    UIView *ImageBackView = [[UIView alloc]initWithFrame:CGRectMake(20, SCREEN_WIDTH / RATIO - 45, 96, 96)];
//    ImageBackView.backgroundColor = [UIColor whiteColor];
//    ImageBackView.layer.cornerRadius = 48;
//    ImageBackView.clipsToBounds = YES;
    
    
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(20-imageW, SCREEN_WIDTH / RATIO - 45 - imageW, 90 + imageW * 2, 90+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImageView *UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 90, 90)];
    NSString *url = [NSString stringWithFormat:@"%@%@",@"",_ImageUrl];
    [UserImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
    UserImage.layer.cornerRadius = 45;
    UserImage.userInteractionEnabled = YES;
    [UserImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickstrechableImageView:)]];
    UserImage.contentMode = UIViewContentModeScaleAspectFit;
    UserImage.clipsToBounds = YES;
    [leveImage addSubview:UserImage];
    
    UILabel *postLabel = [[UILabel alloc]initWithFrame:CGRectMake(115, SCREEN_WIDTH / RATIO - 50, 30, 50)];
    postLabel.text = [NSString stringWithFormat:@"%@\n\%@",NSLocalizedString(@"帖文", nil),postCount];
    postLabel.numberOfLines = 2;
    postLabel.textAlignment = NSTextAlignmentCenter;
    postLabel.textColor = [UIColor whiteColor];
    postLabel.font = FONT(14);
    
    
    UILabel *fansLabel = [[UILabel alloc]initWithFrame:CGRectMake(175, SCREEN_WIDTH / RATIO - 50, 30, 50)];
    fansLabel.numberOfLines = 2;
    fansLabel.text = [NSString stringWithFormat:@"%@\n\%@",NSLocalizedString(@"粉丝", nil),fansCount];
    fansLabel.textColor = [UIColor whiteColor];
    fansLabel.userInteractionEnabled = YES;
    fansLabel.textAlignment = NSTextAlignmentCenter;
    fansLabel.tag = 50;
    [fansLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkFans:)]];
    fansLabel.font = FONT(14);
    
    
    UILabel *followLabel = [[UILabel alloc]initWithFrame:CGRectMake(235, SCREEN_WIDTH / RATIO - 50, 30, 50)];
    followLabel.numberOfLines = 2;
    followLabel.text = [NSString stringWithFormat:@"%@\n\%@",NSLocalizedString(@"追踪", nil),traceCount];
    followLabel.textColor = [UIColor whiteColor];
    followLabel.userInteractionEnabled = YES;
    followLabel.tag = 51;
    followLabel.textAlignment = NSTextAlignmentLeft;;

    [followLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkFans:)]];
    followLabel.font = FONT(14);
    
    UIButton *followButton = [UIButton buttonWithType:UIButtonTypeCustom];
    followButton.frame =CGRectMake(235, SCREEN_WIDTH / RATIO - 50, 60, 50);
    [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [followButton setTitle:[NSString stringWithFormat:@"%@\n\%@",NSLocalizedString(@"追踪", nil),traceCount] forState:UIControlStateNormal];
    //followButton.tag = 51;
    
    [followButton addTarget:self action:@selector(checkFans:) forControlEvents:UIControlEventTouchUpInside];
    followButton.titleLabel.font = FONT(14);
    
    
    CGFloat maxWidth = SCREEN_WIDTH - 60 - 125;
    CGFloat nameWidth = 0;
    //限制最多显示10个汉字
    if (_Username.length > 10) {
        nameWidth = [self getLabelWidthWithText:@"倪妮妮妮妮妮妮妮妮妮" width:SCREEN_WIDTH - 20 font:FONT(19)];
    }else{
         nameWidth = [self getLabelWidthWithText:_Username width:SCREEN_WIDTH - 20 font:FONT(22)];
    }
    
    if (nameWidth > maxWidth) {
        nameWidth = maxWidth;
    }
    UILabel *UserName = [[UILabel alloc]initWithFrame:CGRectMake(115, SCREEN_WIDTH / RATIO + 10, nameWidth, 30)];
    UserName.text = _Username;
    UserName.textAlignment = NSTextAlignmentCenter;
    UserName.font = Bold_FONT(19);
    
    UIImageView *Uimage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(UserName.frame) + 2, SCREEN_WIDTH / RATIO + 10, 30, 30)];
    Uimage.image = [UIImage imageNamed:@"奖牌"];
    Uimage.clipsToBounds = YES;
    Uimage.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(Uimage.frame) + 2, SCREEN_WIDTH / RATIO + 15, 30, 20)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 10;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    
    CGFloat gapwidth = (SCREEN_WIDTH - 300) / 4;
    
    UIButton *friendBut = [UIButton buttonWithType:UIButtonTypeCustom];
    friendBut.frame = CGRectMake(gapwidth, CGRectGetMaxY(leveImage.frame) + 10, 100, 30);
    friendBut.layer.borderWidth=1.0f;
    friendBut.layer.borderColor =MainColor.CGColor;
    NSString *friend = nil;
    
    if ([isInvite isEqualToString:@"Y"] && [isFriend isEqualToString:@"N"]) {
        friend = @"已邀请好友";
    }
    
    if ([isFriend isEqualToString:@"Y"]) {
        friend = NSLocalizedString(@"好友", nil);
    }else{
        friend = NSLocalizedString(@"添加好友", nil);
    }
    
   
    
    if ([isInvited isEqualToString:@"Y"] && [isFriend isEqualToString:@"N"]) {
        friend = NSLocalizedString(@"邀请回复", nil);
    }
    
    [friendBut setTitle:friend forState:UIControlStateNormal];
    friendBut.titleLabel.font = FONT(13);
    friendBut.layer.cornerRadius = 6;
    friendBut.clipsToBounds =YES;
    [friendBut addTarget:self action:@selector(friendBut:) forControlEvents:UIControlEventTouchUpInside];
    [friendBut setTitleColor:MainColor forState:UIControlStateNormal];
    
    NSString *trace = nil;
    if ([isTrace isEqualToString:@"Y"]) {
        trace = NSLocalizedString(@"已追踪", nil);
    }else{
        trace = NSLocalizedString(@"追踪好友", nil);
    }
    UIButton *followBut = [UIButton buttonWithType:UIButtonTypeCustom];
    followBut.frame = CGRectMake(100 +  2 * gapwidth, CGRectGetMaxY(leveImage.frame) + 10, 100, 30);
    followBut.layer.borderWidth=1.0f;
    followBut.layer.cornerRadius = 6;
    followBut.clipsToBounds =YES;
    followBut.layer.borderColor =MainColor.CGColor;
    [followBut setTitle:trace forState:UIControlStateNormal];
    [followBut addTarget:self action:@selector(followBut:) forControlEvents:UIControlEventTouchUpInside];
    followBut.titleLabel.font = FONT(13);
    [followBut setTitleColor:MainColor forState:UIControlStateNormal];
    
    UIButton *MessageBut = [UIButton buttonWithType:UIButtonTypeCustom];
    MessageBut.frame = CGRectMake(200 + 3 * gapwidth, CGRectGetMaxY(leveImage.frame) + 10, 100, 30);
    //MessageBut.layer.borderWidth=1.0f;
    MessageBut.layer.cornerRadius = 6;
    MessageBut.clipsToBounds =YES;
    //MessageBut.layer.borderColor =LYColor(237, 204, 105).CGColor;
    [MessageBut setTitle:NSLocalizedString(@"发送讯息", nil) forState:UIControlStateNormal];
    MessageBut.titleLabel.font = FONT(13);
    [MessageBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if ([isFriend isEqualToString:@"Y"]) {
        MessageBut.backgroundColor = MainColor;
        [MessageBut addTarget:self action:@selector(gotoChat) forControlEvents:UIControlEventTouchUpInside];
    }else{
         MessageBut.backgroundColor = LYColor(218, 218, 218);
    }
   
    
    [headerView addSubview:UserName];
    [headerView addSubview:Uimage];
    [headerView addSubview:_LevelLabel];
    [headerView addSubview:fansLabel];
    [headerView addSubview:followLabel];
    [headerView addSubview:leveImage];
    [headerView addSubview:postLabel];
    
    if ([self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(leveImage.frame) + 5, SCREEN_WIDTH, 3)];
        gapView.backgroundColor = LYColor(223, 223, 223);
        [headerView addSubview:gapView];
    }else{
        [headerView addSubview:friendBut];
        [headerView addSubview:followBut];
        
        [headerView addSubview:MessageBut];
        
        UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(MessageBut.frame) + 10, SCREEN_WIDTH, 3)];
        gapView.backgroundColor = LYColor(223, 223, 223);
        [headerView addSubview:gapView];
    }
    
    return headerView;
}
#pragma mark  - TableView delegage && datasouce

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
   
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0) {
        return sectionHeaderHeight;
    }
    else if (section == 1 && selectIndex == 0){
        if ([[PHUserModel sharedPHUserModel].info_show containsString:@"resume"] || ![self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            return  [self getLabelHeightWithText:resume width:SCREEN_WIDTH - 10 font:FONT(17)] + 110;
            
        }else{
             return 135;
        }
       
    }
    else if (selectIndex == 1 && section == 1){
        if ( _imageDataArray.count > 0 || _videoDataArray.count>0) {
            return  110 + 70;
        }else{
            return 110;
        }
    }
    else if (selectIndex == 3 && section == 1){
        return 0;
    }else if (selectIndex == 2 && section == 1){
        return 0;
    }
        
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   
    if (section == 0) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, sectionHeaderHeight)];
        
        [view addSubview:[self creatHeader]];
        NSArray *titleArray = @[NSLocalizedString(@"个人简介", nil), NSLocalizedString(@"所有帖文", nil), NSLocalizedString(@"好友清单", nil),NSLocalizedString(@"媒体内容", nil)];
        toolBarTwo.SelectIndex = selectIndex;
        toolBarTwo = [[CALToolBarView alloc] initWithFrame:CGRectMake(10, sectionHeaderHeight - 50, SCREEN_WIDTH -20 , 50)
                                                titleArray:titleArray
                                             selectedColor:MainColor
                                             deselectColor:LYColor(218, 218, 218)
                                           bottomlineColor:[NSString colorWithHexString:@"efefef"]
                                         selectedLineColor:nil selectIndex:selectIndex];
        toolBarTwo.isNeedSelectedLine = NO;
        
        
        __weak MainUserViewController *weakVC = self;
        [toolBarTwo setCalToolBarSelectedBlock:^(NSInteger index) {
            selectIndex = index;
            self.postPlayer = nil;
            self.mediaPlayer = nil;
            [weakVC loadType];
        }];
       
        [view addSubview:toolBarTwo];
        
        return view;
    }
    else if (section == 1 && selectIndex == 0){
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  135)];
        [view addSubview:[self creatInfomation]];
        return view;
    }else if (section == 1 && selectIndex == 1){
        return [self creatPostUI];
    }
    else if (section == 1&& selectIndex == 3){
        //return [self creatImageUI];
        return nil;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return 3;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
   
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , 3)];
    return view;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
        if (selectIndex == 0) {
            return 0;
        }else if (selectIndex == 1 && section == 1){
            return _dataArray.count;
        }else if (selectIndex == 2 && section == 1){
            return _friendArray.count;
        }
        else if(selectIndex == 3 && section == 1){
            return 1;
        }
   

    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        if(selectIndex == 1  && indexPath.section == 1){
            MainModel *model = _dataArray[indexPath.row];
            
            if ([model.post_type isEqualToString:@"video"]) {
                [self setPostPlayer];

                VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videocell" forIndexPath:indexPath];
                for (UIView *view in cell.contentView.subviews) {
                    [view removeFromSuperview];
                }
                [cell setMode:model];
                
                cell.CommentTld.delegate = self;
                cell.MainLabel.delegate = self;
                cell.delegate = self;
                [cell setDelegate:self withIndexPath:indexPath];
                
                [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
                [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
                [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
                [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
                [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
                // [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];
                
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckpostInformation:)];
                [cell.UserImage addGestureRecognizer:tap];
                [cell.UserName addGestureRecognizer:tap];
                
                UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
                [cell.ForWhoLabel addGestureRecognizer:forWho];
                cell.ForWhoLabel.tag = 200 + indexPath.row;
                
                cell.CommentTld.tag = 300 + indexPath.row;
                cell.MoreCommentBut.tag = 200 + indexPath.row;
                cell.CommentBut.tag = 200 + indexPath.row;
                cell.CommentButton.tag = 200 + indexPath.row;
                cell.LikeButton.tag = 200 + indexPath.row;
                cell.UserImage.tag = 200 + indexPath.row;
                cell.ForwardButton.tag = 200 + indexPath.row;
                cell.CollectButton.tag = 200 + indexPath.row;
                cell.UserName.tag = 200 +indexPath.row;
                cell.MoreButton.tag = 200 + indexPath.row;
                cell.ReportButton.tag = 200 + indexPath.row;
                // [self comment:cell.CommentButton];
                
                
                return cell;
            }
            else if ([model.post_type isEqualToString:@"share"]){
                [self setPostPlayer];

                ShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sharecell" forIndexPath:indexPath];
                for (UIView *view in cell.contentView.subviews) {
                    [view removeFromSuperview];
                }
                
                if ([model.shareType isEqualToString:@"video"]) {
                    [cell setVideoMode:model];
                }else{
                    [cell setMode:model];
                }
                
                //            if (model.isShowCommentV == YES) {
                //                cell.CommentView.hidden = NO;
                //            }else{
                //                cell.CommentView.hidden = YES;
                //            }
                cell.CommentTld.delegate = self;
                cell.MainLabel.delegate = self;
                cell.delegate = self;
                [cell setDelegate:self withIndexPath:indexPath];
                
                [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
                [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
                [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
                [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
                [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
                // [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];
                
                
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckpostInformation:)];
                [cell.UserImage addGestureRecognizer:tap];
                [cell.UserName addGestureRecognizer:tap];
                
                UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
                [cell.ForWhoLabel addGestureRecognizer:forWho];
                cell.ForWhoLabel.tag = 200 + indexPath.row;
                
                cell.CommentTld.tag = 300 + indexPath.row;
                cell.MoreCommentBut.tag = 200 + indexPath.row;
                cell.CommentBut.tag = 200 + indexPath.row;
                cell.CommentButton.tag = 200 + indexPath.row;
                cell.LikeButton.tag = 200 + indexPath.row;
                cell.UserImage.tag = 200 + indexPath.row;
                cell.ForwardButton.tag = 200 + indexPath.row;
                cell.CollectButton.tag = 200 + indexPath.row;
                cell.UserName.tag = 200 +indexPath.row;
                cell.MoreButton.tag = 200 + indexPath.row;
                cell.ReportButton.tag = 200 + indexPath.row;
                //  [self comment:cell.CommentButton];
                
                
                return cell;
            }
            else{
                MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
                for (UIView *view in cell.contentView.subviews) {
                    [view removeFromSuperview];
                }
                
                // [cell setMode:model];
                cell.model = model;
                //            if (model.isShowCommentV == YES) {
                //                cell.CommentView.hidden = NO;
                //            }else{
                //                cell.CommentView.hidden = YES;
                //            }
                cell.CommentTld.delegate = self;
                cell.MainLabel.delegate = self;
                cell.delegate = self;
                
                [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
                [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
                [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
                [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
                [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
                [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
                
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckpostInformation:)];
                [cell.UserImage addGestureRecognizer:tap];
                [cell.UserName addGestureRecognizer:tap];
                
                UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
                [cell.ForWhoLabel addGestureRecognizer:forWho];
                cell.ForWhoLabel.tag = 200 + indexPath.row;
                
                cell.CommentTld.tag = 300 + indexPath.row;
                cell.MoreCommentBut.tag = 200 + indexPath.row;
                cell.CommentBut.tag = 200 + indexPath.row;
                cell.CommentButton.tag = 200 + indexPath.row;
                cell.LikeButton.tag = 200 + indexPath.row;
                cell.UserImage.tag = 200 + indexPath.row;
                cell.ForwardButton.tag = 200 + indexPath.row;
                cell.CollectButton.tag = 200 + indexPath.row;
                cell.UserName.tag = 200 +indexPath.row;
                cell.MoreButton.tag = 200 + indexPath.row;
                cell.ReportButton.tag = 200 + indexPath.row;
                //  [self comment:cell.CommentButton];
                
                
                return cell;
            }
        }
        else if (selectIndex == 2 && indexPath.section == 1){
            UserFriTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friendcell" forIndexPath:indexPath];
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            
            InviteModel *model = _friendArray[indexPath.row];
            [cell setModel:model];
            
            cell.FreInvitButton.tag = 500 + indexPath.row;
            cell.FollowButton.tag = 500 + indexPath.row;
            cell.UserImage.tag = 500 + indexPath.row;
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
            
            [cell.FreInvitButton addTarget:self action:@selector(InviteFriend:) forControlEvents:UIControlEventTouchUpInside];
            [cell.FollowButton addTarget:self action:@selector(FollowFriend:) forControlEvents:UIControlEventTouchUpInside];
            
            
            return cell;
        }
        else if (selectIndex == 3 && indexPath.section == 1){
            UITableViewCell *cell = [MainTableView dequeueReusableCellWithIdentifier:@"other"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"other"];
            }

            [cell.contentView addSubview:[self creatImageUI]];

            _mediaPlayerManager = [[ZFAVPlayerManager alloc] init];

            [self setMediaPlayer];

            return cell;
        }
    
    return nil;
   
}

-(void)setMediaPlayer{
    self.mediaPlayer = [ZFPlayerController playerWithScrollView:mainCollectionView playerManager:_mediaPlayerManager containerViewTag:100];
    self.mediaPlayer.controlView = self.mediaControlView;
    self.mediaPlayer.assetURLs = self.mediaUrls;
    self.mediaPlayer.shouldAutoPlay = YES;
}

- (NSMutableArray *)mediaUrls {
    _mediaUrls = @[].mutableCopy;
    for (MideaTypeModel *model in _imageArray) {
        if (model.path) {
            NSString *URLString = [model.path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            [_mediaUrls addObject:url];
        } else {
            NSString *URLString = [@"" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            [_mediaUrls addObject:url];
        }
    }
    return _mediaUrls;
}

- (NSMutableArray *)postUrls {
    _postUrls = @[].mutableCopy;
    for (MainModel *model in _dataArray) {
        if (model.videoPath) {
            NSString *URLString = [model.videoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            [_postUrls addObject:url];
        } else {
            NSString *URLString = [@"" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            [_postUrls addObject:url];
        }
    }
    return _postUrls;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (selectIndex == 2 && indexPath.section == 1) {
        InviteModel *model = _friendArray[indexPath.row];
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.invitee_id;
        controller.BannerImage = model.BannerImage;
        controller.ImageUrl = model.UserImage;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //else{
        if (selectIndex == 0) {
            return 0;
        }else if(selectIndex == 1 && indexPath.section ==1){
            MainModel *main = _dataArray[indexPath.row];
            
            return main.cellHeight + main.CommentHeight;
        }else if (selectIndex == 2 && indexPath.section == 1){
            return 80;
        }else if (selectIndex == 3 && indexPath.section == 1){
            CGFloat height = (_imageArray.count / 3+ 1) * (cellectionHeight + 10) +10;
            if ([self.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]){
                return height + 50;
            }
            else{
                return height;
            }
            // return ScreenHeight - Height_NavBar - Height_TabBar;
            
        }
    //}
   
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectIndex == 0) {
        return 0;
    }else if(selectIndex == 1 && indexPath.section ==1){
        MainModel *main = _dataArray[indexPath.row];

         return main.cellHeight + main.CommentHeight;
    }else if (selectIndex == 2 && indexPath.section == 1){
        return 80;
    }else if (selectIndex == 3 && indexPath.section == 1){
        return SCREEN_HEIGHT ;
    }
    return 0;
    
    
    
}

#pragma 聊天
-(void)gotoChat{
    ChatroomViewController *controller = [[ChatroomViewController alloc]init];
    controller.member_id = self.member_id;
    controller.barTitle = _Username;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark 点击网页
- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
//    ViewController *web = [[ViewController alloc]init];
//
//    NSString *stering = url.absoluteString;
//    web.url = stering;
//    //self.viewC = [[MainViewController alloc]init];
//    [self.navigationController pushViewController:web animated:YES];
     [[UIApplication sharedApplication ] openURL: url];
    
}
#pragma mark  - 点击用户头像
-(void)clickstrechableImageView:(UITapGestureRecognizer *)tap{
    CLAmplifyView *amplifyView = [[CLAmplifyView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) andGesture:tap andSuperView:self.view];
    [[UIApplication sharedApplication].keyWindow addSubview:amplifyView];
}
#pragma mark  - 查看粉丝
-(void)checkFans:(UIButton *)button{
    MyfriendViewController *controller = [[MyfriendViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.member_id = self.member_id;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)followBut:(UIButton *)button{
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect: button.bounds toView:window];
    
    NSString *isfriend = nil;
    if ([isTrace isEqualToString:@"Y"]) {
        isfriend = NSLocalizedString(@"取消追踪", nil);
    }else{
        isfriend = NSLocalizedString(@"追踪", nil);
    }
    
    PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[isfriend,NSLocalizedString(@"取消", nil)] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
    menuView.tag = 1100;
    menuView.titleColor = LYColor(237, 204, 105);
    menuView.lineColor = LYColor(237, 204, 105);
    
    currentMenuView = menuView;
}
-(void)friendBut:(UIButton *)button{
    
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect: button.bounds toView:window];
    
    PST_MenuView *menuView = nil;
    
    NSArray *array =nil;
    if ([isFriend isEqualToString:@"Y"]) {
        array = @[NSLocalizedString(@"删除好友", nil),NSLocalizedString(@"取消", nil)];
       
    }else{
        array = @[NSLocalizedString(@"添加好友", nil),NSLocalizedString(@"取消", nil)];
    }
    
    if ([isInvited isEqualToString:@"Y"] && [isFriend isEqualToString:@"N"]) {
        array = @[NSLocalizedString(@"同意", nil),NSLocalizedString(@"拒绝", nil)];

    }
    
    
    menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:array imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
    menuView.tag = 1000;
    menuView.titleColor = LYColor(237, 204, 105);
    menuView.lineColor = LYColor(237, 204, 105);
    currentMenuView = menuView;
   
}
//好友清单好友点击事件
-(void)InviteFriend:(UIButton *)button{
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect: button.bounds toView:window];
    
    InviteModel *model = _friendArray[button.tag - 500];
    NSString *isfriend = nil;
    if ([model.isFriend isEqualToString:@"1"]) {
        isfriend = NSLocalizedString(@"删除好友", nil);
    }else{
        isfriend = NSLocalizedString(@"添加好友", nil);
    }
    
    PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[isfriend,@"取消"] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
     menuView.tag = button.tag + 100;
    menuView.titleColor = LYColor(237, 204, 105);
    menuView.lineColor = LYColor(237, 204, 105);
    
    currentMenuView = menuView;
}
//好友清单追踪点击事件
-(void)FollowFriend:(UIButton *)button{
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[button convertRect: button.bounds toView:window];
   
    InviteModel *model = _friendArray[button.tag - 500];
    
    NSString *istrace = nil;
    if ([model.isTrace isEqualToString:@"1"]) {
        istrace = NSLocalizedString(@"取消追踪", nil);
    }else{
        istrace = NSLocalizedString(@"追踪好友", nil);
    }
    
    PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[istrace,@"取消"] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
    menuView.tag = button.tag + 200;
    menuView.titleColor = LYColor(237, 204, 105);
    menuView.lineColor = LYColor(237, 204, 105);
    
    currentMenuView = menuView;
}
-(void)didSelectRowAtIndex:(NSInteger)index title:(NSString *)title img:(NSString *)img{
    
    //好友清单中的操作
    if (currentMenuView.tag < 1000) {
        if ([title isEqualToString:NSLocalizedString(@"删除好友", nil)]) {
            [self deleFrienddd];
        }else if ([title isEqualToString:NSLocalizedString(@"添加好友", nil)]){
            [self addFriend];
        }
        else if ([title isEqualToString:NSLocalizedString(@"追踪好友", nil)]){
            [self Tracefriend];
        }else if ([title isEqualToString:NSLocalizedString(@"取消追踪", nil)]){
            [self cancelTrace];
        }
    }//当前这人主页的操作
    else if(currentMenuView.tag == 1000 ){

        
        if ([title isEqualToString:NSLocalizedString(@"同意", nil)]) {
              [self SetInvited:0];
        }else if ([title isEqualToString:NSLocalizedString(@"拒绝", nil)]){
              [self SetInvited:1];
        }else if ([title isEqualToString:NSLocalizedString(@"删除好友", nil)]) {
            [self deleFr];//删除当前好友
        }else if ([title isEqualToString:NSLocalizedString(@"添加好友", nil)]){
            [self addFri];//添加当前好友
        }
//        if ([isInvited isEqualToString:@"Y"] && [isFriend isEqualToString:@"N"]) {
//            [self SetInvited:index];
//
//        }
//        else{
//            if ([title isEqualToString:NSLocalizedString(@"删除好友", nil)]) {
//                [self deleFr];//删除当前好友
//            }else if ([title isEqualToString:NSLocalizedString(@"添加好友", nil)]){
//                [self addFri];//添加当前好友
//            }
//        }
       
    }
    else if (currentMenuView.tag == 1100){
        if ([title isEqualToString:NSLocalizedString(@"追踪", nil)]) {
           // [self deleFr];//追踪当前好友
            [self TracefriendNow];
        }else if ([title isEqualToString:NSLocalizedString(@"取消追踪", nil)]){
            [self cancelTraceNow];//取消追踪当前好友
        }
    }
   
}
-(void)SetInvited:(NSInteger)index{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/invite/setInviteStatus"];
    //InviteModel *model = _dataArray[currentIndex];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"invitee_id":self.member_id,@"status":[NSString stringWithFormat:@"%ld",index]};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dict = responseObject;
        [CYToast showSuccessWithString:[NSString stringWithFormat:@"%@",dict[@"message"]]];
        //[self loadData];
        [self getFriendStatus];
        [self getFriendIsInvuted];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)CheckInformation:(UITapGestureRecognizer *)tap{
    InviteModel *model =_friendArray[tap.view.tag - 500];
    
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.invitee_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.m_showBackBt = YES;
    controller.BannerImage = model.BannerImage;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark  -  网络请求
-(void)loadType{
    if (selectIndex != 3) {
        [mainCollectionView removeFromSuperview];
    }
    
    if (selectIndex == 0) {
       // [MainTableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [MainTableView reloadData];
        });
    }
   else if (selectIndex == 1) {
        if (_dataArray && _dataArray.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MainTableView reloadData];
            });
        }else{
            [self LoadSelf];
        }
    }
    else if (selectIndex == 2){
        if (_friendArray && _friendArray.count > 0) {
            //[MainTableView reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MainTableView reloadData];
            });
        }else{
            [self loadFriend];
        }
    }
    else if (selectIndex == 3){
        if (_imageArray && _imageArray.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MainTableView reloadData];
                 // [self.view addSubview:[self creatImageUI]];
            });
        }else{
            [self loadPicture];
        }
    }
}
/**
 获得自己的所有帖文
 */
-(void)LoadSelf{
    [CYToast showStatusWithString:@"正在加载"];
    [_dataArray removeAllObjects];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getSelfPosts];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id,@"perPage":@"10",@"Page":[NSString stringWithFormat:@"%ld",Page],@"decode":@"1"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
         [CYToast dismiss];
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_dataArray addObject:[MainModel pareWithDictionary:parmDic]];
            }
        }

        self.postPlayer.assetURLs = self.postUrls;

        downloadGroup = dispatch_group_create();
        
        for (MainModel *model in _dataArray) {
            [self loadComment:model];
        }
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
            [MainTableView reloadData];
           // MainTableView.contentOffset = CGPointMake(0, 0);
        });
       
    } failure:^(NSError *error) {
       
        NSLog(@"%@",error);
    }];
}
-(void)didCloseWindow{
    
}
-(void)playWith:(ZFPlayerView *)PlView{
    
}
-(void)loadComment:(MainModel  *)CoModel{
    dispatch_group_enter(downloadGroup);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/post/getComments"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":CoModel.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        
        if (dataArr && dataArr.count > 0) {
            [CoModel.CommentArray removeAllObjects];
            for (NSDictionary *Comdict in dataArr) {
                CommentModel *mode =[CommentModel parsenWithDictionary:Comdict];
                [CoModel.CommentArray addObject:mode];
            }
        }
        
        dispatch_group_leave(downloadGroup);
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
/**
 获得自己的取得好友清單
 */
-(void)loadFriend{
     [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getSelfFriend];
     NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_friendArray addObject:[InviteModel pareWithDictionary:parmDic]];
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 获得自己的所有图片
 */
-(void)loadPicture{
   
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getSeflPicture];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        [_postImageArray  removeAllObjects];
        [_imageDataArray removeAllObjects];
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
             [_imageArray removeAllObjects];
            for (NSDictionary *parmDic in dataArr) {
                [_imageArray addObject:[MideaTypeModel parseDict:parmDic]];
                
                if (![[NetDataCommon stringFromDic:parmDic forKey:@"fileType"] isEqualToString:@"movie"]) {
                    [_OnlyImage addObject:[MideaTypeModel parseDict:parmDic]];
                }
            }
        }
        [CYToast dismiss];
        [MainTableView reloadData];
      // [self.view addSubview:[self creatImageUI]];
       // [mainCollectionView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];

        NSLog(@"%@",error);
    }];
}

/**
 加载更多媒体内容
 */
-(void)loadMoreMidea{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getSeflPicture];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_imageArray addObject:[MideaTypeModel parseDict:parmDic]];
            }
        }

        self.mediaPlayer.assetURLs = self.mediaUrls;

        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        
        NSLog(@"%@",error);
    }];
}
//获取自己的粉丝，帖文数量
-(void)loadInfo{
     [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getSelfInfo];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        fansCount = dic[@"data"][@"fansCount"];
        postCount = dic[@"data"][@"postCount"];
        traceCount = dic[@"data"][@"traceCount"];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
//获取用户的个人信息
-(void)getMemeberBrief{

    NSString *url = nil;
    if (self.member_id == [PHUserModel sharedPHUserModel].member_id) {
        url = [NSString stringWithFormat:@"%@%@",TestUrl,getMemberBrief];
    }else{
        url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/member/getMemberBrief"];
    }
    [CYToast showStatusWithString:@"正在加载"];
    //NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/member/getMemberBrief"];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        city = [NetDataCommon stringFromDic:dic forKey:@"city"];
        NSArray *companArr = dic[@"companies"];
        if (companArr && companArr.count > 0) {
            company = [NetDataCommon stringFromDic:companArr[0] forKey:@"company"];
            position = [NetDataCommon stringFromDic:companArr[0] forKey:@"position"];
        }
        NSArray *schoolArr = dic[@"schools"];
        if (schoolArr && schoolArr.count > 0) {
            department = [NetDataCommon stringFromDic:schoolArr[0] forKey:@"department"];
            school = [NetDataCommon stringFromDic:schoolArr[0] forKey:@"school"];
        }
        country = [NetDataCommon stringFromDic:dic forKey:@"country"];
        relationship = [NetDataCommon stringFromDic:dic forKey:@"relationship"];
        resume = [NetDataCommon stringFromDic:dic forKey:@"resume"];

       [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 是否为好友
 */
-(void)loadFriendRelation{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,Isfriend];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        isFriend = dic[@"data"];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 是否已追踪
 */
-(void)loadTraceRelation{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,Istrace];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        isTrace = dic[@"data"];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 删除好友
 */
-(void)deleFrienddd{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,deleteFriend];
    
     InviteModel *model = _friendArray[currentMenuView.tag - 600];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"friend_id":model.invitee_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
       // [CYToast dismiss];
        NSDictionary *dic = responseObject;
      [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
//            if ([model.isFriend isEqualToString:@"1"]) {
//                model.isFriend = @"0";
//            }else{
//                model.isFriend = @"1";
//            }
            [_friendArray removeObjectAtIndex:currentMenuView.tag -600];
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 删除当前好友
 */
-(void)deleFr{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,deleteFriend];
    
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"friend_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([isFriend isEqualToString:@"Y"]) {
                isFriend = @"N";
            }else{
                isFriend = @"Y";
            }
           
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 添加当前好友
 */
-(void)addFri{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,AddFriend];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"invitee_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([isFriend isEqualToString:@"Y"]) {
                isFriend = @"N";
            }else{
                isFriend = @"Y";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)addFriend{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,AddFriend];
    InviteModel *model = _friendArray[currentMenuView.tag - 600];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"invitee_id":model.invitee_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([model.isFriend isEqualToString:@"1"]) {
                model.isFriend = @"0";
            }else{
                model.isFriend = @"1";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 取消追踪当前好友
 */
-(void)cancelTraceNow{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,delTraceFriend];
    
    //InviteModel *model = _friendArray[currentMenuView.tag - 700];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"trace_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([isTrace isEqualToString:@"Y"]) {
                isTrace = @"N";
            }else{
                isTrace = @"Y";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 取消追踪
 */
-(void)cancelTrace{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,delTraceFriend];
    
    InviteModel *model = _friendArray[currentMenuView.tag - 700];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"trace_id":model.invitee_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
      [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([model.isTrace isEqualToString:@"1"]) {
                model.isTrace = @"0";
            }else{
                model.isTrace = @"1";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
 追踪当前好友
 */
-(void)TracefriendNow{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,TraceFriend];
    
    //InviteModel *model = _friendArray[currentMenuView.tag - 700];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"trace_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"data"] hideAfterDelay:0.5f];
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([isTrace isEqualToString:@"Y"]) {
                isTrace = @"N";
            }else{
                isTrace = @"Y";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
/**
追踪好友
 */
-(void)Tracefriend{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,TraceFriend];
    
    InviteModel *model = _friendArray[currentMenuView.tag - 700];
    
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"trace_id":model.invitee_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"data"] hideAfterDelay:0.5f];
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
            if ([model.isTrace isEqualToString:@"1"]) {
                model.isTrace = @"0";
            }else{
                model.isTrace = @"1";
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)getFriendStatus{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,FriendStatus];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        isInvite = dic[@"data"];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)getFriendIsInvuted{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,FriendInvited];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        isInvited = dic[@"data"];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    

}
#pragma mark 上传媒体库
-(void)upLoadPicMov:(UITapGestureRecognizer *)tap{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"上传图片", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"上传视频", nil)];
    
    [actionItems addObject:item];
    [actionItems addObject:item1];
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        if (selectedIndex == 0) {
            [self selectFromAblum];
        }else if (selectedIndex == 1){
            [self Video];
        }
    }];
}
/**
 相册选择
 */
#pragma mark - 相册
-(void)selectFromAblum{
    if (self.imageDataArray.count >= 9 && selectIndex == 1) {
        [CYToast showErrorWithString:@"最多只能上传9张图片"];
        return;
    }

    picker = [[ZYQAssetPickerController alloc] init];
    picker.maximumNumberOfSelection = 9;
    picker.assetsFilter = ZYQAssetsFilterAllAssets;
    picker.showEmptyGroups=NO;
    picker.delegate=self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([(ZYQAsset*)evaluatedObject mediaType]==ZYQAssetMediaTypeVideo) {
            NSTimeInterval duration = [(ZYQAsset*)evaluatedObject duration];
            return duration >= 5;
        } else {
            return YES;
        }


    }];
    [self presentViewController:picker animated:YES completion:nil];
}
#pragma mark - 选择相片
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i=0; i<assets.count; i++) {
            ZYQAsset *asset=assets[i];
            [asset setGetFullScreenImage:^(UIImage *result) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //由于iphone拍照的图片太大，直接存入数组中势必会造成内存警告，严重会导致程序崩溃，所以存入沙盒中
                    //压缩图片，这个压缩的图片就是做为你传入服务器的图片
                    NSData *imageData=UIImageJPEGRepresentation(result, 0.6);
                    if (selectIndex == 3) {
                        [self uploadPicture:imageData WithInde:i];
                        [_MideaPicArray addObject:imageData];
                    }else{
                         [_imageDataArray addObject:imageData];
                    }
                   

                });

            }];
        }


    });


    [self dismissViewControllerAnimated:YES completion:^{
        
       // [MainTableView reloadData];
    }];

}
/**
 上传图片到媒体库
 */
-(void)uploadPicture:(NSData *)imageData WithInde:(NSInteger)index{
    [CYToast showStatusWithString:@"正在加载"];

    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    manger.requestSerializer  = [AFJSONRequestSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,uploadPicAndMov];
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        //NSData *imageData =imageData;
        NSString *fileName = [NSString stringWithFormat:@"ljq.jpg"];
        [formData appendPartWithFileData:imageData name:@"media" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        
        NSDictionary *dict = responseObject;
        if ([dict[@"status"] isEqualToString:@"success"]) {
            
                [_postNewImageArray addObject:dict[@"id"]];
            
            if (_postNewImageArray.count == _MideaPicArray.count ) {
                [self loadPicture];
            }
            [CYToast dismiss];
        }
        else{
            [CYToast showErrorWithString:@"上传失败"];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [CYToast showErrorWithString:@"上传失败"];
    }];
    
}
//#pragma mark - 上传视频
//-(void)Video{
//    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
//    NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
//    picker.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
//    picker.delegate = self;//设置委托
//    [self presentViewController:picker animated:YES completion:nil];
//
//
//}
#pragma mark - 视频按钮点击事件
-(void)Video{
    self.mediaPlayer = nil;
    
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"录像", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"相册", nil)];
    
    [actionItems addObject:item];
    [actionItems addObject:item1];
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (selectedIndex == 0) {
            [self makeVideo];
        }else if (selectedIndex == 1){
            [self SelectVideo];
        }
    }];
}
-(void)SelectVideo{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
    NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
    picker.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
    picker.delegate = self;//设置委托
    [self presentViewController:picker animated:YES completion:nil];
}
#pragma mark - 录像
-(void)makeVideo{
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.delegate = self;
    _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickerController.allowsEditing = YES;
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //录制视频时长，默认10s
    _imagePickerController.videoMaximumDuration = 90;
    
    //相机类型（拍照、录像...）字符串需要做相应的类型转换
    _imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
    
    //视频上传质量
    //UIImagePickerControllerQualityTypeHigh高清
    //UIImagePickerControllerQualityTypeMedium中等质量
    //UIImagePickerControllerQualityTypeLow低质量
    //UIImagePickerControllerQualityType640x480
    _imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    
    //设置摄像头模式（拍照，录制视频）为录像模式
    _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}
//选择视频结束
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)infon{
    
    NSString *mediaType=[infon objectForKey:UIImagePickerControllerMediaType];
    //判断资源类型
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){//图片
        dispatch_async(dispatch_get_main_queue(), ^{
            //由于iphone拍照的图片太大，直接存入数组中势必会造成内存警告，严重会导致程序崩溃，所以存入沙盒中
            //压缩图片，这个压缩的图片就是做为你传入服务器的图片
            UIImage *result = infon[UIImagePickerControllerEditedImage];
            
            NSData *imageData=UIImageJPEGRepresentation(result, 0.6);
            if (selectIndex == 1) {
                //[self.postVideoArray addObject:imageData];
                [self.imageDataArray addObject:imageData];

            }else{
                [self.imageDataArray addObject:imageData];
            }
          
        });
        [self dismissViewControllerAnimated:YES completion:^{
            [MainTableView reloadData];
        }];
    }else{//视频类型
        
        NSURL *sourceURL = [infon objectForKey:UIImagePickerControllerMediaURL];
        //NSLog(@"%@",[NSString stringWithFormat:@"%f s", [self getVideoLength:sourceURL]]);
        //NSLog(@"%@", [NSString stringWithFormat:@"%.2f kb", [self getFileSize:[sourceURL path]]]);
        NSURL *newVideoUrl ; //一般.mp4
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];//用时间给文件全名，以免重复，在测试的时候其实可以判断文件是否存在若存在，则删除，重新生成文件即可
        [formater setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
        newVideoUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mp4", [formater stringFromDate:[NSDate date]]]] ;//这个是保存在app自己的沙盒路径里，后面可以选择是否在上传后删除掉。我建议删除掉，免得占空间。
        [picker dismissViewControllerAnimated:YES completion:nil];
        [self convertVideoQuailtyWithInputURL:sourceURL outputURL:newVideoUrl completeHandler:nil];
    }
    
}
//压缩视频
- (void) convertVideoQuailtyWithInputURL:(NSURL*)inputURL
                               outputURL:(NSURL*)outputURL
                         completeHandler:(void (^)(AVAssetExportSession*))handler
{
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    //  NSLog(resultPath);
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse= YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"AVAssetExportSessionStatusCancelled");
                 break;
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"AVAssetExportSessionStatusUnknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"AVAssetExportSessionStatusWaiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"AVAssetExportSessionStatusExporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
                 NSLog(@"AVAssetExportSessionStatusCompleted");
             {
                 
                 _FilePath = outputURL;
                 [self uploadMovie:outputURL];
//                 //[self.imageDataArray addObject:[self getVideoPreViewImage:outputURL]];
//                 //                 UIImage *image = [UIImage imageNamed:@"默认播放按钮"];
//                 //                 NSData *data =UIImageJPEGRepresentation(image, 1.0);
//                 //[_videoDataArray addObject:[self getVideoPreViewImage:outputURL]];
//                 [_videoDataArray addObject:[UIImage imageNamed:@"默认播放按钮"]];
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     [MainTableView reloadData];
//                 });
                 if (selectIndex == 1) {
                     [_videoDataArray addObject:[UIImage imageNamed:@"默认播放按钮"]];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [MainTableView reloadData];
                     });
                 }
                 
                 
                 
             }

                 break;
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"AVAssetExportSessionStatusFailed");
                 break;
         }
         
     }];
    
}
/**
 上传影片
 */
-(void)uploadMovie:(NSURL *)Url{
    [CYToast showStatusWithString:@"正在加载"];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl
                     ,uploadPicAndMov];
    
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *imageData = [NSData dataWithContentsOfURL:Url];  ;
        NSString *fileName = [NSString stringWithFormat:@"ljq.mp4"];
        [formData appendPartWithFileData:imageData name:@"media" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CYToast dismiss];
        NSDictionary *dict = responseObject;
        if ([dict[@"status"] isEqualToString:@"success"]) {
           [[NSFileManager defaultManager] removeItemAtPath:[_FilePath path] error:nil];
            [self loadPicture];
            if (selectIndex ==1) {
                [self postMessage:dict[@"id"]];
            }
        }else{
             [CYToast showErrorWithString:@"上传失败"];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CYToast showErrorWithString:@"上传失败"];
    }];
}
#pragma mark - 设置图片collectionview下滑隐藏header
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{

   
   
}

#pragma mark - 发文相关
/**
 点击发布
 */
-(void)send{
    [Maintext resignFirstResponder];
    [Maintext endEditing:YES];
    MainString = nil;

    if (_videoDataArray.count > 0) {
        [self uploadMovie:_FilePath];
        return;
    }

    if (_imageDataArray.count > 0) {
        for (NSInteger i = 0 ; i < _imageDataArray.count; i++) {
            [self uploadPicture:_imageDataArray[i] WithIndex:i];
        }
        return;
    }


    if (Maintext.text.length == 0) {
        [CYToast showErrorWithString:@"发文不能为空"];
        return;
    }else{
        [self postMessage:@""];
    }

}
/**
 上传图片
 */
-(void)uploadPicture:(NSData *)imageData WithIndex:(NSInteger )ind{
    [CYToast showStatusWithString:@"正在加载"];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    __weak __block  UIView *newView = UploadView;
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    manger.requestSerializer  = [AFJSONRequestSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,UploadPic];
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        // NSData *imageData =_imageDataArray[0];
        NSString *fileName = [NSString stringWithFormat:@"ljq.jpg"];
        [formData appendPartWithFileData:imageData name:@"picture" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //NSLog(@"%@",uploadProgress);
        CGFloat width = uploadProgress.fractionCompleted * (SCREEN_WIDTH - 30);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%f",width);
            newView.frame = CGRectMake(newView.frame.origin.x, newView.frame.origin.y, width, 3);
        });
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = responseObject;
        
        if ([dict[@"status"] isEqualToString:@"success"] ) {
            [_postNewImageArray addObject:dict[@"id"]];
        }
        if (_postNewImageArray.count == _imageDataArray.count ) {
            [CYToast dismiss];
            [self postMessage:@""];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
        [CYToast showErrorWithString:@"上传失败"];
    }];
    
}

/**
 上传影片
 */
-(void)uploadPostMovie:(NSURL *)Url{
    [CYToast showStatusWithString:@"正在加载"];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl
                     ,UploadMov];
    __weak __block  UIView *newView = UploadView;
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *imageData = [NSData dataWithContentsOfURL:Url];  ;
        NSString *fileName = [NSString stringWithFormat:@"ljq.mp4"];
        [formData appendPartWithFileData:imageData name:@"movie" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%@",uploadProgress);
        CGFloat width = uploadProgress.fractionCompleted * (SCREEN_WIDTH - 30);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%f",width);
            newView.frame = CGRectMake(newView.frame.origin.x, newView.frame.origin.y, width, 3);
        });
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CYToast dismiss];
        NSDictionary *dict = responseObject;
        if ([dict[@"status"] isEqualToString:@"success"]) {
            [self postMessage:dict[@"id"]];
        }else{
            // [CYToast showErrorWithString:@"上传失败"];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CYToast dismiss];
        [CYToast showErrorWithString:@"上传失败"];
    }];
}
-(void)postMessage:(NSString *)image{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *post_type = nil;
    NSString *content1 = Maintext.text;
    NSString *content = [NSString stringWithString:[content1 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableDictionary *parmDic = [[NSMutableDictionary alloc]init];
    if (_imageDataArray.count > 0) {
        [parmDic setObject:@"picture" forKey:@"post_type"];
        [parmDic setObject:_postNewImageArray forKey:@"pictures"];
        [parmDic setObject:content forKey:@"content"];
    }else if (_videoDataArray.count > 0){
        post_type = @"video";
        NSInteger mov = image.integerValue;
        NSString *movie = [NSString stringWithFormat:@"%ld",mov];
        
        [parmDic setObject:@"video" forKey:@"post_type"];
        [parmDic setObject:movie forKey:@"movie"];
        [parmDic setObject:content forKey:@"content"];
    }else{
        [parmDic setObject:@"text" forKey:@"post_type"];
        [parmDic setObject:content forKey:@"content"];
        
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    [parmDic setObject:[PHUserModel sharedPHUserModel].csrf_token forKey:@"csrf_token_name"];
    [parmDic setObject:[PHUserModel sharedPHUserModel].AccessToken forKey:@"token"];
    [parmDic setObject:self.member_id forKey:@"target"];
    
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        //[CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"发文成功"];
            if (_FilePath && _videoDataArray.count > 0) {
                [[NSFileManager defaultManager] removeItemAtPath:[_FilePath path] error:nil];
                [_videoDataArray removeAllObjects];
            }
            [_postNewImageArray removeAllObjects];
            [_imageDataArray removeAllObjects];
            [self LoadSelf];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }] ;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView == Maintext) {
        MainString = textView.text;
    }
    [CurText resignFirstResponder];
    [CurText endEditing:YES];
    [textView resignFirstResponder];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return YES;
}
#pragma mark - 点击取消发文
-(void)cancel{
    //[_dataArray removeAllObjects];
    [self.videoDataArray removeAllObjects];
    [self.imageDataArray removeAllObjects];
    [self.postVideoArray removeAllObjects];
    MainString = nil;
    [UIView animateWithDuration:0.3f animations:^{
        [MainTableView reloadData];
    }];
}
/**
 点击拍照
 */
#pragma mark - 拍照按钮点击事件
-(void)camera{
    [Maintext resignFirstResponder];

    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"拍照", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"相册", nil)];

    [actionItems addObject:item];
    [actionItems addObject:item1];

    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{

    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (selectedIndex == 0) {
            [self takePhoto];
        }else if (selectedIndex == 1){
            [self selectFromAblum];
        }
    }];

}

/**
 拍照
 */
#pragma mark - 拍照

-(void)takePhoto{
    if (self.imageDataArray.count >= 9 && selectIndex == 1) {
        [CYToast showErrorWithString:@"最多只能上传9张图片"];
        return;
    }
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.delegate = self;
    _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickerController.allowsEditing = YES;
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //录制视频时长，默认10s
    //_imagePickerController.videoMaximumDuration = 15;
    
    //相机类型（拍照、录像...）字符串需要做相应的类型转换
    _imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
    
    //视频上传质量
    //UIImagePickerControllerQualityTypeHigh高清
    //UIImagePickerControllerQualityTypeMedium中等质量
    //UIImagePickerControllerQualityTypeLow低质量
    //UIImagePickerControllerQualityType640x480
    _imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    
    //设置摄像头模式（拍照，录制视频）为录像模式
    _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
    
}

#pragma mark - 上传视频

/**
 获取视频的第一zhen
 */
- (UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}
/**
 收藏贴文
 */
-(void)collectPost:(UIButton *)button{
    
    [CYToast showStatusWithString:@"正在加载"];
    
    MainModel *model =_dataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CollectPost];
    
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([model.isCollect isEqualToString:@"Y"]) {
            model.isCollect = @"N";
        }else{
            model.isCollect = @"Y";
        }
        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:1];
        [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
/**
 分享按钮点击
 */
-(void)shareClick:(UIButton *)button{
    
    MainModel *model = _dataArray[button.tag - 200];
    NSMutableString *share = [[NSMutableString alloc]initWithString:NSLocalizedString(@"已有人分享了这则帖文", nil)];
    [share insertString:model.Share atIndex:1];
    DeclareAbnormalAlertView *alertView = [[DeclareAbnormalAlertView alloc]initWithTitle:@"分享帖文" message:share delegate:self leftButtonTitle:@"确定" rightButtonTitle:@"取消"];
    currentShareModel = model;
    alertView.model = model;
    alertView.delegate = self;
    [alertView show];
    
}
- (void)declareAbnormalAlertView:(DeclareAbnormalAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
}
-(void)didSendtext:(NSString *)text{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    NSDictionary *parmDic =@{@"target":[PHUserModel sharedPHUserModel].member_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_type":@"share",@"share_id":currentShareModel.post_id,@"content":[NSString stringWithFormat:@"%@",text]};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([dic[@"status"] isEqualToString:@"success"]) {
           // currentShareModel.content = text;
        }
        [self LoadSelf];
       // [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)pushNavagation:(CommentModel *)model{
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.m_showBackBt = YES;
    //controller.BannerImage = model.BannerImage;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 点击查看贴文的赞
 */
-(void)checkLike:(UIButton *)button{
    
    MainModel *model = _dataArray[button.tag - 200];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeUrl];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSString *data = dic[@"data"];
        if ([data isEqualToString:@"yes"]) {
            model.isLike = @"Y";
            NSInteger likecount = [model.Like integerValue] + 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }else{
            model.isLike = @"N";
            NSInteger likecount = [model.Like integerValue] - 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }
        //[MainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:button.tag - 200 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
/**
 点击查看评论
 */
-(void)comment:(UIButton *)button{

    MainModel *model = _dataArray[button.tag - 200];
    
    
    PostDetailViewController *controller = [[PostDetailViewController alloc]init];
    controller.m_showBackBt = YES;
    if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
        return;
    }
    controller.PostId = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 发布评论
 */
-(void)postComment:(UIButton *)button{
    MainModel *model = _dataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CommentUrl];
    if (CurText.text.length == 0 || [CurText.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"评论不能为空"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":CurText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"评论成功"];
            MainModel *main =_dataArray[button.tag - 200];
            main.isShowCommentV = !main.isShowCommentV;
            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:1];
            [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [CYToast showSuccessWithString:@"评论失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
-(void)checkMoreComment:(UIButton *)button{
    
    MainModel *model = _dataArray[button.tag - 200];
    PostDetailViewController *controller = [[PostDetailViewController alloc]init];
    controller.m_showBackBt = YES;
    if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
        return;
    }
   // controller.member_id = model.member_id;
    controller.PostId = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckShareInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_dataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.shareID;
    controller.Username = model.shareName;
    controller.ImageUrl = model.shareImage;
    controller.BannerImage = model.shareBanner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckpostInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_dataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.BannerImage = model.banner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)checkWhoInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_dataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.ForWhoMember_id;
    controller.Username = model.ForWhoName;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CurText = (SZTextView *)textView;
}
/**
 编辑贴文点击事件
 */
#pragma mark - 帖文编辑弹窗
-(void)EditPost:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"编辑帖文", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"删除帖文", nil)];
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    MainModel *model =_dataArray[button.tag - 200];
    if (model.shareID && model.shareID != nil ) {
        if ([model.shareID isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
            [actionItems addObject:item2];
        }
    }else if (model.member_id ){
        if ([model.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
            [actionItems addObject:item2];
        }
    }
    
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (actionItems.count > 2) {
            if (selectedIndex == 0) {
                [self editPost:button.tag - 200];
            }else if (selectedIndex == 2){
                [self deletePost:button.tag - 200];
            }else if (selectedIndex == 1){
                [self gotoReport];
            }
        }else if (actionItems.count == 1){
            [self gotoReport];
        }
        
    }];
}
-(void)gotoReport{
    ReportViewController *controller = [[ReportViewController alloc]init];
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)clickMore{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
        
    }];
}
-(void)reporeComment:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
    }];
}

#pragma mark - 帖文编辑
-(void)editPost:(NSInteger )index{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT / 2 -70, SCREEN_WIDTH - 20, 130)];
    view.layer.cornerRadius = 10;
    view.backgroundColor = [UIColor whiteColor];
    
    MainModel *model =_dataArray[index];
    editText = [[SZTextView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 100)];
    editText.text = model.content;
    editText.layer.borderWidth = 2.0f;
    editText.layer.borderColor = LYColor(223, 223, 223).CGColor;
    editText.layer.cornerRadius = 8;
    [view addSubview:editText];
    
    UIButton *PostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    PostButton.frame = CGRectMake(SCREEN_WIDTH- 65, 105, 40, 20);
    PostButton.backgroundColor = MainColor;
    [PostButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
    PostButton.titleLabel.font = FONT(13);
    PostButton.layer.cornerRadius = 8;
    [PostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [PostButton addTarget:self action:@selector(sendEditPost:)
         forControlEvents:UIControlEventTouchUpInside];
    PostButton.tag = index;
    [view addSubview:PostButton];
    
    [[QWAlertView sharedMask] show:view withType:QWAlertViewStyleActionSheetDown];
    
    
}
#pragma mark - 发布帖文编辑
-(void)sendEditPost:(UIButton *)button{
    [[QWAlertView sharedMask]dismiss];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,EDPost];
    NSInteger index = button.tag;
    MainModel *model =_dataArray[index];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":editText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        //        if (dic[@"status" isEqualToString:@"success"]) {
        //             model.content = editText.text;
        //        }
        if ([dic[@"status"] isEqualToString:@"success"]) {
            model.content = editText.text;
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
    
}
#pragma mark - 删除帖文
-(void)deletePost:(NSInteger )index{
    
    [PXAlertView showAlertWithTitle:NSLocalizedString(@"确认要删除?", nil) message:nil cancelTitle:NSLocalizedString(@"取消", nil) otherTitle:NSLocalizedString(@"确定", nil) completion:^(BOOL cancelled) {
        if (!cancelled) {
            [CYToast showStatusWithString:@"正在加载"];
            MainModel *model =_dataArray[index];
            NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,DeletePost];
            NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
            
            [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
                NSDictionary *dic = responseObject;
                [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
                if ([dic[@"status"] isEqualToString:@"success"]) {
                    [_dataArray removeObjectAtIndex:index];
                    [MainTableView reloadData];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [CYToast dismiss];
            }];
        }
    }];
}

#pragma mark - ZFTableViewCellDelegate

- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath {
    [self postPlayTheVideoAtIndexPath:indexPath scrollToTop:NO];
}


#pragma mark - zfplay

/// play the video
- (void)mediaPlayTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.mediaPlayer playTheIndexPath:indexPath scrollToTop:scrollToTop];
    MideaTypeModel *model = _imageArray[indexPath.item];
    [self.mediaControlView showTitle:@""
                 coverURLString:model.path
                 fullScreenMode:ZFFullScreenModeLandscape];
}

- (ZFPlayerControlView *)mediaControlView {
    if (!_mediaControlView) {
        _mediaControlView = [ZFPlayerControlView new];
    }
    return _mediaControlView;
}

- (void)postPlayTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.postPlayer playTheIndexPath:indexPath scrollToTop:scrollToTop];
    MainModel *model = _dataArray[indexPath.row];
    [self.postControlView showTitle:@""
                 coverURLString:model.videoPath
                 fullScreenMode:ZFFullScreenModeLandscape];
}

- (ZFPlayerControlView *)postControlView {
    if (!_postControlView) {
        _postControlView = [ZFPlayerControlView new];
    }
    return _postControlView;
}

#pragma mark - 转屏和状态栏

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.mediaPlayer.isFullScreen) {
        return UIStatusBarStyleLightContent;
    }
    return UIStatusBarStyleDefault;
}

- (BOOL)prefersStatusBarHidden {
    return self.mediaPlayer.isStatusBarHidden;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}
@end
