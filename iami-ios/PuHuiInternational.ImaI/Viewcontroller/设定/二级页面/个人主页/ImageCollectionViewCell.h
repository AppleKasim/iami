//
//  ImageCollectionViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MideaTypeModel.h"
@interface ImageCollectionViewCell : UICollectionViewCell
{
    CLAmplifyView *amplifyView;
}
@property (nonatomic,copy) FLAnimatedImageView *MainImage;
//@property (nonatomic,strong) MideaTypeModel *model;

@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, copy  ) void(^playBlock)(UIButton *sender);

@property (nonatomic,strong) NSString *picture_id;

-(void)setModel:(MideaTypeModel *)model;

@end
