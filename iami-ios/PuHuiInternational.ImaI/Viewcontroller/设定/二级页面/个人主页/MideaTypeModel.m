//
//  MideaTypeModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MideaTypeModel.h"

@implementation MideaTypeModel
+(MideaTypeModel *)parseDict:(NSDictionary *)dic{
    MideaTypeModel *model = [[MideaTypeModel alloc]init];
    model.path = [NetDataCommon stringFromDic:dic forKey:@"path"];
    model.fileType = [NetDataCommon stringFromDic:dic forKey:@"fileType"];
    model.picture_id = [NetDataCommon stringFromDic:dic forKey:@"picture_id"];
    return model;
}
@end
