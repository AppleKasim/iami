//
//  ImageCollectionViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/10.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ImageCollectionViewCell.h"
#import <ZFPlayer/UIImageView+ZFCache.h>
#define cellectionHeight (SCREEN_WIDTH - 40) /3

static BOOL SDImageCacheOldShouldDecompressImages = YES;
static BOOL SDImagedownloderOldShouldDecompressImages = YES;

@implementation ImageCollectionViewCell

- (void)clickImageView:(UITapGestureRecognizer *)tap
{
    amplifyView = [[CLAmplifyView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) andGesture:tap andSuperView:nil];
    amplifyView.userInteractionEnabled = YES;

    UIButton *buton =[UIButton buttonWithType:UIButtonTypeCustom];
    buton.frame = CGRectMake(SCREEN_WIDTH - 40, 10, 31, 31);
    [buton setImage:[UIImage imageNamed:@"删除图片"] forState:UIControlStateNormal];
    buton.imageView.contentMode = UIViewContentModeScaleAspectFit;
   // [buton setTitle:@"删除照片" forState:UIControlStateNormal];
    //buton.backgroundColor = LYColor(60, 60, 60);
    [buton setTitleColor:LYColor(237, 204, 105) forState:UIControlStateNormal];
    [buton addTarget:self action:@selector(deleteImage:) forControlEvents:UIControlEventTouchUpInside];
    [amplifyView addSubview:buton];
    [[UIApplication sharedApplication].keyWindow addSubview:amplifyView];
    
//    NSMutableArray *imageArry = [[NSMutableArray alloc]init];
//    for (MideaTypeModel *model in _imageArray) {
//        [imageArry addObject:model.path];
//    }
//    [XLPhotoBrowser showPhotoBrowserWithImages:imageArry currentImageIndex:indexPath.item];
}
-(void)deleteImage:(UIButton *)button{
    [PXAlertView showAlertWithTitle:@"是否删除图片" message:nil cancelTitle:@"取消" otherTitle:@"确定" completion:^(BOOL cancelled) {
        if (!cancelled) {
            [amplifyView removeFromSuperview];
            NSString *url = [NSString stringWithFormat:@"%@/picture/delPicture",TestUrl];
            NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"picture_id":self.picture_id};
            [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
                NSDictionary *dic = responseObject;
                if ([dic[@"status"] isEqualToString:@"success"]) {
                    [CYToast showStatusWithString:@"删除成功" hideAfterDelay:0.3f];
                }
                
                NSNotification *noti = [[NSNotification alloc]initWithName:@"deletePicture" object:nil userInfo:@{@"1":@"1"}];
                [[NSNotificationCenter defaultCenter]postNotification:noti];
                
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
            }];
            
        }
    }];
    
    
}
-(void)setModel:(MideaTypeModel *)model{
    if ([model.fileType isEqualToString:@"movie"]) {


        [self.contentView addSubview:self.videoImageView];
        [self.videoImageView addSubview:self.playButton];


        self.videoImageView.frame = CGRectMake(0, 0, cellectionHeight, cellectionHeight);
        self.playButton.frame = CGRectMake(0, 0, self.videoImageView.frame.size.width, self.videoImageView.frame.size.height);
        [self.videoImageView setImageWithURLString:nil placeholder:[UIImage imageNamed:@"default_movie"]];


//        _playerView = [[ZFPlayerView alloc] init];
//        [self.contentView addSubview:self.playerView];
//        _playerView.frame = CGRectMake(0, 0, cellectionHeight, cellectionHeight);
//
 //       UIView *BView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellectionHeight, cellectionHeight)];
//        // 初始化控制层view(可自定义)
//        ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
//        controlView.frame =CGRectMake(30, 70, SCREEN_WIDTH - 60, 150);
//        // 初始化播放模型
//        ZFPlayerModel *playerModel = [[ZFPlayerModel alloc]init];
//
//        playerModel.videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",model.path]];
//        playerModel.title = @"";
//        playerModel.fatherView = BView;
//        playerModel.placeholderImage = [UIImage imageNamed:@"default_movie"];
//
//        [self.playerView playerControlView:controlView playerModel:playerModel];
//
//        // 设置代理
//        self.playerView.delegate = self;
//
//        UIView *tapView = [[UIView alloc]init];
//        tapView.frame = CGRectMake(0, 0, 80, 80);
//        tapView.center = controlView.center;
//        [controlView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(play)]];
//        tapView.userInteractionEnabled = YES;
//
//        [BView addSubview:tapView];
//
//        // 自动播放
//        // [self.playerView autoPlayTheVideo];
//        [BView addSubview:self.videoImageView];
//        [self.contentView addSubview:BView];
    }else{
        
        _MainImage  = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake(0, 5, cellectionHeight, cellectionHeight)];
        _MainImage.clipsToBounds = YES;
        
        _MainImage.contentMode = UIViewContentModeScaleAspectFill;
        _MainImage.userInteractionEnabled = YES;
        
        
        NSString *url = [NSString stringWithFormat:@"%@%@",@"",model.path];
        [_MainImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图.png"]];
       // _MainImage sd_setImageWithURL:<#(nullable NSURL *)#> completed:<#^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL)completedBlock#>
        
//          [[SDWebImageDownloader sharedDownloader] setShouldDecompressImages:NO];
//        [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
        [self.contentView addSubview:_MainImage];
    }
    
}

- (UIImageView *)videoImageView {
    if (!_videoImageView) {
        _videoImageView = [[UIImageView alloc] init];
        _videoImageView.userInteractionEnabled = YES;
        _videoImageView.tag = 100;
        _videoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _videoImageView.clipsToBounds = YES;
    }
    return _videoImageView;
}


- (void)playButtonClick:(UIButton *)sender {
    if (self.playBlock) {
        self.playBlock(sender);
    }
}

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}


@end
