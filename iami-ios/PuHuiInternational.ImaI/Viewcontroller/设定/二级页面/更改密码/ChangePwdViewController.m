//
//  ChangePwdViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ChangePwdViewController.h"

@interface ChangePwdViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *MainTableView;
    UIButton *confirmBut;
}
#define Headerheight 300
#define cornerR 6

#define ChangePswd @"/api/doEditPassword"
@end

@implementation ChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =LYColor(223, 223, 223);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self creatTable];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
#pragma =======TableView delegeta && datasource ===================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, 300) style:UITableViewStyleGrouped];
    MainTableView.layer.cornerRadius = 10;
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = NO;
    MainTableView.bounces = NO;
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return Headerheight;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 20, Headerheight)];
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH -20, 50)];
    backView.layer.cornerRadius = 10;
    backView.clipsToBounds = YES;
    backView.backgroundColor =MainColor;
    
    [view addSubview:backView];
    //view.backgroundColor = [UIColor whiteColor];
    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 12, SCREEN_WIDTH - 10, 25)];
    TitleLabel.text = NSLocalizedString(@"更改密码", nil);
    TitleLabel.textColor = [UIColor whiteColor];
    TitleLabel.font = FONT(20);
    TitleLabel.textAlignment = NSTextAlignmentCenter;
     // [TitleLabel sizeToFit];
    [backView addSubview:TitleLabel];
    
    UILabel *psdLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(backView.frame) + 15, 40, 20)];
    psdLabel.text = NSLocalizedString(@"输入旧密码", nil);
   // psdLabel.numberOfLines = 0;
    psdLabel.font = FONT(14);
    [psdLabel sizeToFit];
    [view addSubview:psdLabel];
    
   //SZTextView  *UserTld = [[SZTextView alloc]initWithFrame:CGRectMake(83, CGRectGetMaxY(backView.frame) + 10, SCREEN_WIDTH - 120, 30)];
    UITextField *UserTld = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(psdLabel.frame) + 5, CGRectGetMaxY(backView.frame) + 10, SCREEN_WIDTH - CGRectGetMaxX(psdLabel.frame) - 30, 30)];
    UserTld.font = FONT(20);
    UIView *emptyView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    UserTld.leftView=emptyView;
    UserTld.leftViewMode=UITextFieldViewModeAlways;
    UserTld.layer.borderWidth = 1.0f;
    UserTld.secureTextEntry = YES;
    UserTld.tag = 100;
    UserTld.delegate = self;
    UserTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    UserTld.layer.cornerRadius = cornerR;
    [view addSubview:UserTld];
    
    UILabel *newpsdLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(UserTld.frame) + 25, 70, 20)];
    newpsdLabel.text = NSLocalizedString(@"输入新密码", nil);
    newpsdLabel.font = FONT(14);
    [newpsdLabel sizeToFit];
    [view addSubview:newpsdLabel];
    
    UITextField  *newUserTld = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(newpsdLabel.frame) + 5, CGRectGetMaxY(UserTld.frame) + 20, SCREEN_WIDTH - CGRectGetMaxX(newpsdLabel.frame) - 30, 30)];
    newUserTld.font = FONT(20);
    UIView *emptyView1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    newUserTld.leftView=emptyView1;
    newUserTld.leftViewMode=UITextFieldViewModeAlways;
    //newUserTld.placeholder = @"请填写6-12位新密码";
    newUserTld.secureTextEntry = YES;
    newUserTld.layer.borderWidth = 1.0f;
    newUserTld.tag = 200;
    newUserTld.delegate = self;
    newUserTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    newUserTld.layer.cornerRadius = cornerR;
    [view addSubview:newUserTld];
    
    UILabel *warmLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(newpsdLabel.frame) + 5, CGRectGetMaxY(newUserTld.frame) + 1, 280, 15)];
    warmLabel.text = NSLocalizedString(@"请填写6-12位数密码，需含英文大小写，数字", nil);
    warmLabel.font =FONT(10);
    [warmLabel sizeToFit];
    warmLabel.textColor = LYColor(218, 218, 218);
    [view addSubview:warmLabel];
    
    UILabel *repsdLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, CGRectGetMaxY(newUserTld.frame) + 25, 100, 20)];
    repsdLabel.text = NSLocalizedString(@"重新输入新密码", nil);
    repsdLabel.font = FONT(14);
    [repsdLabel sizeToFit];
    [view addSubview:repsdLabel];
    
    UITextField *reUserTld = [[UITextField alloc]initWithFrame:CGRectMake(CGRectGetMaxX(repsdLabel.frame) + 5, CGRectGetMaxY(newUserTld.frame) + 20, SCREEN_WIDTH - CGRectGetMaxX(repsdLabel.frame) - 30, 30)];
    reUserTld.font = FONT(20);
    UIView *emptyView2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 30)];
    reUserTld.leftView=emptyView2;
    reUserTld.leftViewMode=UITextFieldViewModeAlways;
    reUserTld.secureTextEntry = YES;
    reUserTld.layer.borderWidth = 1.0f;
    reUserTld.tag = 300;
    reUserTld.delegate = self;
    reUserTld.layer.borderColor = LYColor(223, 223, 223).CGColor;
    reUserTld.layer.cornerRadius = cornerR;
    [view addSubview:reUserTld];
   
    UILabel *warmLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(repsdLabel.frame) + 5, CGRectGetMaxY(reUserTld.frame) + 1, 200, 15)];
    warmLabel1.text =NSLocalizedString(@"请再次确认您的密码", nil);
    warmLabel1.font =FONT(10);
    warmLabel1.textColor = LYColor(218, 218, 218);
    [view addSubview:warmLabel1];
    
    confirmBut = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBut.frame = CGRectMake(SCREEN_WIDTH / 2 -100, CGRectGetMaxY(reUserTld.frame) + 35, 200, 35);
    confirmBut.layer.cornerRadius = 6;
    [confirmBut setTitle:NSLocalizedString(@"确认", nil) forState:UIControlStateNormal];
    confirmBut.backgroundColor = MainColor;
    [confirmBut addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [confirmBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirmBut.titleLabel.font = FONT(17);
    [view addSubview:confirmBut];
    
    return view;
}
-(void)confirm{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,ChangePswd];
    UITextField *old_password = [self.view viewWithTag:100];
    UITextField *new_password = [self.view viewWithTag:200];
    UITextField *confirm_new_password = [self.view viewWithTag:300];
    
    NSString *old = old_password.text;
    NSString *new = new_password.text;
    NSString *confirm = confirm_new_password.text;
    
    if ([old_password.text isEqualToString:@""] || [new_password.text isEqualToString:@""] || [confirm_new_password.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"输入有误"];
        return;
    }
    if ( ![new_password.text isEqualToString:confirm_new_password.text]) {
        [CYToast showErrorWithString:@"请输入一致的新密码"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"old_password":old,@"new_password":new,@"confirm_new_password":confirm,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            //[CYToast showSuccessWithString:@"修改密码成功!"];
            [CYToast showStatusWithString:@"修改密码成功!" hideAfterDelay:0.5f];
            [PHUserModel sharedPHUserModel].isLogin = NO;
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
            [appdelegate initLoginVC];
        }else{
            [CYToast showErrorWithString:[NSString stringWithFormat:@"%@",dic[@"message"]]];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

@end
