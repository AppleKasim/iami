//
//  HotVideosTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/25.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "HotVideosTableViewCell.h"

@implementation HotVideosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setMode:(MainModel *)model{
   //CGFloat  LeadingSpace = 10;
    self.backgroundColor = LYColor(223, 223, 223);
   
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(3, 4, SCREEN_WIDTH -6, model.cellHeight - 4)];
    backView.backgroundColor = [UIColor whiteColor];
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.userInteractionEnabled = YES;



    self.videoImageView.frame = CGRectMake(15, 0, backView.frame.size.width - 30, 150);
    self.playButton.frame = CGRectMake(0, 0, self.videoImageView.frame.size.width, self.videoImageView.frame.size.height);
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",model.videoPath]];
    [self.videoImageView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default_movie"]];

    [backView addSubview:self.videoImageView];
    [self.videoImageView addSubview:self.playButton];


//    _playerView = [[ZFPlayerView alloc] init];
//    [self.contentView addSubview:self.playerView];
//    _playerView.frame = CGRectMake(15, 0, backView.frame.size.width - 30, 150);
//    _playerView.userInteractionEnabled = YES;
//    UIView *BView = [[UIView alloc]initWithFrame:CGRectMake(15, 3, backView.frame.size.width - 30, 160)];
//    BView.userInteractionEnabled = YES;
//    // 初始化控制层view(可自定义)
//    ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
//    controlView.frame =CGRectMake(15, 0, backView.frame.size.width - 30, 150);
//    controlView.userInteractionEnabled = YES;
//    // 初始化播放模型
//    ZFPlayerModel *playerModel = [[ZFPlayerModel alloc]init];
//    _playerView.stopPlayWhileCellNotVisable = YES;
//
//
//    playerModel.videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",model.videoPath]];
//    playerModel.title = @"";
//    playerModel.fatherView = BView;
//    playerModel.placeholderImage = [UIImage imageNamed:@"default_movie"];
//    [self.playerView playerControlView:controlView playerModel:playerModel];
//    // 设置代理
//    self.playerView.delegate = self;
//    // 自动播放
//    // [self.playerView autoPlayTheVideo];

//    _tapView = [[UIView alloc]init];
//    _tapView.frame = CGRectMake(0, 0, 80, 80);
//    _tapView.center = BView.center;
//    _tapView.userInteractionEnabled = YES;
//    
//    [controlView addSubview:_tapView];
//    
//    [backView addSubview:BView];
//
    [self.contentView addSubview:backView];
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.height;
}
-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}
//-(void)playyy{
//    if (self.playerView.state == ZFPlayerStatePlaying ) {
//        return;
//    }
//    [self.playerView autoPlayTheVideo];
//    //self.playerView playerModel:<#(ZFPlayerModel *)#>
//    //self.playerView
//
//}
-(UIView *)backCommentView:(CommentModel *)model{
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(3, 6, SCREEN_WIDTH - 6, model.cellHeight + 41)];
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.backgroundColor = [UIColor whiteColor];
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(3, 0, SCREEN_WIDTH - 6, 2)];
    gapView.backgroundColor = LYColor(223, 223, 223);
    [backView addSubview:gapView];
    
    UIImageView  *UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 30, 30)];
    [UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    UserImage.layer.cornerRadius = 15;
    UserImage.clipsToBounds = YES;
    UserImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:UserImage];
    
    CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH - 10 * 2 font:FONT(14)];
    UILabel *UserName = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, namelHeght, 20)];
    UserName.text = model.UserName;
    UserName.textColor = LYColor(237, 204, 105);
    UserName.font = FONT(14);
    UserName.textAlignment =NSTextAlignmentLeft;
    [backView addSubview:UserName];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 8, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:memberImage];
    
    UILabel *LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) + 2, 10, 29, 13)];
    LevelLabel.backgroundColor =LYColor(237, 204, 105);
    LevelLabel.text = @"lv.1";
    LevelLabel.textAlignment = NSTextAlignmentCenter;
    LevelLabel.layer.cornerRadius = 10;
    LevelLabel.layer.shouldRasterize = YES;
    LevelLabel.clipsToBounds = YES;
    LevelLabel.font = FONT(11);
    LevelLabel.textColor = [UIColor whiteColor];
    [backView addSubview:LevelLabel];
    
    
    UIButton *LikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    LikeButton.frame = CGRectMake(SCREEN_WIDTH - 70, 5, 50, 40);
    if ([model.isLike isEqualToString:@"N"]) {
        LikeButton.selected = NO;
    }else{
        LikeButton.selected = YES;
    }
    [LikeButton setImage:[UIImage imageNamed:@"喜欢未选中"] forState:UIControlStateNormal];
    [LikeButton setImage:[UIImage imageNamed:@"喜欢"] forState:UIControlStateSelected];
    LikeButton.clipsToBounds = YES;
    [backView addSubview:LikeButton];
    
    CGFloat labelWidth = [self getLabelWidthWithText:model.create_time width:SCREEN_WIDTH - 20 font:FONT(15)];
    
    UILabel *createTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(55,CGRectGetMaxY(UserName.frame) + 2, labelWidth, 10)];
    createTimeLabel.text = model.create_time;
    createTimeLabel.font = FONT(12);
    createTimeLabel.textColor =LYColor(218, 218, 218);
    [backView addSubview:createTimeLabel];
    
    
    UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, CGRectGetMaxY(createTimeLabel.frame) , SCREEN_WIDTH - 80, 20)];
    contentLabel.text = model.content;
    contentLabel.font = Bold_FONT(13);
    contentLabel.numberOfLines = 0;
    [backView addSubview:contentLabel];
    
    return backView;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDelegate:(id<HotVideosCellDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath {
    self.delegate = delegate;
    self.indexPath = indexPath;
}

- (void)playButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(zf_playTheVideoAtIndexPath:)]) {
        [self.delegate zf_playTheVideoAtIndexPath:self.indexPath];
    }
}

#pragma mark - getter
- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_playButton addTarget:self action:@selector(playButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}

- (UIImageView *)videoImageView {
    if (!_videoImageView) {
        _videoImageView = [[UIImageView alloc] init];
        _videoImageView.userInteractionEnabled = YES;
        _videoImageView.tag = 100;
        _videoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _videoImageView.clipsToBounds = YES;
    }
    return _videoImageView;
}

@end
