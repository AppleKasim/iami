//
//  HotVideosTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/25.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"

@protocol HotVideosCellDelegate <NSObject>
- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath;
@end


@interface HotVideosTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) UILabel *MainLabel;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UILabel *TimeLabel;


@property (nonatomic,strong) UIButton *LikeButton;
@property (nonatomic,strong) UIButton *CommentButton;
@property (nonatomic,strong) UIButton *ForwardButton;
@property (nonatomic,strong) UIButton *CollectButton;

@property (nonatomic,strong) UIView *CommentView;
@property (nonatomic,strong) UILabel *Commentlabel;
@property (nonatomic,assign) CGFloat CellHeightt;
@property (nonatomic,strong) SZTextView *CommentTld;
@property (nonatomic,strong) UIButton *CommentBut;
@property (nonatomic,strong) UIButton *MoreCommentBut;

@property (nonatomic,strong) UIView *tapView;


@property (nonatomic,weak) id<HotVideosCellDelegate> delegate;
@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic, strong) NSIndexPath *indexPath;


-(void)setMode:(MainModel *)model;
- (void)setDelegate:(id<HotVideosCellDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath;

@end
