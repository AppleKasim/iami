//
//  MyfriendViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/19.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MyfriendViewController.h"
#import "UserFriTableViewCell.h"
#import "MainUserViewController.h"

@interface MyfriendViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSInteger selectIndex;
    
    CALToolBarView *toolBarTwo;
    UITableView *MainTableView;
}

#define getFanse @"/trace/getMemberByTrace"//取得粉丝清单
#define getTrace @"/trace/getTraceByMember"//取得追蹤(關注)清單

@property (nonatomic,copy) NSMutableArray *TraceArray;
@property (nonatomic,copy) NSMutableArray *FanseArray;
@end

@implementation MyfriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectIndex = 0;
    _TraceArray = [[NSMutableArray alloc]init];
    _FanseArray = [[NSMutableArray alloc]init];
    [self loadFans];
    [self creatTable];
    // Do any additional setup after loading the view.
}
-(UIView *)creatUI{
    NSArray *titleArray = @[NSLocalizedString(@"粉丝", nil), NSLocalizedString(@"追踪", nil)];
    toolBarTwo.SelectIndex = selectIndex;
    toolBarTwo = [[CALToolBarView alloc] initWithFrame:CGRectMake(10, 0, 100, 50)
                                            titleArray:titleArray
                                         selectedColor:MainColor
                                         deselectColor:[NSString colorWithHexString:@"212121"]
                                       bottomlineColor:[NSString colorWithHexString:@"efefef"]
                                     selectedLineColor:nil selectIndex:selectIndex];
    toolBarTwo.isNeedLine = NO;
    __weak MyfriendViewController *weakSelf =  self;
    [toolBarTwo setCalToolBarSelectedBlock:^(NSInteger index) {
        selectIndex = index;
        [weakSelf loadType];
    }];
    
    return toolBarTwo;
}
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 5, SCREEN_WIDTH - 10, SCREEN_HEIGHT -  Height_NavBar - Height_TabBar ) style:UITableViewStylePlain];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.tableHeaderView = [self creatUI];
    [MainTableView registerClass:[UserFriTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:MainTableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (selectIndex == 0) {
        return 1;
    }else{
        return 1;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (selectIndex == 0) {
        return _FanseArray.count;
    }else{
        return _TraceArray.count;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserFriTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    InviteModel *model = nil;
    if (selectIndex == 0) {
        model = _FanseArray[indexPath.section];
    }else{
         model = _TraceArray[indexPath.section];
    }
    [cell setModel:model];
    
    cell.FreInvitButton.tag = 500 + indexPath.row;
    cell.FollowButton.tag = 500 + indexPath.row;
    cell.UserImage.tag = 500 + indexPath.row;
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
//    [cell.UserImage addGestureRecognizer:tap];
//
//    [cell.FreInvitButton addTarget:self action:@selector(InviteFriend:) forControlEvents:UIControlEventTouchUpInside];
//    [cell.FollowButton addTarget:self action:@selector(FollowFriend:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}
-(CGFloat )tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 80;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    InviteModel *model = nil;
    if (selectIndex == 0) {
        model = _FanseArray[indexPath.section];
    }else{
         model = _TraceArray[indexPath.section];
    }
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.invitee_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark 网络请求
-(void)loadType{
    if (selectIndex == 0) {
        [self loadFans];
    }else{
        [self loadFriend];
    }
}
/**
 获得自己好友清單
 */
-(void)loadFriend{
    [_TraceArray removeAllObjects];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getTrace];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_TraceArray addObject:[InviteModel pareWithDictionary:parmDic]];
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}

/**
 获得自己粉丝清单
 */
-(void)loadFans{
    [_FanseArray removeAllObjects];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getFanse];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_FanseArray addObject:[InviteModel pareWithDictionary:parmDic]];
            }
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
