//
//  MyHomeViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/7/12.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MyHomeViewController.h"
#import "ImageCollectionViewCell.h"
@interface MyHomeViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UISegmentedControl *seg;
    
    UITableView *MainTableView;
    
    UICollectionView *mainCollectionView;
}
#define cellectionHeight (SCREEN_WIDTH - 40) /3

@end

@implementation MyHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:[self creatSeg]];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
-(UIView *)creatSeg{
    seg = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedString(@"我的主场", nil),NSLocalizedString(@"掠夺", nil),NSLocalizedString(@"我的仓库", nil)]];
    seg.frame = CGRectMake(10, 5, SCREEN_WIDTH -20, 30);
    seg.backgroundColor = [UIColor whiteColor];
   // seg.selectedSegmentIndex = seleIndex;
    seg.tintColor = MainColor;
    [seg addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    return seg;
}
-(void)segmentValueChanged:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex ==  2) {
        [MainTableView removeFromSuperview];
        [self creatCollectView];
    }else{
        [mainCollectionView removeFromSuperview];
        [self creatTable];
    }
//    if (seg.selectedSegmentIndex == 1) {
//        [self loadFriend];
//    }else if (seg.selectedSegmentIndex == 0 ){
//        [self loadData];
//    }
}
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 35, SCREEN_WIDTH - 20, SCREEN_HEIGHT -  Height_NavBar-Height_TabBar - 38 ) style:UITableViewStylePlain];
    // MainTableView.backgroundColor =LYColor(223, 223, 223);
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    
//    [MainTableView registerClass:[InfoTableViewCell class] forCellReuseIdentifier:@"cell"];
//    [MainTableView registerClass:[MessageFriendTableViewCell class] forCellReuseIdentifier:@"friend"];
//    [MainTableView registerClass:[MessageListTableViewCell class] forCellReuseIdentifier:@"chatroom"];
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.backgroundColor = [UIColor redColor];
    return cell;
    
}
-(void)creatCollectView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置collectionView滚动方向
    //  [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    //设置headerView的尺寸大小
    
    //该方法也可以设置itemSize
    layout.itemSize =CGSizeMake(cellectionHeight, cellectionHeight);
    //  [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    
    //CGFloat height = (_imageArray.count / 3+ 1) * (cellectionHeight + 10) ;
    //2.初始化collectionView
    //        mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, height +10 ) collectionViewLayout:layout];
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 35, SCREEN_WIDTH, SCREEN_HEIGHT - Height_NavBar - Height_TabBar  ) collectionViewLayout:layout];
    mainCollectionView.backgroundColor = [UIColor clearColor];
    
   
    
    
    //3.注册collectionViewCell
    //注意，此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致 均为 cellId
    [mainCollectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"collectioncell"];
    
    
    //注册headerView  此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致  均为reusableView
    [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
    
    //4.设置代理
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    mainCollectionView.bounces = NO;
    mainCollectionView.pagingEnabled = NO;
    mainCollectionView.scrollEnabled = YES;
    mainCollectionView.backgroundColor = [UIColor whiteColor];
    
     [self.view addSubview:mainCollectionView];
    //return mainCollectionView;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 20;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectioncell" forIndexPath:indexPath];

    MideaTypeModel *model = [[MideaTypeModel alloc]init];
    model.fileType = @"1";
    [cell setModel:model];
   // [cell setModel:_imageArray[indexPath.item]];
   // MideaTypeModel *model = _imageArray[indexPath.item];
    [cell.MainImage addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImageView:)]];
    cell.picture_id = model.picture_id;
    cell.MainImage.tag = 2000 + indexPath.item;
    
    return cell;
}
//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellectionHeight, cellectionHeight);
}
//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
