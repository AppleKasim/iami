//
//  ChangeHeadViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ChangeHeadViewController.h"
#import "PhotoViewController.h"
#import "ImageCollectionViewCell.h"


@interface ChangeHeadViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,PhotoViewControllerDelegate,PST_MenuViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UITableView *MainTableView;
    UIImageView *MianImage;//用户头像
    UIImageView *BackImage;
    
    NSString *type;
    UIImagePickerControllerSourceType _sourceType;
    
    
    UICollectionView *mainCollectionView;//图片的collectionview


}
@property (nonatomic,copy) NSMutableArray *imageArray;//取得所有的图片
#define Headerheight 230
#define cellectionHeight (SCREEN_WIDTH - 40) /3

#define avaterUrl @"/picture/uploadUserPicture"//上传头像
#define backUrl @"/picture/uploadBannerPicture"//上传背景图
#define getPic @"/picture/getPictures"//取得所有自己的媒体库资料
@end

@implementation ChangeHeadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor =LYColor(223, 223, 223);
    _imageArray = [[NSMutableArray alloc]init];
    [self creatTable];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarController.tabBar.hidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    //self.tabBarController.tabBar.hidden = YES;

    self.navigationController.navigationBar.hidden = YES;
}
#pragma  mark - TableView delegeta && datasource
-(void)creatTable{
  
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 50, SCREEN_WIDTH - 20, SCREEN_HEIGHT - Height_NavBar - 55 - Height_TabBar) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = LYColor(223, 223, 223)
    ;
    MainTableView.clipsToBounds = YES;
    MainTableView.layer.cornerRadius = 10;
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.bounces = NO;
    MainTableView.showsVerticalScrollIndicator = NO;
    MainTableView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:MainTableView];
    
     [self.view addSubview:[self creatHeader]];
}
-(UIView *)creatHeader{
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(10, 5, SCREEN_WIDTH - 20, 50)];
    backView.layer.cornerRadius = 10;
    backView.backgroundColor =MainColor;
    
    UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, SCREEN_WIDTH - 10, 20)];
    TitleLabel.text = NSLocalizedString(@"更改头像／封面照", nil);
    TitleLabel.textColor = [UIColor whiteColor];
    TitleLabel.font = FONT(20);
    TitleLabel.textAlignment = NSTextAlignmentCenter;
    [backView addSubview:TitleLabel];
    
    return backView;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0) {
        return 10;
    }else{
        return 0.1;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , 10)];
        view.backgroundColor = LYColor(223, 223, 223);
        return view;
    }else{
        UIView *view =[[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , 0.1f)];
        //view.backgroundColor = LYColor(223, 223, 223);
        return view;
    }
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section== 0) {
        return Headerheight;
    }else if (section == 1){
        return Headerheight+ 30 ;
    }
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
  if (section == 0) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH  - 20, Headerheight)];
      view.backgroundColor = [UIColor whiteColor];
      view.layer.cornerRadius = 10;
      view.clipsToBounds = YES;
        MianImage = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH  - 20)/ 2 - 60, 20, 120, 120)];
        NSString *url = [NSString stringWithFormat:@"%@%@",@"",[PHUserModel sharedPHUserModel].UserImage];
        MianImage.layer.cornerRadius = 60;
        MianImage.clipsToBounds = YES;
        MianImage.contentMode = UIViewContentModeScaleAspectFit;
        [MianImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
        [view addSubview:MianImage];
        
        
        UIButton *MessageBut = [UIButton buttonWithType:UIButtonTypeCustom];
        MessageBut.frame = CGRectMake((SCREEN_WIDTH  - 20) / 2 -130, CGRectGetMaxY(MianImage.frame) + 20, 120, 30);
        MessageBut.layer.borderWidth=1.0f;
        MessageBut.layer.borderColor =MainColor.CGColor;
        [MessageBut setTitle:NSLocalizedString(@"选择大头照", nil) forState:UIControlStateNormal];
        MessageBut.titleLabel.font = FONT(13);
        MessageBut.tag = 100;
        [MessageBut addTarget:self action:@selector(changgAvater:) forControlEvents:UIControlEventTouchUpInside];
        [MessageBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        MessageBut.layer.cornerRadius = 6;
        MessageBut.clipsToBounds = YES;
        MessageBut.backgroundColor = MainColor;
        
        UIButton *cancelBut = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBut.frame = CGRectMake(SCREEN_WIDTH / 2 + 10, CGRectGetMaxY(MianImage.frame) + 20, 120, 30);
        cancelBut.layer.borderWidth=1.0f;
        cancelBut.layer.borderColor =LYColor(218, 218, 218).CGColor;
        [cancelBut setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        cancelBut.titleLabel.font = FONT(13);
        cancelBut.layer.cornerRadius = 6;
        cancelBut.clipsToBounds = YES;
        cancelBut.tag = 101;
        [cancelBut addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cancelBut.backgroundColor = LYColor(218, 218, 218);
        
        [view addSubview:MessageBut];
        [view addSubview:cancelBut];
        
        return view;
    }
    else if (section == 1){
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH , Headerheight + 30)];
        view.clipsToBounds = YES;
        view.layer.cornerRadius = 10;
        view.backgroundColor = [UIColor whiteColor];

        BackImage = [[UIImageView alloc]initWithFrame:CGRectMake((SCREEN_WIDTH  - 20)/ 2 - 160, 5, 320, 200)];
        NSString *url = [NSString stringWithFormat:@"%@%@",@"",[PHUserModel sharedPHUserModel].banner];
        //BackImage.layer.cornerRadius = 60;
        //BackImage.image = [UIImage imageNamed:@"cover"];
        BackImage.clipsToBounds = YES;
        BackImage.contentMode = UIViewContentModeScaleAspectFill;
        [BackImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
        [view addSubview:BackImage];
        
        
        UIButton *MessageBut = [UIButton buttonWithType:UIButtonTypeCustom];
        MessageBut.frame = CGRectMake((SCREEN_WIDTH  - 20) / 2 -130, CGRectGetMaxY(BackImage.frame) + 20, 120, 30);
        MessageBut.layer.borderWidth=1.0f;
        MessageBut.tag = 200;
        MessageBut.layer.borderColor =MainColor.CGColor;
        [MessageBut setTitle:NSLocalizedString(@"选择封面图", nil) forState:UIControlStateNormal];
        MessageBut.titleLabel.font = FONT(13);
        MessageBut.layer.cornerRadius = 6;
        MessageBut.clipsToBounds = YES;
        [MessageBut addTarget:self action:@selector(changgAvater:) forControlEvents:UIControlEventTouchUpInside];
        [MessageBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        MessageBut.backgroundColor = MainColor;
        
        UIButton *cancelBut = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBut.frame = CGRectMake(SCREEN_WIDTH / 2 + 10, CGRectGetMaxY(BackImage.frame) + 20, 120, 30);
        cancelBut.layer.borderWidth=1.0f;
        cancelBut.layer.borderColor =LYColor(218, 218, 218).CGColor;
        [cancelBut setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        cancelBut.titleLabel.font = FONT(13);
        cancelBut.layer.cornerRadius = 6;
        cancelBut.clipsToBounds = YES;
        cancelBut.tag = 201;
        [cancelBut addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        [cancelBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cancelBut.backgroundColor = LYColor(218, 218, 218);
        
        [view addSubview:MessageBut];
        [view addSubview:cancelBut];
        
        return view;
    }
    
    
    return nil;
}

#pragma mark  - collectionView代理方法
-(UIView *)creatImageUI{
    //1.初始化layout
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    //设置collectionView滚动方向
    //    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    //设置headerView的尺寸大小
    layout.headerReferenceSize = CGSizeMake(self.view.frame.size.width, 0);
    //该方法也可以设置itemSize
    layout.itemSize =CGSizeMake(cellectionHeight, cellectionHeight);
    
    //2.初始化collectionView
    mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(40, Height_NavBar + 50, SCREEN_WIDTH - 80, 400 ) collectionViewLayout:layout];
    //[self.view addSubview:mainCollectionView];
    mainCollectionView.backgroundColor = [UIColor clearColor];
    
    //3.注册collectionViewCell
    //注意，此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致 均为 cellId
    [mainCollectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    //注册headerView  此处的ReuseIdentifier 必须和 cellForItemAtIndexPath 方法中 一致  均为reusableView
    [mainCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableView"];
    
    //4.设置代理
    mainCollectionView.delegate = self;
    mainCollectionView.dataSource = self;
    mainCollectionView.backgroundColor = [UIColor whiteColor];
    
    
    return mainCollectionView;
}
//返回section个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

//每个section的item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _imageArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ImageCollectionViewCell *cell = (ImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    //cell.model =_imageArray[indexPath.item];
    [cell setModel:_imageArray[indexPath.item]];
    cell.MainImage.userInteractionEnabled = NO;
    return cell;
}
//设置每个item的尺寸
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(cellectionHeight, cellectionHeight);
}
//设置每个item的UIEdgeInsets
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

//设置每个item水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}

//设置每个item垂直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 10;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[QWAlertView sharedMask] dismiss];
    
    MideaTypeModel *model = _imageArray[indexPath.item];
     NSString *url = [NSString stringWithFormat:@"%@",model.path];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage *image = [UIImage imageWithData:data];
    
    PhotoViewController *photoVC = [[PhotoViewController alloc] init];
    photoVC.oldImage = image;
    //    photoVC.btnBackgroundColor = COLOR_NAV;
    //    photoVC.backImage = ;自定义返回按钮图片
    if ([type isEqualToString:@"0"]) {
        photoVC.mode = PhotoMaskViewModeCircle;
        photoVC.cropWidth = CGRectGetWidth(self.view.bounds) - 80;
    }else{
        photoVC.mode = PhotoMaskViewModeSquare;
        photoVC.cropWidth = 320;
        photoVC.cropHeight = 200;
    }
    photoVC.isDark = YES;
    photoVC.delegate = self;
    [self presentViewController:photoVC animated:YES completion:^{

    }];
    //[self.navigationController pushViewController:photoVC animated:YES];
    
}
#pragma mark - 相册
-(void)changgAvater:(UIButton *)button{
    if ([button.titleLabel.text isEqualToString:NSLocalizedString(@"确定", nil)]) {
       
        if ([type isEqualToString:@"0"]) {
         [self upLoadAvater: MianImage.image];
            
        }else{
           
         [self upLoadAvater:BackImage.image];
            
        }
        return;
    }
    
    
    if (button.tag == 100) {
        type = @"0";
        UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
        CGRect rect=[button convertRect: button.bounds toView:window];
        PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[NSLocalizedString(@"本机相册", nil),NSLocalizedString(@"已上传影片", nil)] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
        menuView.tag = 1100;
        menuView.titleColor = LYColor(237, 204, 105);
        menuView.lineColor = LYColor(237, 204, 105);
    }else{
        type = @"1";
        UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
        CGRect rect=[button convertRect: button.bounds toView:window];
        PST_MenuView *menuView = [[PST_MenuView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y - 100, 120, 82) titleArr:@[NSLocalizedString(@"本机相册", nil),NSLocalizedString(@"已上传影片", nil)] imgArr:@[] arrowOffset:104 rowHeight:40 layoutType:PST_MenuViewLayoutTypeTitle directionType:PST_MenuViewDirectionDown delegate:self];
        menuView.tag = 1100;
        menuView.titleColor = LYColor(237, 204, 105);
        menuView.lineColor = LYColor(237, 204, 105);
    }
    
   
}
-(void)didSelectRowAtIndex:(NSInteger)index title:(NSString *)title img:(NSString *)img{
    if ([title isEqualToString:NSLocalizedString(@"本机相册", nil)]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        //            imagePicker.navigationBar.backgroundColor = [UIColor blackColor];
        imagePicker.delegate = self;
        //            imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        _sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        //            [self presentModalViewController:imagePicker animated:YES];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else if ([title isEqualToString:NSLocalizedString(@"已上传影片", nil)]){
        [self loadPicture];
    }
}

/**
 获得自己的所有图片
 */
-(void)loadPicture{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,getPic];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id,@"type":@"lightbox"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_imageArray addObject:[MideaTypeModel parseDict:parmDic]];
            }
             [[QWAlertView sharedMask]show:[self creatImageUI] withType:QWAlertViewStyleActionSheetDown];
        }
        
       // [MainTableView reloadData];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        
        NSLog(@"%@",error);
    }];
}
#pragma mark -  UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    PhotoViewController *photoVC = [[PhotoViewController alloc] init];
    photoVC.oldImage = image;
    if ([type isEqualToString:@"0"]) {
         photoVC.mode = PhotoMaskViewModeCircle;
        photoVC.cropWidth = CGRectGetWidth(self.view.bounds) - 80;
    }else{
         photoVC.mode = PhotoMaskViewModeSquare;
        photoVC.cropWidth = 320;
        photoVC.cropHeight = 200;
    }
   
    photoVC.isDark = YES;
    photoVC.delegate = self;
    [picker pushViewController:photoVC animated:YES];
    
}

-(void)cancel{
    if ([type isEqualToString:@"0"]) {
         [MainTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else{
         [MainTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
#pragma mark - photoViewControllerDelegate
- (void)imageCropper:(PhotoViewController *)cropperViewController didFinished:(UIImage *)editedImage
{
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        CATransition *animation = [CATransition animation];
        animation.duration = 0.4f;
        animation.type = kCATransitionMoveIn;
        animation.subtype = kCATransitionFromBottom;
       
        
        if ([type isEqualToString:@"0"]) {
             [MianImage.layer addAnimation:animation forKey:nil];
             MianImage.image = editedImage;
            
            UIButton *button = [self.view viewWithTag:100];
            [button setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
        }else{
            [BackImage.layer addAnimation:animation forKey:nil];
            BackImage.image = editedImage;
            
            UIButton *button = [self.view viewWithTag:200];
            [button setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
        }
       
        
    }];
    
}

- (void)imageCropperDidCancel:(PhotoViewController *)cropperViewController
{
    if (_sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        [cropperViewController.navigationController popViewControllerAnimated:YES];
    }else{
        //[cropperViewController dismissViewControllerAnimated:YES completion:nil];
         [cropperViewController.navigationController popViewControllerAnimated:YES];
    }
}
-(void)upLoadAvater:(UIImage *)image{
    [CYToast showStatusWithString:@"正在加载"];
   
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    //manger.responseSerializer = [AFJSONResponseSerializer serializer];
    manger.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSString *url = nil;
    if ([type isEqualToString:@"0"]) {
        url = [NSString stringWithFormat:@"%@%@",TestUrl,avaterUrl];
    }else{
        url = [NSString stringWithFormat:@"%@%@",TestUrl,backUrl];
    }

    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
       NSData *imageData=UIImageJPEGRepresentation(image, 0.6);
        NSString *fileName = [NSString stringWithFormat:@"ljqback.jpg"];
        [formData appendPartWithFileData:imageData name:@"picture" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //NSLog(@"%@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //[CYToast dismiss];
        //NSDictionary *dict = responseObject;
        NSData *data = responseObject;
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        if ([dict[@"status"] isEqualToString:@"success"]) {
            [CYToast showStatusWithString:@"上传成功" hideAfterDelay:0.4f];
            if ([type isEqualToString:@"0"]) {
                        [PHUserModel sharedPHUserModel].UserImage = dict[@"data"];
                        [[PHUserModel sharedPHUserModel] saveUserInfoToSanbox];
            }else{
                [PHUserModel sharedPHUserModel].banner = dict[@"data"];
                [[PHUserModel sharedPHUserModel] saveUserInfoToSanbox];
            }

        }
        else{
             [CYToast showErrorWithString:@"上传失败"];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
        [CYToast showErrorWithString:@"上传失败"];
    }];
}


@end
