//
//  SettingViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "SettingViewController.h"
#import "MyAccountViewController.h"
#import "MyCollectViewController.h"
#import "MainUserViewController.h"//用户主页面
#import "PrivateViewController.h"//隐私权
#import "TermsOfOursViewController.h"//服务条款
#import "ChangePwdViewController.h"//更改密码
#import "UserInformationViewController.h"//修改密码
#import "AddLiveViewController.h"//我的直播
#import "ChangeHeadViewController.h"//更新头像
#import "HotPostViewController.h"//人气帖文
#import "HotMoviesViewController.h"//人气视频
#import "ShortMovieViewController.h"
#import "SeetOtherViewController.h"//设定二级页面
#import "MyHomeViewController.h"
#import "PuHuiInternational_ImaI-Swift.h"


@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
}
@property (nonatomic,copy) NSArray *dataArray;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = LYColor(218, 218, 218);

    _dataArray =  @[@"",NSLocalizedString(@"等级", nil),NSLocalizedString(@"抽奖宝箱", nil),NSLocalizedString(@"任务", nil) ,NSLocalizedString(@"我的直播", nil),NSLocalizedString(@"人气视频", nil),NSLocalizedString(@"人气帖文", nil),NSLocalizedString(@"我的收藏", nil),NSLocalizedString(@"设定", nil),NSLocalizedString(@"登出", nil)];
    [self creatTable];
    [self creatTabButton];
  

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
    [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    
    self.navigationController.navigationBar.hidden = NO;
    
}
-(void)rightlogo{
    [UIView animateWithDuration:0.25f animations:^{
        self.tabBarController.selectedIndex = 0;
    }];
}
-(void)creatTabButton{
    UIImage *rightImage = [[UIImage imageNamed:@"直播.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(0, 0, 40, 40);
    [liveButton setImage:[UIImage imageNamed:@"五秒视频.png"] forState:UIControlStateNormal];
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    [liveButton addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *but1 = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(live)];
    UIBarButtonItem *but3 = [[UIBarButtonItem alloc]initWithCustomView:liveButton];
    
    
    
    self.navigationItem.leftBarButtonItems = @[but1,but3];
    //self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
}
-(void)live{
    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark =======TableView delegeta && datasource =========================================
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT -  Height_NavBar - Height_TabBar ) style:UITableViewStylePlain];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DemoTableViewCell";
    UITableViewCell  *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    if (indexPath.row == 0) {
        CGFloat imageW = 4;
        UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(15-imageW, 5 - imageW, 50 + imageW * 2, 50+ imageW * 2)];
        leveImage.image = [UIImage imageNamed:@"銅1"];
        leveImage.contentMode = UIViewContentModeScaleAspectFit;
        
        UIImageView *imageV = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 50, 50)];
        NSString *url = [NSString stringWithFormat:@"%@%@",@"",[PHUserModel sharedPHUserModel].UserImage];
        [imageV sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"占位图"]];
        imageV.clipsToBounds = YES;
        imageV.layer.cornerRadius = 25;
        imageV.contentMode = UIViewContentModeScaleAspectFit;
        
        [leveImage addSubview:imageV];
        
        [cell.contentView addSubview:leveImage];
        
        CGFloat width = [self getLabelWidthWithText:[PHUserModel sharedPHUserModel].nickname width:SCREEN_WIDTH - 20 font:FONT(20)];
        if (width > 170) {
            width = 170;
        }
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(75, 10, width, 30)];
        label.text =[PHUserModel sharedPHUserModel].nickname;
        label.font = FONT(20);
        label.textColor = NameColor;
        [cell.contentView addSubview:label];
        
        UIImageView *Uimage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(label.frame) + 2, 10, 30, 30)];
        Uimage.image = [UIImage imageNamed:@"奖牌"];
        Uimage.clipsToBounds = YES;
        Uimage.contentMode = UIViewContentModeScaleAspectFit;
        
        UILabel *_LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(Uimage.frame),  16, 30, 20)];
        _LevelLabel.backgroundColor =MainColor;
        _LevelLabel.text = @"lv.1";
        _LevelLabel.textAlignment = NSTextAlignmentCenter;
        _LevelLabel.layer.cornerRadius = 10;
        _LevelLabel.layer.shouldRasterize = YES;
        _LevelLabel.clipsToBounds = YES;
        _LevelLabel.font = FONT(11);
        _LevelLabel.textColor = [UIColor whiteColor];
        
        [cell.contentView addSubview:Uimage];
        [cell.contentView addSubview:_LevelLabel];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%@",_dataArray[indexPath.row]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
  
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.m_showBackBt = YES;
        controller.member_id = [PHUserModel sharedPHUserModel].member_id;
        controller.ImageUrl  = [PHUserModel sharedPHUserModel].UserImage;
        controller.Username = [PHUserModel sharedPHUserModel].nickname;
        controller.BannerImage = [PHUserModel sharedPHUserModel].banner;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 1) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LevelStoryboard" bundle:nil];
        LevelViewController *levelViewController = [storyboard instantiateViewControllerWithIdentifier:@"LevelViewController"];

        [self.navigationController pushViewController:levelViewController animated:YES];
    }else if (indexPath.row == 2) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"LevelStoryboard" bundle:nil];
        LotteryBoxViewController *lotteryBoxViewController = [storyboard instantiateViewControllerWithIdentifier:@"LotteryBoxViewController"];

        [self.navigationController pushViewController:lotteryBoxViewController animated:YES];
        
    }else if (indexPath.row == 3) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"QuestStoryboard" bundle:nil];
        QuestViewController *questViewController = [storyboard instantiateViewControllerWithIdentifier:@"QuestViewController"];

        [self.navigationController pushViewController:questViewController animated:YES];
    }else if (indexPath.row == 4) {
       // AddLiveViewController *controller = [[AddLiveViewController alloc]init];
        MainLiveViewController *controller = [[MainLiveViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 5){
        HotMoviesViewController *controller = [[HotMoviesViewController alloc]init];
        controller.m_showBackBt  = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }else if (indexPath.row == 6){
        HotPostViewController *controller = [[HotPostViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 7){
                MyCollectViewController *controller = [[MyCollectViewController alloc]init];
                controller.m_showBackBt = YES;
                [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 8){
        SeetOtherViewController *controller = [[SeetOtherViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row == 9){
        [PXAlertView showAlertWithTitle:NSLocalizedString(@"确认要登出", nil) message:NSLocalizedString(@"登出之后需重新登录", nil) cancelTitle:NSLocalizedString(@"取消", nil) otherTitle:NSLocalizedString(@"确定", nil) completion:^(BOOL cancelled) {
            if (!cancelled) {
                [PHUserModel sharedPHUserModel].isLogin = NO;
                [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
                AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [appdelegate initLoginVC];
            }
        }];
    }else if (indexPath.row == 10){
        MyHomeViewController *controller = [[MyHomeViewController alloc]init];
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
//    else if (indexPath.row == 7){
//        TermsOfOursViewController *controller = [[TermsOfOursViewController alloc]init];
//        controller.m_showBackBt = YES;
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else if (indexPath.row == 8){
//        AddLiveViewController *controller = [[AddLiveViewController alloc]init];
//        controller.m_showBackBt = YES;
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else if (indexPath.row == 9){
//        HotMoviesViewController *controller = [[HotMoviesViewController alloc]init];
//        controller.m_showBackBt  = YES;
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else if (indexPath.row == 10){
//        HotPostViewController *controller = [[HotPostViewController alloc]init];
//        controller.m_showBackBt = YES;
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else if (indexPath.row == 11){
//        [PXAlertView showAlertWithTitle:@"是否退出登录" message:nil cancelTitle:@"取消" otherTitle:@"确定" completion:^(BOOL cancelled) {
//            if (!cancelled) {
//                [PHUserModel sharedPHUserModel].isLogin = NO;
//                [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
//                AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//                [appdelegate initLoginVC];
//            }
//        }];
//    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1f;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
