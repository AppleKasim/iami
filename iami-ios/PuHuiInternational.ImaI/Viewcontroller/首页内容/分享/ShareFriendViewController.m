//
//  ShareFriendViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/22.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ShareFriendViewController.h"
#import "TagFriedTableViewCell.h"
@interface ShareFriendViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *MainTableView;
    
    SZTextView *text;
}
@property (nonatomic,copy) NSMutableArray *DataArray;
#define GetComment @"/post/getComments"//获得所有评论
#define LikeCommetn @"/like/toggleCommentLike"//评论点赞
@end

@implementation ShareFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _DataArray = [[NSMutableArray alloc]init];
   
    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)loadData{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,GetComment];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":self.post_id};
    [CYToast showStatusWithString:@"正在加载"];
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr.count == 0) {
            [CYToast showErrorWithString:@"暂无评论"];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            for (NSDictionary *Comdict in dataArr) {
                [_DataArray addObject:[CommentModel parsenWithDictionary:Comdict]];
            }
            [self creatTable];
            
        }
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 10, SCREEN_WIDTH - 10, SCREEN_HEIGHT - Height_NavBar) style:UITableViewStylePlain];
    //MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    MainTableView.scrollEnabled = YES;
    [MainTableView registerClass:[TagFriedTableViewCell class] forCellReuseIdentifier:@"cell"];
    MainTableView.fd_debugLogEnabled = YES;
    MainTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    MainTableView.layer.borderWidth = 1.0f;
    MainTableView.layer.borderColor = LYColor(237, 204, 105).CGColor;
    [self.view addSubview:MainTableView];
    
    //LYColor(237, 204, 105);
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _DataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TagFriedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    CommentModel *model = _DataArray[indexPath.row];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    [cell setCModel:model];
    [cell.LikeButton addTarget:self action:@selector(likeCom:) forControlEvents:UIControlEventTouchUpInside];
    cell.LikeButton.tag = 100 + indexPath.row;

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentModel *model = _DataArray[indexPath.row];
    return model.cellHeight + 45;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 100)];
//    text = [[SZTextView alloc]initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH - 30, 50)];
//    text.placeholder = @"想说什么？";
//    text.layer.borderWidth = 1.0f;
//    text.font = FONT(19);
//    text.layer.borderColor = LYColor(207, 157, 61).CGColor;
//    text.layer.cornerRadius = 8;
//    [view addSubview:text];
//
//    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    shareButton.frame = CGRectMake(SCREEN_WIDTH - 80, 65, 60, 20);
//    [shareButton setTitle:@"分享" forState:UIControlStateNormal];
//    [shareButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    shareButton.titleLabel.font  =FONT(14);
//    shareButton.backgroundColor = LYColor(237, 204, 105);
//    shareButton.layer.cornerRadius = 10;
//    shareButton.clipsToBounds = YES;
//    [view addSubview:shareButton];
//    return view;
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 100;
//}
-(void)likeCom:(UIButton *)button{
    CommentModel *model = _DataArray[button.tag - 100];
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeCommetn];
    NSDictionary *parmDic  = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"comment_id":model.comment_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            //[CYToast showSuccessWithString:@""]
            if ([model.isLike isEqualToString:@"Y"]) {
                model.isLike = @"N";
            }else{
                model.isLike = @"Y";
            }
            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -100 inSection:0];
            [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     self.navigationController.navigationBar.hidden = NO;
    [text becomeFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
