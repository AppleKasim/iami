//
//  FriendInformationViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/20.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "FriendInformationViewController.h"
#import "MainTableViewCell.h"

@interface FriendInformationViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
}
//#define height
@end

@implementation FriendInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
#pragma  ============== creatUI ==============

-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -  64 ) style:UITableViewStyleGrouped];
    //MainTableView.backgroundColor = LYColor(255, 255, 255);
    //MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    [MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:MainTableView];
}
#pragma  ========== tableviewdelegate && datasource ==========
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    MainModel *model = [[MainModel alloc]init];
    //[cell setMode:model];
    cell.model = model;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 330;
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
