//
//  ShortMovTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ShortMovTableViewCell.h"

@implementation ShortMovTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setUrl:(NSString *)str{
    self.backgroundColor = [UIColor blackColor];






//    _playerView = [[ZFPlayerView alloc] init];
//    [self.contentView addSubview:self.playerView];
//    _playerView.frame = CGRectMake(10, 0, SCREEN_WIDTH - 20, 220);
//
    UIView *BView = [[UIView alloc]initWithFrame:CGRectMake(10, 10, SCREEN_WIDTH - 20, 170)];
    BView.layer.borderWidth = 1.0f;
    BView.layer.borderColor = MainColor.CGColor;
    BView.userInteractionEnabled = YES;
//    // 初始化控制层view(可自定义)
//    ZFPlayerControlView *controlView = [[ZFPlayerControlView alloc] init];
//    controlView.frame =CGRectMake(10, 70, SCREEN_WIDTH - 20, 180);
//
//    // 初始化播放模型
//    ZFPlayerModel *playerModel = [[ZFPlayerModel alloc]init];
//    playerModel.videoURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",str]];
//    playerModel.title = @"";
//    playerModel.fatherView = BView;
//    playerModel.placeholderImage = [UIImage imageNamed:@"default_movie"];
//    //playerModel.placeholderImage = [UIImage imageNamed:@"默认播放按钮"];
//    [self.playerView playerControlView:controlView playerModel:playerModel];
//
//
//
//
//    // 设置代理
//    self.playerView.delegate = self;
//    // 自动播放

    
//    UIView *tapView = [[UIView alloc]init];
//    tapView.frame = CGRectMake(0, 0, 80, 80);
//    tapView.center = BView.center;
//    [tapView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(play)]];
//    tapView.userInteractionEnabled = YES;
//    [BView addSubview:tapView];
//
    [self.contentView addSubview:BView];

    [BView addSubview:self.videoImageView];
    [self.videoImageView addSubview:self.playButton];

    self.videoImageView.frame = CGRectMake(5, 5, SCREEN_WIDTH - 30, 160);
    self.videoImageView.contentMode = UIViewContentModeScaleToFill;


    self.playButton.frame = CGRectMake(0, 0, self.videoImageView.frame.size.width, self.videoImageView.frame.size.height);
    //NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",model.videoPath]];
    [self.videoImageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"default_movie"]];


    _NameLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 190, 50, 20)];
    _NameLabel.text = NSLocalizedString(@"测试", nil);
    _NameLabel.font = FONT(16);
    _NameLabel.textColor = MainColor;
    [self.contentView addSubview:_NameLabel];
    
    _CountryLabel = [[UILabel alloc]initWithFrame:CGRectMake(65, 190, 200, 20)];
    _CountryLabel.text = @"";
    _CountryLabel.textAlignment = NSTextAlignmentLeft;
    _CountryLabel.font = FONT(16);
    _CountryLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_CountryLabel];
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, 219, SCREEN_WIDTH - 20, 1)];
    view.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:view];
    //self.backgroundColor = [UIColor whiteColor];
}
//-(void)play{
//    if (self.playerView.state == ZFPlayerStatePlaying ) {
//        return;
//    }
//    [self.playerView autoPlayTheVideo];
//    [self.delegate playWith:self.playerView];
//}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setDelegate:(id<ShortMovieDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath {
    self.delegate = delegate;
    self.indexPath = indexPath;
}

- (void)playBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(zf_playTheVideoAtIndexPath:)]) {
        [self.delegate zf_playTheVideoAtIndexPath:self.indexPath];
    }
}

#pragma mark - getter

- (UIButton *)playButton {
    if (!_playButton) {
        _playButton = [UIButton buttonWithType:UIButtonTypeCustom];
        // [_playButton setImage:[UIImage imageNamed:@"默认播放按钮"] forState:UIControlStateNormal];
        [_playButton addTarget:self action:@selector(playBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _playButton;
}

- (UIImageView *)videoImageView {
    if (!_videoImageView) {
        _videoImageView = [[UIImageView alloc] init];
        _videoImageView.userInteractionEnabled = YES;
        _videoImageView.tag = 100;
        _videoImageView.contentMode = UIViewContentModeScaleAspectFill;
        _videoImageView.clipsToBounds = YES;
    }
    return _videoImageView;
}

@end
