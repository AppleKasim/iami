//
//  ShortMovTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ShortMovieDelegate <NSObject>
- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath;
@end
@interface ShortMovTableViewCell : UITableViewCell
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) ZFPlayerView *playerView;

@property (nonatomic,strong) UILabel *NameLabel;
@property (nonatomic,strong) UILabel *CountryLabel;

@property (nonatomic,weak)  id<ShortMovieDelegate> delegate;

@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) NSIndexPath *indexPath;

-(void)setUrl:(NSString *)str;
- (void)setDelegate:(id<ShortMovieDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath;
@end
