//
//  ShortMovieViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/11.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ShortMovieViewController.h"
#import "ShortMovTableViewCell.h"
@interface ShortMovieViewController ()<UITableViewDelegate,UITableViewDataSource,ShortMovieDelegate>
{
    UITableView *MainTableView;
    
    NSInteger page;
    
    
}
@property (nonatomic,copy) NSMutableArray *dataArray;
@property (nonatomic,strong) ZFPlayerView *currentPlayer;

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) NSMutableArray *urls;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;

#define movieUrl @"/api/movie5s"
@end

@implementation ShortMovieViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    page = 1;
    _dataArray = [[NSMutableArray alloc]init];
    self.view.backgroundColor = [UIColor blackColor];
     [self creatTable];

    _playerManager = [[ZFAVPlayerManager alloc] init];
    /// player的tag值必须在cell里设置
    self.player = [ZFPlayerController playerWithScrollView:MainTableView playerManager:_playerManager containerViewTag:100];
    self.player.controlView = self.controlView;
    self.player.shouldAutoPlay = NO;
    /// 1.0是完全消失的时候
    self.player.playerDisapperaPercent = 1.0;

    [self loadData];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;

    self.player = [ZFPlayerController playerWithScrollView:MainTableView playerManager:_playerManager containerViewTag:100];
    self.player.controlView = self.controlView;
    self.player.shouldAutoPlay = NO;
    /// 1.0是完全消失的时候
    self.player.playerDisapperaPercent = 1.0;

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.player = nil;
}
-(void)playWith:(ZFPlayerView *)PlView{
    _currentPlayer = PlView;
}

- (NSMutableArray *)urls {
    _urls = @[].mutableCopy;
    for (NSString *path in _dataArray) {
            NSString *URLString = [path stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
            NSURL *url = [NSURL URLWithString:URLString];
            [_urls addObject:url];
    }
    return _urls;
}

- (BOOL)shouldAutorotate {
    return self.player.shouldAutorotate;
}

#pragma mark - TableView delegeta && datasource
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -  70 ) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor blackColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    
    //[MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[ShortMovTableViewCell class] forCellReuseIdentifier:@"videocell"];
    
    
    MJRefreshStateHeader *header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh)];
    MainTableView.mj_header = header;
    MJRefreshAutoStateFooter *footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadDataMore)];
    // MJRefreshFooter *footer = [MJRefreshFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadDataMore)];
    MainTableView.mj_footer = footer;
    [self.view addSubview:MainTableView];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShortMovTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videocell" forIndexPath:indexPath];
    [cell setUrl:[NSString stringWithFormat:@"%@",_dataArray[indexPath.row]]];

    [cell setDelegate:self withIndexPath:indexPath];

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 220;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 60;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , 50)];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = MainColor;
    [button setTitle:NSLocalizedString(@"上传影片", nil) forState:UIControlStateNormal];
    button.titleLabel.font = FONT(14);
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.frame =CGRectMake(10, 10, SCREEN_WIDTH - 20, 40);
    [view addSubview:button];
    return view;
}
-(void)loadData{
    [_dataArray removeAllObjects];
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,movieUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"Page":[NSString stringWithFormat:@"%ld",page]};
    
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
         NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count>0) {
            for (NSDictionary *dataDic in dataArr) {
                [_dataArray addObject: [NetDataCommon stringFromDic:dataDic forKey:@"path"]];
            }
        }
        self.player.assetURLs = self.urls;

        [MainTableView reloadData];
        [MainTableView.mj_footer endRefreshing];
        [MainTableView.mj_header endRefreshing];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
-(void)refresh{
    page = 1;
    [self loadData];
}
-(void)loadDataMore{
    page = page + 1;
    [self loadData];
}

#pragma mark - ShortMovieDelegate

- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath {
    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
}

#pragma mark - private method

/// play the video
- (void)playTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.player playTheIndexPath:indexPath scrollToTop:scrollToTop];
    [self.controlView showTitle:@""
                 coverURLString:_dataArray[indexPath.row]
                 fullScreenMode:ZFFullScreenModeLandscape];
}

#pragma mark - getter
- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
    }
    return _controlView;
}

@end
