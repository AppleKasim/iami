//
//  MainTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"
#import "TagFriedTableViewCell.h"

@protocol mainCellDelegate <NSObject>

-(void)clickMore;
@end

@interface MainTableViewCell : UITableViewCell<UITextViewDelegate,CJLinkLabelDelegate,UITableViewDelegate,UITableViewDataSource,XLPhotoBrowserDelegate, XLPhotoBrowserDatasource,TTTAttributedLabelDelegate>
{
    UITableView *commetTable;
}

@property (nonatomic,strong) UIImageView *MainImage;
@property (nonatomic,strong) UIView *MainImageView;


@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) TTTAttributedLabel *MainLabel;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UILabel *TimeLabel;
@property (nonatomic,strong) UILabel *ForWhoLabel;
//@property (nonatomic,strong) TTTAttributedLabel *mainLabel;


@property (nonatomic,strong) UIButton *LikeButton;
@property (nonatomic,strong) UIButton *CommentButton;
@property (nonatomic,strong) UIButton *ForwardButton;
@property (nonatomic,strong) UIButton *CollectButton;


@property (nonatomic,strong) UIView *CommentView;
@property (nonatomic,strong) UILabel *Commentlabel;
@property (nonatomic,assign) CGFloat CellHeightt;
@property (nonatomic,strong) SZTextView *CommentTld;
@property (nonatomic,strong) UIButton *CommentBut;
@property (nonatomic,strong) UIButton *MoreCommentBut;
@property (nonatomic,strong) UIButton *ReportButton;

@property (nonatomic,strong) UIButton *MoreButton;

@property (nonatomic,copy) NSMutableArray *imageArray;
@property (nonatomic,copy) NSMutableArray *positionArray;

@property (nonatomic,strong) NSString *isNoti;

@property (nonatomic,strong) MainModel *model;

@property (nonatomic,weak) id<mainCellDelegate> delegate;


@property (nonatomic,strong) MainViewController *viewC;
//-(void)setMode:(MainModel *)model;
-(CGFloat)getImageHeight:(NSInteger)count;
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font;
-(CGFloat)returnCellHeight:(MainModel*)model;
@end
