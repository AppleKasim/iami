//
//  MainModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainModel.h"
#define GetComment @"/post/getComments"//获得所有评论


@implementation MainModel

+(MainModel *)pareWithDictionary:(NSDictionary *)dict{
    MainModel *model = [[MainModel alloc]init];

    model.CommentArray = [[NSMutableArray alloc]init];
    
    model.post_type = [NetDataCommon stringFromDic:dict forKey:@"post_type"];
    NSString *content = [NetDataCommon stringFromDic:dict forKey:@"content"];
    model.content = [model retunrNewString:content];
    
    model.create_time = [NetDataCommon stringFromDic:dict forKey:@"create_time"];
    model.Like = [NetDataCommon stringFromDic:dict forKey:@"likeCount"];
    model.Comment = [NetDataCommon stringFromDic:dict forKey:@"commentCount"];
    model.Share = [NetDataCommon stringFromDic:dict forKey:@"shareCount"];
    model.post_id = [NetDataCommon stringFromDic:dict forKey:@"post_id"];
    model.isLike = [NetDataCommon stringFromDic:dict forKey:@"isLike"];
    model.member_id = [NetDataCommon stringFromDic:dict forKey:@"member_id"];
    model.isCollect = [NetDataCommon stringFromDic:dict forKey:@"isCollect"];
    model.UserLevel = [NetDataCommon stringFromDic:dict forKey:@"level"];
    
    
    if (model.Comment.integerValue >= 2) {
         model.CommentHeight = 40 + 63 * 2;
    }else{
        model.CommentHeight = 40 + 63* [model.Comment integerValue];
    }
   

    NSDictionary *postForm = dict[@"member"];
    model.UserImage = [NetDataCommon stringFromDic:postForm forKey:@"avatar"];
    model.UserName   = [NetDataCommon stringFromDic:postForm forKey:@"nickname"];
    model.banner = [NetDataCommon stringFromDic:postForm forKey:@"banner"];
    model.PostMember_id = [NetDataCommon stringFromDic:postForm forKey:@"member_id"];
    model.PostMembeName = [NetDataCommon stringFromDic:postForm forKey:@"nickname"];
    
    //留言
    NSDictionary *forwhoDic = dict[@"for_who"];
    model.ForWhoMember_id = forwhoDic[@"member_id"];
    model.ForWhoName =  forwhoDic[@"nickname"];
    
    if ([dict objectForKey:@"pictures"]) {
        model.ImageArray = [[NSMutableArray alloc]initWithArray:dict[@"pictures"]];
    }

    model.cellHeight =  [model returnCellHeight:model];
     CGFloat LeadingSpace = 10;
    
    if ([model.post_type isEqualToString:@"video"]) {
        model.cellHeight = [model getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 3 font:FONT(16)] + 300;
        NSArray *videoArr = dict[@"video"];
        if (videoArr.count > 0) {
            NSDictionary *videoDic = videoArr[0];
            model.videoPath = [NetDataCommon stringFromDic:videoDic forKey:@"path"];
        }
    }
    
    if ([model.post_type isEqualToString:@"share"]) {
        NSDictionary *shareDic = dict[@"share_post"][@"data"];
        
        
        NSDictionary *fromDic = dict[@"member"];
        
        model.shareName = [NetDataCommon stringFromDic:fromDic forKey:@"nickname"];
        model.shareImage = [NetDataCommon stringFromDic:fromDic forKey:@"avatar"];
        model.shareBanner = [NetDataCommon stringFromDic:fromDic forKey:@"banner"];
        model.shareID = [NetDataCommon stringFromDic:fromDic forKey:@"member_id"];
        model.shareLevel = [NetDataCommon stringFromDic:fromDic forKey:@"level"];
        
        NSDictionary *postForm = shareDic[@"member"];
       
        model.UserImage = [NetDataCommon stringFromDic:postForm forKey:@"avatar"];
        model.UserName   = [NetDataCommon stringFromDic:postForm forKey:@"nickname"];
        model.UserLevel = [NetDataCommon stringFromDic:postForm forKey:@"level"];
        model.shareTime = [NetDataCommon stringFromDic:shareDic forKey:@"create_time"];
        NSString *content = [NetDataCommon stringFromDic:shareDic forKey:@"content"];
        model.shareContent = [model retunrNewString:content];
        model.shareType = [NetDataCommon stringFromDic:shareDic forKey:@"post_type"];
        model.banner = [NetDataCommon stringFromDic:postForm forKey:@"banner"];
        model.member_id = [NetDataCommon stringFromDic:postForm forKey:@"member_id"];

        NSArray *videoArr = shareDic[@"video"];
        if (videoArr.count > 0) {
            NSDictionary *videoDic = videoArr[0];
            model.videoPath = [NetDataCommon stringFromDic:videoDic forKey:@"path"];
        }



         model.ImageArray = [[NSMutableArray alloc]initWithArray:shareDic[@"pictures"]];
        
        CGFloat stringHright = [model retureStringHeight:model.shareContent];
        
       model.shareHeight = [model retureStringHeight:model.content];
        
        if ([model.shareType isEqualToString:@"video"]) {
             model.cellHeight = stringHright + 380+model.shareHeight- 20;
            if (model.shareContent.length == 0) {
                model.cellHeight = model.cellHeight + 18;
            }
        }else{
            CGFloat imageHeight  = 0;
            if (model.ImageArray && model.ImageArray.count > 0) {
                  imageHeight = [model getImageHeight:model.ImageArray.count] + 20;
            }
            model.cellHeight = stringHright + 75+ 132  +imageHeight +model.shareHeight - 20 ;

       }
    }
    
    
    return model;
}
+(MainModel *)pareWithMovieDictionary:(NSDictionary *)dict{
    MainModel *model = [[MainModel alloc]init];
    model.CommentArray = [[NSMutableArray alloc]init];

    model.post_type = [NetDataCommon stringFromDic:dict forKey:@"post_type"];
    model.content = [NetDataCommon stringFromDic:dict forKey:@"content"];
    model.create_time = [NetDataCommon stringFromDic:dict forKey:@"create_time"];
    model.Like = [NetDataCommon stringFromDic:dict forKey:@"likeCount"];
    model.Comment = [NetDataCommon stringFromDic:dict forKey:@"commentCount"];
    model.Share = [NetDataCommon stringFromDic:dict forKey:@"shareCount"];
    model.post_id = [NetDataCommon stringFromDic:dict forKey:@"post_id"];
    model.isLike = [NetDataCommon stringFromDic:dict forKey:@"isLike"];
    model.isCollect = [NetDataCommon stringFromDic:dict forKey:@"isCollect"];
    model.member_id = [NetDataCommon stringFromDic:dict forKey:@"member_id"];
    NSDictionary *postForm = dict[@"member"];
    model.UserImage = [NetDataCommon stringFromDic:postForm forKey:@"avatar"];
    model.UserName   = [NetDataCommon stringFromDic:postForm forKey:@"nickname"];

    if (model.Comment.integerValue >= 2) {
        model.CommentHeight = 40 + 63 * 2;
    }else{
        model.CommentHeight = 40 + 63* [model.Comment integerValue];
    }
    
    model.cellHeight =  [model returnCellHeight:model];
    CGFloat LeadingSpace = 10;
        model.cellHeight = [model getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 3 font:FONT(16)] + 280;
        NSArray *videoArr = dict[@"video"];
        if (videoArr.count > 0) {
            NSDictionary *videoDic = videoArr[0];
            model.videoPath = [NetDataCommon stringFromDic:videoDic forKey:@"path"];
        }
    
    return model;
}
+(MainModel *)parsenWithShareDictionary:(NSDictionary *)dict{
    MainModel *model = [[MainModel alloc]init];
    NSDictionary *shareDic = dict[@"share_post"][@"data"];
    model.CommentArray = [[NSMutableArray alloc]init];

    model.post_type = [NetDataCommon stringFromDic:shareDic forKey:@"post_type"];
    model.content = [NetDataCommon stringFromDic:shareDic forKey:@"content"];
    model.create_time = [NetDataCommon stringFromDic:shareDic forKey:@"create_time"];
    model.Like = [NetDataCommon stringFromDic:dict forKey:@"likeCount"];
    model.Comment = [NetDataCommon stringFromDic:dict forKey:@"commentCount"];
    model.Share = [NetDataCommon stringFromDic:dict forKey:@"shareCount"];
    model.post_id = [NetDataCommon stringFromDic:dict forKey:@"post_id"];
    model.isLike = [NetDataCommon stringFromDic:dict forKey:@"isLike"];
    model.member_id = [NetDataCommon stringFromDic:dict forKey:@"member_id"];
    model.isCollect = [NetDataCommon stringFromDic:dict forKey:@"isCollect"];
    
    NSDictionary *fromDic = dict[@"member"];
    model.shareName = [NetDataCommon stringFromDic:fromDic forKey:@"nickname"];
    model.shareImage = [NetDataCommon stringFromDic:fromDic forKey:@"avatar"];
    
    NSDictionary *postForm = shareDic[@"member"];
    model.UserImage = [NetDataCommon stringFromDic:postForm forKey:@"avatar"];
    model.UserName   = [NetDataCommon stringFromDic:postForm forKey:@"nickname"];
    if ([dict objectForKey:@"pictures"]) {
        model.ImageArray = [[NSMutableArray alloc]initWithArray:dict[@"pictures"]];
        
    }else{
        
    }
    model.cellHeight =  [model returnCellHeight:model] + 100;
    CGFloat LeadingSpace = 10;
    if ([model.post_type isEqualToString:@"video"]) {
        model.cellHeight = [model getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 2 font:FONT(16)] + 280;
        NSArray *videoArr = dict[@"video"];
        if (videoArr.count > 0) {
            NSDictionary *videoDic = videoArr[0];
            model.videoPath = [NetDataCommon stringFromDic:videoDic forKey:@"path"];
        }
        
    }
    
   
    return model;
}

//获取图片的整体高度
-(CGFloat)getImageHeight:(NSInteger)count {
    CGFloat LeadingSpace = 10;
    CGFloat ImageSpace = 5;
    if (count < 1) {
        //上面计算高度的时候预留了配图上下两个间隙的高度。所以，如果没有配图，返回 - LeadingSpace才合适。
        return - LeadingSpace;
    }
    else if (count == 1) {
        //return SCREEN_WIDTH * 0.85;
        return SCREEN_WIDTH - 30;
    }
    else if (count / 3 < 1 || count == 3) {
        //一行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3;
    }
    else if (count > 3 && count <= 6) {
        //两行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3 * 2 + ImageSpace;
    }
    else {
        //三行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) + ImageSpace * 2;
    }
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.height;
}
-(CGFloat)returnCellHeight:(MainModel*)model{
    CGFloat imageheight = 0;
    CGFloat LeadingSpace = 10;
    CGFloat labelHeght = 0;
    if (model.content && model.content.length > 0) {
        labelHeght =   [self getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 3 font:FONT(16)];
    }else{
        labelHeght = 0;
    }
    
    if (model.ImageArray && model.ImageArray.count >0 ) {
        imageheight =    [self getImageHeight:model.ImageArray.count] + 20;
    }else{
        imageheight = 0 ;
    }
    return imageheight + labelHeght + 120;
}
-(CGFloat)returnshareCellHeight:(MainModel*)model{
    CGFloat imageheight = 0;
    CGFloat LeadingSpace = 10;
    CGFloat labelHeght = 0;
    if (model.shareContent && model.shareContent.length > 0) {
        labelHeght =   [self getLabelHeightWithText:model.shareContent width:SCREEN_WIDTH - LeadingSpace * 3 font:FONT(16)];
    }else{
        labelHeght = 0;
    }
    
    if (model.ImageArray && model.ImageArray.count >0 ) {
        imageheight =    [self getImageHeight:model.ImageArray.count] + 20;
    }else{
        imageheight = 0 ;
    }
    
    return imageheight + labelHeght + 75;
  
}
-(CGFloat)retureStringHeight:(NSString *)string{
    CGFloat LeadingSpace = 10;
    CGFloat labelHeght = 0;
    if (string && string.length > 0) {
        labelHeght =   [self getLabelHeightWithText:string width:SCREEN_WIDTH - LeadingSpace * 3 font:FONT(16)];
    }else{
        labelHeght = 0;
    }
    return labelHeght;
}
-(NSString *)retunrNewString:(NSString *)string{
    NSString *newStr=[string stringByReplacingOccurrencesOfString:@"<br />\n" withString:@"\n"];
    NSString *new = [self filterHTMLTag:newStr];
    return new;
}
- (NSString *)filterHTMLTag:(NSString *)str {
         //替换字符
         str  =  [str  stringByReplacingOccurrencesOfString:@"&mdash;" withString:@"-"];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&ldquo;" withString:@"\""];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&rdquo;" withString:@"\""];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&rsquo;" withString:@"’"];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&lsquo;" withString:@"‘"];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&middot;" withString:@"·"];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
         str  =  [str  stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
         str  =  [str  stringByReplacingOccurrencesOfString:@"<strong>" withString:@""];
         str  =  [str  stringByReplacingOccurrencesOfString:@"</strong>" withString:@""];
        str = [str stringByReplacingOccurrencesOfString:@"&hellip;" withString:@"..."];
         return str;
    
}
@end
