//
//  VideoTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/28.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"

@protocol mainVideoCellDelegate <NSObject>

-(void)clickMore;
-(void)playWith:(ZFPlayerView *)PlView;
- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface VideoTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *MainImage;
@property (nonatomic,strong) UIView *MainImageView;
@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) TTTAttributedLabel *MainLabel;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UILabel *TimeLabel;
@property (nonatomic,strong) UILabel *ForWhoLabel;

@property (nonatomic,strong) UIButton *LikeButton;
@property (nonatomic,strong) UIButton *CommentButton;
@property (nonatomic,strong) UIButton *ForwardButton;
@property (nonatomic,strong) UIButton *CollectButton;

@property (nonatomic,strong) UIView *CommentView;
@property (nonatomic,strong) UILabel *Commentlabel;
@property (nonatomic,assign) CGFloat CellHeightt;
@property (nonatomic,strong) SZTextView *CommentTld;
@property (nonatomic,strong) UIButton *CommentBut;
@property (nonatomic,strong) UIButton *MoreCommentBut;
@property (nonatomic,strong) UIButton *ReportButton;
@property (nonatomic,strong) UIView *BView;

@property (nonatomic,strong) UIButton *MoreButton;

@property (nonatomic,strong) NSString *isNoti;

@property (nonatomic,weak) id<mainVideoCellDelegate> delegate;

@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic, strong) NSIndexPath *indexPath;

-(void)setMode:(MainModel *)model;
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font;
- (void)setDelegate:(id<mainVideoCellDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath;

@end
