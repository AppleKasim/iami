//
//  MainModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommentModel.h"
@interface MainModel : NSObject

@property (nonatomic,strong) NSString *MainImage;
@property (nonatomic,strong) NSString *UserImage;
@property (nonatomic,strong) NSString *UserName;
@property (nonatomic,strong) NSString *MainLabel;
@property (nonatomic,strong) NSString *UserLevel;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *Like;
@property (nonatomic,strong) NSString *Comment;
@property (nonatomic,strong) NSString *Share;
@property (nonatomic,strong) NSString *CollectButton;
@property (nonatomic,strong) NSString *post_type;
@property (nonatomic,strong) NSString *create_time;
@property (nonatomic,copy)   NSMutableArray *ImageArray;
@property (nonatomic) BOOL   isShowCommentV;
@property (nonatomic,assign) CGFloat cellHeight;
@property (nonatomic,strong) NSString *post_id;
@property (nonatomic,strong) NSString *isLike;
@property (nonatomic,strong) NSString *videoPath;
@property (nonatomic,strong) NSString *member_id;
@property (nonatomic,strong) NSString *isCollect;
@property (nonatomic,strong) NSString *banner;
@property (nonatomic,strong) NSString *ForWhoMember_id;
@property (nonatomic,strong) NSString *ForWhoName;
@property (nonatomic,strong) NSString *PostMember_id;
@property (nonatomic,strong) NSString *PostMembeName;

@property (nonatomic,retain) NSMutableArray *CommentArray;

@property (nonatomic,assign) CGFloat CommentHeight;
//分享
@property (nonatomic,strong) NSString *shareImage;
@property (nonatomic,strong) NSString *shareName;
@property (nonatomic,strong) NSString *shareTime;
@property (nonatomic,strong) NSString *shareContent;
@property (nonatomic,strong) NSString *shareType;
@property (nonatomic,strong) NSString *shareBanner;
@property (nonatomic,strong) NSString *shareID;
@property (nonatomic,assign) CGFloat shareHeight;
@property (nonatomic,strong) NSString *shareLevel;

@property (nonatomic,strong) NSString *isNoti;

//@property (nonatomic) BOOL IsShowFooter;
+(MainModel *)pareWithDictionary:(NSDictionary *)dict;
-(CGFloat)returnCellHeight:(MainModel*)model;

+(MainModel *)pareWithMovieDictionary:(NSDictionary *)dict;
+(MainModel *)parsenWithShareDictionary:(NSDictionary *)dict;
@end
