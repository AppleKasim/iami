//
//  MainTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainTableViewCell.h"

@implementation MainTableViewCell
{
    NSInteger column;
    
    NSInteger row;
    
    CGFloat imageWidth;
}

#define CellHeight 330
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
//获取图片的整体高度
-(CGFloat)getImageHeight:(NSInteger)count {
    CGFloat LeadingSpace = 10;
    CGFloat ImageSpace = 5;
    if (count < 1) {
        //上面计算高度的时候预留了配图上下两个间隙的高度。所以，如果没有配图，返回 - LeadingSpace才合适。
        return - LeadingSpace;
    }
    else if (count == 1) {
        //return SCREEN_WIDTH * 0.85;
     return SCREEN_WIDTH - 30;
    }
    else if (count / 3 < 1 || count == 3) {
        //一行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3;
    }
    else if (count > 3 && count <= 6) {
        //两行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3 * 2 + ImageSpace;
    }
    else {
        //三行
        return (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) + ImageSpace * 2;
    }
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:attribute
                                    context:nil].size;
    
    //NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
    return returnSize.height;
}
-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}
-(CGFloat)returnCellHeight:(MainModel*)model{
    CGFloat imageheight = 0;
    CGFloat LeadingSpace = 10;
    CGFloat labelHeght = 0;
    if (model.content && model.content.length > 0) {
      labelHeght =   [self getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 3  font:FONT(16)];
    }else{
        labelHeght = 0;
    }
    
    if (model.ImageArray && model.ImageArray.count >0 ) {
     imageheight =  [self getImageHeight:model.ImageArray.count] + 20;
    }else{
        imageheight = 0 ;
    }
    return imageheight + labelHeght + 120;
}
#pragma 暂定高度280
-(void)setModel:(MainModel *)model{
    self.backgroundColor = LYColor(223, 223, 223);
    CGFloat LeadingSpace = 10;
    CGFloat backViewHeight  = [self returnCellHeight:model] ;
    
    UIView *backView = nil;
    if ([model.isNoti isEqualToString:@"1"]) {
         backView = [[UIView alloc]initWithFrame:CGRectMake(3, 4, SCREEN_WIDTH - 6, backViewHeight - 4)];
    }else{
         backView = [[UIView alloc]initWithFrame:CGRectMake(3, 4, SCREEN_WIDTH - 6, backViewHeight - 4 + model.CommentHeight)];
    }
   
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.backgroundColor = [UIColor whiteColor];
    
    
    _imageArray = [NSMutableArray array];
    _positionArray = [NSMutableArray array];
    
    //添加更多按钮
    //if ([model.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        _MoreButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _MoreButton.frame = CGRectMake(backView.frame.size.width - 30, 10, 20, 15);
        [_MoreButton setImage:[UIImage imageNamed:@"查看更多"] forState:UIControlStateNormal];
        _MoreButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [backView addSubview:_MoreButton];
        
   // }
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(15-imageW, 10 - imageW, 50 + imageW * 2, 50+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    leveImage.userInteractionEnabled = YES;
    [backView addSubview:leveImage];
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW,  imageW, 50, 50)];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 25;
    _UserImage.clipsToBounds = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    _UserImage.userInteractionEnabled = YES;
    [leveImage addSubview:_UserImage];
    
    CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH - LeadingSpace * 2  font:FONT(17)];
    if (namelHeght > 170) {
        namelHeght = 170;
    }
    if (SCREEN_WIDTH < 370 && namelHeght == 170) {
        namelHeght = namelHeght - 15;
    }
    
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(70,  10, namelHeght, 20)];
    _UserName.text = model.UserName;
    _UserName.textColor = NameColor;
    _UserName.font = FONT(17);
    _UserName.userInteractionEnabled = YES;
    [backView addSubview:_UserName];
    
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 10, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:memberImage];
    
    _LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) +2, memberImage.frame.origin.y + 5, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [backView addSubview:_LevelLabel];
    
    if ([model.ForWhoMember_id isEqualToString:model.PostMember_id] == NO) {
        UIImageView *rightImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_LevelLabel.frame), _UserName.frame.origin.y+6, 11, 11)];
        rightImage.image = [UIImage imageNamed:@"向右箭头"];
        rightImage.contentMode = UIViewContentModeScaleAspectFit;
        [backView addSubview:rightImage];
        
        _ForWhoLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(rightImage.frame) + 1, _UserName.frame.origin.y, SCREEN_WIDTH - CGRectGetMaxX(rightImage.frame) - 30, 20)];
        _ForWhoLabel.userInteractionEnabled=  YES;
        _ForWhoLabel.textColor = NameColor;
        _ForWhoLabel.text = model.ForWhoName;
        _ForWhoLabel.font = FONT(17);
        [backView addSubview:_ForWhoLabel];
        
    }
    
    
    _TimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, CGRectGetMaxY(_UserName.frame) + 5, 120, 20)];
    _TimeLabel.text = [NSString stringWithFormat:@"%@",model.create_time];
    _TimeLabel.textColor = LYColor(184, 184, 184);
    _TimeLabel.font = FONT(13);
    [backView addSubview:_TimeLabel];
    
    CGFloat labelHeght = 0;
    if (model.content && model.content.length > 0) {
        labelHeght =   [self getLabelHeightWithText:model.content width:SCREEN_WIDTH - LeadingSpace * 3  font:FONT(16)];
    }else{
        labelHeght = 0;
    }
    
    _MainLabel = [[TTTAttributedLabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(_UserName.frame) + 38 , SCREEN_WIDTH - 6 - 24, labelHeght)];
    _MainLabel.numberOfLines = 0;
    _MainLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    _MainLabel.textColor = LYColor(105, 105, 105);
    _MainLabel.font = FONT(16);
    _MainLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
    _MainLabel.text = [NSString stringWithFormat:@"%@",model.content];
    NSString *URL;
    NSArray *urlArr;
    if([self isURL:model.content]) {
        URL = model.content;
        [_MainLabel addLinkToURL:[NSURL URLWithString:URL] withRange:NSMakeRange(0, model.content.length)];
    } else {
        urlArr = [self getURLFromStr:model.content];
        for (NSString *url in urlArr) {
            NSRange range = [_MainLabel.text rangeOfString:url];
             [_MainLabel addLinkToURL:[NSURL URLWithString:url] withRange:range];
        }
    }
    [_MainLabel setUserInteractionEnabled:YES];
    [_MainLabel setNumberOfLines:0];
    [backView addSubview:_MainLabel];
    
    
    
    
#pragma 获取图片个数 创建view
    CGFloat ImageSpace = 15;
    if (model.ImageArray.count == 1) {
        //一列
        column = 1;
        //imageWidth = SCREEN_WIDTH * 0.85;
        imageWidth =   SCREEN_WIDTH - 30;
    }
    else if (model.ImageArray.count == 2 || model.ImageArray.count == 4) {
        //两列
        column = 2;
        imageWidth = (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3;
    }
    else {
        //三列
        column = 3;
        imageWidth = (SCREEN_WIDTH - (LeadingSpace + ImageSpace) * 2) / 3;
    }
    
    //根据图片的数量和列数获得行数
    if (model.ImageArray.count % column == 0) {
        row = model.ImageArray.count / column;
    }
    else {
        row = model.ImageArray.count / column + 1;
    }
    
    NSInteger ImageViewTag = 500;
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < column; j++) {
            //用来判断数据是否越界
            if (i * column + j < model.ImageArray.count) {
                NSString   *imageUrl = model.ImageArray[i * column + j];
                
                if (i * column + j >= 9) {
                    break;
                }
                
                if (imageUrl) {
                    FLAnimatedImageView *imageView = [[FLAnimatedImageView alloc] initWithFrame:CGRectMake(LeadingSpace + j * (ImageSpace + imageWidth), CGRectGetMaxY(_MainLabel.frame) + 10 + LeadingSpace + i * (ImageSpace + imageWidth), imageWidth, imageWidth)];
                    imageView.tag = ImageViewTag + i * column + j;
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    imageView.clipsToBounds = YES;
                    
                    NSDictionary *dict = model.ImageArray[i * column + j];
                    NSString *path = [NetDataCommon stringFromDic:dict forKey:@"path"];
                    
                    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",path]] placeholderImage:[UIImage imageNamed:@"占位图"]];
                    
                
                    
                    //添加图片数组
                   
                    [_imageArray addObject:[NSString stringWithFormat:@"%@",path]];
                    //[_imageArray addObject:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",path]]];
                    
                    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
                    CGRect rect=[imageView convertRect:imageView.frame toView:window];
                    NSValue *val = [NSValue valueWithCGRect:rect];
                    [_positionArray addObject:val];
                    
                    //添加点击事件
                    imageView.userInteractionEnabled = YES;
                    [imageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickImageView:)]];
                    
                    //block通知，点击了图片，展示大图
                    // __weak typeof(self) weakSelf = self;
                    //                    imageView.didTouchImage = ^(NSInteger index) {
                    //                        [weakSelf showFullScreenImage:index];
                    //                    };
                    //
                    //                    [imageView setImageUrl:imageUrl index:i * column + j];
                    
                    //原创微博直接添加的cell中，非原创则加入一个容器中UIView，再将容器加入cell中
                    
                    [backView addSubview:imageView];
                }
            }
            else {
                //越界后跳出for循环
                break;
            }
        }
    }
    
    
    
    CGFloat imageHeight =  backViewHeight - 40;
    
    CGFloat Width =  SCREEN_WIDTH / 4;
    _LikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _LikeButton.frame = CGRectMake(0, imageHeight, Width, 30);
    if ([model.isLike isEqualToString:@"Y"]) {
        _LikeButton.selected = YES;
    }else{
        _LikeButton.selected = NO;
    }
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢未选中"] forState:UIControlStateNormal];
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢"] forState:UIControlStateSelected];
    [_LikeButton setTitle:[NSString stringWithFormat:@" %@",model.Like] forState:UIControlStateNormal];
    _LikeButton.titleLabel.font = FONT(13);
    [_LikeButton addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    [_LikeButton setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    [backView addSubview:_LikeButton];
    
    UIView *likeGap = [[UIView alloc]initWithFrame:CGRectMake(Width - 0.3, imageHeight + 5, 0.7, 20)];
    likeGap.backgroundColor = LYColor(184, 184, 184);
    [backView addSubview:likeGap];
    
    _CommentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _CommentButton.frame = CGRectMake(Width, imageHeight, Width, 30);
    [_CommentButton setImage:[UIImage imageNamed:@"评论"] forState:UIControlStateNormal];
    [_CommentButton setTitle:[NSString stringWithFormat:@" %@",model.Comment] forState:UIControlStateNormal];
    _CommentButton.titleLabel.font = FONT(13);
    [_CommentButton setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    [backView addSubview:_CommentButton];
    
    UIView *CommentGap = [[UIView alloc]initWithFrame:CGRectMake(Width *2 - 0.3, imageHeight + 5, 0.7, 20)];
    CommentGap.backgroundColor = LYColor(184, 184, 184);
    [backView addSubview:CommentGap];
    
    _ForwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _ForwardButton.frame = CGRectMake(Width *2, imageHeight, Width, 30);
    [_ForwardButton setImage:[UIImage imageNamed:@"转发"] forState:UIControlStateNormal];
    [_ForwardButton setTitle:[NSString stringWithFormat:@"%@",model.Share] forState:UIControlStateNormal];
    _ForwardButton.titleLabel.font = FONT(13);
    [_ForwardButton setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    [backView addSubview:_ForwardButton];
    
    UIView *CollectGap = [[UIView alloc]initWithFrame:CGRectMake(Width *3 - 0.3, imageHeight + 5, 0.7, 20)];
    CollectGap.backgroundColor = LYColor(184, 184, 184);
    [backView addSubview:CollectGap];
    
    _CollectButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _CollectButton.frame = CGRectMake(Width *3, imageHeight, Width, 30);
    if ([model.isCollect isEqualToString:@"Y"]) {
        _CollectButton.selected = YES;
    }else{
        _CollectButton.selected = NO;
    }
    [_CollectButton setImage:[UIImage imageNamed:@"未收藏"] forState:UIControlStateNormal];
    [_CollectButton setImage:[UIImage imageNamed:@"收藏"] forState:UIControlStateSelected];
    [_CollectButton setTitle:@"" forState:UIControlStateNormal];
    _CollectButton.titleLabel.font = FONT(13);
    [_CollectButton addTarget:self action:@selector(clickCollect) forControlEvents:UIControlEventTouchUpInside];
    [_CollectButton setTitleColor:LYColor(184, 184, 184) forState:UIControlStateNormal];
    [backView addSubview:_CollectButton];
    
    //创建间隔线
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(10, backViewHeight - 42, SCREEN_WIDTH - 30, 0.5)];
    gapView.backgroundColor =LYColor(184, 184, 184);
    [backView addSubview:gapView];
    
  
    
    
    //70
    _CommentView = [[UIView alloc]initWithFrame:CGRectMake(3, backViewHeight , SCREEN_WIDTH - 6, model.CommentHeight)];
    _CommentView.backgroundColor = [UIColor whiteColor];
    _CommentView.layer.cornerRadius = 8;
    
    
    NSString *comme = nil;
    if ([model.Comment isEqualToString:@"0"]) {
        comme = NSLocalizedString(@"请输入评论", nil);
    }else{
        comme = NSLocalizedString(@"查看更多评论", nil);
    }
    
    _MoreCommentBut = [UIButton buttonWithType:UIButtonTypeCustom];
    _MoreCommentBut.frame = CGRectMake(SCREEN_WIDTH / 2 -40, 5, 80, 20);
    _MoreCommentBut.backgroundColor =MainColor;
    _MoreCommentBut.layer.cornerRadius = 6;
    [_MoreCommentBut setTitle:comme forState:UIControlStateNormal];
    _MoreCommentBut.titleLabel.font = FONT(13);
    [_MoreCommentBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_CommentView addSubview:_MoreCommentBut];
    
    NSInteger count = 0;
    if (model.CommentArray.count >= 2) {
        count = 2;
    }else{
        count = model.CommentArray.count;
    }
    
    
    for (NSInteger i = 0; i < count; i++) {
        CommentModel *comm = model.CommentArray[i];
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 30 + i*63, SCREEN_WIDTH, 63)];
        [view addSubview:[self backCommentView:comm]];
        
        //        UIView *gapview1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 3)];
        //        gapView.backgroundColor = LYColor(223, 223, 223);
        //        [view addSubview:gapview1];
        [_CommentView addSubview:view];
        
    }
    
    
//    _CommentTld = [[SZTextView alloc]initWithFrame:CGRectMake(15, 35 + count * 63, SCREEN_WIDTH - 80 - 15, 30)];
//    _CommentTld.placeholder = NSLocalizedString(@"请输入评论", nil);
//    _CommentTld.font = FONT(15);
//    _CommentTld.layer.borderWidth = 1.0f;
//    _CommentTld.layer.borderColor = LYColor(207, 157, 61).CGColor;
//    _CommentTld.layer.cornerRadius = 8;
//    [_CommentView addSubview:_CommentTld];
//
//    _CommentBut = [UIButton buttonWithType:UIButtonTypeCustom];
//    _CommentBut.frame = CGRectMake(SCREEN_WIDTH - 70, 40 + count * 63, 60, 25);
//    [_CommentBut setTitle:NSLocalizedString(@"评论", nil) forState:UIControlStateNormal];
//    _CommentBut.backgroundColor = LYColor(237, 204, 105);
//    [_CommentBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    _CommentBut.titleLabel.font = FONT(15);
//    _CommentBut.layer.cornerRadius = 10;
//    _CommentBut.clipsToBounds = YES;
//    [_CommentView addSubview:_CommentBut];
    
    
    [backView addSubview:_CommentView];
    
      [self.contentView addSubview:backView];
}
//-(void)setMode:(MainModel *)model{
//  
//    
//}
- (void)linkLabel:(CJLinkLabel *)label linkClick:(NSString *)URL
{
   // NSLog(@"%@",URL);
}
- (void)clickImageView:(UITapGestureRecognizer *)tap
{
//    CLAmplifyView *amplifyView = [[CLAmplifyView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) andGesture:tap andSuperView:self.contentView];
//    [[UIApplication sharedApplication].keyWindow addSubview:amplifyView];
//
//     XLPhotoBrowser *browser = [XLPhotoBrowser showPhotoBrowserWithCurrentImageIndex:tap.view.tag imageCount:_imageArray.count datasource:self];
//   // browser.pageDotColor = [UIColor purpleColor]; ///< 此属性针对动画样式的pagecontrol无效
//    browser.currentPageDotColor = [UIColor whiteColor];
//    browser.pageControlStyle = XLPhotoBrowserPageControlStyleClassic;
    
    [XLPhotoBrowser showPhotoBrowserWithImages:_imageArray currentImageIndex:tap.view.tag - 500];
}

-(void)click{
    NSInteger likecount;
    if (_LikeButton.selected) {
        likecount = [_LikeButton.titleLabel.text integerValue] - 1;
        [_LikeButton setTitle:[NSString stringWithFormat:@"%ld", (long)likecount] forState:UIControlStateNormal];
    } else {
        likecount = [_LikeButton.titleLabel.text integerValue] + 1;
    }

    [_LikeButton setTitle:[NSString stringWithFormat:@"%ld", (long)likecount] forState:UIControlStateNormal];
    _LikeButton.selected = !_LikeButton.selected;
}

-(void)clickCollect{
    _CollectButton.selected = !_CollectButton.selected;
}

-(UIView *)backCommentView:(CommentModel *)model{
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(0, 6, SCREEN_WIDTH - 6, model.cellHeight + 41)];
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.backgroundColor = [UIColor whiteColor];
    
    UIView *gapView = [[UIView alloc]initWithFrame:CGRectMake(3, 0, SCREEN_WIDTH - 6, 2)];
    gapView.backgroundColor = LYColor(223, 223, 223);
    [backView addSubview:gapView];
    
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(20-imageW, 7 - imageW, 30 + imageW * 2, 30+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:leveImage];
    
   UIImageView  *UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 30, 30)];
    [UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    UserImage.layer.cornerRadius = 15;
    UserImage.clipsToBounds = YES;
    UserImage.contentMode = UIViewContentModeScaleAspectFit;
    [leveImage addSubview:UserImage];
    
    CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH  font:FONT(14)];
   UILabel *UserName = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, namelHeght, 20)];
    UserName.text = model.UserName;
    UserName.textColor = NameColor;
    UserName.font = FONT(14);
    UserName.textAlignment =NSTextAlignmentLeft;
    [backView addSubview:UserName];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(UserName.frame) , 5, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:memberImage];
    
   UILabel *LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) + 2, 10, 29, 13)];
    LevelLabel.backgroundColor =MainColor;
    LevelLabel.text = @"lv.1";
    LevelLabel.textAlignment = NSTextAlignmentCenter;
    LevelLabel.layer.cornerRadius = 6;
    LevelLabel.layer.shouldRasterize = YES;
    LevelLabel.clipsToBounds = YES;
    LevelLabel.font = FONT(11);
    LevelLabel.textColor = [UIColor whiteColor];
    [backView addSubview:LevelLabel];
    
    
   UIButton *LikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    LikeButton.frame = CGRectMake(SCREEN_WIDTH - 60, 25, 50, 40);
    if ([model.isLike isEqualToString:@"N"]) {
        LikeButton.selected = NO;
    }else{
        LikeButton.selected = YES;
    }
    [LikeButton setImage:[UIImage imageNamed:@"喜欢未选中"] forState:UIControlStateNormal];
    [LikeButton setImage:[UIImage imageNamed:@"喜欢"] forState:UIControlStateSelected];
    LikeButton.clipsToBounds = YES;
    [backView addSubview:LikeButton];
    
   _ReportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _ReportButton.frame = CGRectMake(SCREEN_WIDTH - 40, 5, 20, 15);
    [_ReportButton setImage:[UIImage imageNamed:@"查看更多"] forState:UIControlStateNormal];
    [_ReportButton addTarget:self action:@selector(clickReport) forControlEvents:UIControlEventTouchUpInside];
    _ReportButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:_ReportButton];
    
    CGFloat labelWidth = [self getLabelWidthWithText:model.create_time width:SCREEN_WIDTH - 20 font:FONT(15)];
    
   UILabel *createTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(55,CGRectGetMaxY(UserName.frame) + 2, labelWidth, 10)];
    createTimeLabel.text = model.create_time;
    createTimeLabel.font = FONT(12);
    createTimeLabel.textColor =LYColor(218, 218, 218);
    [backView addSubview:createTimeLabel];
    
    
   UILabel *contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, CGRectGetMaxY(createTimeLabel.frame) , SCREEN_WIDTH - 80, 20)];
    contentLabel.text = model.content;
    contentLabel.font = FONT(13);
    contentLabel.numberOfLines = 0;
    
    [backView addSubview:contentLabel];
   
    return backView;
}

- (BOOL)isURL:(NSString *)url {
    if(url.length < 1)
        return NO;
    if (url.length>4 && [[url substringToIndex:4] isEqualToString:@"www."]) {
        url = [NSString stringWithFormat:@"http://%@",url];
    } else {
        url = url;
    }
    NSString *urlRegex = @"(https|http|ftp|rtsp|igmp|file|rtspt|rtspu)://((((25[0-5]|2[0-4]\\d|1?\\d?\\d)\\.){3}(25[0-5]|2[0-4]\\d|1?\\d?\\d))|([0-9a-z_!~*'()-]*\\.?))([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\.([a-z]{2,6})(:[0-9]{1,4})?([a-zA-Z/?_=]*)\\.\\w{1,5}";
    
    NSPredicate* urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegex];
    
    return [urlTest evaluateWithObject:url];
}
- (NSArray*)getURLFromStr:(NSString *)string {
    NSError *error;
    //可以识别url的正则表达式
    NSString *regulaStr = @"((http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)|(www.[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&*+?:_/=<>]*)?)";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    NSArray *arrayOfAllMatches = [regex matchesInString:string
                                                options:0
                                                  range:NSMakeRange(0, [string length])];
    
    //NSString *subStr;
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    
    for (NSTextCheckingResult *match in arrayOfAllMatches){
        NSString* substringForMatch;
        substringForMatch = [string substringWithRange:match.range];
        [arr addObject:substringForMatch];
    }
    return arr;
}
-(void)clickReport{
    [self.delegate clickMore];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
