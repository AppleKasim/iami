//
//  ShareTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/4/18.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainModel.h"


@protocol ShareCellDelegate <NSObject>

-(void)clickMore;
- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface ShareTableViewCell : UITableViewCell
//共有的
@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) TTTAttributedLabel *MainLabel;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UILabel *TimeLabel;
@property (nonatomic,strong) UILabel *ForWhoLabel;


@property (nonatomic,strong) UIButton *LikeButton;
@property (nonatomic,strong) UIButton *CommentButton;
@property (nonatomic,strong) UIButton *ForwardButton;
@property (nonatomic,strong) UIButton *CollectButton;


@property (nonatomic,strong) UIView *CommentView;
@property (nonatomic,strong) UILabel *Commentlabel;
@property (nonatomic,assign) CGFloat CellHeightt;
@property (nonatomic,strong) SZTextView *CommentTld;
@property (nonatomic,strong) UIButton *CommentBut;
@property (nonatomic,strong) UIButton *MoreCommentBut;
@property (nonatomic,strong) UIButton *ReportButton;

@property (nonatomic,strong) UIButton *MoreButton;

@property (nonatomic,copy) NSMutableArray *imageArray;

@property (nonatomic,weak) id<ShareCellDelegate> delegate;

//视频类型
//@property (nonatomic,strong) AVPlayer *player;
//@property (nonatomic,strong) ZFPlayerView *playerView;

//分享的用户
@property (nonatomic,strong) UIImageView *shareImageView;
@property (nonatomic,strong) UILabel *shareLabel;
@property (nonatomic,strong) UILabel *shareTimeLabel;
@property (nonatomic,strong) UILabel *shareLevelLabel;
@property (nonatomic,strong) UILabel *shareContentLabel;

@property (nonatomic,strong) NSString *isNoti;

@property (nonatomic, strong) UIImageView *videoImageView;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) NSIndexPath *indexPath;


-(void)setMode:(MainModel *)model;
-(void)setVideoMode:(MainModel *)model;
//-(void)setMovieMode:(MainModel *)model;
- (void)setDelegate:(id<ShareCellDelegate>)delegate withIndexPath:(NSIndexPath *)indexPath;
@end
