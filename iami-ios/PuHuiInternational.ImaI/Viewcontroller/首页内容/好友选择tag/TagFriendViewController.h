//
//  TagFriendViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"

@protocol TagFriDelegate <NSObject>

-(void)sendSelect:(NSString *)string;
@end
@interface TagFriendViewController : XTViewController

@property (nonatomic,weak) id<TagFriDelegate> delegate;
@end
