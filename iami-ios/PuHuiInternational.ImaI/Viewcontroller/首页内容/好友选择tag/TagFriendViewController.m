//
//  TagFriendViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "TagFriendViewController.h"
#import "TagFriedTableViewCell.h"

@interface TagFriendViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    UISearchBar *_searchBar;
    
    UITableView *MainTableView;
}
@property (nonatomic,copy) NSArray *dataArray;
@end

@implementation TagFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self creatSearchBar];
    [self creatTable];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardHide)];
    //设置成NO表示当前控件响应后会传播到其他控件上，默认为YES。
    tapGestureRecognizer.cancelsTouchesInView = NO;
    //将触摸事件添加到view上
    [self.view addGestureRecognizer:tapGestureRecognizer];
    
    // Do any additional setup after loading the view.
}
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(5, 45, SCREEN_WIDTH - 10, 280) style:UITableViewStylePlain];
    //MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    [MainTableView registerClass:[TagFriedTableViewCell class] forCellReuseIdentifier:@"cell"];
    MainTableView.layer.borderWidth = 1.0f;
    MainTableView.layer.borderColor =LYColor(247, 192, 116).CGColor;
    [self.view addSubview:MainTableView];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TagFriedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    TagFriModel *model = [[TagFriModel alloc]init];
    [cell setModel:model];
    [cell.UserSelect addTarget:self action:@selector(clickSelect:) forControlEvents:UIControlEventTouchUpInside];
    cell.tag = 100 + indexPath.row;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightFooterInSection:(NSInteger)section{
    return 40;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(5, 0, SCREEN_WIDTH - 10, 40)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIButton *confirmBut = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBut.frame = CGRectMake(SCREEN_WIDTH - 100, 10, 80, 30);
    [confirmBut setTitle:@"确定" forState:UIControlStateNormal];
    confirmBut.backgroundColor = LYColor(228, 187, 101);
    [confirmBut addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [confirmBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [view addSubview:confirmBut];
    
    UIButton *cancelBut = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBut.frame = CGRectMake(SCREEN_WIDTH - 190, 10, 80, 30);
    [cancelBut setTitle:@"取消" forState:UIControlStateNormal];
    cancelBut.backgroundColor = LYColor(223, 223, 223);
    [cancelBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [view addSubview:cancelBut];
    
    return view;
}
-(void)creatBack{
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(5, 24, 40, 31)];
    rightButton.titleLabel.font = Bold_FONT(17);
    rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -10);
    [rightButton setImage:[UIImage imageNamed:@"灰色返回"] forState:UIControlStateNormal];
    rightButton.titleLabel.frame = rightButton.frame;
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightButton addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightButton];
}
-(void)backBtnPressed{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
-(void)creatSearchBar{
    // _searchBar = [[UISearchBar alloc] init];
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 2, SCREEN_WIDTH - 20, 40)];
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.showsCancelButton = NO;
    _searchBar.delegate = self;
    _searchBar.tintColor = [UIColor orangeColor];
    _searchBar.placeholder = @"搜索感兴趣的内容";
    
    for (UIView *subView in _searchBar.subviews) {
        if ([subView isKindOfClass:[UIView  class]]) {
            [[subView.subviews objectAtIndex:0] removeFromSuperview];
            if ([[subView.subviews objectAtIndex:0] isKindOfClass:[UITextField class]]) {
                UITextField *textField = [subView.subviews objectAtIndex:0];
                textField.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
                
                //设置输入框边框的颜色
                // textField.layer.borderColor = [UIColor blackColor].CGColor;
                //textField.layer.borderWidth = 1;
                
                //设置输入字体颜色
                //                    textField.textColor = [UIColor lightGrayColor];
                
                //设置默认文字颜色
                UIColor *color = [UIColor grayColor];
                [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:@"搜索要标记的人"
                                                                                    attributes:@{NSForegroundColorAttributeName:color}]];
                //修改默认的放大镜图片
                //                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13, 13)];
                //                imageView.backgroundColor = [UIColor clearColor];
                //                imageView.image = [UIImage imageNamed:@"搜索放大镜"];
                //                textField.leftView = imageView;
            }
        }
    }
    [self.view addSubview:_searchBar];
    
}
#pragma 点击事件
-(void)clickSelect:(UIButton*)button{
    button.selected = !button.selected;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    [_searchBar becomeFirstResponder];
}
-(void)keyboardHide{
    [_searchBar resignFirstResponder];
    [_searchBar endEditing:YES];
}
-(void)confirm{
    if (self.delegate) {
         [self.delegate sendSelect:@"123"];
    }
    [self.navigationController popViewControllerAnimated:YES];
    
}
@end
