//
//  TagFriedTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "TagFriedTableViewCell.h"
//#define cellHeight 30
@implementation TagFriedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setFrame:(CGRect)frame {
    
    
    frame.size.width = SCREEN_WIDTH ;
    
    [super setFrame:frame];
}
-(void)setNewCModel:(CommentModel *)model{
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(3, 6, SCREEN_WIDTH -6, model.cellHeight + 41)];
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.backgroundColor = LYColor(240, 240, 240);
    
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(20-imageW, 5 - imageW, 30 + imageW * 2, 30+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:leveImage];
    
//    UIImageView  *UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 30, 30)];
//    [UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
//    UserImage.layer.cornerRadius = 15;
//    UserImage.clipsToBounds = YES;
//    UserImage.contentMode = UIViewContentModeScaleAspectFit;
//    [leveImage addSubview:UserImage];
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 30, 30)];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 15;
    _UserImage.clipsToBounds = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    [leveImage addSubview:_UserImage];
    
    CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH - 10 * 2 font:FONT(14)];
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, namelHeght, 20)];
    _UserName.text = model.UserName;
    _UserName.textColor = NameColor;
    _UserName.font = FONT(14);
    _UserName.textAlignment =NSTextAlignmentLeft;
    [backView addSubview:_UserName];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 6, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:memberImage];
    
    _LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) + 2, 11, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [backView addSubview:_LevelLabel];
    
    
    _LikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _LikeButton.frame = CGRectMake(SCREEN_WIDTH - 70, 25, 50, 40);
    if ([model.isLike isEqualToString:@"N"]) {
        _LikeButton.selected = NO;
    }else{
        _LikeButton.selected = YES;
    }
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢未选中"] forState:UIControlStateNormal];
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢"] forState:UIControlStateSelected];
    _LikeButton.clipsToBounds = YES;
    [backView addSubview:_LikeButton];
    
    _ReportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _ReportButton.frame = CGRectMake(SCREEN_WIDTH - 40, 5, 20, 15);
    [_ReportButton setImage:[UIImage imageNamed:@"查看更多"] forState:UIControlStateNormal];
    _ReportButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:_ReportButton];
    
    CGFloat labelWidth = [self getLabelWidthWithText:model.create_time width:SCREEN_WIDTH - 20 font:FONT(15)];
    
    _createTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(55,CGRectGetMaxY(_UserName.frame) + 2, labelWidth, 10)];
    _createTimeLabel.text = model.create_time;
    _createTimeLabel.font = FONT(12);
    _createTimeLabel.textColor =LYColor(218, 218, 218);
    [backView addSubview:_createTimeLabel];
    
    CGFloat height =   [self getLabelHeightWithText:model.content width:SCREEN_WIDTH - 80 font:Bold_FONT(12)];
    
    _contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, CGRectGetMaxY(_createTimeLabel.frame) , SCREEN_WIDTH - 80, height)];
    _contentLabel.text = model.content;
    _contentLabel.font = Bold_FONT(12);
    _contentLabel.numberOfLines = 0;
    [backView addSubview:_contentLabel];
    
    [self.contentView addSubview:backView];
}
-(void)setCModel:(CommentModel *)model{
   self.backgroundColor = [UIColor whiteColor];
    
    UIView *backView = [[UIView alloc]initWithFrame:CGRectMake(3, 6, SCREEN_WIDTH - 26, model.cellHeight + 41)];
    backView.layer.cornerRadius = 8;
    backView.clipsToBounds = YES;
    backView.userInteractionEnabled = YES;
    backView.backgroundColor = LYColor(240, 240, 240);
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 30, 30)];
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",@"",model.UserImage]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    _UserImage.layer.cornerRadius = 15;
    _UserImage.clipsToBounds = YES;
    _UserImage.userInteractionEnabled = YES;
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:_UserImage];
    
     CGFloat namelHeght  =  [self getLabelWidthWithText:model.UserName width:SCREEN_WIDTH - 10 * 2 font:FONT(14)];
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, namelHeght, 20)];
    _UserName.text = model.UserName;
    _UserName.textColor = NameColor;
    _UserName.font = FONT(14);
    _UserName.textAlignment =NSTextAlignmentLeft;
    [backView addSubview:_UserName];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 8, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [backView addSubview:memberImage];
    
    _LevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) + 2, 10, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 10;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [backView addSubview:_LevelLabel];
    
    
    _LikeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _LikeButton.frame = CGRectMake(SCREEN_WIDTH - 70, 5, 50, 40);
    if ([model.isLike isEqualToString:@"N"]) {
        _LikeButton.selected = NO;
    }else{
        _LikeButton.selected = YES;
    }
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢未选中"] forState:UIControlStateNormal];
    [_LikeButton setImage:[UIImage imageNamed:@"喜欢"] forState:UIControlStateSelected];
    _LikeButton.clipsToBounds = YES;
    [backView addSubview:_LikeButton];
    
    CGFloat labelWidth = [self getLabelWidthWithText:model.create_time width:SCREEN_WIDTH - 20 font:FONT(15)];
    
    _createTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(55,CGRectGetMaxY(_UserName.frame) + 2, labelWidth, 10)];
    _createTimeLabel.text = model.create_time;
    _createTimeLabel.font = FONT(12);
    _createTimeLabel.textColor =LYColor(218, 218, 218);
    [backView addSubview:_createTimeLabel];
    
    
    _contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, CGRectGetMaxY(_createTimeLabel.frame) , SCREEN_WIDTH - 80, 20)];
    _contentLabel.text = model.content;
    _contentLabel.font = Bold_FONT(13);
    _contentLabel.numberOfLines = 0;
    [backView addSubview:_contentLabel];
    
    [self.contentView addSubview:backView];
    
}
-(void)setModel:(TagFriModel *)model{
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 20, 20)];
    _UserImage.clipsToBounds = YES;
    _UserImage.image = [UIImage imageNamed:@"avatar"];
    _UserImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_UserImage];
    
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, 120, 20)];
    _UserName.text = @"霹雳可爱萌萌哒";
    _UserName.font = FONT(12);
    _UserName.textAlignment =NSTextAlignmentLeft;
    [self addSubview:_UserName];

    _UserSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    _UserSelect.frame  = CGRectMake(SCREEN_WIDTH - 25, 10, 10, 10);
    [_UserSelect setImage:[UIImage imageNamed:@"好友未选择"] forState:UIControlStateNormal];
    [_UserSelect setImage:[UIImage imageNamed:@"好友选择"] forState:UIControlStateSelected];
    [self addSubview:_UserSelect];

    _FollowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _FollowButton.frame = CGRectMake(SCREEN_WIDTH - 90, 5, 60, 20);
    _FollowButton.backgroundColor = LYColor(237, 204, 105);
    [_FollowButton setTitle:@"追踪" forState:UIControlStateNormal];
    _FollowButton.hidden=  YES;
    [self addSubview:_FollowButton];
}

-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.height;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
