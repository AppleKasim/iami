//
//  CommentModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject
@property (nonatomic,strong) NSString *UserImage;
@property (nonatomic,strong) NSString *content;
@property (nonatomic,strong) NSString *UserName;
@property (nonatomic,strong) NSString *comment_id;
@property (nonatomic,assign) CGFloat cellHeight;
@property (nonatomic,strong) NSString *isLike;
@property (nonatomic,strong) NSString *member_id;
@property (nonatomic,strong) NSString *create_time;

+(CommentModel *)parsenWithDictionary:(NSDictionary *)dict;
@end
