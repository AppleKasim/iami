//
//  TagFriedTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagFriModel.h"
#import "CommentModel.h"

@interface TagFriedTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *UserImage ;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) UIButton *UserSelect;
@property (nonatomic,strong) UIButton *FollowButton;
@property (nonatomic,strong) UIButton *LikeButton;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UIButton *ReportButton;


@property (nonatomic,strong) UILabel *contentLabel;
@property (nonatomic,strong) UILabel *createTimeLabel;

-(void)setModel:(TagFriModel *)model;
-(void)setCModel:(CommentModel *)model;
-(void)setNewCModel:(CommentModel *)model;
@end
