//
//  TagFriModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/21.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagFriModel : NSObject

@property (nonatomic,copy) NSString *ImageUrl;
@property (nonatomic,copy) NSString *UserName;
@property (nonatomic,assign) BOOL *isSelect;
@end
