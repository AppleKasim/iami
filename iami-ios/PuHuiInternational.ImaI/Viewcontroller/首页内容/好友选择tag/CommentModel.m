//
//  CommentModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "CommentModel.h"

@implementation CommentModel
+(CommentModel *)parsenWithDictionary:(NSDictionary *)dict{
    CommentModel *model = [[CommentModel alloc]init];
    model.content = [NetDataCommon stringFromDic:dict forKey:@"content"];
    NSDictionary *UserDic = dict[@"member"];
    model.UserName = [NetDataCommon stringFromDic:UserDic forKey:@"nickname"];
    model.UserImage = [NetDataCommon stringFromDic:UserDic forKey:@"avatar"];
    model.cellHeight =  [model getLabelHeightWithText:model.content width:SCREEN_WIDTH - 80 font:FONT(12)];
    model.comment_id = [NetDataCommon stringFromDic:dict forKey:@"sn"];
    model.isLike = [NetDataCommon stringFromDic:dict forKey:@"isLike"];
    model.member_id = [NetDataCommon stringFromDic:dict forKey:@"member_id"];
    model.create_time = [NetDataCommon stringFromDic:dict forKey:@"create_time"];
    return model;
    
}
-(CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.height;
}
@end
