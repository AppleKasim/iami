//
//  MainViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MainViewController.h"
#import "MainTableViewCell.h"
#import "VideoTableViewCell.h"//video的cell
#import "FriendInformationViewController.h"//好友个人主页
#import "TagFriendViewController.h"//tag好友选择页面
#import "TagFriedTableViewCell.h"
#import "ShareFriendViewController.h"//分享页面
#import "ShortMovieViewController.h"//五秒影片
#import "MainUserViewController.h"//个人主页
#import "SearchViewController.h"//search
#import "ShareTableViewCell.h"//分享cell
#import "DeclareAbnormalAlertView.h"//弹出分享框
#import "WeChat.h"
#import "EditorView.h"
#import "Messages+CoreDataClass.h"
#import "TextMessageTableViewCell.h"
#import "SearchTableViewCell.h"
#import "ViewController.h"
#import "PuHuiInternational_ImaI-Swift.h"

@interface MainViewController ()<UITableViewDelegate,UITableViewDataSource,TagFriDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ZYQAssetPickerControllerDelegate,UISearchBarDelegate,DeclareAbnormalAlertViewDelegate,UISearchDisplayDelegate,TTTAttributedLabelDelegate,mainVideoCellDelegate,mainCellDelegate,ShareCellDelegate>
{
    UITableView *MainTableView;
    
    UITableView *likeTableview;
    UIButton *ChargeButton;
    UIButton *GetMoney;
    
    TagFriendViewController *tag;
    UIView *ForgetPwdView;
    UIView *SubView;
    
    SZTextView *Maintext;
    NSString *MainString;
    NSInteger Page;
    ZYQAssetPickerController *picker;
    
    SZTextView *CurText;
    MainModel *currentShareModel;
    UISearchBar *_searchBar;//搜索栏
    
    UIImagePickerController *_imagePickerController;
    
    SZTextView *editText;
    
    UIView *UploadView;
    BOOL searchShow;
    
    UISearchDisplayController *searchDisplayController;

    UITableView *searchTableview;
    
    dispatch_group_t downloadGroup;
    
    BOOL isSeenAlert;
    NSString *currenyDay;


}
@property (nonatomic,copy) NSMutableArray *dataArray;

@property (nonatomic,copy) NSMutableArray *DataArray;

@property (nonatomic,copy) NSMutableArray *imageDataArray;

@property (nonatomic,copy) NSMutableArray *videoDataArray;

@property (nonatomic,copy) NSMutableArray *NewImageArray;

@property (nonatomic,copy) NSURL *FilePath;//本地视频地址

@property (nonatomic,assign)  CGFloat SmallimageHeight;

@property (nonatomic,copy) NSMutableArray *SearchdataArr;

@property (nonatomic, strong) ZFPlayerController *player;
@property (nonatomic, strong) ZFPlayerControlView *controlView;
@property (nonatomic, strong) NSMutableArray *urls;
@property (nonatomic, strong) ZFAVPlayerManager *playerManager;


//@property(nonatomic,strong)ZFPlayerView *playerView;
//
//@property(nonatomic,strong)ZFPlayerModel *playerModel;

#define PostUrl @"/post/doPost" //发布消息
#define GetUrl @"/post/getPosts" //获得所有发文
#define UploadPic @"/post/uploadPicture"//上传图片
#define UploadMov @"/post/uploadMovie"//上传影片
#define LikeUrl @"/like/togglePostLike"//贴文按赞
#define CommentUrl @"/post/doComment"//评论
#define CollectPost @"/post/collectPost"//收藏帖文
#define DeletePost @"/post/delPost"//删除帖文
#define EDPost @"/post/editPost"//编辑帖文
#define searchurl @"/page/memberQuery"
#define LoginUrl @"/api/doLogin"

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Page = 1;
    searchShow = NO;
    isSeenAlert = NO;
    currenyDay = @"";
    _SmallimageHeight = 0;
    _dataArray = [[NSMutableArray alloc]init];
    _DataArray = [[NSMutableArray alloc]init];
    _imageDataArray = [[NSMutableArray alloc]init];
    _videoDataArray = [[NSMutableArray alloc]init];
    _NewImageArray = [[NSMutableArray alloc]init];
    _SearchdataArr = [[NSMutableArray alloc]init];
    [self creatTabButton];
       
    tag =[[TagFriendViewController alloc]init];
    tag.delegate = self;

    [self getCSRF_Token];
    // Do any additional setup after loading the view.
   
}

- (NSMutableArray *)urls {
        _urls = @[].mutableCopy;
        for (MainModel *model in _DataArray) {
            if (model.videoPath) {

                NSString *URLString = [model.videoPath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                NSURL *url = [NSURL URLWithString:URLString];
                [_urls addObject:url];
            } else {
                NSString *URLString = [@"" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
                NSURL *url = [NSURL URLWithString:URLString];
                [_urls addObject:url];
            }
        }
    return _urls;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (BOOL)shouldAutorotate {
    return self.player.shouldAutorotate;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     self.navigationController.navigationBar.hidden = NO;
    if (searchShow) {
        [self showSearch];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(toUp) name:@"tabbarReloadMain" object:nil];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"newMessage" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadComment:) name:@"CommentSuccess" object:nil
     ];
    [_searchBar resignFirstResponder];
     [searchTableview removeFromSuperview];
    if (!MainTableView && _DataArray.count > 0) {
        [self creatTable];
        [MainTableView reloadData];
    }

    self.player = [ZFPlayerController playerWithScrollView:MainTableView playerManager:_playerManager containerViewTag:100];
    self.player.controlView = self.controlView;
    self.player.shouldAutoPlay = NO;
    self.player.assetURLs = self.urls;
    /// 1.0是完全消失的时候
    self.player.playerDisapperaPercent = 1.0;

    @weakify(self)
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
        //[UIViewController attemptRotationToDeviceOrientation];
        //self->MainTableView.scrollsToTop = !isFullScreen;
    };

    if ([self isNextDay]) {
        isSeenAlert = NO;
    }

    if (isSeenAlert == NO) {
        [self isSignInToday];
    }
}

-(BOOL)isNextDay{
    if ([currenyDay isEqualToString:@""]) {
        NSDate *currentDate = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd";
        currenyDay = [formatter stringFromDate:currentDate];
        return YES;
    }

    NSDate *currentDate = [NSDate date];
    //NSString *string = @"2017-11-14 16:14:00";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    //设置日期格式
    formatter.dateFormat = @"dd";
    //字符串转为date对象
    NSString *currentDateString = [formatter stringFromDate:currentDate];

    if (![currenyDay isEqualToString:currentDateString]) {
        return YES;
    } else {
        return NO;
    }


    //利用NSCalendar处理日期
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:date];
//
//    NSLog(@"%zd", month);
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [_searchBar removeFromSuperview];
    self.player = nil;
}

-(void)reloadComment:(NSNotification *)noti{
    NSDictionary *dic = noti.userInfo;
    for (MainModel *model in _DataArray) {
        if ([model.post_id isEqualToString:dic[@"post_id"]]) {
            model.Comment = [NSString stringWithFormat:@"%@",dic[@"count"]];
            NSLog(@"%@",model.Comment);
            [MainTableView reloadData];
        }
    }
}
-(void)new:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
    if ([[dic allKeys]containsObject:@"notice"]) {
        [self loadNotiCount];
        //InformViewController *info = self.tabBarController.childViewControllers[2];
        //info.tabBarItem.badgeValue = [NSString stringWithFormat:@"1"];
    }else if ([[dic allKeys]containsObject:@"chat"]){
        


        NSDictionary *json = dic;
        //接收新消息，在页面刷新
        NSDate* date = [NSDate date];
        
        NSInteger i = 0;

        //:)
        //face_icon.png
        if ([json[@"msg"] isEqualToString:@":)"]) {
            i = 1;
        }
        NewMessage* meModel = [[WeChat sharedManager] insertRecordInRecordTable:@{
                                                                                  kdb_Messages_message : json[@"msg"],
                                                                                  kdb_Messages_sender : @2,
                                                                                  kdb_Messages_sendTime : date,
                                                                                  kdb_Messages_showSendTime : @(YES),
                                                                                  kdb_Messages_messageType : @(i)
                                                                                  ,
                                                                                  kdb_Messages_member_id:json[@"master"]
                                                                                  ,
                                                                                  kdb_Messages_avatar:json[@"chat"][@"avatar"],
                                                                                  kdb_Messages_nickname:json[@"chat"][@"nickname"]}];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadChatNew" object:meModel];
        [self loadMessCount];
//        MessageViewController *mess = self.tabBarController.childViewControllers[3];
//        
//        mess.tabBarItem.badgeValue = [NSString stringWithFormat:@"1"];

    }
    else if ([[dic allKeys]containsObject:@"invite"]){
        [self loadInviteData];
//        InviteViewController *inte = self.tabBarController.childViewControllers[1];
//        inte.tabBarItem.badgeValue = @"1";

    }
}
-(void)toUp{
    CGPoint offset = MainTableView.contentOffset;
    offset.y =  - MainTableView.contentInset.top;
    [MainTableView setContentOffset:offset animated:YES];
}

-(void)sendSelect:(NSString *)string{
    if (string && string.length > 0) {
        [_dataArray addObject:string];
        [MainTableView reloadData];
    }
}
//创建tabbar按钮
-(void)creatTabButton{
    UIImage *rightImage = [[UIImage imageNamed:@"直播.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(0, 0, 35, 32);
    [liveButton setImage:[UIImage imageNamed:@"五秒视频.png"] forState:UIControlStateNormal];
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    [liveButton addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImage *searchImage = [[UIImage imageNamed:@"搜索.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UIBarButtonItem *but1 = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(live)];
    UIBarButtonItem *but3 = [[UIBarButtonItem alloc]initWithCustomView:liveButton];
    
    self.navigationItem.leftBarButtonItems = @[but1,but3];
   self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:searchImage style:UIBarButtonItemStylePlain target:self action:@selector(creatSearchBar)];
}
-(void)live{
    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    MainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    if (scrollView != searchTableview) {
        [_searchBar resignFirstResponder];
        [searchTableview removeFromSuperview];
    }
   
//    NSArray  *indexs =  [MainTableView indexPathsForVisibleRows];
//    for (NSIndexPath *indexpath in indexs) {
//        NSLog(@"%ld",indexpath.row);
//        UITableViewCell *cell = [MainTableView cellForRowAtIndexPath:indexpath];
//
//        if ([cell isKindOfClass:[MainTableViewCell class]]) {
//            MainTableViewCell *cel = (MainTableViewCell *)cell;
//             [self comment:cel.CommentButton];
//        }else if ([cell isKindOfClass:[VideoTableViewCell class]]){
//            VideoTableViewCell *cel = (VideoTableViewCell *)cell;
//            [self comment:cel.CommentButton];
//        }else if ([cell isKindOfClass:[ShareTableViewCell class]]){
//            ShareTableViewCell *cel = (ShareTableViewCell *)cell;
//            [self comment:cel.CommentButton];
//        }
//    }
    
    
}

#pragma  mark - TableView delegeta && datasource
-(void)creatTable{
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -  Height_NavBar - Height_TabBar ) style:UITableViewStyleGrouped];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    MainTableView.fd_debugLogEnabled = YES;
    
    [MainTableView registerClass:[MainTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[VideoTableViewCell class] forCellReuseIdentifier:@"videocell"];
    [MainTableView registerClass:[ShareTableViewCell class] forCellReuseIdentifier:@"sharecell"];
    
    
    MJRefreshStateHeader *header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(refresh)];
    MainTableView.mj_header = header;
    
    MJRefreshAutoFooter *fotter = [MJRefreshAutoFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadDataMore)];
    MainTableView.mj_footer = fotter;
    [self.view addSubview:MainTableView];

    _playerManager = [[ZFAVPlayerManager alloc] init];

    /// player的tag值必须在cell里设置
    self.player = [ZFPlayerController playerWithScrollView:MainTableView playerManager:_playerManager containerViewTag:100];
    self.player.controlView = self.controlView;
    self.player.shouldAutoPlay = NO;
    /// 1.0是完全消失的时候
    self.player.playerDisapperaPercent = 1.0;

    @weakify(self)
    self.player.orientationWillChange = ^(ZFPlayerController * _Nonnull player, BOOL isFullScreen) {
        @strongify(self)
        [self setNeedsStatusBarAppearanceUpdate];
        [UIViewController attemptRotationToDeviceOrientation];
        //self->MainTableView.scrollsToTop = !isFullScreen;
    };
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == searchTableview) {
        return _SearchdataArr.count;
    }
    return _DataArray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:MainTableView]) {
        MainModel *model = _DataArray[indexPath.row];
        if ([model.post_type isEqualToString:@"video"]) {

            VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"videocell" forIndexPath:indexPath];
  
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }

            cell.opaque = YES;
           [cell setMode:model];

            cell.CommentTld.delegate = self;
            cell.MainLabel.delegate = self;
            cell.delegate = self;
            [cell setDelegate:self withIndexPath:indexPath];
            
            
            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
            [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
           // [cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            
            
           
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
            
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap1];
            
            
            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
             cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            [cell.UserName addGestureRecognizer:tap];
            cell.ReportButton.tag = 200 + indexPath.row;
            cell.CommentTld.tag = 300 + indexPath.row;
            cell.MoreCommentBut.tag = 200 + indexPath.row;
            cell.CommentBut.tag = 200 + indexPath.row;
            cell.CommentButton.tag = 200 + indexPath.row;
            cell.LikeButton.tag = 200 + indexPath.row;
            cell.UserImage.tag = 200 + indexPath.row;
            cell.ForwardButton.tag = 200 + indexPath.row;
            cell.CollectButton.tag = 200 + indexPath.row;
            cell.UserName.tag = 200 +indexPath.row;
            cell.MoreButton.tag = 200 + indexPath.row;
           
            
            
            return cell;
        }
        else if ([model.post_type isEqualToString:@"share"]){
            ShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sharecell" forIndexPath:indexPath];

            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            cell.opaque = YES;

            if ([model.shareType isEqualToString:@"video"]) {
                [cell setVideoMode:model];
            }else{
                [cell setMode:model];
            }
           
//            if (model.isShowCommentV == YES) {
//                cell.CommentView.hidden = NO;
//            }else{
//                cell.CommentView.hidden = YES;
//            }
            cell.CommentTld.delegate = self;
            cell.MainLabel.delegate = self;
            cell.delegate = self;
            [cell setDelegate:self withIndexPath:indexPath];
            
            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//点击评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
            [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
            
           
            
            
            
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
            [cell.shareImageView addGestureRecognizer:tap1];
            
             UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckShareInformation:)];
            [cell.shareLabel addGestureRecognizer:tap3];
            
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
            
            UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap2];
            
            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
            cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            
            cell.ReportButton.tag = 200 + indexPath.row;
            cell.CommentTld.tag = 300 + indexPath.row;
            cell.MoreCommentBut.tag = 200 + indexPath.row;
            cell.CommentBut.tag = 200 + indexPath.row;
            cell.CommentButton.tag = 200 + indexPath.row;
            cell.LikeButton.tag = 200 + indexPath.row;
            cell.UserImage.tag = 200 + indexPath.row;
           // cell.UserImage.tag = 200 + indexPath.row;
           
            cell.UserName.tag = 200 + indexPath.row;
            cell.shareImageView.tag = 200 + indexPath.row;
             cell.shareLabel.tag = 200 + indexPath.row;
            cell.ForwardButton.tag = 200 + indexPath.row;
            cell.CollectButton.tag = 200 + indexPath.row;
            cell.MoreButton.tag = 200 + indexPath.row;
            
//            if (cell &&_DataArray.count > 0) {
//                [self comment:cell.CommentButton];
//            }
            return cell;
        }
        else{
            
            MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            
            cell.opaque = YES;

            [cell setModel:model];

           
            cell.CommentTld.delegate = self;
            cell.MainLabel.delegate = self;
            cell.delegate = self;

            [cell.LikeButton addTarget:self action:@selector(checkLike:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.ForwardButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
            [cell.CommentBut addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];//发布评论
            [cell.MoreCommentBut addTarget:self action:@selector(checkMoreComment:) forControlEvents:UIControlEventTouchUpInside];//查看更多评论
            [cell.CollectButton addTarget:self action:@selector(collectPost:) forControlEvents:UIControlEventTouchUpInside];
            [cell.MoreButton addTarget:self action:@selector(EditPost:) forControlEvents:UIControlEventTouchUpInside];
             //[cell.ReportButton addTarget:self action:@selector(reporeComment:) forControlEvents:UIControlEventTouchUpInside];
           
            
            
           
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserImage addGestureRecognizer:tap];
           
             UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CheckInformation:)];
            [cell.UserName addGestureRecognizer:tap1];
            
            UITapGestureRecognizer *forWho = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkWhoInformation:)];
            [cell.ForWhoLabel addGestureRecognizer:forWho];
            cell.ForWhoLabel.tag = 200 + indexPath.row;
            
            cell.ReportButton.tag = 200 + indexPath.row;
            cell.CommentTld.tag = 300 + indexPath.row;
            cell.MoreCommentBut.tag = 200 + indexPath.row;
            cell.CommentBut.tag = 200 + indexPath.row;
            cell.CommentButton.tag = 200 + indexPath.row;
            cell.LikeButton.tag = 200 + indexPath.row;
            cell.UserImage.tag = 200 + indexPath.row;
            cell.UserName.tag = 200 +indexPath.row;
            cell.ForwardButton.tag = 200 + indexPath.row;
             cell.CollectButton.tag = 200 + indexPath.row;
            cell.MoreButton.tag = 200 + indexPath.row;
            
//            if (cell &&_DataArray.count > 0) {
//                [self comment:cell.CommentButton];
//            }
            return cell;
        }
       
    }else if([tableView isEqual:likeTableview]){
        TagFriedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"tagcell" forIndexPath:indexPath];
        TagFriModel *model = [[TagFriModel alloc]init];
        [cell setModel:model];
        cell.UserSelect.hidden = YES;
        cell.FollowButton.hidden = NO;
        return cell;
    }
    else if ([tableView isEqual: searchTableview]){
        SearchModel *model = _SearchdataArr[indexPath.row];
        
        static NSString *CellIdentifier = @"search";
        SearchTableViewCell  *cell = (SearchTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        if (cell == nil)
        {
            cell = [[SearchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if (tableView == searchTableview) {
            [cell setModel:model];
        }
        
        return cell;
    }
   
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == MainTableView) {
        MainModel *main =_DataArray[indexPath.row];
        return main.cellHeight + main.CommentHeight;

    }else if (tableView == searchTableview){
            return 50;
    }
    else{
        return 40;
    }
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == MainTableView) {
        MainModel *main =_DataArray[indexPath.row];
        return main.cellHeight + main.CommentHeight;
//        if (main.isShowCommentV) {
//            return main.cellHeight + main.CommentHeight;
//        }else{
//            return   main.cellHeight;
//        }
    }
    else{
        return 40;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == searchTableview) {
        return 0;
    }
    if ( _imageDataArray.count > 0 || _videoDataArray.count>0) {
        NSLog(@"%f",110 + _SmallimageHeight + 30);
      return  110 + 70;
    }else{
         return 110;
    }
   
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == MainTableView) {
        if ( _imageDataArray.count > 0 || _videoDataArray.count > 0) {
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
            view.backgroundColor = [UIColor whiteColor];
           Maintext = [[SZTextView alloc]initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH - 30, 60)];
            if (MainString && MainString.length > 0) {
                Maintext.text = MainString;
            }else{
                 Maintext.placeholder = NSLocalizedString(@"有什么新鲜事", nil);
            }
            Maintext.delegate = self;
            Maintext.autocapitalizationType = UITextAutocapitalizationTypeNone;
            Maintext.layer.borderWidth = 1.0f;
            Maintext.font = FONT(19);
            Maintext.layer.borderColor = MainColor.CGColor;
            Maintext.layer.cornerRadius = 8;
            [view addSubview:Maintext];
            
            UploadView = [[UIView alloc]initWithFrame:CGRectMake(15, 72, 0, 3)];
            UploadView.backgroundColor = LYColor(94, 194, 248);
            [view addSubview:UploadView];
            
            UIButton *cameraBut = [UIButton buttonWithType:UIButtonTypeCustom];
            cameraBut.frame = CGRectMake(15, 77, 30, 30);
            [cameraBut setImage:[UIImage imageNamed:@"相机"] forState:UIControlStateNormal];
            [cameraBut addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:cameraBut];
            
            UIButton *camera1But = [UIButton buttonWithType:UIButtonTypeCustom];
            camera1But.frame = CGRectMake(CGRectGetMaxX(cameraBut.frame) + 10, 77, 34, 25);
            [camera1But setImage:[UIImage imageNamed:@"摄像"] forState:UIControlStateNormal];
            [camera1But addTarget:self action:@selector(Video) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:camera1But];
            
            UIButton *pagBut = [UIButton buttonWithType:UIButtonTypeCustom];
            pagBut.frame = CGRectMake(CGRectGetMaxX(camera1But.frame) + 10, 77, 25, 25);
            [pagBut setImage:[UIImage imageNamed:@"标签"] forState:UIControlStateNormal];
            [pagBut addTarget:self action:@selector(tagfriend) forControlEvents:UIControlEventTouchUpInside];
            //[view addSubview:pagBut];
            
            UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
            sendButton.frame = CGRectMake(SCREEN_WIDTH - 70, 77, 60, 25);
            [sendButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
            sendButton.titleLabel.font  =FONT(14);
            sendButton.backgroundColor = MainColor;
            sendButton.layer.cornerRadius = 10;
            sendButton.clipsToBounds = YES;
            [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:sendButton];
            
            UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.frame = CGRectMake(SCREEN_WIDTH - 140, 77, 60, 25);
            [cancelButton setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
            [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
            cancelButton.titleLabel.font  =FONT(14);
            cancelButton.backgroundColor = LYColor(223, 223, 223);
            cancelButton.layer.cornerRadius = 10;
            cancelButton.clipsToBounds = YES;
            [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [view addSubview:cancelButton];
            


            CGFloat imageWidth = (SCREEN_WIDTH - 20 - 45) / 9;
            _SmallimageHeight = imageWidth;
            
            if (_videoDataArray.count > 0) {
                UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(55 , 115, imageWidth , imageWidth )];
                image.image = [UIImage imageNamed:@"默认播放按钮"];
                [view addSubview:image];
            }else{
                
                for (NSInteger i = 0; i < _imageDataArray.count; i++) {
                    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(10 + (imageWidth + 5) * i  , 115, imageWidth , imageWidth )];
                    id dataArray =_imageDataArray[i];

                    image.image = [UIImage imageWithData:dataArray];
                    image.clipsToBounds = YES;
                    image.userInteractionEnabled = YES;
                    image.backgroundColor = [UIColor blackColor];
                    image.contentMode = UIViewContentModeScaleAspectFit;
                    image.userInteractionEnabled = YES;
                    image.tag = 920 + i;
                    [image addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(watchImage:)]];
                    [view addSubview:image];
                    
                    UIButton *deleBut = [UIButton buttonWithType:UIButtonTypeCustom];
                    deleBut.frame = CGRectMake(CGRectGetMaxX(image.frame) - 10,image.frame.origin.y - 5, 15, 15);
                    [deleBut setImage:[UIImage imageNamed:@"图片删除按钮"] forState:UIControlStateNormal];
                    deleBut.imageView.contentMode = UIViewContentModeScaleAspectFit;
                    [deleBut addTarget:self action:@selector(deleteImage:)
                      forControlEvents:UIControlEventTouchUpInside];
                    deleBut.tag = 900 + i;
                    [view addSubview:deleBut];
                }
            }
            

            return view;
        }else{
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200)];
            view.backgroundColor = [UIColor whiteColor];
            Maintext = [[SZTextView alloc]initWithFrame:CGRectMake(15, 10, SCREEN_WIDTH - 30, 60)];
            if (MainString && MainString.length > 0) {
                Maintext.text = MainString;
            }else{
                Maintext.placeholder = NSLocalizedString(@"有什么新鲜事", nil);
            }
            Maintext.layer.borderWidth = 1.0f;
            Maintext.font = FONT(19);
            Maintext.autocapitalizationType = UITextAutocapitalizationTypeNone;
            Maintext.delegate = self;

            Maintext.layer.borderColor = MainColor.CGColor;
            Maintext.layer.cornerRadius = 8;
            [view addSubview:Maintext];
            
            UIButton *cameraBut = [UIButton buttonWithType:UIButtonTypeCustom];
            cameraBut.frame = CGRectMake(15, 77, 25, 25);
            [cameraBut addTarget:self action:@selector(camera) forControlEvents:UIControlEventTouchUpInside];
            [cameraBut setImage:[UIImage imageNamed:@"相机"] forState:UIControlStateNormal];
            [view addSubview:cameraBut];
            
            UIButton *camera1But = [UIButton buttonWithType:UIButtonTypeCustom];
            camera1But.frame = CGRectMake(15+30, 77, 25, 25);
            [camera1But setImage:[UIImage imageNamed:@"摄像"] forState:UIControlStateNormal];
            [camera1But addTarget:self action:@selector(Video) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:camera1But];
            
            UIButton *pagBut = [UIButton buttonWithType:UIButtonTypeCustom];
            pagBut.frame = CGRectMake(15+30 + 30, 77, 25, 25);
            [pagBut setImage:[UIImage imageNamed:@"标签"] forState:UIControlStateNormal];
            [pagBut addTarget:self action:@selector(tagfriend) forControlEvents:UIControlEventTouchUpInside];
           // [view addSubview:pagBut];
            
            UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
            sendButton.frame = CGRectMake(SCREEN_WIDTH - 70, 77, 60, 25);
            [sendButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
            sendButton.titleLabel.font  =FONT(14);
            sendButton.backgroundColor = MainColor;
            sendButton.layer.cornerRadius = 10;
            sendButton.clipsToBounds = YES;
            [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
             [sendButton addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:sendButton];
            
            UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
            cancelButton.frame = CGRectMake(SCREEN_WIDTH - 140, 77, 60, 25);
            [cancelButton setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
            cancelButton.titleLabel.font  =FONT(14);
            cancelButton.backgroundColor = LYColor(223, 223, 223);
            cancelButton.layer.cornerRadius = 10;
            cancelButton.clipsToBounds = YES;
            [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
            [cancelButton setTitleColor:[UIColor whiteColor]
                           forState:UIControlStateNormal];
            [view addSubview:cancelButton];
            
            return view;
        }
    }else{
        return nil;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (tableView == searchTableview) {
        if (_SearchdataArr.count == 0) {
            return;
        }
        SearchModel *model =_SearchdataArr[indexPath.row];
        MainUserViewController *controller = [[MainUserViewController alloc]init];
        controller.member_id = model.member_id;
        controller.Username = model.nickname;
        controller.ImageUrl = model.avatar;
        controller.m_showBackBt = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
}

#pragma  mark  - 按钮点击事件
#pragma mark 点击网页
- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
//    ViewController *web = [[ViewController alloc]init];
//
//    NSString *stering = url.absoluteString;
//     web.url = stering;
//    //self.viewC = [[MainViewController alloc]init];
//    [self.navigationController pushViewController:web animated:YES];
    //NSURL* url = [[ NSURL alloc ] initWithString :@"http://www.baidu.com"];
    [[UIApplication sharedApplication ] openURL: url];

}
-(void)deleteImage:(UIButton *)button{
    [_imageDataArray removeObjectAtIndex:button.tag - 900];
    [MainTableView reloadData];
}
-(void)tagfriend{
    tag.m_showBackBt = YES;
    [tag setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self.navigationController pushViewController:tag animated:YES];
}
-(void)watchImage:(UITapGestureRecognizer *)tap{
     [XLPhotoBrowser showPhotoBrowserWithImages:_imageDataArray currentImageIndex:tap.view.tag - 920];
}
/**
 点击取消发文
 */
#pragma mark - 点击取消发文
-(void)cancel{
    [_dataArray removeAllObjects];
    [self.videoDataArray removeAllObjects];
    [self.imageDataArray removeAllObjects];
    MainString = nil;
    [UIView animateWithDuration:0.3f animations:^{
         [MainTableView reloadData];
    }];
}
-(void)reporeComment:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
     [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
            [self gotoReport];
        
        
    }];
}
/**
 编辑贴文点击事件
 */
#pragma mark - 帖文编辑弹窗
-(void)EditPost:(UIButton *)button{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"编辑帖文", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"删除帖文", nil)];
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    MainModel *model =_DataArray[button.tag - 200];
    if (model.shareID && model.shareID != nil ) {
        if ([model.shareID isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
            [actionItems addObject:item2];
        }
    }else if (model.member_id ){
        if ([model.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
            [actionItems addObject:item];
            [actionItems addObject:item2];
            [actionItems addObject:item1];
        }else{
             [actionItems addObject:item2];
        }
    }
    
   
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (actionItems.count > 2) {
            if (selectedIndex == 0) {
                [self editPost:button.tag - 200];
            }else if (selectedIndex == 2){
                [self deletePost:button.tag - 200];
            }else if (selectedIndex == 1){
                [self gotoReport];
            }
        }else if (actionItems.count == 1){
              [self gotoReport];
        }
        
       
    }];
}
-(void)gotoReport{
    ReportViewController *controller = [[ReportViewController alloc]init];
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - 帖文编辑
-(void)editPost:(NSInteger )index{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(10, SCREEN_HEIGHT / 2 -70, SCREEN_WIDTH - 20, 130)];
    view.layer.cornerRadius = 10;
    view.backgroundColor = [UIColor whiteColor];
    
     MainModel *model =_DataArray[index];
    editText = [[SZTextView alloc]initWithFrame:CGRectMake(0, 0, view.frame.size.width, 100)];
    editText.text = model.content;
    editText.layer.borderWidth = 2.0f;
    editText.layer.borderColor = LYColor(223, 223, 223).CGColor;
    editText.layer.cornerRadius = 8;
    [view addSubview:editText];
    
    UIButton *PostButton = [UIButton buttonWithType:UIButtonTypeCustom];
    PostButton.frame = CGRectMake(SCREEN_WIDTH- 65, 105, 40, 20);
    PostButton.backgroundColor =MainColor;
    [PostButton setTitle:NSLocalizedString(@"发布", nil) forState:UIControlStateNormal];
    PostButton.titleLabel.font = FONT(13);
    PostButton.layer.cornerRadius = 8;
    [PostButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [PostButton addTarget:self action:@selector(sendEditPost:)
         forControlEvents:UIControlEventTouchUpInside];
    PostButton.tag = index;
    [view addSubview:PostButton];
    
    [[QWAlertView sharedMask] show:view withType:QWAlertViewStyleActionSheetDown];
    
    
}
#pragma mark - 发布帖文编辑
-(void)sendEditPost:(UIButton *)button{
    [[QWAlertView sharedMask]dismiss];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,EDPost];
    NSInteger index = button.tag;
     MainModel *model =_DataArray[index];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":editText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
         NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
//        if (dic[@"status" isEqualToString:@"success"]) {
//             model.content = editText.text;
//        }
        if ([dic[@"status"] isEqualToString:@"success"]) {
             model.content = editText.text;
        }
        [MainTableView reloadData];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
    
}
#pragma mark - 删除帖文
-(void)deletePost:(NSInteger )index{
    [PXAlertView showAlertWithTitle:NSLocalizedString(@"确认要删除?", nil) message:nil cancelTitle:NSLocalizedString(@"取消", nil) otherTitle:NSLocalizedString(@"确定", nil) completion:^(BOOL cancelled) {
        if (!cancelled) {
            [CYToast showStatusWithString:@"正在加载"];
            MainModel *model =_DataArray[index];
            NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,DeletePost];
            NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};
            
            [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
                NSDictionary *dic = responseObject;
                [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
                if ([dic[@"status"] isEqualToString:@"success"]) {
                    [_DataArray removeObjectAtIndex:index];
                    [MainTableView reloadData];
                }
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [CYToast dismiss];
            }];
        }
    }];
    
   
}
/**
收藏贴文
 */
#pragma mark - 收藏帖文
-(void)collectPost:(UIButton *)button{
   
    //[CYToast showStatusWithString:@"正在加载"];

    MainModel *model =_DataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CollectPost];

    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id};

    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;

         //[CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        if ([model.isCollect isEqualToString:@"Y"]) {
            model.isCollect = @"N";
        }else{
            model.isCollect = @"Y";
        }
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//        [MainTableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
#pragma mark - 点击用户头像，查看其个人信息
-(void)CheckShareInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.shareID;
    controller.Username = model.shareName;
    controller.ImageUrl = model.shareImage;
    controller.BannerImage = model.shareBanner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)CheckInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.BannerImage = model.banner;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)checkWhoInformation:(UITapGestureRecognizer *)tap{
    MainModel *model =_DataArray[tap.view.tag - 200];
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.ForWhoMember_id;
    controller.Username = model.ForWhoName;
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 点击查看贴文的赞
 */
#pragma mark - 查看帖文的赞

-(void)checkLike:(UIButton *)button{

    MainModel *model =_DataArray[button.tag - 200];
    //[CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,LikeUrl];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        // [CYToast dismiss];
        NSDictionary *dic = responseObject;
        NSString *data = dic[@"data"];
        if ([data isEqualToString:@"yes"]) {
            model.isLike = @"Y";
            NSInteger likecount = [model.Like integerValue] + 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }else{
            model.isLike = @"N";
            NSInteger likecount = [model.Like integerValue] - 1;
            model.Like = [NSString stringWithFormat:@"%ld",likecount];
        }
        //[MainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:button.tag - 200 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
}
/**
 关闭查看赞的View
 */
-(void)closeForgetView{
    [UIView animateWithDuration:0.3f animations:^{
        [ForgetPwdView removeFromSuperview];
        //[MainTableView reloadData];
    }];
    
}
-(void)clickMore{
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    
    AWRActionSheetItem *item2 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"检举", nil)];
    
    [actionItems addObject:item2];
    
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        
        [self gotoReport];
        
        
    }];
}
/**
 点击查看评论
 */
#pragma mark - 查看帖文评论
-(void)comment:(UIButton *)button{
   
    MainModel *model = _DataArray[button.tag - 200];
    
    
    PostDetailViewController *controller = [[PostDetailViewController alloc]init];
    controller.m_showBackBt = YES;
    if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
        return;
    }
    controller.PostId = model.post_id;
    [self.navigationController pushViewController:controller animated:YES];
   
//    MainModel *main =_DataArray[button.tag - 200];
//
//    if (main.isShowCommentV) {
//        main.isShowCommentV =! main.isShowCommentV;
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//
//        [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationNone];
//        return;
//    }
//
//    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/post/getComments"];
//    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":main.post_id};
//   // [CYToast showStatusWithString:@"正在加载"];
//    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
//        NSDictionary *dic = responseObject;
//        NSArray *dataArr = dic[@"data"];
//
//        if (dataArr && dataArr.count > 0) {
//             [main.CommentArray removeAllObjects];
//            for (NSDictionary *Comdict in dataArr) {
//                CommentModel *mode =[CommentModel parsenWithDictionary:Comdict];
//                [main.CommentArray addObject:mode];
//            }
//
//        }
//
//        NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
//
//        [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationNone];
//        [CYToast dismiss];
//    } failure:^(NSError *error) {
//        [CYToast dismiss];
//    }];
    

}
#pragma  mark 取得当前帖文评论
-(void)LoadComment:(UIButton *)button{
    //#define GetComment @"/post/getComments"//获得所有评论
  
}
/**
 发布评论
 */
#pragma mark - 发布评论

-(void)postComment:(UIButton *)button{
    MainModel *model = _DataArray[button.tag - 200];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,CommentUrl];
    if (CurText.text.length == 0 || [CurText.text isEqualToString:@""]) {
        [CYToast showErrorWithString:@"评论不能为空"];
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":model.post_id,@"content":CurText.text};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:@"评论成功"];
             MainModel *main =_DataArray[button.tag - 200];
            main.isShowCommentV = !main.isShowCommentV;
            NSInteger newComment = [main.Comment integerValue];
            main.Comment = [NSString stringWithFormat:@"%ld",newComment + 1];
            NSIndexPath *index = [NSIndexPath indexPathForRow:button.tag -200 inSection:0];
            [MainTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:index,nil]  withRowAnimation:UITableViewRowAnimationFade];
        }else{
            [CYToast showErrorWithString:@"评论失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    CurText = (SZTextView *)textView;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView == Maintext) {
        MainString = textView.text;
    }
    
    [CurText resignFirstResponder];
    [CurText endEditing:YES];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    if ([text isEqualToString:@"\n"]){ //判断输入的字是否是回车，即按下return
//        //在这里做你响应return键的代码
//        [_currentSZTF resignFirstResponder];
//        [_currentSZTF endEditing:YES];
//        return NO; //这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
//    }
    
    return YES;
}
/**
 查看更多评论
 */
#pragma mark - 查看更多评论
-(void)checkMoreComment:(UIButton *)button{
    MainModel *model = _DataArray[button.tag - 200];
    
   
        PostDetailViewController *controller = [[PostDetailViewController alloc]init];
        controller.m_showBackBt = YES;
        if (model.post_id == nil || [model.post_id isEqualToString:@""]) {
            return;
        }
        controller.PostId = model.post_id;
        [self.navigationController pushViewController:controller animated:YES];
}
/**
 点击拍照
 */
#pragma mark - 拍照按钮点击事件
-(void)camera{
    [Maintext resignFirstResponder];
    
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"拍照", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"相册", nil)];
    
    [actionItems addObject:item];
    [actionItems addObject:item1];
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (selectedIndex == 0) {
            [self takePhoto];
        }else if (selectedIndex == 1){
            [self selectFromAblum];
        }
    }];
    
}

/**
 拍照
 */
#pragma mark - 拍照

-(void)takePhoto{
    if (self.imageDataArray.count >= 9) {
        [CYToast showErrorWithString:@"最多只能上传9张图片"];
        return;
    }
    
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.delegate = self;
    _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickerController.allowsEditing = YES;
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //录制视频时长，默认10s
    //_imagePickerController.videoMaximumDuration = 15;
    
    //相机类型（拍照、录像...）字符串需要做相应的类型转换
    _imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
    
    //视频上传质量
    //UIImagePickerControllerQualityTypeHigh高清
    //UIImagePickerControllerQualityTypeMedium中等质量
    //UIImagePickerControllerQualityTypeLow低质量
    //UIImagePickerControllerQualityType640x480
    _imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    
    //设置摄像头模式（拍照，录制视频）为录像模式
    _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
    
}
/**
 相册选择
 */
#pragma mark - 相册
-(void)selectFromAblum{
    if (self.imageDataArray.count >= 9) {
        [CYToast showErrorWithString:@"最多只能上传9张图片"];
        return;
    }
        picker = [[ZYQAssetPickerController alloc] init];
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = ZYQAssetsFilterAllAssets;
        picker.showEmptyGroups=NO;
        picker.delegate=self;
        picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            if ([(ZYQAsset*)evaluatedObject mediaType]==ZYQAssetMediaTypeVideo) {
                NSTimeInterval duration = [(ZYQAsset*)evaluatedObject duration];
                return duration >= 5;
            } else {
                return YES;
            }
    
        }];
        [self presentViewController:picker animated:YES completion:nil];
}
#pragma mark - 选择相片
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i=0; i<assets.count; i++) {
            ZYQAsset *asset=assets[i];
            [asset setGetFullScreenImage:^(UIImage *result) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //由于iphone拍照的图片太大，直接存入数组中势必会造成内存警告，严重会导致程序崩溃，所以存入沙盒中
                    //压缩图片，这个压缩的图片就是做为你传入服务器的图片
                    NSData *imageData=UIImageJPEGRepresentation(result, 0.6);
                    [self.imageDataArray addObject:imageData];
//                    [self WriteToBox:imageData];
//                    //添加到显示图片的数组中
//                    UIImage *image = [self OriginImage:result scaleToSize:CGSizeMake(80, 80)];
//                    [self.imageArray addObject:image];
//                    [self.collectionView reloadData];
                    
                });
                
            }];
        }
        
        
    });
    
    [self dismissViewControllerAnimated:YES completion:^{
        [MainTableView reloadData];
    }];
    
}
//点击拍照
#pragma mark - 视频按钮点击事件
-(void)Video{

    if (_videoDataArray.count > 0) {
        [CYToast showErrorWithString:@"最多只能上传一个视频"];
        return;
    }
    
    NSMutableArray<AWRActionSheetItem*> *actionItems = [[NSMutableArray<AWRActionSheetItem*> alloc] init];
    AWRActionSheetItem *item = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"录像", nil)];
    AWRActionSheetItem *item1 = [AWRActionSheetItem actionWithTitle:NSLocalizedString(@"相册", nil)];
    
    [actionItems addObject:item];
    [actionItems addObject:item1];
    
    AWRActionSheetView *actionSheet = [[AWRActionSheetView alloc]initWithTitle:NSLocalizedString(@"请选择", nil) message:nil actionItems:actionItems cancelText:NSLocalizedString(@"取消", nil)];
    [actionSheet show:^{
        
    } selectedBlock:^(NSInteger selectedIndex, AWRActionSheetItem *actionItem) {
        if (selectedIndex == 0) {
            [self makeVideo];
        }else if (selectedIndex == 1){
            [self SelectVideo];
        }
    }];
}
-(void)SelectVideo{
            UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;//sourcetype有三种分别是camera，photoLibrary和photoAlbum
        NSArray *availableMedia = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];//Camera所支持的Media格式都有哪些,共有两个分别是@"public.image",@"public.movie"
        picker.mediaTypes = [NSArray arrayWithObject:availableMedia[1]];//设置媒体类型为public.movie
        picker.delegate = self;//设置委托
        [self presentViewController:picker animated:YES completion:nil];
}
#pragma mark - 录像
-(void)makeVideo{
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.delegate = self;
    _imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    _imagePickerController.allowsEditing = YES;
    
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //录制视频时长，默认10s
    _imagePickerController.videoMaximumDuration = 90;
    
    //相机类型（拍照、录像...）字符串需要做相应的类型转换
    _imagePickerController.mediaTypes = @[(NSString *)kUTTypeMovie,(NSString *)kUTTypeImage];
    
    //视频上传质量
    //UIImagePickerControllerQualityTypeHigh高清
    //UIImagePickerControllerQualityTypeMedium中等质量
    //UIImagePickerControllerQualityTypeLow低质量
    //UIImagePickerControllerQualityType640x480
    _imagePickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
    
    //设置摄像头模式（拍照，录制视频）为录像模式
    _imagePickerController.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
    [self presentViewController:_imagePickerController animated:YES completion:nil];
}
//选择视频结束
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)infon{
   
    NSString *mediaType=[infon objectForKey:UIImagePickerControllerMediaType];
    //判断资源类型
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]){//图片
        dispatch_async(dispatch_get_main_queue(), ^{
            //由于iphone拍照的图片太大，直接存入数组中势必会造成内存警告，严重会导致程序崩溃，所以存入沙盒中
            //压缩图片，这个压缩的图片就是做为你传入服务器的图片
            UIImage *result = infon[UIImagePickerControllerEditedImage];
            
            NSData *imageData=UIImageJPEGRepresentation(result, 0.6);
            [self.imageDataArray addObject:imageData];
            
        });
        [self dismissViewControllerAnimated:YES completion:^{
            [MainTableView reloadData];
        }];
    }else{//视频类型
        
        NSURL *sourceURL = [infon objectForKey:UIImagePickerControllerMediaURL];
        //NSLog(@"%@",[NSString stringWithFormat:@"%f s", [self getVideoLength:sourceURL]]);
        //NSLog(@"%@", [NSString stringWithFormat:@"%.2f kb", [self getFileSize:[sourceURL path]]]);
        NSURL *newVideoUrl ; //一般.mp4
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];//用时间给文件全名，以免重复，在测试的时候其实可以判断文件是否存在若存在，则删除，重新生成文件即可
        [formater setDateFormat:@"yyyy-MM-dd-HH:mm:ss"];
        newVideoUrl = [NSURL fileURLWithPath:[NSHomeDirectory() stringByAppendingFormat:@"/Documents/output-%@.mp4", [formater stringFromDate:[NSDate date]]]] ;//这个是保存在app自己的沙盒路径里，后面可以选择是否在上传后删除掉。我建议删除掉，免得占空间。
        [picker dismissViewControllerAnimated:YES completion:nil];
        [self convertVideoQuailtyWithInputURL:sourceURL outputURL:newVideoUrl completeHandler:nil];
    }
 }
//压缩视频
- (void) convertVideoQuailtyWithInputURL:(NSURL*)inputURL
                               outputURL:(NSURL*)outputURL
                         completeHandler:(void (^)(AVAssetExportSession*))handler
{
    AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:avAsset presetName:AVAssetExportPresetMediumQuality];
    //  NSLog(resultPath);
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.shouldOptimizeForNetworkUse= YES;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void)
     {
         switch (exportSession.status) {
             case AVAssetExportSessionStatusCancelled:
                 NSLog(@"AVAssetExportSessionStatusCancelled");
                 break;
             case AVAssetExportSessionStatusUnknown:
                 NSLog(@"AVAssetExportSessionStatusUnknown");
                 break;
             case AVAssetExportSessionStatusWaiting:
                 NSLog(@"AVAssetExportSessionStatusWaiting");
                 break;
             case AVAssetExportSessionStatusExporting:
                 NSLog(@"AVAssetExportSessionStatusExporting");
                 break;
             case AVAssetExportSessionStatusCompleted:
                 NSLog(@"AVAssetExportSessionStatusCompleted");
             {
                 _FilePath = outputURL;
                 //[self.imageDataArray addObject:[self getVideoPreViewImage:outputURL]];
//                 UIImage *image = [UIImage imageNamed:@"默认播放按钮"];
//                 NSData *data =UIImageJPEGRepresentation(image, 1.0);
                 //[_videoDataArray addObject:[self getVideoPreViewImage:outputURL]];
                 [_videoDataArray addObject:[UIImage imageNamed:@"默认播放按钮"]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                      [MainTableView reloadData];
                 });
                 
             }
                 //NSLog(@"%@",[NSString stringWithFormat:@"%f s", [self getVideoLength:outputURL]]);
                 //NSLog(@"%@", [NSString stringWithFormat:@"%.2f kb", [self getFileSize:[outputURL path]]]);
                 
                 //UISaveVideoAtPathToSavedPhotosAlbum([outputURL path], self, nil, NULL);//这个是保存到手机相册
                 
                // [self uploadMovie:outputURL];
               
                 break;
             case AVAssetExportSessionStatusFailed:
                 NSLog(@"AVAssetExportSessionStatusFailed");
                 break;
         }
         
     }];
    
}

/**
 获取视频的第一zhen
 */
- (UIImage*) getVideoPreViewImage:(NSURL *)path
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:path options:nil];
    AVAssetImageGenerator *assetGen = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    
    assetGen.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [assetGen copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage *videoImage = [[UIImage alloc] initWithCGImage:image];
    CGImageRelease(image);
    return videoImage;
}
/**
 分享按钮点击
 */
#pragma mark - 分享帖文
-(void)shareClick:(UIButton *)button{
   
    MainModel *model1 = _DataArray[button.tag - 200];
    NSMutableString *share = [[NSMutableString alloc]initWithString:NSLocalizedString(@"已有人分享了这则帖文", nil)];
    [share insertString:model1.Share atIndex:1];
    DeclareAbnormalAlertView *alertView = [[DeclareAbnormalAlertView alloc]initWithTitle:NSLocalizedString(@"分享帖文", nil) message:share delegate:self leftButtonTitle:NSLocalizedString(@"确定", nil) rightButtonTitle:NSLocalizedString(@"取消", nil)];
    currentShareModel = model1;
    alertView.model = model1;
    alertView.delegate = self;
    [alertView show];
}
-(void)didCloseWindow{
    
}
-(void)playWith:(ZFPlayerView *)PlView{
    
}
-(void)didSendtext:(NSString *)text{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    NSDictionary *parmDic =@{@"target":[PHUserModel sharedPHUserModel].member_id,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_type":@"share",@"share_id":currentShareModel.post_id,@"content":[NSString stringWithFormat:@"%@",text]};
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showStatusWithString:dic[@"message"] hideAfterDelay:0.5f];
        currentShareModel = nil;
        [self load];
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
- (void)declareAbnormalAlertView:(DeclareAbnormalAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
}
-(void)pushNavagation:(CommentModel *)model{
    
    MainUserViewController *controller = [[MainUserViewController alloc]init];
    controller.member_id = model.member_id;
    controller.Username = model.UserName;
    controller.ImageUrl = model.UserImage;
    controller.m_showBackBt = YES;
    //controller.BannerImage = model.BannerImage;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma  mark  请求
/**
 获取所有发文
 */
#pragma mark - 所有帖文
-(void)loadDataMore{
    Page ++;
    [self loadMore];
}
-(void)refresh{
    [self load];
}
-(void)loadMore{
    
    [_searchBar resignFirstResponder];
    [searchTableview removeFromSuperview];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       // [CYToast showStatusWithString:@"正在加载"];
        NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,GetUrl];
        NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id,@"perPage":@"10",@"Page":[NSString stringWithFormat:@"%long",Page],@"decode":@"1"};
        [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
            // [CYToast dismiss];
            NSDictionary *dic = responseObject;
            NSArray *dataArr = dic[@"data"];
            
            NSMutableArray *moreArray = [[NSMutableArray alloc]init];
            if (dataArr && dataArr.count > 0) {
                for (NSDictionary *parmDic in dataArr) {
                    MainModel *newModel = [MainModel pareWithDictionary:parmDic];
                    [moreArray addObject:newModel];
                    [_DataArray addObject:newModel];
                }

                self.player.assetURLs = self.urls;

                downloadGroup = dispatch_group_create();
                for (MainModel *model in moreArray) {
                    [self loadComment:model];
                }
                dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
                    
                    [MainTableView reloadData];
                });
            }else{
                [MainTableView.mj_footer resetNoMoreData];
                Page -- ;
            }
             [MainTableView.mj_footer endRefreshing];
        } failure:^(NSError *error) {
            NSLog(@"%@",error);
            [CYToast dismiss];
        }];
        
    });
    
 
}
-(void)load{
    [CYToast showStatusWithString:@"正在加载"];
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,GetUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id,@"perPage":@"10",@"Page":@"1",@"decode":@"1"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject)
    {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
         [_DataArray removeAllObjects];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_DataArray addObject:[MainModel pareWithDictionary:parmDic]];
            }
        }

        self.player.assetURLs = self.urls;

        downloadGroup = dispatch_group_create();
        
        for (MainModel *model in _DataArray) {
            [self loadComment:model];
        }
        
        [CYToast dismiss];
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{

            [MainTableView.mj_footer endRefreshing];
            [MainTableView.mj_header endRefreshing];
            [MainTableView reloadData];
        });
    } failure:^(NSError *error)
    {
        [CYToast dismiss];
        [MainTableView.mj_footer endRefreshing];
        [MainTableView.mj_header endRefreshing];
    }];
}
-(void)loadComment:(MainModel  *)CoModel{
    dispatch_group_enter(downloadGroup);
    
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/post/getComments"];
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"post_id":CoModel.post_id};
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        
        if (dataArr && dataArr.count > 0) {
            [CoModel.CommentArray removeAllObjects];
            for (NSDictionary *Comdict in dataArr) {
                CommentModel *mode =[CommentModel parsenWithDictionary:Comdict];
                [CoModel.CommentArray addObject:mode];
            }
        }
       
        dispatch_group_leave(downloadGroup);
        [CYToast dismiss];
    } failure:^(NSError *error) {
        [CYToast dismiss];
    }];
    
}
-(void)loadData{
    [_DataArray removeAllObjects];
    [CYToast showStatusWithString:@"正在加载"];
    
    NSString *language_id=nil;
    if ([[self getPreferredLanguage] isEqualToString:@"en"]) {
        
        language_id=@"english";
    }else if ([[self getPreferredLanguage] isEqualToString:@"zh-Hans-CN"]){
        
        
        language_id=@"zh-CN";
    }
    else if ([[self getPreferredLanguage] isEqualToString:@"zh-Hant-CN"]){
        
        language_id=@"zh-TW";
    }
    
     [PPNetworkHelper setValue:language_id forHTTPHeaderField:@"set-lg"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,GetUrl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id,@"perPage":@"10",@"Page":[NSString stringWithFormat:@"%ld",(long)Page],@"decode":@"1"};

    
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];

        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_DataArray addObject:[MainModel pareWithDictionary:parmDic]];
            }
            
        }

        downloadGroup = dispatch_group_create();
        
        for (MainModel *model in _DataArray) {
            [self loadComment:model];
        }
        
        dispatch_group_notify(downloadGroup, dispatch_get_main_queue(), ^{
             [self creatTable];
             self.player.assetURLs = self.urls;
        });
        
        [CYToast dismiss];
        [self loadNotiCount];
        [self loadInviteData];
        [self loadMessCount];
    } failure:^(NSError *error) {
        NSLog(@"loaddata:%@",error);
        [CYToast dismiss];
    }];
}
/**
 点击发布
 */
-(void)send{
    [Maintext resignFirstResponder];
    [Maintext endEditing:YES];
    MainString = nil;
    
    if (_videoDataArray.count > 0) {
        [self uploadMovie:_FilePath];
        return;
    }
    
    if (_imageDataArray.count > 0) {
        for (NSInteger i = 0 ; i < _imageDataArray.count; i++) {
            [self uploadPicture:_imageDataArray[i] WithIndex:i];
        }
        return;
    }
    
    
    if (Maintext.text.length == 0) {
        [CYToast showErrorWithString:@"发文不能为空"];
        return;
    }else{
        [self postMessage:@""];
    }
 
}
/**
 上传图片
 */
-(void)uploadPicture:(NSData *)imageData WithIndex:(NSInteger )ind{
   [CYToast showStatusWithString:@"正在加载"];
   
    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    __weak __block  UIView *newView = UploadView;
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    manger.requestSerializer  = [AFJSONRequestSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,UploadPic];
     manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
       // NSData *imageData =_imageDataArray[0];
         NSString *fileName = [NSString stringWithFormat:@"ljq.jpg"];
       [formData appendPartWithFileData:imageData name:@"picture" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        //NSLog(@"%@",uploadProgress);
        CGFloat width = uploadProgress.fractionCompleted * (SCREEN_WIDTH - 30);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%f",width);
            newView.frame = CGRectMake(newView.frame.origin.x, newView.frame.origin.y, width, 3);
        });
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        NSDictionary *dict = responseObject;
    
        if ([dict[@"status"] isEqualToString:@"success"] ) {
            [_NewImageArray addObject:dict[@"id"]];
        }
        if (_NewImageArray.count == _imageDataArray.count ) {
            [CYToast dismiss];
            [self postMessage:@""];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
         [CYToast dismiss];
         [CYToast showErrorWithString:@"上传失败"];
    }];

}

/**
 上传影片
 */
-(void)uploadMovie:(NSURL *)Url{
    [CYToast showStatusWithString:@"正在加载"];

    NSDictionary *parmDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    
    AFHTTPSessionManager *manger = [AFHTTPSessionManager manager];
    manger.responseSerializer = [AFJSONResponseSerializer serializer];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl
,UploadMov];
    __weak __block  UIView *newView = UploadView;
    manger.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html",@"text/json", @"text/javascript", nil];
    
    [manger POST:url parameters:parmDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSData *imageData = [NSData dataWithContentsOfURL:Url];  ;
        NSString *fileName = [NSString stringWithFormat:@"ljq.mp4"];
        [formData appendPartWithFileData:imageData name:@"movie" fileName:fileName mimeType:@"image/jpg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"%@",uploadProgress);
        CGFloat width = uploadProgress.fractionCompleted * (SCREEN_WIDTH - 30);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"%f",width);
            newView.frame = CGRectMake(newView.frame.origin.x, newView.frame.origin.y, width, 3);
        });
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [CYToast dismiss];
        NSDictionary *dict = responseObject;
        if ([dict[@"status"] isEqualToString:@"success"]) {
            [self postMessage:dict[@"id"]];
        }else{
           // [CYToast showErrorWithString:@"上传失败"];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [CYToast dismiss];
        [CYToast showErrorWithString:@"上传失败"];
    }];
}
-(void)postMessage:(NSString *)image{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *post_type = nil;
    NSString *content1 = Maintext.text;
    NSString *content = [NSString stringWithString:[content1 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
   // [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *content2 = [content1 stringByRemovingPercentEncoding];
    NSMutableDictionary *parmDic = [[NSMutableDictionary alloc]init];
    if (_imageDataArray.count > 0) {
        [parmDic setObject:@"picture" forKey:@"post_type"];
        [parmDic setObject:_NewImageArray forKey:@"pictures"];
        [parmDic setObject:content forKey:@"content"];
    }else if (_videoDataArray.count > 0){
        post_type = @"video";
        NSInteger mov = image.integerValue;
        NSString *movie = [NSString stringWithFormat:@"%ld",mov];
        
        [parmDic setObject:@"video" forKey:@"post_type"];
        [parmDic setObject:movie forKey:@"movie"];
        [parmDic setObject:content forKey:@"content"];
    }else{
        [parmDic setObject:@"text" forKey:@"post_type"];
        [parmDic setObject:content2 forKey:@"content"];
        
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,PostUrl];
    [parmDic setObject:[PHUserModel sharedPHUserModel].csrf_token forKey:@"csrf_token_name"];
    [parmDic setObject:[PHUserModel sharedPHUserModel].AccessToken forKey:@"token"];
    [parmDic setObject:[PHUserModel sharedPHUserModel].member_id forKey:@"target"];
    
    
    [PPNetworkHelper POST:url parameters:parmDic success:^(id responseObject) {
        //[CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            [CYToast showSuccessWithString:dic[@"message"]];
            if (_FilePath && _videoDataArray.count > 0) {
                [[NSFileManager defaultManager] removeItemAtPath:[_FilePath path] error:nil];
                 [_videoDataArray removeAllObjects];
            }
            [_imageDataArray removeAllObjects];
            [_NewImageArray removeAllObjects];
            [self refresh];
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }] ;
}
//拿token
-(void)getAccess_Token{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSDictionary *paramDic = @{@"email":[PHUserModel sharedPHUserModel].email,@"password":[PHUserModel sharedPHUserModel].Password,@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token};
    
     NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,LoginUrl];
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"message"] isEqualToString:@"success"]) {
            
            dispatch_queue_t  queue = dispatch_queue_create("com.PuHuiShiJi.Imai", DISPATCH_QUEUE_SERIAL);
            
            dispatch_async(queue, ^{
                
                [PHUserModel sharedPHUserModel].AccessToken =dic[@"data"][@"token"];
                [PHUserModel sharedPHUserModel].language = dic[@"data"][@"language_name"];
                [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
                [[PHUserModel sharedPHUserModel] connect];
                
                if ([dic[@"data"][@"language_id"] isEqualToString:@"english"] && ![[self getPreferredLanguage] isEqualToString:@"en"]) {


                    [[NSUserDefaults standardUserDefaults] setObject:@[@"en"] forKey:@"AppleLanguages"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [NSBundle setLanguage:@"en"];


                    dispatch_async(queue, ^{
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            // NSLog(@"任务执行完毕，回到主线程 thread:%@",[NSThread currentThread]);
                            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                            appdelegate.window.rootViewController = nil;
                            [appdelegate.window resignKeyWindow];

                            [appdelegate initTabBarVC];
                            [appdelegate.window makeKeyAndVisible];
                        });

                    });


                }else if ([dic[@"data"][@"language_id"] isEqualToString:@"zh-CN"] && ![[self getPreferredLanguage] isEqualToString:@"zh-Hans-CN"]){


                    [[NSUserDefaults standardUserDefaults] setObject:@[@"zh-Hans-CN"] forKey:@"AppleLanguages"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [NSBundle setLanguage:@"zh-Hans-CN"];

                    dispatch_async(queue, ^{
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            // NSLog(@"任务执行完毕，回到主线程 thread:%@",[NSThread currentThread]);
                            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                            appdelegate.window.rootViewController = nil;
                            [appdelegate.window resignKeyWindow];

                            [appdelegate initTabBarVC];
                            [appdelegate.window makeKeyAndVisible];
                        });

                    });
                }
                else if ([dic[@"data"][@"language_id"] isEqualToString:@"zh-TW"] && ![[self getPreferredLanguage] isEqualToString:@"zh-Hant-CN"]){


                    NSArray *lans = @[@"zh-Hant-CN"];
                    [[NSUserDefaults standardUserDefaults] setObject:lans forKey:@"AppleLanguages"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [NSBundle setLanguage:@"zh-Hant-CN"];

                    dispatch_async(queue, ^{
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            // NSLog(@"任务执行完毕，回到主线程 thread:%@",[NSThread currentThread]);
                            AppDelegate *appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
                            appdelegate.window.rootViewController = nil;
                            [appdelegate.window resignKeyWindow];

                            [appdelegate initTabBarVC];
                            [appdelegate.window makeKeyAndVisible];
                        });

                    });
                }
            });
            
           
            dispatch_async(queue, ^{
                dispatch_sync(dispatch_get_main_queue(), ^{
                   // NSLog(@"任务执行完毕，回到主线程 thread:%@",[NSThread currentThread]);
                     [self loadData];
                });
                
            });
        
            
        }
    } failure:^(NSError *error) {
        NSLog(@"1%@",error);
        [CYToast dismiss];
    } getToken:false];
}
-(void)getCSRF_Token{

    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@/api/get_csrf_token",TestUrl];
    [PPNetworkHelper GET:url parameters:nil success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            
            dispatch_queue_t  queue = dispatch_queue_create("com.PuHuiShiJi.Imai", DISPATCH_QUEUE_SERIAL);
            
            dispatch_async(queue, ^{

                [PHUserModel sharedPHUserModel].csrf_token = dic[@"data"][@"token"];
                [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            });
            
            
            dispatch_async(queue, ^{
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self getAccess_Token];
                });
            });
       
           
        }else{
            [CYToast showErrorWithString:@"token获取失败"];
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"2%@",error);
    } getToken:false];
}
#pragma mark - searchBar相关

/**
 *  searchBar
 */
-(void)showSearch{
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(120, 2, SCREEN_WIDTH - 175, 40)];
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.showsCancelButton = NO;
    _searchBar.tintColor = [UIColor orangeColor];
    _searchBar.placeholder = @"搜索感兴趣的内容";
    _searchBar.delegate = self;
    
  
    
    for (UIView *subView in _searchBar.subviews) {
        if ([subView isKindOfClass:[UIView  class]]) {
            [[subView.subviews objectAtIndex:0] removeFromSuperview];
            if ([[subView.subviews objectAtIndex:0] isKindOfClass:[UITextField class]]) {
                UITextField *textField = [subView.subviews objectAtIndex:0];
                textField.backgroundColor = [UIColor whiteColor];

                //设置默认文字颜色
                UIColor *color = [UIColor grayColor];
                [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"搜索好友", nil)
                                                                                    attributes:@{NSForegroundColorAttributeName:color}]];
            }
        }
    }
    
    [self.navigationController.navigationBar addSubview:_searchBar];
    searchShow = YES;
}
-(void)creatSearchBar{
    
    if (_searchBar) {
        [_searchBar removeFromSuperview];
        _searchBar = nil;
        searchShow = NO;
        return;
    }
    
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(120, 2, SCREEN_WIDTH - 175, 40)];
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.showsCancelButton = NO;
    _searchBar.tintColor = [UIColor orangeColor];
    _searchBar.placeholder = @"搜索感兴趣的内容";
    _searchBar.delegate = self;
    
    
    for (UIView *subView in _searchBar.subviews) {
        if ([subView isKindOfClass:[UIView  class]]) {
            [[subView.subviews objectAtIndex:0] removeFromSuperview];
            if ([[subView.subviews objectAtIndex:0] isKindOfClass:[UITextField class]]) {
                UITextField *textField = [subView.subviews objectAtIndex:0];
//                textField.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
                textField.backgroundColor = [UIColor whiteColor];


                //设置默认文字颜色
                UIColor *color = [UIColor grayColor];
                [textField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"搜索好友", nil)
                                                                                    attributes:@{NSForegroundColorAttributeName:color}]];
            }
        }
    }
    
    [self.navigationController.navigationBar addSubview:_searchBar];
    searchShow = YES;
    
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if (searchBar.text.length > 0) {
          [self loadData:searchBar.text];
    }
//    else if (searchBar.text.length == 0){
//        [_searchBar resignFirstResponder];
//        [searchTableview removeFromSuperview];
//        return;
//    }
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    if (searchBar.text.length == 0) {
        [_searchBar resignFirstResponder];
        [searchTableview removeFromSuperview];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
         [_searchBar resignFirstResponder];
        [searchTableview removeFromSuperview];
        return;
    }else{
        [self loadData:searchText];
    }
   
}
//- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    if (text.length > 0) {
//        [self loadData:text];
//        return YES;
//    }else if (text.length == 0){
//        [_searchBar resignFirstResponder];
//        [searchTableview removeFromSuperview];
//        //return;
//        return NO;
//    }
//
//    return YES;
//
//}
-(void)creatSearchTableview{
    if (searchTableview) {
        [searchTableview removeFromSuperview];
    }
    searchTableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 200) style:UITableViewStylePlain];
    [searchTableview registerClass:[SearchTableViewCell class] forCellReuseIdentifier:@"search"];

    searchTableview.delegate = self;
    searchTableview.dataSource = self;
    searchTableview.separatorStyle = NO;
    searchTableview.scrollEnabled = YES;
    [self.view addSubview:searchTableview];
}
//- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
//{
//
//    [self gotoSearch];
//    return NO;
//}
//- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
//shouldReloadTableForSearchString:(NSString *)searchString
//
//{
//    //一旦SearchBar輸入內容有變化，則執行這個方法，詢問要不要重裝searchResultTableView的數據
//    // Return YES to cause the search result table view to be reloaded.
//
//    [self loadData:searchString];
//    return YES;
//}
-(void)loadData:(NSString *)string{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,searchurl];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"keyword":string};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        
        if (dataArr && dataArr.count > 0) {
            [_SearchdataArr removeAllObjects];

            for (NSDictionary *parm in dataArr) {
                [_SearchdataArr addObject:[SearchModel parsenWith:parm]];
            }
             [self creatSearchTableview];
        }
//        if (searchTableview) {
//            [searchTableview reloadData];
//        }else{
//
//        }
        
       
      //  [searchDisplayController.searchResultsTableView reloadData];
       // [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)gotoSearch{
    SearchViewController *controller = [[SearchViewController alloc]init];
    controller.m_showBackBt = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

//获取未读通知数
-(void)loadNotiCount{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/notifies"];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSInteger i = 0;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                NSString *is_read = parmDic[@"is_read"];
                if ([is_read integerValue] == 0) {
                    i ++;
                }
            }
        }
        if (i >0) {
            InformViewController *info = self.tabBarController.childViewControllers[2];
            info.tabBarItem.badgeValue = (i > 99) ? @"99+" : [NSString stringWithFormat:@"%ld",i];
        }
       
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"3%@",error);
    }];
}
//获取讯息未读数
-(void)loadMessCount{
   
    
        NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,@"/Chat_pop/recv_chatunit"];
        NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id};
        [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
            NSDictionary *dic = responseObject;
            NSArray *dataArr = dic[@"result"];
             NSInteger i = 0;
            
            if (dataArr && dataArr.count > 0) {
                for (NSDictionary *parmDic in dataArr) {
                    NSString *is_read = parmDic[@"is_read"];
                    if ([is_read integerValue] == 0) {
                        //i ++;
                        NSString *count = parmDic[@"count"];
                        i = [count integerValue] + i;
                    }
                }
            }
            
            if (i >0) {
                MessageViewController *info = self.tabBarController.childViewControllers[3];
                info.tabBarItem.badgeValue = (i > 99) ? @"99+" : [NSString stringWithFormat:@"%ld",i];
            }
            
        } failure:^(NSError *error) {
            [CYToast dismiss];
            NSLog(@"%@",error);
        }];
    
}
//获取未读邀请数
-(void)loadInviteData{
    [CYToast showStatusWithString:@"正在加载"];
    ///api/invite_notifies
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/invite_notifies"];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSInteger i = - 0;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
               // [_dataArray addObject:[InviteModel pareWithDictionary:parmDic]];
                i++;
            }
        }
        if (i > 0) {
            InviteViewController *info = self.tabBarController.childViewControllers[1];
            info.tabBarItem.badgeValue = (i > 99) ? @"99+" : [NSString stringWithFormat:@"%ld",i];
        }

    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"4%@",error);
    }];
    
}

//-(void)checkAllMissions{
//    [CYToast showStatusWithString:@"正在加载"];
//    ///api/invite_notifies
//    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/check_all_missions"];
//    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
//    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
//        NSDictionary *dic = responseObject;
//        [CYToast dismiss];
//
//
//    } failure:^(NSError *error) {
//        [CYToast dismiss];
//        NSLog(@"4%@",error);
//    }];
//
//}


-(void)isSignInToday{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/isSignInToday"];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        NSString *isSignIn = dic[@"data"];

        if ([isSignIn isEqualToString:@"N"]) {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"每日簽到"
                                         message:@"連續登入七天贈送168積分\n連續登入二十八天會員可開啟商城功能\n請前往簽到頁面"
                                         preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"確定"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"QuestStoryboard" bundle:nil];
                                            QuestViewController *questViewController = [storyboard instantiateViewControllerWithIdentifier:@"QuestViewController"];

                                            [self.navigationController pushViewController:questViewController animated:YES];
                                        }];

            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"取消"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];

            //Add your buttons to alert controller

            [alert addAction:noButton];
            [alert addAction:yesButton];


            [self presentViewController:alert animated:YES completion:nil];

            isSeenAlert = YES;
        }


    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"3%@",error);
    }];
}

#pragma mark - ZFTableViewCellDelegate

- (void)zf_playTheVideoAtIndexPath:(NSIndexPath *)indexPath {
    [self playTheVideoAtIndexPath:indexPath scrollToTop:NO];
}

#pragma mark - private method

/// play the video
- (void)playTheVideoAtIndexPath:(NSIndexPath *)indexPath scrollToTop:(BOOL)scrollToTop {
    [self.player playTheIndexPath:indexPath scrollToTop:scrollToTop];
    MainModel *model = _DataArray[indexPath.row];
    [self.controlView showTitle:@""
                 coverURLString:model.videoPath
                 fullScreenMode:ZFFullScreenModeLandscape];
}

#pragma mark - getter
- (ZFPlayerControlView *)controlView {
    if (!_controlView) {
        _controlView = [ZFPlayerControlView new];
    }
    return _controlView;
}

@end
