//
//  ReportViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "XTViewController.h"

@interface ReportViewController : XTViewController<UIPickerViewDelegate,UIPickerViewDataSource>
{
    UIView * mPickView;
    UIPickerView * pick;
    UIView *mGrayView;
}
@end
