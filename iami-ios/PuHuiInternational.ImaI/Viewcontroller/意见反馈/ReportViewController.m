//
//  ReportViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/29.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ReportViewController.h"

@interface ReportViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *MainTableView;
    
    UIButton *ReasonButton;
    
    SZTextView *MainText;
    
    UIButton *RegistButton;
    
    NSInteger selectindex;
}
@property (nonatomic,copy) NSArray *TypeArray;
@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // _TypeArray = [[NSMutableArray alloc]init];
    [self creatTable];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
      self.navigationController.tabBarController.tabBar.hidden = NO;
}
-(void)creatTable{
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT -  70 ) style:UITableViewStyleGrouped];
    MainTableView.backgroundColor = [UIColor whiteColor];
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    //MainTableView.fd_debugLogEnabled = YES;
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 400;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 400)];
    
    UILabel *mainLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, SCREEN_WIDTH, 20)];
    mainLabel.text = NSLocalizedString(@"检举", nil);
    mainLabel.textAlignment = NSTextAlignmentCenter;
    mainLabel.font = Bold_FONT(20);
    mainLabel.textColor = MainColor;
    [view addSubview:mainLabel];
    
    ReasonButton = [UIButton buttonWithType:UIButtonTypeCustom];
    ReasonButton.frame = CGRectMake(30, CGRectGetMaxY(mainLabel.frame) + 15, SCREEN_WIDTH - 60, 40);
    ReasonButton.layer.cornerRadius = 8;
    ReasonButton.layer.borderColor = LYColor(223, 223, 223).CGColor;
    ReasonButton.layer.borderWidth = 0.5f;
    [ReasonButton setTitle:NSLocalizedString(@"检举", nil) forState:UIControlStateNormal];
    ReasonButton.titleLabel.font = FONT(17);
    ReasonButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [ReasonButton setTitleColor:LYColor(223, 223, 223) forState:UIControlStateNormal];
    [ReasonButton addTarget:self action:@selector(checkReason) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:ReasonButton];
    
    UIButton *tapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tapButton.frame = CGRectMake(SCREEN_WIDTH - 90, 5, 15, 30);
    [tapButton setTitle:@"▼" forState:UIControlStateNormal];
    tapButton.titleLabel.font = FONT(12);
    [tapButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [tapButton addTarget:self action:@selector(checkReason) forControlEvents:UIControlEventTouchUpInside];
    [ReasonButton addSubview:tapButton];
    
    
    
    
    //UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(ReasonButton.frame.size.width - 30, 5, 20, 20)];
    
    MainText = [[SZTextView alloc]initWithFrame:CGRectMake(30, CGRectGetMaxY(ReasonButton.frame) + 15, SCREEN_WIDTH - 60, 150)];
    MainText.placeholder = NSLocalizedString(@"备注", nil);
    MainText.placeholderTextColor = LYColor(223, 223, 223);
    MainText.layer.cornerRadius = 8;
    MainText.layer.borderWidth = 0.5f;
    MainText.layer.borderColor =  LYColor(223, 223, 223).CGColor;
    [view addSubview:MainText];
    
    RegistButton = [UIButton buttonWithType:UIButtonTypeCustom];
    RegistButton.frame = CGRectMake(30, CGRectGetMaxY(MainText.frame) + 15, SCREEN_WIDTH - 60, 40);
    [RegistButton setTitle:NSLocalizedString(@"送出", nil) forState:UIControlStateNormal];
     [RegistButton setBackgroundImage:[UIImage imageWithColor:MainColor] forState:UIControlStateNormal];
    [RegistButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [RegistButton addTarget:self action:@selector(Regist) forControlEvents:UIControlEventTouchUpInside];
    RegistButton.layer.cornerRadius = 8;
    RegistButton.clipsToBounds = YES;
    [view addSubview:RegistButton];
    
    return view;
}
-(void)Regist{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/addFeedback"];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"subject":@"",@"content":[NSString stringWithFormat:@"%@",MainText.text],@"category":[NSString stringWithFormat:@"%ld",selectindex]};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast showSuccessWithString:@"提交成功"];
      [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
-(void)checkReason{
    [self loadType];
}
-(void)showPicker{
    if (mPickView == nil) {
        mPickView = [[UIView alloc] init];
        mPickView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 250);
        
        mPickView.backgroundColor = [UIColor whiteColor];
        
        pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 34, SCREEN_WIDTH, 216)];
        pick.delegate = self;
        pick.dataSource = self;
        pick.showsSelectionIndicator = YES;
        [mPickView addSubview:pick];
        
        UIButton * leftBt = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        leftBt.frame = CGRectMake(10, 5, 61, 30);
        leftBt.tag = 100;
        [leftBt setTitle:NSLocalizedString(@"取消", nil) forState:UIControlStateNormal];
        leftBt.titleLabel.font = Bold_FONT(15);
        [leftBt addTarget:self action:@selector(hidePickView:) forControlEvents:UIControlEventTouchUpInside];
        [mPickView addSubview:leftBt];
        
        UIButton * rightBt = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        rightBt.frame = CGRectMake(SCREEN_WIDTH-10-71, 5, 71, 30);
        rightBt.tag = 101;
        [rightBt setTitle:NSLocalizedString(@"确定", nil) forState:UIControlStateNormal];
        rightBt.titleLabel.font = Bold_FONT(15);
        [rightBt addTarget:self action:@selector(hidePickView:) forControlEvents:UIControlEventTouchUpInside];
        [mPickView addSubview:rightBt];
        
        mGrayView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        mGrayView.backgroundColor = [UIColor lightGrayColor];
        mGrayView.alpha = 0.f;
        
        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [delegate.window addSubview:mGrayView];
        
        [delegate.window addSubview:mPickView];
        
    }
    
    [self.view setUserInteractionEnabled:NO];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    mGrayView.alpha = 0.6;
    mPickView.frame = CGRectMake(0, SCREEN_HEIGHT-250, SCREEN_WIDTH, 250);
    
    [UIView commitAnimations];
}
-(void)hidePickView:(id)sender{
    UIButton * tmpBt = (UIButton*)sender;
    if (tmpBt.tag == 101) {
//        if ([slectType isEqualToString:@"rel"]) {
//            NSInteger index = [pick selectedRowInComponent:0];
//            NSString *string = _RelationArray[index];
//            UIButton *relBut = [self.view viewWithTag:207];
//            RelationIndex = index;
//            [relBut setTitle:string forState:UIControlStateNormal];
//            [relBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//
//        }else{
//            NSInteger index = [pick selectedRowInComponent:0];
//            CountryModel *model = _CountryArray[index];
//            UIButton *countryBut = nil;
//
//            if ([model.type isEqualToString:@"coun"]) {
//
//                countryBut = [self.view viewWithTag:206];
//                [countryBut setTitle:[NSString stringWithFormat:@"%@",model.cname] forState:UIControlStateNormal];
//                countryId = model.country_id;
//            }else{
//                countryBut = [self.view viewWithTag:208];
//                [countryBut setTitle:[NSString stringWithFormat:@"%@",model.language_name] forState:UIControlStateNormal];
//                langId= model.language_id;
//            }
//            [countryBut setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//
//        }
         selectindex = [pick selectedRowInComponent:0];
                  NSString *string = _TypeArray[selectindex];
    
        [ReasonButton setTitle:string forState:UIControlStateNormal];
        [ReasonButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        
        
        
    }
    
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationStoped)];
    //mPickView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, 250);
    //mGrayView.alpha = 0.0;
    [self animationStoped];
    [UIView commitAnimations];
    [self.view setUserInteractionEnabled:YES];
}
- (void)animationStoped
{
    if (mPickView) {
        [mPickView removeFromSuperview];
        mPickView = nil;
        [mGrayView removeFromSuperview];
        
        mGrayView = nil;
        pick.delegate = nil;
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
//returns the # of rows in each component.
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return _TypeArray.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    NSString *string = _TypeArray[row];
    return string;
}
-(void)loadType{
    NSString *url = [NSString stringWithFormat:@"%@%@",TestUrl,@"/api/fbCategory"];
    //NSDictionary *parmDic =
    [PPNetworkHelper GET:url parameters:nil success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSDictionary *arr = dic[@"data"];
       
        _TypeArray = [arr allValues];
        [self showPicker];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}















/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
