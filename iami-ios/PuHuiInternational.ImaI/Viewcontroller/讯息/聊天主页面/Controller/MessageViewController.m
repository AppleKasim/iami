//
//  MessageViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/13.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MessageViewController.h"
#import "InfoTableViewCell.h"
#import "ShortMovieViewController.h"
#import "ChatroomViewController.h"
#import "MessageFriendTableViewCell.h"//好友名单cell
#import "MessageListTableViewCell.h"//聊天室cell



@interface MessageViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UISegmentedControl *seg;
    
    UITableView *MainTableView;
    
    NSInteger seleIndex;
}
@property (nonatomic,copy) NSMutableArray *FriendArray;
@property (nonatomic,copy) NSMutableArray *MessageArray;
///api/message_notifies
//chat_pop/getChat
#define getUrl @"/chat_pop/getChat"
#define getFriend @"/friend/getFriendList"
#define getChat @"chat_pop/getChat"
#define getChatList @"/Chat_pop/recv_chatunit"
@end

@implementation MessageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _FriendArray = [[NSMutableArray alloc]init];
    _MessageArray = [[NSMutableArray alloc]init];
   
    [self creatTable];
    [self creatTabButton];
    [self loadData];
     seleIndex = 0;
    // Do any additional setup after loading the view.
}
#pragma mark -  TableView delegeta && datasource
-(void)creatTable{
    
    [self.view addSubview:[self creatSeg]];
    
    MainTableView = [[UITableView alloc]initWithFrame:CGRectMake(10, 35, SCREEN_WIDTH - 20, SCREEN_HEIGHT -  Height_NavBar-Height_TabBar - 38 ) style:UITableViewStylePlain];
    // MainTableView.backgroundColor =LYColor(223, 223, 223);
    MainTableView.delegate = self;
    MainTableView.dataSource = self;
    MainTableView.separatorStyle = NO;
    MainTableView.scrollEnabled = YES;
    [MainTableView registerClass:[InfoTableViewCell class] forCellReuseIdentifier:@"cell"];
    [MainTableView registerClass:[MessageFriendTableViewCell class] forCellReuseIdentifier:@"friend"];
    [MainTableView registerClass:[MessageListTableViewCell class] forCellReuseIdentifier:@"chatroom"];
    [self.view addSubview:MainTableView];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (seg.selectedSegmentIndex == 1) {
        return _FriendArray.count;
    }else if (seg.selectedSegmentIndex == 0){
        return _MessageArray.count;
    }
    else{
       return 10;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 55;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 1)];
    view.backgroundColor =LYColor(223, 223, 223);
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (seg.selectedSegmentIndex == 1) {
        MessageFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"friend" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        ChatFreidnModel *model = _FriendArray[indexPath.section];
        [cell setModel:model];
        
        cell.chatButton.tag = 100 + indexPath.section;
        [cell.chatButton addTarget:self action:@selector(gotoChatroom:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
        
    }else if (seg.selectedSegmentIndex == 0){
        MessageListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatroom" forIndexPath:indexPath];
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        ChatFreidnModel *model = _MessageArray[indexPath.section];
        [cell setModel:model];
        return cell;
    }
    
    return nil;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return [self creatSeg];
//    }
//    return nil;
//}
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0) {
//        return 30;
//    }
//    return 0;
//}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (seg.selectedSegmentIndex == 0) {
        ChatFreidnModel *moedel = _MessageArray[indexPath.section];
        ChatroomViewController *controller = [[ChatroomViewController alloc]init];
        controller.member_id = moedel.member_id;
        controller.barTitle =  moedel.nickname;
        [self.navigationController pushViewController:controller animated:YES];
    }
   
}
-(UIView *)creatSeg{
    seg = [[UISegmentedControl alloc]initWithItems:@[NSLocalizedString(@"聊天室", nil),NSLocalizedString(@"好友名单", nil)]];
    seg.frame = CGRectMake(10, 5, SCREEN_WIDTH -20, 30);
    seg.backgroundColor = [UIColor whiteColor];
    seg.selectedSegmentIndex = seleIndex;
    seg.tintColor = MainColor;
    [seg addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];

    return seg;
}
-(void)segmentValueChanged:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex == 1) {
        [self loadFriend];
    }else if (seg.selectedSegmentIndex == 0 ){
         [self loadData];
    }
}
-(void)creatTabButton{
    UIImage *rightImage = [[UIImage imageNamed:@"直播.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    UIButton *liveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    liveButton.frame = CGRectMake(0, 0, 40, 40);
    [liveButton setImage:[UIImage imageNamed:@"五秒视频.png"] forState:UIControlStateNormal];
    liveButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    [liveButton addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIBarButtonItem *but1 = [[UIBarButtonItem alloc]initWithImage:rightImage style:UIBarButtonItemStylePlain target:self action:@selector(live)];
    UIBarButtonItem *but3 = [[UIBarButtonItem alloc]initWithCustomView:liveButton];
    
    
    
    self.navigationItem.leftBarButtonItems = @[but1,but3];
    //self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithImage:leftImage style:UIBarButtonItemStylePlain target:self action:@selector(setting)];
}
-(void)live{
    MainLiveViewController *controller = [[MainLiveViewController alloc]init];
    controller.m_showBackBt = YES;
    controller.hidesBottomBarWhenPushed = YES;

    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - 点击好友名单聊天
-(void)gotoChatroom:(UIButton *)button{
        ChatFreidnModel *moedel = _FriendArray[button.tag - 100];
        ChatroomViewController *controller = [[ChatroomViewController alloc]init];
        controller.member_id = moedel.member_id;
        controller.barTitle = moedel.nickname;
        [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - 网络请求
-(void)loadData{
    [CYToast showStatusWithString:@"正在加载"];
    
    ///Chat_pop/recv_chatunit
    NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,getChatList];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":[PHUserModel sharedPHUserModel].member_id};
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"result"];
        if (dataArr && dataArr.count > 0) {
             [_MessageArray removeAllObjects];
            for (NSDictionary *parmDic in dataArr) {
                ChatFreidnModel *model =[ChatFreidnModel parsenWithDictonary:parmDic];
                if (![model.member_id isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
                    [_MessageArray addObject:model];
                }
            }
            [MainTableView reloadData];
            seleIndex = 0;
        }
        [CYToast dismiss];

    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)loadFriend{
    [_FriendArray removeAllObjects];
    NSString *str = [NSString stringWithFormat:@"%@%@",TestUrl,getFriend];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken};
    [PPNetworkHelper POST:str parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *dataArr = dic[@"data"];
        if (dataArr && dataArr.count > 0) {
            for (NSDictionary *parmDic in dataArr) {
                [_FriendArray addObject:[ChatFreidnModel parsenWithFriendDictionary:parmDic]];
            }
            [MainTableView reloadData];
            seleIndex = 1;
        }
    } failure:^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"%@",error);
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = NO;
    self.tabBarItem.badgeValue = 0;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"newMessage" object:nil];
     [self loadData];
    [self loadFriend];
}
-(void)dealloc{
     [[NSNotificationCenter defaultCenter]removeObserver:self];
}
-(void)new:(NSNotification *)noti{
    NSDictionary *dic = noti.object;
    if ([[dic allKeys]containsObject:@"chat"]){
       [self loadData];
    }
}











@end
