//
//  ChatFreidnModel.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/28.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ChatFreidnModel.h"

@implementation ChatFreidnModel
+(ChatFreidnModel *)parsenWithDictonary:(NSDictionary *)dic{
    ChatFreidnModel *model = [[ChatFreidnModel alloc]init];
    model.avatar = [NetDataCommon stringFromDic:dic forKey:@"avatar"];
    model.nickname = [NetDataCommon stringFromDic:dic forKey:@"nickname"];
    
    NSString *client = [NetDataCommon stringFromDic:dic forKey:@"client"];
    NSString *master = [NetDataCommon stringFromDic:dic forKey:@"master"];
    if ([client isEqualToString:[PHUserModel sharedPHUserModel].member_id]) {
        model.member_id =master;
    }else{
        model.member_id = client;
    }
 //   model.member_id = [NetDataCommon stringFromDic:dic forKey:@"member_id" orKey:@"client"];
    model.msg = [NetDataCommon stringFromDic:dic forKey:@"msg"];
    model.time = [NetDataCommon stringFromDic:dic forKey:@"time"];
    model.unread_count = [NetDataCommon stringFromDic:dic forKey:@"count"];
    return model;
}
+(ChatFreidnModel *)parsenWithFriendDictionary:(NSDictionary *)dic{
    ChatFreidnModel *model = [[ChatFreidnModel alloc]init];
    model.avatar = [NetDataCommon stringFromDic:dic forKey:@"avatar"];
    model.nickname = [NetDataCommon stringFromDic:dic forKey:@"nickname"];
    model.member_id = [NetDataCommon stringFromDic:dic forKey:@"member_id"];
    model.msg = [NetDataCommon stringFromDic:dic forKey:@"msg"];
    model.time = [NetDataCommon stringFromDic:dic forKey:@"time"];
    model.unread_count = [NetDataCommon stringFromDic:dic forKey:@"count"];
    return model;
}
@end
