//
//  ChatFreidnModel.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/28.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatFreidnModel : NSObject
@property (nonatomic,strong) NSString *avatar;
@property (nonatomic,strong) NSString *nickname;
@property (nonatomic,strong) NSString *member_id;
@property (nonatomic,strong) NSString *msg;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *unread_count;

+(ChatFreidnModel *)parsenWithDictonary:(NSDictionary *)dic;
+(ChatFreidnModel *)parsenWithFriendDictionary:(NSDictionary *)dic;
@end
