//
//  MessageListTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "MessageListTableViewCell.h"

@implementation MessageListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setModel:(ChatFreidnModel *)model{
    CGFloat imageW = 4;
    UIImageView *leveImage = [[UIImageView alloc]initWithFrame:CGRectMake(10-imageW, 5 - imageW, 40 + imageW * 2, 40+ imageW * 2)];
    leveImage.image = [UIImage imageNamed:@"銅1"];
    leveImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:leveImage];
    
    _UserImage = [[UIImageView alloc]initWithFrame:CGRectMake(imageW, imageW, 40, 40)];
    _UserImage.contentMode  = UIViewContentModeScaleAspectFit;
    _UserImage.layer.cornerRadius = 20;
    _UserImage.clipsToBounds = YES;
    [_UserImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.avatar]] placeholderImage:[UIImage imageNamed:@"占位图"]];
    [leveImage addSubview:_UserImage];
    
    CGFloat width = [self getLabelWidthWithText:model.nickname width:SCREEN_WIDTH - 20 font:FONT(16)];
    if (width > 150) {
        width = 150;
    }
    _UserName = [[UILabel alloc]initWithFrame:CGRectMake(63, 5, width, 20)];
    _UserName.font = FONT(16);
    _UserName.text = model.nickname;
    _UserName.textColor = NameColor;
    [self.contentView addSubview:_UserName];
    
    CGFloat widthmsg = [self getLabelWidthWithText:model.msg width:SCREEN_WIDTH - 20 font:FONT(15)];
    if (widthmsg > 150) {
        widthmsg = 150;
    }
    _MessageLabel  = [[UILabel alloc]initWithFrame:CGRectMake(63, 25, widthmsg, 20)];
    _MessageLabel.font = FONT(14);
    _MessageLabel.text = model.msg;
    _MessageLabel.textColor = LYColor(218, 218, 218);
    [self.contentView addSubview:_MessageLabel];
    
    UIImageView *memberImage = [[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(_UserName.frame) , 5, 16, 23)];
    memberImage.image = [UIImage imageNamed:@"奖牌"];
    memberImage.clipsToBounds = YES;
    memberImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:memberImage];
    
    _LevelLabel  = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(memberImage.frame) +2, memberImage.frame.origin.y + 5, 29, 13)];
    _LevelLabel.backgroundColor =MainColor;
    _LevelLabel.text = @"lv.1";
    _LevelLabel.textAlignment = NSTextAlignmentCenter;
    _LevelLabel.layer.cornerRadius = 6;
    _LevelLabel.layer.shouldRasterize = YES;
    _LevelLabel.clipsToBounds = YES;
    _LevelLabel.font = FONT(11);
    _LevelLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:_LevelLabel];
    
    _chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _chatButton.frame = CGRectMake(SCREEN_WIDTH - 120, 5, 95, 20);
    //_chatButton.backgroundColor = LYColor(237, 204, 105);
    [_chatButton setTitle:model.time forState:UIControlStateNormal];
    [_chatButton setTitleColor:LYColor(218, 218, 218) forState:UIControlStateNormal];
    _chatButton.titleLabel.font = FONT(10);
    //_chatButton.layer.cornerRadius = 6;
    //_chatButton.clipsToBounds = YES;
    _chatButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.contentView addSubview:_chatButton];
    
    _unreadLabel = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 80, 22, 20, 20)];
    _unreadLabel.text = ([model.unread_count intValue] > 99) ? @"99+": model.unread_count;
    _unreadLabel.backgroundColor = [UIColor redColor];
    _unreadLabel.textColor = [UIColor whiteColor];
    _unreadLabel.layer.cornerRadius = 10;
    _unreadLabel.font = FONT(10);
    _unreadLabel.textAlignment=  NSTextAlignmentCenter;
    _unreadLabel.clipsToBounds = YES;
    if (![model.unread_count isEqualToString:@"0"]) {
        [self.contentView addSubview:_unreadLabel];
    }
    
}

-(CGFloat)getLabelWidthWithText:(NSString *)text width:(CGFloat)width font:(UIFont *)font {
    CGSize size = CGSizeMake(width, MAXFLOAT);//设置一个行高的上限
    CGSize returnSize;
    
    NSDictionary *attribute = @{ NSFontAttributeName : font };
    returnSize = [text boundingRectWithSize:size
                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                 attributes:attribute
                                    context:nil].size;
    
    return returnSize.width;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
