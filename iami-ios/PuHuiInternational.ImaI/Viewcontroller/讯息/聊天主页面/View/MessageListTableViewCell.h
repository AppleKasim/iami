//
//  MessageListTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/30.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatFreidnModel.h"
#import "ChatroomViewController.h"

@interface MessageListTableViewCell : UITableViewCell
@property (nonatomic,strong) UIImageView *UserImage;
@property (nonatomic,strong) UILabel *UserName;
@property (nonatomic,strong) UILabel *LevelLabel;
@property (nonatomic,strong) UIButton *chatButton;
@property (nonatomic,strong) UILabel *MessageLabel;
@property (nonatomic,strong) UILabel *unreadLabel;
@property (nonatomic,strong) MessageViewController *controller;

@property (nonatomic,strong) ChatFreidnModel *model;
@end
