//
//  ChatroomViewController.m
//  WeChat
//
//  Created by Siegrain on 16/4/3.
//  Copyright © 2016年 siegrain. weChat. All rights reserved.
//

#import "AppDelegate.h"
#import "ChatroomViewController.h"
#import "EditorView.h"
//#import "MJRefresh/MJRefresh/MJRefresh.h"
//#import "Masonry/Masonry/Masonry.h"
#import "Messages+CoreDataClass.h"
//#import "TRRTuringRequestManager.h"
#import "TextMessageTableViewCell.h"
#import "ImageMessageTableViewCell.h"

//#import "UITableView+FDTemplateLayoutCell/Classes/UITableView+FDTemplateLayoutCell.h"
#import "WeChat.h"
@import SocketIO;

static NSString* const kTuringAPIKey = @"7b698d636ca822b96f78a2fcef16a47f";
static NSInteger const kEditorHeight = 50;
static NSUInteger const kShowSendTimeInterval = 60;
static NSUInteger const kFetchLimit = 15;

@interface
ChatroomViewController ()<UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate>
{
    NSInteger page;
}
@property (strong, nonatomic) UITableView* tableView;
@property (strong, nonatomic) EditorView* editorView;

@property (assign, nonatomic) NSInteger contentOffsetYFarFromBottom;

//@property (strong, nonatomic) TRRTuringAPIConfig* apiConfig;
//@property (strong, nonatomic) TRRTuringRequestManager* apiRequest;

@property (strong, nonatomic) NSMutableArray<NewMessage*>* chatModelArray;

@property (strong, nonatomic) NSManagedObjectContext* context;

@property (assign, nonatomic) BOOL isLoading;

@property (nonatomic,strong) SocketManager *manager;

@property (nonatomic,strong) SocketIOClient *socket;

#define sendMessage @"Chat/send"
#define ReadMessage @"/api/readChatMsg"
#define LoadHistories @"/Chat/recv"
@end

@implementation ChatroomViewController
#pragma mark - release
- (void)dealloc
{
    //    NSLog(@"chatroom VC 已释放");
}
- (void)viewDidDisappear:(BOOL)animated
{
    self.context = nil;
    //self.apiConfig = nil;
   // self.apiRequest = nil;
    self.chatModelArray = nil;

    [self.editorView removeFromSuperview];
    self.editorView = nil;

    [self.tableView removeFromSuperview];
    self.tableView = nil;
}
#pragma mark - accessors
- (NSManagedObjectContext*)context
{
    if (_context == nil) {
        _context = [[WeChat sharedManager] managedObjectContext];
    }
    return _context;
}
- (NSString*)chatroomIdentifier:(NSIndexPath*)indexPath
{
    if (self.chatModelArray[indexPath.row].messageType == ChatMessageTypeText ) {
        return self.chatModelArray[indexPath.row].sender == 1 ? kCellIdentifierRight
        : kCellIdentifierLeft;
    }else{
        return self.chatModelArray[indexPath.row].sender == 1 ? kImageCellIdentifierRight
        : kImageCellIdentifierLeft;
    }
    
    return nil;
}
- (NSMutableArray<NewMessage*>*)chatModelArray
{
    if (_chatModelArray == nil) {
        _chatModelArray = [NSMutableArray array];
    }
    return _chatModelArray;
}
+ (NSInteger)EditorHeight
{
    return kEditorHeight;
}
#pragma mark - init
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    NSString *phoneVersion = [[UIDevice currentDevice] systemVersion];
    
    
    if (IsiPhoneX) {
        _contentOffsetYFarFromBottom = 5;
    }else{
        _contentOffsetYFarFromBottom = 35;
    }
    
    if (SCREEN_WIDTH < 370) {
        _contentOffsetYFarFromBottom = 45;
    }
    
    if ([phoneVersion floatValue] > 11.3) {
        _contentOffsetYFarFromBottom = 45;
    }
    [self buildView];
    page = 1;
    [self creatBack];
    
    
    [self hasRead];
    [self loadHistory];
    id target = self.navigationController.interactivePopGestureRecognizer.delegate;
    
    // handleNavigationTransition:为系统私有API,即系统自带侧滑手势的回调方法，我们在自己的手势上直接用它的回调方法
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:target action:@selector(handleNavigationTransition:)];
    panGesture.delegate = self; // 设置手势代理，拦截手势触发
    [self.view addGestureRecognizer:panGesture];
    
    // 一定要禁止系统自带的滑动手势
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    // 当当前控制器是根控制器时，不可以侧滑返回，所以不能使其触发手势
    if(self.navigationController.childViewControllers.count == 1)
    {
        return NO;
    }
    return YES;
    
}
-(void)creatBack{
    
        UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35,18)];
        [leftButton setImage:[UIImage imageNamed:@"白色返回"] forState:UIControlStateNormal];
        [leftButton setImage:[UIImage imageNamed:@"灰色返回"] forState:UIControlStateSelected];
    
    [leftButton addTarget:self action:@selector(backBtPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
}
-(void)backBtPressed{

     [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.tabBarController.tabBar.hidden = YES;
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if (view.frame.size.width == 60 && view.frame.size.height == 20) {
            [view removeFromSuperview];
        }
    }
    self.navigationController.navigationBar.hidden = NO;
    //滚不到最下面。。将就一下了只有。。。
    [self scrollToBottom:false];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(new:) name:@"loadChatNew" object:nil];
    
   // [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillShow:)
//                                                 name:UIKeyboardWillShowNotification
//                                               object:nil];
    
    
    
}
//-(void)keyboardWillShow:(NSNotificationCenter *)noti{
//
//
//    __weak typeof(self) weakSelf = self;
//    [weakSelf.editorView
//     setKeyboardWasShown:^(NSInteger animCurveKey, CGFloat duration,
//                           CGSize keyboardSize) {
//         if (keyboardSize.height == 0)
//             return;
//
//         /*
//          若要在修改约束的同时进行动画的话，需要调用其父视图的layoutIfNeeded方法，并在动画中再调用一次
//          */
//         [weakSelf.editorView mas_updateConstraints:^(MASConstraintMaker* make) {
//             make.bottom.offset(-keyboardSize.height);
//         }];
//         [weakSelf.tableView mas_updateConstraints:^(MASConstraintMaker* make) {
//             make.bottom.offset(-keyboardSize.height - kEditorHeight + _contentOffsetYFarFromBottom );
//         }];
//         [UIView animateWithDuration:duration
//                               delay:0
//                             options:animCurveKey
//                          animations:^{
//                              [weakSelf.view layoutIfNeeded];
//
//                              //滚动动画必须在约束动画之后执行，不然会被中断
//                              [weakSelf scrollToBottom:true];
//                          }
//                          completion:nil];
//     }];
//}
-(void)new:(NSNotification *)noti{
    
    NewMessage *message = noti.object;
    NSLog(@"%@",noti);
        if ([message.member_id isEqualToString:self.member_id]) {

            NSString *url = [NSString stringWithFormat:@"%@/%@",TestUrl,ReadMessage];
            NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
            [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
            } failure:^(NSError *error) {
                NSLog(@"%@",error);
                [CYToast dismiss];
            }];
            
            [self.chatModelArray addObject:noti.object];
            [self updateNewOneRowInTableview];
        }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
   // [_socket disconnect];
    //[_manager disconnect];
    
}
#pragma mark - build
- (void)buildView
{
    self.navigationItem.title = self.barTitle;

    [self buildTableView];
    [self buildEditorView];

    [self bindConstraints];
    [self bindGestureRecognizer];
    [self setupTuringRobot];
}
- (void)setupTuringRobot
{
//    self.apiConfig = [[TRRTuringAPIConfig alloc] initWithAPIKey:kTuringAPIKey];
//    self.apiRequest =
//      [[TRRTuringRequestManager alloc] initWithConfig:self.apiConfig];
}
- (void)buildTableView
{
    if (self.tableView != nil)
        return;

    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor colorWithWhite:.95 alpha:1];
    self.tableView.fd_debugLogEnabled = false;

    [self.tableView registerClass:[TextMessageTableViewCell class]
           forCellReuseIdentifier:kCellIdentifierLeft];
    [self.tableView registerClass:[TextMessageTableViewCell class]
           forCellReuseIdentifier:kCellIdentifierRight];
    
    [self.tableView registerClass:[ImageMessageTableViewCell class] forCellReuseIdentifier:kImageCellIdentifierLeft];
    [self.tableView registerClass:[ImageMessageTableViewCell class] forCellReuseIdentifier:kImageCellIdentifierRight];


//    NSDate* date = [NSDate date];
//    NSPredicate* predicate =
//    [NSPredicate predicateWithFormat:@"member_id = %@ ", self.member_id];
    
//    [self.chatModelArray
//     addObjectsFromArray:
//     [[WeChat sharedManager]
//      allRecordsSortByAttribute:kdb_Messages_sendTime wherePredicate:predicate ascending:false fetchLimit:kFetchLimit]];
        

    //下拉刷新控件
    MJRefreshNormalHeader* header =
      [MJRefreshNormalHeader headerWithRefreshingBlock:^{
          [self loadMore];
      }];
    header.lastUpdatedTimeLabel.hidden = true;
    header.stateLabel.hidden = true;
    self.tableView.mj_header = header;

    [self.view addSubview:self.tableView];
}

- (void)buildEditorView
{
    if (self.editorView != nil)
        return;

    self.editorView = [EditorView editor];
    [self.view addSubview:self.editorView];

    __weak typeof(self) weakSelf = self;
    [weakSelf.editorView
      setKeyboardWasShown:^(NSInteger animCurveKey, CGFloat duration,
                            CGSize keyboardSize) {
          if (keyboardSize.height == 0)
              return;
          /*
       若要在修改约束的同时进行动画的话，需要调用其父视图的layoutIfNeeded方法，并在动画中再调用一次
       */
          [weakSelf.editorView mas_updateConstraints:^(MASConstraintMaker* make) {
              make.bottom.offset(-keyboardSize.height);
          }];
          [weakSelf.tableView mas_updateConstraints:^(MASConstraintMaker* make) {
              make.bottom.offset(-keyboardSize.height- kEditorHeight);
              make.top.left.right.offset(0);
              [weakSelf.view layoutIfNeeded];
          }];
          [UIView animateWithDuration:duration
                                delay:0
                              options:animCurveKey
                           animations:^{
                               [weakSelf.view layoutIfNeeded];

                               //滚动动画必须在约束动画之后执行，不然会被中断
                               [weakSelf scrollToBottom:true];
                           }
                           completion:nil];
      }];
    
    
    [weakSelf.editorView
      setKeyboardWillBeHidden:^(NSInteger animCurveKey, CGFloat duration,
                                CGSize keyboardSize) {
          [weakSelf.view layoutIfNeeded];
          [weakSelf.editorView mas_updateConstraints:^(MASConstraintMaker* make) {
              make.bottom.offset(0);
          }];
          [weakSelf.tableView mas_updateConstraints:^(MASConstraintMaker* make) {
              make.top.left.right.offset(0);
              make.bottom.offset(-kEditorHeight );
              [weakSelf.view layoutIfNeeded];
          }];
          [UIView animateWithDuration:duration
                                delay:0
                              options:animCurveKey
                           animations:^{
                               [weakSelf.view layoutIfNeeded];
                           }
                           completion:nil];
      }];
    
    
    [weakSelf.editorView
      setMessageWasSend:^(id message, ChatMessageType messageType) {

          if ([message isEqualToString:@""]) {
              return ;
          }
          
          if ([message isEqualToString:@":)"]) {
              [self sendImage];
              return ;
          }
          
          NSDate* date = [NSDate date];
          NSLog(@"%@",date);
          NewMessage* meModel = [[WeChat sharedManager] insertRecordInRecordTable:@{
              kdb_Messages_message : message,
              kdb_Messages_sender : @1,
              kdb_Messages_sendTime : date,
              kdb_Messages_showSendTime : @([self needsShowSendTime:date]),
              kdb_Messages_messageType : @(ChatMessageTypeText),
              kdb_Messages_member_id:self.member_id
          }];
          [weakSelf SendMessage:message];
          [weakSelf.chatModelArray addObject:meModel];
          [self updateNewOneRowInTableview];
      }];
    
    //发送单个表情
    [weakSelf.editorView.emotionButton addTarget:self action:@selector(sendImage) forControlEvents:UIControlEventTouchUpInside];
}
-(void)sendImage{
    NSDate* date = [NSDate date];
   // NSLog(@"%@",date);
    NewMessage* meModel = [[WeChat sharedManager] insertRecordInRecordTable:@{
                                                                              kdb_Messages_message : @":)",
                                                                              kdb_Messages_sender : @1,
                                                                              kdb_Messages_sendTime : date,
                                                                              kdb_Messages_showSendTime : @([self needsShowSendTime:date]),
                                                                              kdb_Messages_messageType : @(ChatMessageTypeImage),
                                                                              kdb_Messages_member_id:self.member_id
                                                                              }];
    [self SendMessage:@":)"];
    [self.chatModelArray addObject:meModel];
    [self updateNewOneRowInTableview];
}
- (void)bindConstraints
{
//    [self.editorView
//     setKeyboardWillBeHidden:^(NSInteger animCurveKey, CGFloat duration,
//                               CGSize keyboardSize) {
//         [self.view layoutIfNeeded];
//         [self.editorView mas_updateConstraints:^(MASConstraintMaker* make) {
//             make.bottom.offset(0);
//         }];
//         [self.tableView mas_updateConstraints:^(MASConstraintMaker* make) {
//             make.bottom.offset(-kEditorHeight+40);
//         }];
//         [UIView animateWithDuration:duration
//                               delay:0
//                             options:animCurveKey
//                          animations:^{
//                              [weakSelf.view layoutIfNeeded];
//                          }
//                          completion:nil];
//     }];
    
    __weak typeof(self) weakSelf = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker* make) {
        
        make.top.left.right.offset(0);
        make.bottom.offset(-kEditorHeight );
         [weakSelf.view layoutIfNeeded];
    }];
    [self.editorView mas_makeConstraints:^(MASConstraintMaker* make) {
        
        make.left.right.bottom.offset(0);
        make.height.offset(kEditorHeight);
        [weakSelf.view layoutIfNeeded];
    }];
}
- (void)bindGestureRecognizer
{
    UITapGestureRecognizer* singleTapGestureRecognizer =
      [[UITapGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(endTextEditing)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.tableView addGestureRecognizer:singleTapGestureRecognizer];
}
#pragma mark - tableview
- (CGFloat)tableView:(UITableView*)tableView
  estimatedHeightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    return [self tableView:tableView heightForRowAtIndexPath:indexPath];
}
- (CGFloat)tableView:(UITableView*)tableView
  heightForRowAtIndexPath:(NSIndexPath*)indexPath
{
    NewMessage* model = self.chatModelArray[indexPath.row];
    CGFloat height = model.height;

    if (!height) {
        height = [self.tableView
          fd_heightForCellWithIdentifier:[self chatroomIdentifier:indexPath]
                        cacheByIndexPath:indexPath
                           configuration:^(TextMessageTableViewCell* cell) {
                               cell.model = model;
                           }];

        model.height = height;
    }

    return height;
}
- (NSInteger)tableView:(UITableView*)tableView
  numberOfRowsInSection:(NSInteger)section
{
    return self.chatModelArray.count;
}
- (UITableViewCell*)tableView:(UITableView*)tableView
        cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    TextMessageTableViewCell* cell = [tableView
      dequeueReusableCellWithIdentifier:[self chatroomIdentifier:indexPath]];

    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (void)configureCell:(BaseMessageTableViewCell*)cell
          atIndexPath:(NSIndexPath*)indexPath
{
    NewMessage *model = self.chatModelArray[indexPath.row];
    cell.model = model;
}
#pragma mark - scrollview
- (void)scrollViewWillBeginDragging:(UIScrollView*)scrollView
{
    [self endTextEditing];
}
#pragma mark -
- (void)endTextEditing
{
    [self.view endEditing:true];
}
- (void)scrollToBottom:(BOOL)animated
{
    if (self.chatModelArray.count == 0)
        return;

//    [self.tableView
//      scrollToRowAtIndexPath:[NSIndexPath
//                               indexPathForRow:self.chatModelArray.count - 1
//                                     inSection:0]
//            atScrollPosition:UITableViewScrollPositionBottom
//                    animated:animated];
    
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSIndexPath *lastIndex = [NSIndexPath indexPathForRow:self.chatModelArray.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:lastIndex atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    });
    
    
}
- (BOOL)needsShowSendTime:(NSDate*)date
{
    if (!self.chatModelArray.lastObject)
        return true;
#pragma 修改了！
    NSTimeInterval lastRecordDatetimeInterval =
    [self.chatModelArray.lastObject.sendTime timeIntervalSinceReferenceDate];

    return date.timeIntervalSinceReferenceDate - lastRecordDatetimeInterval >=
           kShowSendTimeInterval;
}
- (void)scrollViewDidScroll:(UIScrollView*)scrollView
{
    //  NSLog(@"%f", scrollView.contentOffset.y);
}
- (void)updateNewOneRowInTableview
{
    NSIndexPath* insertion =
      [NSIndexPath indexPathForRow:self.chatModelArray.count - 1
                         inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[ insertion ]
                          withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:insertion
                          atScrollPosition:UITableViewScrollPositionBottom
                                  animated:true];
}
- (void)updateHistoryDataInTableview
{
    if (self.isLoading)
        return;

    self.isLoading = true;
    __weak typeof(self) weakSelf = self;
    dispatch_after(
      dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)),
      dispatch_get_main_queue(), ^{
          NSInteger oldCount = self.chatModelArray.count;

          NSArray* datas = [[WeChat sharedManager]
                            messagesBeforeTimeInterval:[self.chatModelArray.firstObject.sendTime timeIntervalSinceReferenceDate]
                            fetchLimit:kFetchLimit];
          [self.chatModelArray
            insertObjects:datas
                atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, datas.count)]];

          /*
       Mark:
       使用beginUpdates更新的row就算指定了AnimationNone，也会有一个莫名其妙的SlideDown的动画
       必须全局禁止动画，更新后再恢复...蛇精病
		
		而且这个方法一般是用来插一条数据加动画的，插多了会卡，不能用于正常的数据源更新
		   正常更新直接ReloadData就好了。
       */
          //      [UIView setAnimationsEnabled:false];
          //      [self.tableView beginUpdates];
          //      [self.tableView insertRowsAtIndexPaths:indexPaths
          
          //                            withRowAnimation:UITableViewRowAnimationNone];
          //      [self.tableView endUpdates];
          //
          //      [UIView setAnimationsEnabled:true];

          [self.tableView reloadData];

          NSInteger newCount = weakSelf.chatModelArray.count;
          NSIndexPath* indexPath =
            [NSIndexPath indexPathForRow:newCount - oldCount
                               inSection:0];
          [weakSelf.tableView scrollToRowAtIndexPath:indexPath
                                    atScrollPosition:UITableViewScrollPositionTop
                                            animated:NO];

          //刷新结束后通知菊花停止转动
          [self.tableView.mj_header endRefreshing];
          self.isLoading = false;
      });
}
#pragma mark - 发送消息
-(void)SendMessage:(NSString *)message{
    NSString *url = [NSString stringWithFormat:@"%@/%@",TestUrl,sendMessage];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"msg":message,@"name":[PHUserModel sharedPHUserModel].member_id,@"client":self.member_id,@"roomid":@"0"};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
//-(void)backBtPressed{
//    NSLog(@"!!!!!!!");
//}
#pragma mark - websocket
//-(void)setSocket{
//    NSURL* url = [[NSURL alloc] initWithString:@"http://push.iami-web.me:2120/"];
//    _manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @NO, @"forcePolling": @YES,}];
//    _socket = _manager.defaultSocket;
//
//    //登录
//    [_socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
//        NSLog(@"socket connected");
//        [_socket emit:@"login" with:@[[PHUserModel sharedPHUserModel].member_id]];
//    }];
//
//
//    [_socket on:@"currentAmount" callback:^(NSArray* data, SocketAckEmitter* ack) {
//        double cur = [[data objectAtIndex:0] floatValue];
//
//        [[_socket emitWithAck:@"canUpdate" with:@[@(cur)]] timingOutAfter:0 callback:^(NSArray* data) {
//            [_socket emit:@"update" with:@[@{@"amount": @(cur + 2.50)}]];
//        }];
//
//        [ack with:@[@"Got your currentAmount, ", @"dude"]];
//    }];
//
//    //接收新消息
//    [_socket on:@"new_msg" callback:^(NSArray *data, SocketAckEmitter *ack) {
//
//        NSString *string = [NSString stringWithFormat:@"%@",data[0]];
//           NSString*replacedStr = [string stringByReplacingOccurrencesOfString:@"&quot;"withString:@"\""];
//        NSString *newreplace = [replacedStr stringByReplacingOccurrencesOfString:@"[" withString:@""];
//        NSString *newreplace1 = [newreplace stringByReplacingOccurrencesOfString:@"]" withString:@""];
//
//        NSDictionary *json = [self dictionaryWithJsonString:newreplace1];
//
//      //接收新消息，在页面刷新
//
//
//
//    }];
//
//    [_socket connect];
//
//}
//
//-(NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString
//{
//    if (jsonString == nil) {
//        return nil;
//    }
//
//    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *err;
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                        options:NSJSONReadingMutableContainers
//                                                          error:&err];
//    if(err)
//    {
//        NSLog(@"json解析失败：%@",err);
//        return nil;
//    }
//    return dic;
//}
//-(NSString *)dictionaryToJson:(NSDictionary *)dic
//{
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
//    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//}
#pragma mark -更新消息为已读
-(void)hasRead{
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@/%@",TestUrl,ReadMessage];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"member_id":self.member_id};
    [PPNetworkHelper POST:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
         [CYToast dismiss];
    }];
}
#pragma mark - 读取历史消息
-(void)loadMore{
    page ++;
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@/%@",TestUrl,LoadHistories];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"client":self.member_id,@"perPage":@"10",@"Page":[NSString stringWithFormat:@"%ld",page]};
    [PPNetworkHelper GET:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *arr = dic[@"result"];
        if (arr && arr.count > 0) {
            for (NSInteger i = arr.count - 1; i>0; i--) {
                NSDictionary *chatDic = arr[i];
                [self.chatModelArray insertObject:[self parsenWithDic:chatDic] atIndex:0];
            }
        }
        [_tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
    
}
-(void)loadHistory{
    
    [CYToast showStatusWithString:@"正在加载"];
    NSString *url = [NSString stringWithFormat:@"%@/%@",TestUrl,LoadHistories];
    NSDictionary *paramDic = @{@"csrf_token_name":[PHUserModel sharedPHUserModel].csrf_token,@"token":[PHUserModel sharedPHUserModel].AccessToken,@"client":self.member_id,@"perPage":@"10",@"Page":[NSString stringWithFormat:@"%ld",page]};
    [PPNetworkHelper GET:url parameters:paramDic success:^(id responseObject) {
        NSDictionary *dic = responseObject;
        NSArray *arr = dic[@"result"];
        if (arr && arr.count > 0) {
            for (NSDictionary *chatDic in arr) {
                [self.chatModelArray addObject:[self parsenWithDic:chatDic]];
            }
        }
        [_tableView reloadData];
        [self scrollToBottom:YES];
        [CYToast dismiss];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [CYToast dismiss];
    }];
}
-(NewMessage *)parsenWithDic:(NSDictionary *)dic{

    NSString *senTime = [NetDataCommon stringFromDic:dic forKey:@"time"];
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"yyyy-MM-dd HH-mm-sss"];
    NSDate *resDate = [formatter1 dateFromString:senTime];
    NSNumber *sender = nil;
    NSString *master = [NetDataCommon stringFromDic:dic forKey:@"master"];
    if ([master isEqualToString:self.member_id]) {
        sender=@2;
    }else{
        sender = @1;
    }
    NSString *avater = [NetDataCommon stringFromDic:dic forKey:@"avatar"];
    
    
    NSString *msg = [NetDataCommon stringFromDic:dic forKey:@"msg"];
    
    NSInteger i = 0;
    
    if ([msg isEqualToString:@":)"]) {
        i = 1;
    }
    
    NewMessage *message = [[WeChat sharedManager]insertRecordInRecordTable:@{
                                                                             kdb_Messages_message : msg,
                                                                             kdb_Messages_sender : sender,
                                                                             kdb_Messages_sendTime :resDate,
                                                                             kdb_Messages_showSendTime : @([self needsShowSendTime:resDate]),
                                                                             kdb_Messages_messageType : @(i),
                                                                             kdb_Messages_member_id:self.member_id,
                                                                             kdb_Messages_avatar:avater
                                                                             }];
    
    return message;
    
    
}









@end
