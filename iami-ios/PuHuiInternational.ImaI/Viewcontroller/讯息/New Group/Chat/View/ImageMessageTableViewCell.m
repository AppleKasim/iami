//
//  ImageMessageTableViewCell.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/15.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ImageMessageTableViewCell.h"
#import "InsetsTextField.h"
#import "Messages+CoreDataClass.h"
@implementation ImageMessageTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString*)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
    }
    return self;
}
- (void)setModel:(NewMessage*)model
{
    //self.messageLabel.text = (NSString*)model.message;
    self.MainImageView.image = [UIImage imageNamed:@"喜欢"];
    [super setModel:model];
}
- (void)buildCell
{
    [super buildCell];
    
//    self.messageLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectZero];
//    self.messageLabel.backgroundColor = [UIColor clearColor];
//    self.messageLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
//    self.messageLabel.font = textFont;
//    self.messageLabel.numberOfLines = 0;
//    [super.bubbleView addSubview:self.messageLabel];
    self.MainImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    self.MainImageView.backgroundColor = [UIColor clearColor];
    self.MainImageView.contentMode = UIViewContentModeScaleAspectFit;
    [super.bubbleView addSubview:self.MainImageView];
}
- (void)bindConstraints
{
    [super bindConstraints];
    
    [self.MainImageView mas_makeConstraints:^(MASConstraintMaker* make) {
        make.edges.insets(UIEdgeInsetsMake(13, 20, 20, 20));
    }];
    
    [self.MainImageView setContentHuggingPriority:1000 forAxis:UILayoutConstraintAxisVertical];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
