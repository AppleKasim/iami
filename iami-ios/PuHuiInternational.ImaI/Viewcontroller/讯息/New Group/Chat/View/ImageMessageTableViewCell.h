//
//  ImageMessageTableViewCell.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/6/15.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "BaseMessageTableViewCell.h"
@class NewMessage;
@interface ImageMessageTableViewCell : BaseMessageTableViewCell


@property (nonatomic,strong) UIImageView *MainImageView;
@end
