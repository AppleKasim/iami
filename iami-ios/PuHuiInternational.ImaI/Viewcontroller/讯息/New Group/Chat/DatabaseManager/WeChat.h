//
//  MyDatabaseManager.h
//  DatabaseManager

#import "IQDatabaseManager.h"
#import "WechatConstants.h"

@interface WeChat : IQDatabaseManager

- (NSArray*)allRecordsSortByAttribute:(NSString*)attribute;
- (NSArray*)allRecordsSortByAttribute:(NSString*)attribute
                                where:(NSString*)key
                             contains:(id)value;
- (NSArray*)allRecordsSortByAttribute:(NSString*)attribute
                       wherePredicate:(NSPredicate*)predicate
                            ascending:(BOOL)ascending
                           fetchLimit:(NSUInteger)limit;
- (NSArray*)messagesBeforeTimeInterval:(NSTimeInterval)interval
                            fetchLimit:(NSUInteger)fetchLimit;

- (NewMessage*)insertRecordInRecordTable:(NSDictionary*)recordAttributes;
- (NewMessage*)insertUpdateRecordInRecordTable:(NSDictionary*)recordAttributes;

- (NewMessage*)updateRecord:(NewMessage*)record
            inRecordTable:(NSDictionary*)recordAttributes;


- (BOOL)deleteTableRecord:(NewMessage*)record;

- (BOOL)deleteAllTableRecord;

@end
