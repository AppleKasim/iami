//
//  NewMessage+CoreDataProperties.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/29.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import "NewMessage+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface NewMessage (CoreDataProperties)

+ (NSFetchRequest<NewMessage *> *)fetchRequest;

@property (nonatomic) float height;
@property (nullable, nonatomic, copy) NSString *member_id;
@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic) int16_t messageType;
@property (nonatomic) int64_t sender;
@property (nullable, nonatomic, copy) NSDate *sendTime;
@property (nonatomic) BOOL showSendTime;
@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSString *nickname;

@end

NS_ASSUME_NONNULL_END
