//
//  NewMessage+CoreDataClass.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/29.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewMessage : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "NewMessage+CoreDataProperties.h"
