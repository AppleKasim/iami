//
//  NewMessage+CoreDataProperties.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/29.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import "NewMessage+CoreDataProperties.h"

@implementation NewMessage (CoreDataProperties)

+ (NSFetchRequest<NewMessage *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"NewMessage"];
}

@dynamic height;
@dynamic member_id;
@dynamic message;
@dynamic messageType;
@dynamic sender;
@dynamic sendTime;
@dynamic showSendTime;
@dynamic avatar;
@dynamic nickname;

@end
