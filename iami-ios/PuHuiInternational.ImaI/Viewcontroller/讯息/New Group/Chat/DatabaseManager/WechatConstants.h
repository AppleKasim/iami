//
//  MyDatabaseConstants.h
//  DatabaseManager

#ifndef DatabaseManager_WechatConstants_h
#define DatabaseManager_WechatConstants_h

//#import "Messages+CoreDataClass.h"
#import "NewMessage+CoreDataClass.h"
#import <Foundation/Foundation.h>

static NSString* const kdb_Messages = @"Messages";
static NSString* const kdb_Messages_identifier = @"identifier";
static NSString* const kdb_Messages_message = @"message";
static NSString* const kdb_Messages_messageType = @"messageType";
static NSString* const kdb_Messages_sender = @"sender";
static NSString* const kdb_Messages_sendTime = @"sendTime";
static NSString* const kdb_Messages_showSendTime = @"showSendTime";
static NSString* const kdb_Messages_member_id = @"member_id";
static NSString* const kdb_Messages_avatar = @"avatar";
static NSString* const kdb_Messages_nickname = @"nickname";
#endif
