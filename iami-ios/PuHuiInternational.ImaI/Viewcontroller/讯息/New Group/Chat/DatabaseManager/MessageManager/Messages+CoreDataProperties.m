//
//  Messages+CoreDataProperties.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/17.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import "Messages+CoreDataProperties.h"

@implementation Messages (CoreDataProperties)

+ (NSFetchRequest<Messages *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Messages"];
}

@dynamic height;
@dynamic message;
@dynamic messageType;
@dynamic sender;
@dynamic sendTime;
@dynamic showSendTime;

@end
