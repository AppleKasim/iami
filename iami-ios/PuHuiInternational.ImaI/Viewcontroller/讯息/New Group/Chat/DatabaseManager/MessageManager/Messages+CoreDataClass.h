//
//  Messages+CoreDataClass.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/17.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Messages : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Messages+CoreDataProperties.h"
