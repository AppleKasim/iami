//
//  Messages+CoreDataProperties.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/5/17.
//  Copyright © 2018年 ljq. All rights reserved.
//
//

#import "Messages+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Messages (CoreDataProperties)

+ (NSFetchRequest<Messages *> *)fetchRequest;

@property (nonatomic) float height;
@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic) int16_t messageType;
@property (nonatomic) int64_t sender;
@property (nullable, nonatomic, copy) NSDate *sendTime;
@property (nonatomic) BOOL showSendTime;

@end

NS_ASSUME_NONNULL_END
