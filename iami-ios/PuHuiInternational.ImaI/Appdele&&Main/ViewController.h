//
//  ViewController.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import<WebKit/WebKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>
{
    UIWebView *mWebView;
    UIActivityIndicatorView *activityIndicator;
    WKWebView *WebV;
}
@property (nonatomic,copy) NSString *url;
@end

