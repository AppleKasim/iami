//
//  AppDelegate.h
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void) initTabBarVC;
-(void) initLoginVC;

@end

