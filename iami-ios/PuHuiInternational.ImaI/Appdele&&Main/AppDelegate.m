//
//  AppDelegate.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "LoginViewController.h"
#import <PLMediaStreamingKit/PLMediaStreamingKit.h>
#import "XTCustomNavigationController.h"


@interface AppDelegate ()

#define GetLanguageApi @"/api/get_language"
#define GetLanguageMD5Api @"/api/get_language_md5_version"

@end

@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [PLStreamingEnv initEnv];
    
    self.window = [[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    
    UITextField *lagFreeField = [[UITextField alloc] init];
    [self.window addSubview:lagFreeField];
    [lagFreeField becomeFirstResponder];
    [lagFreeField resignFirstResponder];
    [lagFreeField removeFromSuperview];
    
    // 取得語系 & 判斷語系版本
    //[self GetLanguageAssetBundle];
    
    [self initApp];
    return YES;
}

- (void)GetLanguageAssetBundle {
    NSString *url = [NSString stringWithFormat:@"%@/%@", TestUrl, GetLanguageApi];
    NSLog(@"%@", url);
    [PPNetworkHelper GET:url parameters:nil success:^(id responseObject) {
        //NSLog(@"success %@:%@", GetLanguageApi, responseObject);
        NSDictionary *dic = responseObject;
        NSDictionary *lan_dic = dic[@"data"];
        [lan_dic enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            NSDictionary *lan_data = obj;
            //NSLog(@"%@ --> %@, %@, %@",key, lan_data[@"english"], lan_data[@"zh-TW"], lan_data[@"zh-CN"]);
            //[key writeToFile: @"Localizable.strings" atomically: YES  encoding: NSUTF16StringEncoding  error: nil];
        }];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
        NSString *docDir = [paths objectAtIndex:0];
        NSLog(@"%@", docDir);
        [self initApp];
    } failure:^(NSError *error) {
        [self initApp];
        NSLog(@"failure %@:%@", GetLanguageApi, error);
    } getToken:false];
    [self initApp];
}

- (void)initApp {
    PHUserModel *info = [PHUserModel sharedPHUserModel];
    [info loadUserInfoFromSanbox];
    if (!info.isLogin) {
        [self initLoginVC];
    }else{
        [self initTabBarVC];
        [self connecttoSocket];
    }
    [self.window makeKeyAndVisible];
}


- (void)initTabBarVC
{
    XLTabBarController *tabBarVC = [[XLTabBarController alloc] init];
//    PHUserModel *info = [PHUserModel sharedPHUserModel];
//    [info loadUserInfoFromSanbox];
    self.window.rootViewController = tabBarVC;
}
-(void)initLoginVC
{
    LoginViewController *log = [[LoginViewController alloc]init];
    XTCustomNavigationController *controller = [[XTCustomNavigationController alloc]initWithRootViewController:log];
    self.window.rootViewController = controller;
}
-(void)connecttoSocket{
    [[PHUserModel sharedPHUserModel] connect];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    if ([PHUserModel sharedPHUserModel].isLogin == NO) {
        return;
    }
    [CYToast showStatusWithString:@"正在加载"];
    [PPNetworkHelper GET_CSRF_TOKEN:[self getCRSFSuccessCB] failure:[self getCRSFFailureCB]];
}

- (PPHttpRequestSuccess) getCRSFSuccessCB {
    PPHttpRequestSuccess successCB;
    successCB = ^(id responseObject) {
        //[CYToast dismiss];
        NSDictionary *dic = responseObject;
        if ([dic[@"status"] isEqualToString:@"success"]) {
            dispatch_queue_t  queue = dispatch_queue_create("com.PuHuiShiJi.Imai", DISPATCH_QUEUE_SERIAL);
            
            dispatch_async(queue, ^{
                [PHUserModel sharedPHUserModel].csrf_token = dic[@"data"][@"token"];
                [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            });
            
            dispatch_async(queue, ^{
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [CYToast showStatusWithString:@"正在加载"];
                    [PPNetworkHelper GET_ACCESS_TOKEN:[self getAccessTokenSuccessCB] failure:[self getAccessTokenFailureCB]];
                });
            });
        }else{
            [CYToast showErrorWithString:@"token获取失败"];
        }
    };
    return successCB;
}

- (PPHttpRequestFailed) getCRSFFailureCB {
    PPHttpRequestFailed failureCB;
    failureCB = ^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"[AppDelegate(getCRSFFailureCB)] Get CRSF Token Fail (ErrorMsg:%@)",error);
    };
    return failureCB;
}

- (PPHttpRequestSuccess) getAccessTokenSuccessCB {
    PPHttpRequestSuccess successCB;
    successCB = ^(id responseObject) {
        NSDictionary *dic = responseObject;
        [CYToast dismiss];
        if ([dic[@"message"] isEqualToString:@"success"]) {
            [PHUserModel sharedPHUserModel].isLogin = YES;
            [PHUserModel sharedPHUserModel].AccessToken =dic[@"data"][@"token"];
            [[PHUserModel sharedPHUserModel]saveUserInfoToSanbox];
            
        }else{
            [CYToast showErrorWithString:@"账号或密码错误"];
        }
    };
    return successCB;
}

- (PPHttpRequestFailed) getAccessTokenFailureCB {
    PPHttpRequestFailed failureCB;
    failureCB = ^(NSError *error) {
        [CYToast dismiss];
        NSLog(@"[AppDelegate(getAccessTokenFailureCB)] Get CRSF Token Fail (ErrorMsg:%@)",error);
    };
    return failureCB;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
