//
//  ViewController.m
//  PuHuiInternational.ImaI
//
//  Created by user on 2018/3/1.
//  Copyright © 2018年 ljq. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()<WKUIDelegate,WKNavigationDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createBtnBack];
    [self creatFreshBut];
    [self monitorNetworkState];
    // Do any additional setup after loading the view, typically from a nib.
    WebV = [[WKWebView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT )];
    WebV.allowsBackForwardNavigationGestures = YES;
   
    if (![self.url hasPrefix:@"http://"]) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.url]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [WebV loadRequest:request];
    }else{
        NSURL *url = [NSURL URLWithString:self.url];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [WebV loadRequest:request];
    }
    WebV.navigationDelegate = self;
    WebV.UIDelegate = self;
   
     [self.view addSubview:WebV];
}

//页面开始加载时调用
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
//    view.tag = 100;
//    view.backgroundColor = [UIColor blackColor];
//    view.alpha = 0.5f;
//    [self.view addSubview:view];
//
//    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
//    [activityIndicator setCenter:view.center];
//    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
//    [view addSubview:activityIndicator];
//    [activityIndicator startAnimating];
     [CYToast showStatusWithString:@"正在加载"];
}
//页面加载完成之后调用
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    [activityIndicator stopAnimating];
//    UIView *view = (UIView *)[self.view viewWithTag:100];
//    [view removeFromSuperview];
    [CYToast dismiss];
    
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation {
//    [activityIndicator stopAnimating];
//    UIView *view = (UIView *)[self.view viewWithTag:100];
//    [view removeFromSuperview];
     [CYToast dismiss];
}


//网页开始加载的时候调用
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    //创建UIActivityIndicatorView背底半透明view
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
//    view.tag = 100;
//    view.backgroundColor = [UIColor blackColor];
//    view.alpha = 0.5f;
//    [self.view addSubview:view];
//
//    activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
//    [activityIndicator setCenter:view.center];
//    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
//    [view addSubview:activityIndicator];
//    [activityIndicator startAnimating];
    
   
}

//网页加载完成的时候调用
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
//    [activityIndicator stopAnimating];
//    UIView *view = (UIView *)[self.view viewWithTag:100];
//    [view removeFromSuperview];
    //[CYToast dismiss];
}

//网页加载错误的时候调用
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
    UIView *view = (UIView *)[self.view viewWithTag:100];
    [view removeFromSuperview];
}
//创建返回键
-(void)createBtnBack
{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20,20)];
    [leftButton setImage:[UIImage imageNamed:@"灰色返回"] forState:UIControlStateNormal];
    [leftButton setImage:[UIImage imageNamed:@"白色返回"] forState:UIControlStateSelected];
    
    [leftButton addTarget:self action:@selector(backBt) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.leftBarButtonItem = leftItem;
}
//创建刷新按钮
-(void)creatFreshBut{
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20,20)];
    [leftButton setImage:[UIImage imageNamed:@"刷新按钮"] forState:UIControlStateNormal];
    [leftButton setImage:[UIImage imageNamed:@"刷新按钮"] forState:UIControlStateSelected];
    [leftButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.rightBarButtonItem = leftItem;
}
//返回点击事件
-(void)backBt{
//    NSString *backString = @"history.go(-1)";
//    [WebV evaluateJavaScript:backString completionHandler:^(id _Nullable response, NSError * _Nullable error) {
//         NSLog(@"value: %@ error: %@", response, error);
//    }];
//    [self dismissViewControllerAnimated:YES completion:^{
//
//    }];
    [self.navigationController popViewControllerAnimated:YES];
}
//刷新点击事件
-(void)refresh{
    NSString *backString = @"history.go(0)";
    [WebV evaluateJavaScript:backString completionHandler:^(id _Nullable response, NSError * _Nullable error) {
        NSLog(@"value: %@ error: %@", response, error);
    }];
}
//网络状态监测
- (void)monitorNetworkState{
    
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusNotReachable:
               // NSLog(@"没有网络");
                [PXAlertView showAlertWithTitle:@"提示" message:@"请您连接网络"];
                break;
            case AFNetworkReachabilityStatusUnknown:
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                //NSLog(@"wifi");
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
               // NSLog(@"3G|4G");
                break;
            default:
                break;
        }
    }];
}
















- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
